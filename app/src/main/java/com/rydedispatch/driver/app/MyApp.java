package com.rydedispatch.driver.app;

import android.app.Application;
import android.os.Build;
import android.os.StrictMode;
import com.crashlytics.android.Crashlytics;
import com.rydedispatch.driver.utils.AppSignatureHelper;

import io.fabric.sdk.android.Fabric;

public class MyApp extends Application {
    private static MyApp singleton;
//    public String BASE_URL = "https://portal.rydedispatch.com/api/";
    public String BASE_URL = "http://167.172.247.117/api/";
//    public String BASE_IMAGE_URL = "https://portal.rydedispatch.com/";
    public String BASE_IMAGE_URL = "http://167.172.247.117/";
    public static MyApp getInstance(){
        return singleton;
    }
    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        singleton = this;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder()
                    .detectNonSdkApiUsage()
                    .penaltyLog()
                    .build());
        }

        AppSignatureHelper appSignatureHelper = new AppSignatureHelper(this);
        appSignatureHelper.getAppSignatures();
    }
}
