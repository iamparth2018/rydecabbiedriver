package com.rydedispatch.driver.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Rating;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.rydedispatch.driver.R;
import com.rydedispatch.driver.network.ApiClient;
import com.rydedispatch.driver.network.ApiInterface;
import com.rydedispatch.driver.response.RespCompleteRide;
import com.rydedispatch.driver.response.RespSubmitRating;
import com.rydedispatch.driver.response.RespUpdateProfile;
import com.rydedispatch.driver.response.RespUploadSignature;
import com.rydedispatch.driver.utils.AppConfig;
import com.rydedispatch.driver.utils.PreferenceHelper;
import com.rydedispatch.driver.utils.Prefs;
import com.rydedispatch.driver.utils.Progress;
import com.rydedispatch.driver.utils.Utils;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RatingActivity extends AppCompatActivity {

    private static final String TAG = RatingActivity.class.getSimpleName();

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.tvTitle)
    AppCompatTextView tvTitle;
    @BindView(R.id.btn_submit)
    Button btn_submit;
    @BindView(R.id.et_comment)
    TextInputEditText etComments;
    @BindView(R.id.skip)
    AppCompatTextView tvSkip;
    @BindView(R.id.profile_image)
    CircleImageView profile_image;
    @BindView(R.id.tv_rate)
    AppCompatTextView tv_rate;
    @BindView(R.id.rl_layout_rating)
    RelativeLayout rl_layout_rating;

    private String stringComment = "", accept = "",
            authorization = "";
    double rate;
    @BindView(R.id.rating_bar)
    RatingBar rating_bar;

    protected PreferenceHelper helper;
    protected Progress progress;
    private Utils utils;

    private Bundle bundle;
    ImageView signImage;
    private String requestId = "" , passengerfirstname = "", passengerimage = "",
            displaypassengername = "";
    Bitmap bitmap;
    private File mCurrentPhoto;
    String image_path = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rating);
        ButterKnife.bind(this);

        tvTitle.setText(getString(R.string.res_rating));

        bundle = getIntent().getExtras();
        if (bundle != null) {
            requestId = bundle.getString(AppConfig.BUNDLE.request_id);
            displaypassengername = bundle.getString(AppConfig.BUNDLE.display_passenger_name);
            passengerimage = bundle.getString(AppConfig.BUNDLE.passenger_image);
        }
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();
            }
        });


        helper = new PreferenceHelper(this, Prefs.PREF_FILE);
        progress = new Progress(this);
        utils = new Utils(this);
        accept = "application/json";
        authorization = "Bearer " + helper.LoadStringPref(Prefs.token, "");

        signImage = (ImageView) findViewById(R.id.imageView1);

        image_path = getIntent().getStringExtra("imagePath");
        bitmap = BitmapFactory.decodeFile(image_path);
        signImage.setImageBitmap(bitmap);


        tv_rate.setText("Rate " + displaypassengername);

        if (TextUtils.isEmpty(displaypassengername)) {
            tv_rate.setText("Rate Anonymous");
            return;
        }

        if (passengerimage == null || passengerimage.isEmpty()) {

            Picasso.with(RatingActivity.this)
                    .load(AppConfig.URL.BASE_IMAGEURL + passengerimage)
                    .placeholder(R.mipmap.ic_launcher)
                    .error(R.drawable.dummy_pic)
                    .into(profile_image);
            return;
        }

        if (passengerimage.contains("https://")) {

            Picasso.with(RatingActivity.this)
                    .load(passengerimage)
                    .placeholder(R.mipmap.ic_launcher)
                    .error(R.drawable.dummy_pic)
                    .into(profile_image);

        } else {

            Picasso.with(RatingActivity.this)
                    .load(AppConfig.URL.BASE_IMAGEURL + passengerimage)
                    .placeholder(R.mipmap.ic_launcher)
                    .error(R.drawable.dummy_pic)
                    .into(profile_image);
        }

        Log.d(TAG , "rating passenger image : "+ passengerimage);

    }


    @OnClick({R.id.iv_back, R.id.btn_submit, R.id.skip})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;

            case R.id.btn_submit:
                submitReview();
                break;

            case R.id.profile_image:
//                postSignature(accept, authorization, requestId, image_path);
                break;
            case R.id.skip:
                Intent intent = new Intent(RatingActivity.this, MainActivity.class);
                startActivity(intent);
                break;
        }
    }

    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager =
                (InputMethodManager) activity.getSystemService(
                        Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(
                activity.getCurrentFocus().getWindowToken(), 0);
    }

    private void submitReview() {
        stringComment = Objects.requireNonNull(etComments.getText().toString());
        rate = Objects.requireNonNull(rating_bar.getRating());

        Log.d(TAG , "comment :" + stringComment);
        Log.d(TAG , "rating :" + rate);
        /*if (TextUtils.isEmpty(stringComment)) {
            Toast.makeText(this, "Enter Your Comments", Toast.LENGTH_SHORT).show();
            return;
        }*/


        submitReviewPost(accept, authorization, requestId, rate, stringComment);

    }

    private void submitReviewPost(String accept, String authorization, String requestid,
                                  double rate, String stringComment) {
        progress.createDialog(false);
        progress.DialogMessage(getString(R.string.please_wait));
        progress.showDialog();

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        RequestBody request_id1 = RequestBody.create(MediaType.parse("multipart/form-data"), requestid);
        RequestBody rate1 = RequestBody.create(MediaType.parse("multipart/form-data"), String.valueOf(rate));
        RequestBody stringComment1 = RequestBody.create(MediaType.parse("multipart/form-data"), stringComment);

        final Call<RespSubmitRating> respPassRequestCall = apiService.SUBMIT_CALL(accept, authorization, request_id1,
                rate1, stringComment1);
        respPassRequestCall.enqueue(new Callback<RespSubmitRating>() {
            @Override
            public void onResponse(Call<RespSubmitRating> call, Response<RespSubmitRating> response) {
                progress.hideDialog();
                Log.d(TAG, "response complete :" + response.body().getMessage());
                Snackbar.make(rl_layout_rating, "Your signature has been uploaded successfully." +response.body().getMessage(), Snackbar.LENGTH_SHORT).show();

                if (response.body().getMessage().contains("Rating has been submitted successfully.")) {
                    Intent intent = new Intent(RatingActivity.this, MainActivity.class);
                    startActivity(intent);
                }
            }

            @Override
            public void onFailure(Call<RespSubmitRating> call, Throwable t) {
                progress.hideDialog();
                Toast.makeText(RatingActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
