package com.rydedispatch.driver.activity;

import android.annotation.SuppressLint;
import android.app.KeyguardManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.SystemClock;
import android.os.Vibrator;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.rydedispatch.driver.R;
import com.rydedispatch.driver.utils.AppConfig;
import com.rydedispatch.driver.utils.PreferenceHelper;
import com.rydedispatch.driver.utils.Prefs;
import com.rydedispatch.driver.utils.Progress;
import com.rydedispatch.driver.utils.Utils;
import com.twilio.voice.Call;
import com.twilio.voice.CallException;
import com.twilio.voice.CallInvite;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class IncomingActivity extends AppCompatActivity {
    public static final String INCOMING_CALL_INVITE = "INCOMING_CALL_INVITE";
    public static final String INCOMING_CALL_NOTIFICATION_ID = "INCOMING_CALL_NOTIFICATION_ID";
    public static final String ACTION_INCOMING_CALL = "ACTION_INCOMING_CALL";
    private static final String TAG = "IncomingActivity";
    public static boolean active = false;
    public static AppCompatActivity fa;
    private static Intent myIntent;
    private static CallInvite activeCallInvite;
    @BindView(R.id.txt_clinetname)
    TextView txtDoornumber;
    @BindView(R.id.connect_call)
    ImageButton connectCall;
    @BindView(R.id.connect_disconnect)
    ImageButton connectDisconnect;
    private Utils utils;
    private Progress progress;
    private PreferenceHelper helper;
    private String apikey = "";
    private Bundle bundle;
    private Ringtone mRingtone;
    private Vibrator vibrator;
    private Call activeCall;
//    Call.Listener callListener = callListener();
    String client;

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Get extra data included in the Intent

            if (intent.getBooleanExtra(AppConfig.BUNDLE.END, false) == true) {

                Log.e(TAG, "onReceiveeeee : ");
//                Log.e(TAG, "activeCallInvite : " + activeCallInvite.getState());
                DismissVibrate();
                StopRingtone();
                finish();

            }
        }
    };

    private ImageButton btnaccept, btndeclinel;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
                | WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD
                | WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON
                | WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
        setContentView(R.layout.activity_incoming);
        ButterKnife.bind(this);

        utils = new Utils(this);
        progress = new Progress(this);
//        helper = new PreferenceHelper(this, Prefs.PREF_FILE);

        btnaccept = findViewById(R.id.connect_call);
        btndeclinel = findViewById(R.id.connect_disconnect);
//        apikey = helper.LoadStringPref(AppConfig.PREFERENCE.AUTHKEY,"");

        fa = this;
        utils = new Utils(this);
        active = true;
        myIntent = getIntent();

        bundle = myIntent.getExtras();


        if (bundle != null) {

            KeyguardManager myKM = (KeyguardManager) getApplicationContext().getSystemService(Context.KEYGUARD_SERVICE);
            if (myKM.inKeyguardRestrictedInputMode()) {
                //it is locked
                Log.e("Incomimg", "Device Locked");

                handleIncomingCallIntent(myIntent);

                Uri defaultRintoneUri = RingtoneManager.getActualDefaultRingtoneUri(this
                        , RingtoneManager.TYPE_RINGTONE);
                vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                mRingtone = RingtoneManager.getRingtone(this, defaultRintoneUri);
                long pattern[] = {0, 100, 200, 300, 400};
                if (mRingtone != null)
                    mRingtone.play();
                vibrator.vibrate(pattern, 0);

            } else {
                //it is not locked
                Log.e("Incomimg", "Device Not Locked");
                handleIncomingCallIntent(myIntent);

                Uri defaultRintoneUri = RingtoneManager.getActualDefaultRingtoneUri(this
                        , RingtoneManager.TYPE_RINGTONE);
                vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                mRingtone = RingtoneManager.getRingtone(this, defaultRintoneUri);
                long pattern[] = {0, 100, 200, 300, 400};
                if (mRingtone != null)
                    mRingtone.play();
                vibrator.vibrate(pattern, 0);
            }


        } else {
            finish();
        }

        client = activeCallInvite.getFrom().replace("client:", "");
        txtDoornumber.setText(client);
//        helper.initPref();
//        helper.SaveStringPref(AppConfig.PREFERENCE.CLIENTNAME,client);
//        helper.ApplyPref();

        btnaccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                connect();

            }
        });

        btndeclinel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                disconnect();
            }
        });

    }


    @Override
    protected void onResume() {
        super.onResume();

        active = true;
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver,
                new IntentFilter(AppConfig.BUNDLE.CALL_END));
    }


    @Override
    protected void onStop() {

        StopRingtone();
        DismissVibrate();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);
        active = false;
        super.onStop();
    }


    @Override
    public void onBackPressed() {
        Toast.makeText(this, R.string.msg_back, Toast.LENGTH_SHORT).show();

    }


    private void handleIncomingCallIntent(Intent intent) {
        if (intent != null && intent.getAction() != null) {
            if (intent.getAction() == ACTION_INCOMING_CALL) {
                activeCallInvite = intent.getParcelableExtra(INCOMING_CALL_INVITE);

                Log.d(TAG, "activeCallInvite :" + activeCallInvite);

            } else {
                Log.d(TAG, "activeCallInvite :" + activeCallInvite);
                finish();
            }
        }
    }


    @SuppressLint("WrongConstant")
    private void connect() {
        StopRingtone();
        DismissVibrate();


        Intent intent = new Intent(IncomingActivity.this, VoiceActivity.class);
        intent.setAction(VoiceActivity.ACTION_INCOMING_CALL);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP + Intent.FLAG_ACTIVITY_NEW_TASK
                + WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
                + WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD
                + WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON
                + WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.putExtra(AppConfig.EXTRA.CHECKOUTGOING, 2);
        intent.putExtras(myIntent);
        intent.putExtra(AppConfig.BUNDLE.RING, false);
        intent.putExtra(AppConfig.BUNDLE.User_Name, client);
        startActivity(intent);
//        finish();
//        activeCallInvite.accept(this, callListener);

    }

    private void disconnect() {
        DismissVibrate();
        StopRingtone();
        activeCallInvite.reject(IncomingActivity.this);
        finish();
    }



    private void DismissVibrate() {
        if (vibrator != null) {
            vibrator.cancel();
            vibrator = null;
        }

        disconnectnew();
    }

    private void disconnectnew() {
//        if (activeCallInvite != null && (activeCallInvite.getState() == CallInvite.State.CANCELED)) {
//            activeCall.disconnect();
//        }
    }

    private void StopRingtone() {
        if (mRingtone != null) {
            mRingtone.stop();
            mRingtone = null;
        }
    }



}
