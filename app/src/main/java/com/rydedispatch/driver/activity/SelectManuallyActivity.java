package com.rydedispatch.driver.activity;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.rydedispatch.driver.R;
import com.rydedispatch.driver.network.ApiClient;
import com.rydedispatch.driver.network.ApiInterface;
import com.rydedispatch.driver.response.RespAddVehicle;
import com.rydedispatch.driver.response.RespGetCarType;
import com.rydedispatch.driver.response.RespGetMakeOfCar;
import com.rydedispatch.driver.response.RespGetModelOfCar;
import com.rydedispatch.driver.response.RespGetVehicleDetails;
import com.rydedispatch.driver.response.RespGetYearCar;
import com.rydedispatch.driver.utils.AppConfig;
import com.rydedispatch.driver.utils.PreferenceHelper;
import com.rydedispatch.driver.utils.Prefs;
import com.rydedispatch.driver.utils.Progress;
import com.rydedispatch.driver.utils.Utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SelectManuallyActivity extends AppCompatActivity {
    private static final String TAG = SelectManuallyActivity.class.getSimpleName();

    private Bundle bundle;
    TextInputEditText etVinCode;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.tvTitle)
    AppCompatTextView tvTitle;

    @BindView(R.id.et_color)
    TextInputEditText et_color;
    @BindView(R.id.view_vin)
    View viewvin;
    @BindView(R.id.ll_vin_code)
    LinearLayout llvincode;
    @BindView(R.id.btn_next)
    Button btnNext;
    @BindView(R.id.spinner_get_year)
    Spinner spinnerYear;
    @BindView(R.id.spinner_get_make)
    Spinner spinnerMake;
    @BindView(R.id.spinner_get_model)
    Spinner spinnergetModel;
    @BindView(R.id.spinner_get_drive)
    Spinner spinnergetdrive;
    @BindView(R.id.spinner_transmission)
    Spinner spinnertransmission;
    @BindView(R.id.spinner_car_type)
    Spinner spinnerCarType;
    @BindView(R.id.et_car_number)
    TextInputEditText etcarNumber;
    boolean selectManually, vehicle = true;
    private String accept = "", authorization = "", token = "", vinCode = "", colorFinal = "", yearFinal = "", makeFinal = "", driveFinal = "",
            transmisFinal = "", modelFinal = "", service = "", barcodeValue = "", carTypeFinal = "",
            selectedItemDrive = "", selectedtrans, carNumber = "", yearFinalDetails = "";
    protected PreferenceHelper helper;
    protected Progress progress;

    //drive+transmission
    List<String> driveList = new ArrayList<String>();
    List<String> transmissionList = new ArrayList<String>();

    //year+make+model
    ArrayList<String> yearOfCarList = new ArrayList<>();
    ArrayList<String> makeOfCarList = new ArrayList<>();
    ArrayList<String> modelOfList = new ArrayList<>();
    ArrayList<String> carTypeList = new ArrayList<>();
    private Utils utils;
    boolean login = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_manually);
        ButterKnife.bind(this);

        tvTitle.setText(getString(R.string.res_update_vehicle));

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();
            }
        });


        bundle = getIntent().getExtras();
        if (bundle != null) {
            barcodeValue = bundle.getString(AppConfig.BUNDLE.barcode);
            selectManually = bundle.getBoolean(AppConfig.BUNDLE.SelectManually);
            login = bundle.getBoolean(AppConfig.BUNDLE.LOGIN);

        }

        Log.d(TAG, "select manually login :" + login);


        utils = new Utils(this);


        helper = new PreferenceHelper(this, Prefs.PREF_FILE);
        progress = new Progress(this);

        accept = "application/json";
        authorization = "Bearer " + helper.LoadStringPref(Prefs.token, "");

        etVinCode = findViewById(R.id.et_vin_code);

        etVinCode.setText(barcodeValue);


        Log.d(TAG, "barcodeValue :" + barcodeValue);
        Log.d(TAG, "barcodeValue :" + driveFinal);
        if (selectManually) {
            llvincode.setVisibility(View.VISIBLE);
            viewvin.setVisibility(View.VISIBLE);

        } else {
            llvincode.setVisibility(View.GONE);
            viewvin.setVisibility(View.GONE);
        }

        if (!TextUtils.isEmpty(barcodeValue)) {
            getDetailsOfVehicle(accept, authorization, barcodeValue);
        }


        getDriveTrains(vehicle);

        getTrans(vehicle);

        utils.hideKeyboard();

        getYearOfCar(accept);
        getCarType(accept, authorization);
    }


    private void getDetailsOfVehicle(String accept, String authorization, String barcodeValue) {
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        RequestBody barcodeValue1 = RequestBody.create(MediaType.parse("multipart/form-data"), barcodeValue);
        final Call<RespGetVehicleDetails> respGetYearCarCall = apiService.GET_VEHICLE_DETAILS_CALL(accept, authorization, barcodeValue1);
        respGetYearCarCall.enqueue(new Callback<RespGetVehicleDetails>() {
            @Override
            public void onResponse(Call<RespGetVehicleDetails> call, Response<RespGetVehicleDetails> response) {

                if (response.body().getMessage().equalsIgnoreCase("VIN number details fetch successully")) {
                    Log.d(TAG, "barcodeValue : " + barcodeValue);
                    Log.d(TAG, "drive : " + response.body().getData().getDriveType());
                    Log.d(TAG, "trans set : " + response.body().getData().getTrims());

                    modelFinal = response.body().getData().getModel();
                    yearFinal = response.body().getData().getModelYear();
                    makeFinal = response.body().getData().getMake();
                    driveFinal = response.body().getData().getDriveType();
                    transmisFinal = response.body().getData().getTrims();

                    getYearOfCar(accept);
                    getCarType(accept, authorization);

                    vehicle = false;

                    getDriveTrains(vehicle);
                } else {
                    openAlertUnauthLogout(response.body().getMessage());
//                    Toast.makeText(SelectManuallyActivity.this, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }


            }

            @Override
            public void onFailure(Call<RespGetVehicleDetails> call, Throwable t) {
                Toast.makeText(SelectManuallyActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void openAlertUnauthLogout(String message) {


        final Dialog mBottomSheetDialog = new Dialog(this, R.style.MaterialDialogSheet);
        mBottomSheetDialog.setContentView(R.layout.dialog_unauthorised);
        mBottomSheetDialog.setCancelable(false);
        mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        mBottomSheetDialog.getWindow().setGravity(Gravity.CENTER);
        mBottomSheetDialog.setCanceledOnTouchOutside(false);
        AppCompatTextView tv_msg = mBottomSheetDialog.findViewById(R.id.tv_msg);
        AppCompatTextView btn_ok = mBottomSheetDialog.findViewById(R.id.btn_ok);

        tv_msg.setText(message);
        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBottomSheetDialog.dismiss();
                finish();
            }
        });
        mBottomSheetDialog.show();
    }


    private void getTrans(boolean vehicle) {


        transmissionList.add("Select Transmission");
        transmissionList.add("Automatic");
        transmissionList.add("Manual");


        ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(this
                , R.layout.row_auto_complete1, transmissionList);

        adapter1.setDropDownViewResource(R.layout.row_auto_complete1);

        spinnertransmission.setAdapter(adapter1);
        spinnertransmission.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
//                selectedtrans = spinnertransmission.getItemAtPosition(i).toString();
                transmisFinal = spinnertransmission.getItemAtPosition(i).toString();

                if (!adapterView.getItemAtPosition(i).toString().equalsIgnoreCase("Select Transmission")) {

                    transmisFinal = spinnertransmission.getItemAtPosition(i).toString();
                }

                try {
                } catch (IndexOutOfBoundsException e) {
                    e.printStackTrace();
                }

                Log.d(TAG, "transmission : " + transmisFinal);

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void getDriveTrains(boolean vehicle) {
        if (!vehicle) {
            driveList = new ArrayList<>();

            ArrayAdapter<String> adapter = new ArrayAdapter<String>(SelectManuallyActivity.this
                    , R.layout.row_auto_complete1, driveList);

            spinnergetdrive.setAdapter(adapter);

            for (int i = 0; i < driveList.size(); i++) {
                if (driveList.get(i).equalsIgnoreCase(driveFinal)) {
                    spinnergetdrive.setSelection(i);
                    break;
                }
            }
            spinnergetdrive.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {


                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {
                }
            });
        } else {
            driveList.add("Select Drive Train");
            driveList.add("Front wheel drive");
            driveList.add("Rear wheel drive");
            driveList.add("4X4");
            driveList.add("I don't know");

            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this
                    , R.layout.row_auto_complete1, driveList);

            adapter.setDropDownViewResource(R.layout.row_auto_complete1);

            spinnergetdrive.setAdapter(adapter);
            spinnergetdrive.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                    if (!parent.getItemAtPosition(position).toString().equalsIgnoreCase("Select Drive Train")) {

                        driveFinal = spinnergetdrive.getItemAtPosition(position).toString();
                    }

                    Log.d(TAG, "drive train : " + driveFinal);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        }


    }


    private void getYearOfCar(String accept) {


        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        final Call<RespGetYearCar> respGetYearCarCall = apiService.GET_YEAR_CAR(accept);
        respGetYearCarCall.enqueue(new Callback<RespGetYearCar>() {
            @Override
            public void onResponse(Call<RespGetYearCar> call, Response<RespGetYearCar> response) {

                yearOfCarList = new ArrayList<>();

                yearOfCarList.add("Select Year");

                for (RespGetYearCar.DataEntity getyearofcar : response.body().getData()) {
                    yearOfCarList.add(getyearofcar.getModelYear());
                }
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(SelectManuallyActivity.this
                        , R.layout.row_auto_complete1, yearOfCarList);

                spinnerYear.setAdapter(adapter);

                for (int i = 0; i < yearOfCarList.size(); i++) {
                    if (yearOfCarList.get(i).equalsIgnoreCase(yearFinal)) {
                        spinnerYear.setSelection(i);
                        break;
                    }
                }
                spinnerYear.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                        if (!adapterView.getItemAtPosition(i).toString().equalsIgnoreCase("Select Year")) {
                            yearFinal = adapterView.getItemAtPosition(i).toString();
                            getMakeofCar(accept, yearFinal);

                        } else {
                            yearFinal = adapterView.getItemAtPosition(i).toString();

                            getMakeofCar(accept, "2019");
                        }


                        Log.d(TAG, "year of final :" + yearFinal);

                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {
                    }
                });


            }

            @Override
            public void onFailure(Call<RespGetYearCar> call, Throwable t) {
                Toast.makeText(SelectManuallyActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getMakeofCar(String accept, String yearOf) {

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        RequestBody yearOf1 = RequestBody.create(MediaType.parse("multipart/form-data"), yearOf);
        final Call<RespGetMakeOfCar> respGetMakeOfCarCall = apiService.GET_MAKE_OF_CAR(accept, yearOf1);
        respGetMakeOfCarCall.enqueue(new Callback<RespGetMakeOfCar>() {
            @Override
            public void onResponse(Call<RespGetMakeOfCar> call, Response<RespGetMakeOfCar> response) {
                makeOfCarList = new ArrayList<>();

                makeOfCarList.add("Select Make");

                for (RespGetMakeOfCar.DataEntity getMake : response.body().getData()) {
                    makeOfCarList.add(getMake.getModelMakeId());
                }
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(SelectManuallyActivity.this
                        , R.layout.row_auto_complete1, makeOfCarList);

                spinnerMake.setAdapter(adapter);

                for (int i = 0; i < makeOfCarList.size(); i++) {
                    if (makeOfCarList.get(i).equalsIgnoreCase(makeFinal)) {
                        spinnerMake.setSelection(i);
                        break;
                    }
                }
                spinnerMake.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {


                        if (!adapterView.getItemAtPosition(i).toString().equalsIgnoreCase("Select Make")) {
                            makeFinal = adapterView.getItemAtPosition(i).toString();

                            getModel(accept, makeFinal, yearFinal);

                            Log.d(TAG, "make of final :" + makeFinal);

                        } else {

                            makeFinal = adapterView.getItemAtPosition(i).toString();
                            getModel(accept, "Acura", yearFinal);

                        }

                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });

            }

            @Override
            public void onFailure(Call<RespGetMakeOfCar> call, Throwable t) {
                Toast.makeText(SelectManuallyActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getModel(String accept, String makeof, String yearOf) {

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        RequestBody yearOf1 = RequestBody.create(MediaType.parse("multipart/form-data"), String.valueOf(yearOf));
        RequestBody make1 = RequestBody.create(MediaType.parse("multipart/form-data"), makeof);
        final Call<RespGetModelOfCar> respGetModelOfCarCall = apiService.MODEL_OF_CAR(accept, yearOf1, make1);
        respGetModelOfCarCall.enqueue(new Callback<RespGetModelOfCar>() {
            @Override
            public void onResponse(Call<RespGetModelOfCar> call, Response<RespGetModelOfCar> response) {
                modelOfList = new ArrayList<>();

                modelOfList.add("Select Model");


                for (RespGetModelOfCar.DataEntity model : response.body().getData()) {
                    modelOfList.add(model.getModelName());
                }
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(SelectManuallyActivity.this
                        , R.layout.row_auto_complete1, modelOfList);

                spinnergetModel.setAdapter(adapter);
                for (int i = 0; i < modelOfList.size(); i++) {
                    if (modelOfList.get(i).equalsIgnoreCase(modelFinal)) {
                        spinnergetModel.setSelection(i);
                        break;
                    }
                }


                spinnergetModel.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {


                        if (!adapterView.getItemAtPosition(i).toString().equalsIgnoreCase("Select Model")) {

                            modelFinal = adapterView.getItemAtPosition(i).toString();
                            Log.d(TAG, " modelFinal :" + modelFinal);

                        } else {
                            modelFinal = adapterView.getItemAtPosition(i).toString();
                        }


                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });

            }

            @Override
            public void onFailure(Call<RespGetModelOfCar> call, Throwable t) {

                Toast.makeText(SelectManuallyActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void getCarType(String accept, String authorization) {
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        final Call<RespGetCarType> respGetCarTypeCall = apiService.CAR_TYPE_CALL(accept, authorization);
        respGetCarTypeCall.enqueue(new Callback<RespGetCarType>() {
            @Override
            public void onResponse(Call<RespGetCarType> call, Response<RespGetCarType> response) {
                carTypeList = new ArrayList<>();

                carTypeList.add("Select Car Type");

                for (RespGetCarType.DataEntity getyearofcar : response.body().getData()) {
                    carTypeList.add(getyearofcar.getCarTypeName());
                }
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(SelectManuallyActivity.this
                        , R.layout.row_auto_complete1, carTypeList);

                spinnerCarType.setAdapter(adapter);
                spinnerCarType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                        /*if (!adapterView.getItemAtPosition(i).toString().equalsIgnoreCase("Select Car Type")) {
                            carTypeFinal = response.body().getData().get(i).getCarTypeName();
                        }*/

                        if (!adapterView.getItemAtPosition(i).toString().equalsIgnoreCase("Select Car Type")) {

                            carTypeFinal = adapterView.getItemAtPosition(i).toString();
                        } else {
                            carTypeFinal = adapterView.getItemAtPosition(i).toString();
                        }

                        Log.d(TAG, "car type :" + carTypeFinal);
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });

//                getVehicle(accept, authorization);
            }

            @Override
            public void onFailure(Call<RespGetCarType> call, Throwable t) {

            }
        });
    }

    @NonNull
    @OnClick({R.id.btn_next, R.id.iv_back})
    public void onViewClicked(View view) {
        switch (view.getId()) {

            case R.id.btn_next:
                next1(accept, authorization);
                break;

            case R.id.iv_back:
                finish();
                break;

        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "on resume :");
    }

    private void next1(String accept, String authorization) {
        /*try {


        } catch (NullPointerException e) {
            e.printStackTrace();
        }*/

        vinCode = Objects.requireNonNull(etVinCode.getText()).toString();
        makeFinal = spinnerMake.getSelectedItem().toString();
        modelFinal = spinnergetModel.getSelectedItem().toString();
        yearFinal = spinnerYear.getSelectedItem().toString();
        colorFinal = et_color.getText().toString();
        driveFinal = spinnergetdrive.getSelectedItem().toString();
        transmisFinal = spinnertransmission.getSelectedItem().toString();
        carTypeFinal = spinnerCarType.getSelectedItem().toString();
        carNumber = etcarNumber.getText().toString();

        if (makeFinal == null || modelFinal == null || yearFinal == null || colorFinal == null
                || driveFinal == null || transmisFinal == null || carTypeFinal == null
                || carNumber == null) {
            Toast.makeText(this, "" + getString(R.string.msg_fields_e), Toast.LENGTH_SHORT).show();

        } else if (spinnerMake.getSelectedItem().toString().equalsIgnoreCase("") || TextUtils.isEmpty(makeFinal) || makeFinal.contains("Select Make")) {
            Toast.makeText(this, "" + getString(R.string.msg_fields_e), Toast.LENGTH_SHORT).show();
        } else if (spinnergetModel.getSelectedItem().toString().equalsIgnoreCase("") || TextUtils.isEmpty(modelFinal) || modelFinal.contains("Select Model")) {
            Toast.makeText(this, "" + getString(R.string.msg_fields_e), Toast.LENGTH_SHORT).show();

        } else if (spinnerYear.getSelectedItem().toString().equalsIgnoreCase("") || TextUtils.isEmpty(yearFinal) || yearFinal.contains("Select Year")) {
            Toast.makeText(this, "" + getString(R.string.msg_fields_e), Toast.LENGTH_SHORT).show();
        } else if (TextUtils.isEmpty(colorFinal)) {
            Toast.makeText(this, "" + getString(R.string.msg_fields_e), Toast.LENGTH_SHORT).show();
        } else if (spinnergetdrive.getSelectedItem().toString().equalsIgnoreCase("")
                || TextUtils.isEmpty(driveFinal) || driveFinal.contains("Select Drive Train")) {
            Toast.makeText(this, "" + getString(R.string.msg_fields_e), Toast.LENGTH_SHORT).show();

        } else if (spinnertransmission.getSelectedItem().toString().equalsIgnoreCase("") || TextUtils.isEmpty(transmisFinal) || transmisFinal.contains("Select Transmission")) {
            Toast.makeText(this, "" + getString(R.string.msg_fields_e), Toast.LENGTH_SHORT).show();

        } else if (spinnerCarType.getSelectedItem().toString().equalsIgnoreCase("") || TextUtils.isEmpty(carTypeFinal) || carTypeFinal.contains("Select Car Type")) {
            Toast.makeText(this, "" + getString(R.string.msg_fields_e), Toast.LENGTH_SHORT).show();

        } else {

            saveDetails(accept, authorization, vinCode,
                    makeFinal, modelFinal, yearFinal,
                    colorFinal, transmisFinal, driveFinal, carTypeFinal, carNumber);

        }

    }

    private void saveDetails(String accept, String authorization, String vinCode,
                             String makeFinal, String modelFinal, String yearFinal,
                             String colorFinal, String transmisFinal, String driveFinal,
                             String carTypeFinal, String carNumber) {

        progress.createDialog(true);
        progress.DialogMessage(getString(R.string.please_wait));
        progress.showDialog();

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        RequestBody vincode1 = RequestBody.create(MediaType.parse("multipart/form-data"), vinCode);
        RequestBody make1 = RequestBody.create(MediaType.parse("multipart/form-data"), makeFinal);
        RequestBody model1 = RequestBody.create(MediaType.parse("multipart/form-data"), modelFinal);
        RequestBody yearOf1 = RequestBody.create(MediaType.parse("multipart/form-data"), yearFinal);
        RequestBody color1 = RequestBody.create(MediaType.parse("multipart/form-data"), colorFinal);
        RequestBody transmis1 = RequestBody.create(MediaType.parse("multipart/form-data"), transmisFinal);
        RequestBody drive1 = RequestBody.create(MediaType.parse("multipart/form-data"), driveFinal);
        RequestBody carType1 = RequestBody.create(MediaType.parse("multipart/form-data"), carTypeFinal);
        RequestBody car_number1 = RequestBody.create(MediaType.parse("multipart/form-data"), carNumber);
        final Call<RespAddVehicle> respAddVehicleCall = apiService.ADD_VEHICLE_CALL(accept, authorization,
                vincode1, make1, model1, yearOf1, color1, transmis1, drive1, carType1, car_number1);

        respAddVehicleCall.enqueue(new Callback<RespAddVehicle>() {
            @Override
            public void onResponse(Call<RespAddVehicle> call, Response<RespAddVehicle> response) {
                progress.hideDialog();

                if (response.isSuccessful()) {

                    if (response.body().getMessage().contains("Vehicle has been updated successfully.")) {
                        Toast.makeText(SelectManuallyActivity.this, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();

                    } else {
                        Toast.makeText(SelectManuallyActivity.this, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();

                    }
                }


            }

            @Override
            public void onFailure(Call<RespAddVehicle> call, Throwable t) {
                progress.hideDialog();
                Toast.makeText(SelectManuallyActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });
    }


}
