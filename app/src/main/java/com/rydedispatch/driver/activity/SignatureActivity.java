package com.rydedispatch.driver.activity;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.github.gcacace.signaturepad.views.SignaturePad;
import com.rydedispatch.driver.R;
import com.rydedispatch.driver.network.ApiInterface;
import com.rydedispatch.driver.utils.AppConfig;
import com.rydedispatch.driver.utils.PreferenceHelper;
import com.rydedispatch.driver.utils.Prefs;
import com.rydedispatch.driver.utils.Progress;
import com.rydedispatch.driver.utils.Utils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class SignatureActivity extends AppCompatActivity {
    private static final String TAG = SignatureActivity.class.getSimpleName();

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.tvTitle)
    AppCompatTextView tvTitle;
    @BindView(R.id.sign_done)
    AppCompatTextView tvDone;
    @BindView(R.id.iv_back)
    ImageView iv_back;
    @BindView(R.id.tv_eta)
    AppCompatTextView tvEta;
    @BindView(R.id.tv_eta_fare)
    AppCompatTextView tv_eta_fare;
    @BindView(R.id.ib_close)
    ImageView ib_close;
    @BindView(R.id.ib_info)
    ImageView ib_info;
    @BindView(R.id.signature_pad)
    SignaturePad mSignaturePad;
    @BindView(R.id.tv_corporate_user)
    AppCompatTextView tv_corporate_user;
    @BindView(R.id.rl_layout_signature)
    RelativeLayout rl_layout_signature;


    private Bundle bundle;
    private Context context = SignatureActivity.this;
    private String requestId = "", accept = "",
            authorization = "", filePath = "", totaltime = "",
            totalcost = "", eta = "", est_fare = "", actualcost = "", actualDiscount = "",
            basefare = "", extraDistancecost = "",
            passengerfirstname = "", displaypassengername = "", paymentType = "", passengerimage = "";

    LinearLayout mContent;

    protected PreferenceHelper helper;
    protected Progress progress;
    private Utils utils;
    Bitmap bitmap;
    View view;

    ApiInterface apiService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signature);
        ButterKnife.bind(this);

        tvTitle.setText(getString(R.string.res_signature));

        bundle = getIntent().getExtras();
        if (bundle != null) {
            requestId = bundle.getString(AppConfig.BUNDLE.request_id);
            actualcost = bundle.getString(AppConfig.BUNDLE.actual_cost);
            actualDiscount = bundle.getString(AppConfig.BUNDLE.actual_discount);
            basefare = bundle.getString(AppConfig.BUNDLE.base_fare);
            extraDistancecost = bundle.getString(AppConfig.BUNDLE.extra_distance_cost);

            totaltime = bundle.getString(AppConfig.BUNDLE.TotalTime);
            totalcost = bundle.getString(AppConfig.BUNDLE.TotalCost);
            est_fare = bundle.getString(AppConfig.BUNDLE.est_fare);
            eta = bundle.getString(AppConfig.BUNDLE.eta);
            passengerfirstname = bundle.getString(AppConfig.BUNDLE.passenger_first_name);
            displaypassengername = bundle.getString(AppConfig.BUNDLE.display_passenger_name);
            paymentType = bundle.getString(AppConfig.BUNDLE.payment_type);
            passengerimage = bundle.getString(AppConfig.BUNDLE.passenger_image);
        }


        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        helper = new PreferenceHelper(this, Prefs.PREF_FILE);
        progress = new Progress(this);
        utils = new Utils(this);

        accept = "application/json";
        authorization = "Bearer " + helper.LoadStringPref(Prefs.token, "");


        if (paymentType.equalsIgnoreCase("none")) {
            tv_corporate_user.setVisibility(View.VISIBLE);
//            tv_corporate_user.setText("Corporate User");
            tv_corporate_user.setText("Invoice");
        } else {
            tv_corporate_user.setVisibility(View.GONE);
        }

        tvEta.setText(eta + " MIN");
        tv_eta_fare.setText("$ " + est_fare);


        initRetrofitClient();
        Log.d(TAG, "passenger image : " + passengerimage);

    }

    private void initRetrofitClient() {
        OkHttpClient client = new OkHttpClient.Builder().build();

        apiService = new Retrofit.Builder().baseUrl("http://167.172.247.117").client(client).build().create(ApiInterface.class);
//        apiService = new Retrofit.Builder().baseUrl("https://portal.rydedispatch.com").client(client).build().create(ApiInterface.class);
    }

    @OnClick({R.id.iv_back, R.id.ib_info, R.id.sign_done, R.id.ib_close})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ll_main:
                break;
            case R.id.ib_info:
                infoAlert();
                break;
            case R.id.sign_done:
                bitmap = mSignaturePad.getSignatureBitmap();

                if (bitmap != null)
                    multipartImageUpload();
                else {
                    Snackbar.make(rl_layout_signature, "Signature is Empty. Try again", Snackbar.LENGTH_SHORT).show();
                }

                break;

            case R.id.ib_close:
                mSignaturePad.clear();
                break;
        }
    }

    private void multipartImageUpload() {
        try {
            File filesDir = getApplicationContext().getFilesDir();
            File file = new File(filesDir, "image" + ".png");

            OutputStream os;
            try {
                os = new FileOutputStream(file);
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, os);
                os.flush();
                os.close();
            } catch (Exception e) {
                Log.e(getClass().getSimpleName(), "Error writing bitmap", e);
            }

            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 0, bos);
            byte[] bitmapdata = bos.toByteArray();


            FileOutputStream fos = new FileOutputStream(file);
            fos.write(bitmapdata);
            fos.flush();
            fos.close();

            progress.createDialog(true);
            progress.DialogMessage(getString(R.string.please_wait));
            progress.showDialog();

            String requestid = requestId;

            RequestBody request_id1 = RequestBody.create(MediaType.parse("multipart/form-data"), requestid);
            RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), file);
            MultipartBody.Part body = MultipartBody.Part.createFormData("signature_image", file.getName(), reqFile);

            Call<ResponseBody> req = apiService.postImage(accept, authorization, body, request_id1);
            req.enqueue(new Callback<ResponseBody>() {
                @SuppressLint("SetTextI18n")
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    progress.hideDialog();
                    if (response.code() == 200) {

                        Snackbar.make(rl_layout_signature, "Your signature has been uploaded successfully.", Snackbar.LENGTH_SHORT).show();

                        startActivity(new Intent(SignatureActivity.this, RatingActivity.class)
                                .putExtra(AppConfig.BUNDLE.display_passenger_name, displaypassengername)
                                .putExtra(AppConfig.BUNDLE.passenger_image, passengerimage)
                                .putExtra(AppConfig.BUNDLE.request_id, requestId));
                    }

                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    progress.hideDialog();
                    Toast.makeText(getApplicationContext(), "" + t.getMessage(), Toast.LENGTH_SHORT).show();
                    t.printStackTrace();
                }
            });


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void infoAlert() {
        final Dialog mBottomSheetDialog = new Dialog(this, R.style.MaterialDialogSheet);
        mBottomSheetDialog.setContentView(R.layout.dialog_info_fare);
        mBottomSheetDialog.setCancelable(true);
        mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        mBottomSheetDialog.getWindow().setGravity(Gravity.CENTER);
        mBottomSheetDialog.setCanceledOnTouchOutside(true);
        ImageButton ib_alert_close = mBottomSheetDialog.findViewById(R.id.ib_alert_close);
        AppCompatTextView tv_total_distance = mBottomSheetDialog.findViewById(R.id.tv_total_distance);
        AppCompatTextView tv_actual_cost = mBottomSheetDialog.findViewById(R.id.tv_actual_cost);
        AppCompatTextView tv_actual_discount = mBottomSheetDialog.findViewById(R.id.tv_actual_discount);
        AppCompatTextView tv_base_fare = mBottomSheetDialog.findViewById(R.id.tv_base_fare);
        AppCompatTextView tv_extra_distance_cost = mBottomSheetDialog.findViewById(R.id.tv_extra_distance_cost);

        if (actualcost == null) {
            tv_total_distance.setText("$ " + "0");
            tv_actual_cost.setText("$" + "0");

        } else {
            tv_total_distance.setText("$ " + actualcost);
            tv_actual_cost.setText("$ " + actualcost);

        }
        if (actualDiscount == null) {
            tv_actual_discount.setText("$ " + "0");
        } else {
            tv_actual_discount.setText("$ " + actualDiscount);
        }

        if (basefare == null) {
            tv_base_fare.setText("$ " + "0");
        } else {
            tv_base_fare.setText(" $" + basefare);

        }
        if (extraDistancecost == null) {
            tv_extra_distance_cost.setText("$ " + "0");

        } else {
            tv_extra_distance_cost.setText("$ "+ extraDistancecost);

        }

        ib_alert_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBottomSheetDialog.dismiss();
            }
        });
        mBottomSheetDialog.show();
    }

}
