package com.rydedispatch.driver.activity;

import android.Manifest;
import android.animation.ObjectAnimator;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.edwardvanraak.materialbarcodescanner.MaterialBarcodeScanner;
import com.edwardvanraak.materialbarcodescanner.MaterialBarcodeScannerBuilder;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.gson.Gson;
import com.rydedispatch.driver.R;
import com.rydedispatch.driver.network.ApiClient;
import com.rydedispatch.driver.network.ApiInterface;
import com.rydedispatch.driver.response.RespDashboardDriver;
import com.rydedispatch.driver.response.RespGetVehicle;
import com.rydedispatch.driver.utils.AppConfig;
import com.rydedispatch.driver.utils.PreferenceHelper;
import com.rydedispatch.driver.utils.Prefs;
import com.rydedispatch.driver.utils.Progress;
import com.rydedispatch.driver.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddVehicleActivity extends AppCompatActivity {

    private static final String TAG = AddVehicleActivity.class.getSimpleName();

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.circular_progress_bar)
    ProgressBar pbar;
    @BindView(R.id.rl_vehicle_details)
    RelativeLayout rl_vehicle_details;
    @BindView(R.id.tvTitle)
    AppCompatTextView tvTitle;
    @BindView(R.id.skip)
    AppCompatTextView tvSkip;

    @BindView(R.id.ll_scan_vin)
    LinearLayout llscanvin;
    @BindView(R.id.ll_enter_vin)
    LinearLayout llEnterVin;
    @BindView(R.id.ll_select_manually)
    LinearLayout llSelectManually;
    @BindView(R.id.ll_next)
    LinearLayout ll_next;
    @BindView(R.id.tv_car_type_value)
    AppCompatTextView tv_car_type_value;
    @BindView(R.id.tv_make_value)
    AppCompatTextView tv_make_value;
    @BindView(R.id.tv_model_value)
    AppCompatTextView tv_model_value;
    @BindView(R.id.tv_year_value)
    AppCompatTextView tv_year_value;
    @BindView(R.id.tv_transmission_value)
    AppCompatTextView tv_transmission_value;
    @BindView(R.id.tv_driver_value)
    AppCompatTextView tv_driver_value;
    @BindView(R.id.tv_color_value)
    AppCompatTextView tv_color_value;
    @BindView(R.id.tv_car_num_value)
    AppCompatTextView tv_car_num_value;
    @BindView(R.id.ll_list_vehicle)
    LinearLayout ll_list_vehicle;
    private final int MY_PERMISSION_REQUEST_CAMERA = 1001;
    private Barcode barcodeResult;
    private Bundle bundle;
    boolean selectManually;
    boolean login = false;


    private List<RespDashboardDriver.DataEntity> listData = new ArrayList<>();
    private Utils utils;

    private String accept = "", authorization = "";
    protected PreferenceHelper helper;
    protected Progress progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_vehicle);
        ButterKnife.bind(this);

        tvTitle.setText(getString(R.string.res_add_vehicle));

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();
            }
        });

        bundle = getIntent().getExtras();
        if (bundle != null) {
            login = bundle.getBoolean(AppConfig.BUNDLE.LOGIN);
            selectManually = bundle.getBoolean(AppConfig.BUNDLE.SelectManually);
        }

        if (login) {
            Log.d(TAG, "login :" + login);
            ll_next.setVisibility(View.GONE);
            tvSkip.setVisibility(View.GONE);
            ll_list_vehicle.setVisibility(View.VISIBLE);
        }
        else {
            ll_next.setVisibility(View.VISIBLE);
            tvSkip.setVisibility(View.VISIBLE);
            ll_list_vehicle.setVisibility(View.INVISIBLE);

        }

        utils = new Utils(AddVehicleActivity.this);
        progress = new Progress(AddVehicleActivity.this);
        helper = new PreferenceHelper(AddVehicleActivity.this, Prefs.PREF_FILE);

        accept = "application/json";
        authorization = "Bearer " + helper.LoadStringPref(Prefs.token, "");

        getVehicle(accept, authorization);

    }

    private void getVehicle(String accept, String authorization) {
//        progress.createDialog(true);
//        progress.DialogMessage(getString(R.string.please_wait));
//        progress.showDialog();
        pbar.setVisibility(View.VISIBLE);
        ObjectAnimator anim = ObjectAnimator.ofInt(pbar, "progress", 0, 100);
        anim.setDuration(15000);
        anim.setInterpolator(new DecelerateInterpolator());
        anim.start();

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        final Call<RespGetVehicle> respVehicleDriverCall = apiService.DRIVER_VEHICLE(accept, authorization);
        respVehicleDriverCall.enqueue(new Callback<RespGetVehicle>() {
            @Override
            public void onResponse(Call<RespGetVehicle> call, Response<RespGetVehicle> response) {
//                progress.hideDialog();
                pbar.setVisibility(View.GONE);
                rl_vehicle_details.setVisibility(View.VISIBLE);
                Log.d(TAG, "vehicle response : " + new Gson().toJson(response.body()));

                final RespGetVehicle respGetVehicle = response.body();
                if (response.body().getMessage().contains("Unauthenticated")) {
                    progress.hideDialog();

                    openAlertUnauthLogout();

                }
                else {
                    tv_car_type_value.setText(response.body().getData().getVehicle().getCarType());
                    tv_make_value.setText(response.body().getData().getVehicle().getMake());
                    tv_model_value.setText(response.body().getData().getVehicle().getModel());
                    tv_year_value.setText(response.body().getData().getVehicle().getYear());
                    tv_transmission_value.setText(response.body().getData().getVehicle().getTransmission());
                    tv_driver_value.setText(response.body().getData().getVehicle().getDriverTrain());
                    tv_color_value.setText(response.body().getData().getVehicle().getColor());
                    tv_car_num_value.setText(response.body().getData().getVehicle().getCarNumber());
                }



            }

            @Override
            public void onFailure(Call<RespGetVehicle> call, Throwable t) {
//                progress.hideDialog();
                pbar.setVisibility(View.GONE);
                rl_vehicle_details.setVisibility(View.VISIBLE);
            }
        });
    }


    private void openAlertUnauthLogout() {
        final Dialog mBottomSheetDialog = new Dialog(this, R.style.MaterialDialogSheet);
        mBottomSheetDialog.setContentView(R.layout.dialog_unauthorised);
        mBottomSheetDialog.setCancelable(true);
        mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        mBottomSheetDialog.getWindow().setGravity(Gravity.CENTER);
        mBottomSheetDialog.setCanceledOnTouchOutside(true);
        AppCompatTextView btn_ok = mBottomSheetDialog.findViewById(R.id.btn_ok);

        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                helper.clearAllPrefs();
                startActivity(new Intent(AddVehicleActivity.this, LoginActivity.class));
                finish();
            }
        });
        mBottomSheetDialog.show();


    }


    @OnClick({R.id.ll_scan_vin, R.id.ll_enter_vin,
            R.id.ll_select_manually, R.id.iv_back,
            R.id.skip})
    public void onViewClicked(View view) {
        switch (view.getId()) {

            case R.id.ll_scan_vin:
                scanVin();
                break;
            case R.id.ll_enter_vin:
                startActivity(new Intent(AddVehicleActivity.this, EnterVinActivity.class)
                        .putExtra(AppConfig.BUNDLE.SelectManually, true));
                break;

            case R.id.ll_select_manually:
                startActivity(new Intent(AddVehicleActivity.this, SelectManuallyActivity.class)
                        .putExtra(AppConfig.BUNDLE.SelectManually, false));
                break;

            case R.id.iv_back:
                finish();
                break;

            case R.id.skip:
                startActivity(new Intent(AddVehicleActivity.this, MainActivity.class));
                break;
        }
    }

    private void scanVin() {
        checkPermission();
    }

    public void checkPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
            Log.d(TAG, getResources().getString(R.string.camera_permission_granted));
            startScanningBarcode();
        } else {
            requestCameraPermission();
        }
    }

    private void requestCameraPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA)) {

            ActivityCompat.requestPermissions(AddVehicleActivity.this, new String[]{Manifest.permission.CAMERA}, MY_PERMISSION_REQUEST_CAMERA);

        } else {
            ActivityCompat.requestPermissions(AddVehicleActivity.this, new String[]{Manifest.permission.CAMERA}, MY_PERMISSION_REQUEST_CAMERA);
        }
    }


    private void startScanningBarcode() {
        final MaterialBarcodeScanner materialBarcodeScanner = new MaterialBarcodeScannerBuilder()
                .withActivity(AddVehicleActivity.this)
                .withEnableAutoFocus(true)
                .withBleepEnabled(false)
                .withBackfacingCamera()
                .withCenterTracker()
                .withText("Scanning...")
                .withResultListener(new MaterialBarcodeScanner.OnResultListener() {
                    @Override
                    public void onResult(Barcode barcode) {
                        barcodeResult = barcode;

                        Log.d(TAG, "barcode :" + barcode.displayValue);
                        startActivity(new Intent(AddVehicleActivity.this, SelectManuallyActivity.class)
                                .putExtra(AppConfig.BUNDLE.barcode, barcode.displayValue)
                                .putExtra(AppConfig.BUNDLE.LOGIN, login)
                                .putExtra(AppConfig.BUNDLE.SelectManually, true));
                    }
                })
                .build();
        materialBarcodeScanner.startScan();
    }
}
