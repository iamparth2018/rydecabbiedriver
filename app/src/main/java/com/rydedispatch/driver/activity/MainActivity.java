package com.rydedispatch.driver.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.internal.NavigationMenu;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.gson.Gson;
import com.google.maps.android.SphericalUtil;
import com.rydedispatch.driver.R;
import com.rydedispatch.driver.fragments.DashboardFragment;
import com.rydedispatch.driver.fragments.EarningsFragment;
import com.rydedispatch.driver.fragments.HistoryFragment;
import com.rydedispatch.driver.fragments.ProfileFragment;
import com.rydedispatch.driver.network.ApiClient;
import com.rydedispatch.driver.network.ApiInterface;
import com.rydedispatch.driver.network.PostRequest;
import com.rydedispatch.driver.response.RespAcceptRide;
import com.rydedispatch.driver.response.RespArrived;
import com.rydedispatch.driver.response.RespCancelDriver;
import com.rydedispatch.driver.response.RespCompleteRide;
import com.rydedispatch.driver.response.RespDashboardDriver;
import com.rydedispatch.driver.response.RespForceDashboard;
import com.rydedispatch.driver.response.RespNoShow;
import com.rydedispatch.driver.response.RespPassRequest;
import com.rydedispatch.driver.response.RespStartRide;
import com.rydedispatch.driver.response.RespUpdateMyLocation;
import com.rydedispatch.driver.response.User;
import com.rydedispatch.driver.utils.AppConfig;
import com.rydedispatch.driver.utils.Config;
import com.rydedispatch.driver.utils.DrawRouteMaps;
import com.rydedispatch.driver.utils.GPSTracker;
import com.rydedispatch.driver.utils.MapUtils;
import com.rydedispatch.driver.utils.PreferenceHelper;
import com.rydedispatch.driver.utils.Prefs;
import com.rydedispatch.driver.utils.Progress;
import com.rydedispatch.driver.utils.SessionManager;
import com.rydedispatch.driver.utils.Utils;
import com.squareup.picasso.Picasso;
import com.twilio.voice.RegistrationException;
import com.twilio.voice.RegistrationListener;
import com.twilio.voice.Voice;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import io.github.yavski.fabspeeddial.FabSpeedDial;
import ng.max.slideview.SlideView;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.rydedispatch.driver.utils.AppConfig.URL_GET_AUDIO_TOKEN;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,
        OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, View.OnTouchListener/*, PermissionsListener*/ {

    private static final String TAG = MainActivity.class.getSimpleName();

    @BindView(R.id.root_main)
    CoordinatorLayout root_main;
    @BindView(R.id.profile_image_header)
    CircleImageView profile_image_header;
    @BindView(R.id.ll_user_status)
    LinearLayout lluserstatus;
    @BindView(R.id.ll_map)
    LinearLayout llmap;
    @BindView(R.id.ll_more)
    LinearLayout ll_more;
    @BindView(R.id.navigation_map)
    LinearLayout navigation_map;
    @BindView(R.id.ll_main_map_header)
    LinearLayout ll_main_map_header;
    @BindView(R.id.tv_passenger_name)
    AppCompatTextView passengerName;
    @BindView(R.id.ll_main)
    LinearLayout ll_main;
    @BindView(R.id.ll_header)
    LinearLayout ll_header;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.tvTitle)
    AppCompatTextView tvTitle;
    @BindView(R.id.iv_status)
    ImageView ivStatus;
    @BindView(R.id.driver_image)
    ImageView driverimage;
    @BindView(R.id.frame_layout_action)
    FrameLayout frameLayout;
    @BindView(R.id.panel_payment_type)
    TextView panel_payment_type;
    @BindView(R.id.tv_eta)
    AppCompatTextView etaTime;
    @BindView(R.id.tv_eta_details)
    AppCompatTextView tvetadetails;
    @BindView(R.id.tv_eta_fare)
    AppCompatTextView tvetafare;
    @BindView(R.id.tv_eta_fare_details)
    AppCompatTextView tvetafaredetails;
    @BindView(R.id.btn_accept)
    Button btn_accept;
    @BindView(R.id.btn_accept_details)
    Button btnacceptdetails;
    @BindView(R.id.btn_pass)
    Button btnpass;
    @BindView(R.id.btn_pass_details)
    Button btnpassDetails;
    @BindView(R.id.panel_content)
    TextView panel_content;
    @BindView(R.id.tv_open_bottom_sheet)
    TextView tvopenbottomsheet;
    @BindView(R.id.iv_close)
    AppCompatImageView ivclose;
    @BindView(R.id.tv_destination_location_details)
    AppCompatTextView tvdestinationlocationdetails;
    @BindView(R.id.tv_source_location_details)
    AppCompatTextView tvsourcelocationdetails;
    @BindView(R.id.tv_destination_location_arrived)
    AppCompatTextView tvdestinationlocationarrived;
    @BindView(R.id.anchor_panel)
    FrameLayout anchor_panel;
    @BindView(R.id.ll_call)
    LinearLayout ll_call;
    @BindView(R.id.tv_vehicle_status)
    AppCompatTextView tv_vehicle_status;
    @BindView(R.id.fab_speed_dial)
    FabSpeedDial fab_speed_dial;
    @BindView(R.id.map_progress)
    ProgressBar map_progress;
    @BindView(R.id.tv_details_instruction)
    AppCompatTextView tv_details_instruction;


    //    NavigationLauncherOptions options;
    GPSTracker gps;

    private int mInterval = 500; // 5 seconds by default, can be changed later
    boolean alert = true, tripAlert = false;
    boolean donotupdate = true;

    SlideView slideView;
    FragmentTransaction fragmentTransaction;
    FragmentManager manager;
    protected PreferenceHelper helper;
    private String deviceToken = "", token = "", accept = "",
            authorization = "", firstname = "", lastname = "",
            email = "", city = "", userUniqueCode = "", id = "", forceToken = "",
            documentId = "", userStatus = "",
            destination_latitude = "", destination_longitude = "", payment_type = "", passengerphonenumber = "",
            user_id = "", request_id = "", source_longitude = "", source_location = "",
            source_latitude = "", destination_location = "", user_image = "",
            eta = "", est_fare = "", status = "", body = "", totalTime = "", totalCost = "",
            clientlatitude = "", clientlongitude = "", userId = "", myLat = "", myLang = "", myUserID = "",
            is_schedule = "", schedule_date = "", uniqueCode = "", passenger_unique_code = "", runningRideStatus = "",
            pick_lat = "", pick_longg = "", pick_location = "", drop_lat = "",
            drop_longg = "", drop_location = "", laterTime = "", laterDate = "",
            imgURL = "", imgFcm = "", driverImage = "", passengerfirstname = "", acceptRide = "",
            passenger_first_name = "", actualcost = "", actualdiscount = "", basefare = "", maintitle = "",
            extraDistancecost = "", passmsg = "", documentStatus = "",
            passenger_rating = "", myProfileString = "", displaypassengername = "", supportnumber = "",
            vehicle_status = "", authToken = "", myProfile = "",
            rideinstruction = "", distance_type = "", driver_id = "", passengerImage = "", address = "", user_unique_code = "",
            vehicleInspectStatus = "";

    protected Progress progress;
    private Utils utils;
//    MapboxMap mapboxMap;

    TextView tv_name, tv_driver_name, tv_email, driver_city_txt, driver_;
    CircleImageView profile_pic;
    LinearLayout llProfile;

    boolean doubleBackToExitPressedOnce = false;
    private Bundle bundle;

    //for bottom sheet
    View bottomSheet, bottomSheetOpen, bottomSheetArrived;

    //Google map on bottom
    private TextView content, content_open, content_arrived;
    BottomSheetBehavior behavior, behaviorOpen, behaviorArrived;

    //Main map
    private SupportMapFragment mapFragment;

    private GoogleMap.OnCameraIdleListener onCameraIdleListener;


    //for location
    private final static int MY_PERMISSION_FINE_LOCATION = 101;
    private GoogleApiClient mGoogleApiClient;

    //bottom map
    Double distanceFromDestination;

    private GoogleMap mMap;
    LatLng myLatLng, driverLatlang;
    LocationManager locationManager;
    private static final int REQUEST_LOCATION_PERMISSION = 199;
    Marker marker;
    LocationListener locationListener;

    //firebase update location
    private DatabaseReference mFirebaseDatabase;
    private FirebaseDatabase mFirebaseInstance;
    SessionManager sessionManager;

    //bottom map
    GoogleMap mGooglemap;
    private String SELECTED_PICK_LATITUDE;
    private String SELECTED_PICK_LONGITUDE;
    private String SELECTED_PICK_LOCATION = "";
    private String SELECTED_DROP_LATITUDE;
    private String SELECTED_DROP_LONGITUDE;
    private String SELECTED_DROP_LOCATION = "";
    private RelativeLayout rl_layout;


    //polylines with car anim

    double latitude, longitude;
    //fab drag
    float dX;
    float dY;
    int lastAction;
//    private LocationComponent locationComponent;
//    private PermissionsManager permissionsManager;
//    private DirectionsRoute currentRoute;


//    private MapView mapView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        Mapbox.getInstance(this, getString(R.string.map_box_token));

        gps = new GPSTracker(MainActivity.this);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);


        helper = new PreferenceHelper(this, Prefs.PREF_FILE);
        progress = new Progress(this);
        utils = new Utils(this);


        rl_layout = findViewById(R.id.rl_layout);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setElevation(0);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);

        drawer.addDrawerListener(toggle);
        toggle.syncState();


        navigationView.setNavigationItemSelectedListener(this);

        toolbar.post(new Runnable() {
            @Override
            public void run() {
                Drawable d = ResourcesCompat.getDrawable(getResources(), R.drawable.home_white_side_menu, null);
                toolbar.setNavigationIcon(d);

            }
        });

        bundle = getIntent().getExtras();
        if (bundle != null) {
            destination_latitude = bundle.getString(AppConfig.BUNDLE.destination_latitude);
            payment_type = bundle.getString(AppConfig.BUNDLE.payment_type);
            user_id = bundle.getString(AppConfig.BUNDLE.user_id);
            destination_longitude = bundle.getString(AppConfig.BUNDLE.destination_longitude);
            request_id = bundle.getString(AppConfig.BUNDLE.request_id);
            source_longitude = bundle.getString(AppConfig.BUNDLE.source_longitude);
            source_location = bundle.getString(AppConfig.BUNDLE.source_location);
            source_latitude = bundle.getString(AppConfig.BUNDLE.source_latitude);
            destination_location = bundle.getString(AppConfig.BUNDLE.destination_location);
            user_image = bundle.getString(AppConfig.BUNDLE.user_image);
            eta = bundle.getString(AppConfig.BUNDLE.eta);
            est_fare = bundle.getString(AppConfig.BUNDLE.est_fare);
            is_schedule = bundle.getString(AppConfig.BUNDLE.is_schedule);
            body = bundle.getString(AppConfig.EXTRA.body);
            maintitle = bundle.getString(AppConfig.EXTRA.main_title);
            passmsg = bundle.getString(AppConfig.BUNDLE.pass_msg);
            passenger_unique_code = bundle.getString(AppConfig.BUNDLE.passenger_unique_code);
            status = bundle.getString(AppConfig.BUNDLE.status);
            displaypassengername = bundle.getString(AppConfig.BUNDLE.display_passenger_name);
            acceptRide = bundle.getString(AppConfig.BUNDLE.acceptRide, acceptRide);
            rideinstruction = bundle.getString(AppConfig.BUNDLE.ride_instruction, rideinstruction);
            driver_id = bundle.getString(AppConfig.BUNDLE.driver_id, driver_id);
            passengerImage = bundle.getString(AppConfig.BUNDLE.driver_id, passengerImage);

            est_fare = "" + getIntent().getExtras().getString(AppConfig.BUNDLE.est_fare);
            eta = "" + getIntent().getExtras().getString(AppConfig.BUNDLE.eta);

        }


        try {
            SELECTED_PICK_LATITUDE = "" + getIntent().getExtras().getString(Config.IntentKeys.PICK_LATITUDE);
            SELECTED_PICK_LONGITUDE = "" + getIntent().getExtras().getString(Config.IntentKeys.PICK_LONGITUDE);
            SELECTED_DROP_LATITUDE = "" + getIntent().getExtras().getString(Config.IntentKeys.DROP_LATITUDE);
            SELECTED_DROP_LONGITUDE = "" + getIntent().getExtras().getString(Config.IntentKeys.DROP_LONGITUDE);
            passengerfirstname = "" + getIntent().getExtras().getString(AppConfig.BUNDLE.passenger_first_name);
            passenger_rating = "" + getIntent().getExtras().getString(AppConfig.BUNDLE.passenger_rating);


        } catch (Exception e) {
            e.printStackTrace();
        }

        Log.d(TAG, "oncreate request_id : " + request_id);


        manager = getSupportFragmentManager();

        fragmentTransaction = manager.beginTransaction();
        fragmentTransaction.add(R.id.frame_main, DashboardFragment.newInstance());
        fragmentTransaction.commit();

        tvTitle.setText(getString(R.string.dashboard));

        frameLayout.setVisibility(View.VISIBLE);

        deviceToken = helper.LoadStringPref(Prefs.deviceToken, "");
        token = helper.LoadStringPref(Prefs.token, "");
        id = helper.LoadStringPref(Prefs.id, "");
        firstname = helper.LoadStringPref(Prefs.first_name, "");
        lastname = helper.LoadStringPref(Prefs.last_name, "");
        email = helper.LoadStringPref(Prefs.email, "");
        city = helper.LoadStringPref(Prefs.city, "");
        myProfile = helper.LoadStringPref(Prefs.profile_picture, "");
        supportnumber = helper.LoadStringPref(Prefs.support_number, "");

        Log.d(TAG, "oncreate token : " + token);
        Log.d(TAG, "oncreate deviceToken : " + deviceToken);
//        Log.d(TAG, "city : " + city);


        registerForCallInvites();


        myUserID = id;

        utils.hideKeyboard();

        View headerView = navigationView.getHeaderView(0);

        updateMyDb();

        tv_name = headerView.findViewById(R.id.tv_name);
        tv_driver_name = headerView.findViewById(R.id.tv_driver_name);
        tv_email = headerView.findViewById(R.id.tv_email);
        profile_pic = headerView.findViewById(R.id.profile_image_dashboard);
        llProfile = headerView.findViewById(R.id.ll_profile);
        driver_city_txt = headerView.findViewById(R.id.driver_city_txt);
        driver_ = headerView.findViewById(R.id.driver_);

        getDashboard(accept, authorization);


        llProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, ProfileActivity.class));
            }
        });

        content = findViewById(R.id.panel_content);

        //this is for first bottom sheet from notificaation

        bottomSheet = findViewById(R.id.anchor_panel);
        behavior = BottomSheetBehavior.from(bottomSheet);
        behavior.setHideable(false);
        behavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                // React to state change
                content.setText(newState == BottomSheetBehavior.STATE_EXPANDED ? R.string.res_pick_up : R.string.res_pick_up);

                if (newState == BottomSheetBehavior.STATE_DRAGGING) {
                    behavior.setHideable(false);
                    behavior.setState(BottomSheetBehavior.STATE_HALF_EXPANDED);
                }

                if (newState == BottomSheetBehavior.STATE_COLLAPSED) {
                    tvopenbottomsheet.setVisibility(View.GONE);
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                // React to dragging events
            }
        });


        //this for hide bottom view details
        content_open = findViewById(R.id.panel_content_open);

        bottomSheetOpen = findViewById(R.id.anchor_panel_open);
        behaviorOpen = BottomSheetBehavior.from(bottomSheetOpen);
        behaviorOpen.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                // React to state change
                content_open.setText(newState == BottomSheetBehavior.STATE_EXPANDED ? R.string.res_pick_up : R.string.res_pick_up);

                if (newState == BottomSheetBehavior.STATE_DRAGGING) {
                    behaviorOpen.setHideable(false);
                    behaviorOpen.setState(BottomSheetBehavior.STATE_EXPANDED);
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                // React to dragging events
            }
        });

        //this is for arrived bottom sheet
        content_arrived = findViewById(R.id.content_arrived);

        bottomSheetArrived = findViewById(R.id.anchor_panel_arrived);
        behaviorArrived = BottomSheetBehavior.from(bottomSheetArrived);
        behaviorArrived.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                // React to state change
                content_arrived.setText(newState == BottomSheetBehavior.STATE_EXPANDED ? R.string.res_arrived :
                        R.string.res_arrived);

                if (newState == BottomSheetBehavior.STATE_DRAGGING) {
                    behaviorArrived.setHideable(false);
                    behaviorArrived.setState(BottomSheetBehavior.STATE_HALF_EXPANDED);
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                // React to dragging events
            }
        });

        if (tvTitle.getText().toString().equals(getString(R.string.dashboard))) {
            frameLayout.setVisibility(View.VISIBLE);
        } else {
            frameLayout.setVisibility(View.GONE);
        }


        Log.d(TAG, "oncreate profile :" + myProfile);

        if (myProfile.contains("https://")) {

            Picasso.with(MainActivity.this)
                    .load(myProfile)
                    .placeholder(R.mipmap.ic_launcher)
                    .error(R.mipmap.ic_launcher)
                    .into(profile_pic);
        } else if (!myProfile.contains(AppConfig.URL.BASE_IMAGEURL)) {

            Picasso.with(MainActivity.this)
                    .load(AppConfig.URL.BASE_IMAGEURL + myProfile)
                    .placeholder(R.mipmap.ic_launcher)
                    .error(R.mipmap.ic_launcher)
                    .into(profile_pic);
        } else {
            Picasso.with(MainActivity.this)
                    .load(myProfile)
                    .placeholder(R.mipmap.ic_launcher)
                    .error(R.mipmap.ic_launcher)
                    .into(profile_pic);
        }

//        tv_name.setText(firstname + " " + lastname);
//        tv_email.setText(email);
//        tv_driver_name.setText(firstname + " " + lastname);
//        driver_city_txt.setText(city);

        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver,
                new IntentFilter("custom-event-name"));


        slideView = ((SlideView) findViewById(R.id.slider1));
        slideView.setText(getString(R.string.res_arrived));
        slideView.setOnSlideCompleteListener(new SlideView.OnSlideCompleteListener() {
            @Override
            public void onSlideComplete(SlideView slideView) {
//                Vibrator vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
//                vibrator.vibrate(100);
                slideView.setEnabled(true);
                Log.d(TAG, "slide arrive:" + slideView.getTextView().getText().toString());

                Log.d(TAG, "accept ride :" + request_id);
                Log.d(TAG, "startRide ride :" + request_id);
                Log.d(TAG, "completeRide ride :" + request_id);

                if (slideView.getTextView().getText().toString().contains(getString(R.string.res_arrived))) {
                    arrivedRide(accept, authorization, request_id, String.valueOf(distanceFromDestination));
                }
                if (slideView.getTextView().getText().toString().contains(getString(R.string.res_begin_trip))) {
//                    startRide(accept, authorization, request_id);
                }

                if (slideView.getTextView().getText().toString().contains(getString(R.string.res_complete_ride))) {
                    completeRide(accept, authorization, distanceFromDestination, request_id);
                }
            }
        });

        //for firebase instance for real time data
        mFirebaseInstance = FirebaseDatabase.getInstance();
        mFirebaseDatabase = FirebaseDatabase.getInstance("https://rydedispatch-15739.firebaseio.com/").getReference().child("location");

        mFirebaseInstance.getReference("location").addValueEventListener(new ValueEventListener() {

            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });


        llmap.setVisibility(View.VISIBLE);

        //floating buttons
        fab_speed_dial = (FabSpeedDial) findViewById(R.id.fab_speed_dial);

        fab_speed_dial.setOnTouchListener(this);


        //my location
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

        if (ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]
                            {Manifest.permission.ACCESS_FINE_LOCATION},
                    REQUEST_LOCATION_PERMISSION);
        }
        locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {


                latitude = location.getLatitude();
                longitude = location.getLongitude();

                myLat = String.valueOf(location.getLatitude());
                myLang = String.valueOf(location.getLongitude());

                //get the location name from latitude and longitude
                Geocoder geocoder = new Geocoder(MainActivity.this);
                try {
                    List<Address> addresses =
                            geocoder.getFromLocation(latitude, longitude, 1);
                    String result = addresses.get(0).getLocality() + ":";
                    result += addresses.get(0).getCountryName();
                    myLatLng = new LatLng(latitude, longitude);
                    if (marker != null) {
                        marker.remove();
                        marker = mMap.addMarker(new MarkerOptions().position(myLatLng).title(result));
                    } else {
                        marker = mMap.addMarker(new MarkerOptions().position(myLatLng).title(result));
                    }

                    updateLocationRealtime();


                    try {
                        if (SELECTED_DROP_LONGITUDE.equals("")
                                || SELECTED_DROP_LONGITUDE.equalsIgnoreCase("null")
                                || SELECTED_DROP_LONGITUDE.contains("null")) {

                            map_progress.setVisibility(View.GONE);

                        } else {

                            myLatLng = new LatLng(latitude, longitude);
                            LatLng from = new LatLng(Double.parseDouble(SELECTED_PICK_LATITUDE), Double.parseDouble(SELECTED_PICK_LONGITUDE));

                            //Calculating the distance in meters
                            distanceFromDestination = SphericalUtil.computeDistanceBetween(from, myLatLng);

                            if (distanceFromDestination.floatValue() < 200) {
                                openArriveRideNow();
                            } else {
                                hideCompleteHere();

                            }

                            if (runningRideStatus.equalsIgnoreCase("start_riding")) {

                                if (tvTitle.getText().toString().equalsIgnoreCase(getString(R.string.dashboard))) {
                                    ll_main_map_header.setVisibility(View.VISIBLE);
//                                    initMap();
                                    fab_speed_dial.setVisibility(View.VISIBLE);

                                    fab_speed_dial.setMenuListener(new FabSpeedDial.MenuListener() {
                                        @SuppressLint("RestrictedApi")
                                        @Override
                                        public boolean onPrepareMenu(NavigationMenu navigationMenu) {
                                            navigationMenu.findItem(R.id.complete_trip).setVisible(true);
                                            navigationMenu.findItem(R.id.tv_no_show).setVisible(false);
                                            navigationMenu.findItem(R.id.tv_cancel).setVisible(false);
                                            return true;
                                        }

                                        @Override
                                        public boolean onMenuItemSelected(MenuItem menuItem) {
                                            if (slideView.getTextView().getText().toString().contains(getString(R.string.res_complete_ride))) {
                                                completeRide(accept, authorization, distanceFromDestination, request_id);
                                                return true;
                                            }
                                            if (menuItem.getTitle().toString().contains(getString(R.string.res_no_show))) {
                                                noShowDialog();
                                                return true;
                                            }

                                            if (menuItem.getTitle().toString().contains(getString(R.string.res_cancel))) {
                                                cancelDialog();
                                                return true;
                                            }


                                            return true;
                                        }

                                        @Override
                                        public void onMenuClosed() {

                                        }
                                    });
                                } else {
                                    ll_main_map_header.setVisibility(View.GONE);
                                    fab_speed_dial.setVisibility(View.GONE);

                                }


                                return;
                            }

                            if (runningRideStatus.equalsIgnoreCase("accepted")) {

                                if (tripAlert) {

                                } else {
                                    ll_main_map_header.setVisibility(View.GONE);

                                    fab_speed_dial.setMenuListener(new FabSpeedDial.MenuListener() {
                                        @SuppressLint("RestrictedApi")
                                        @Override
                                        public boolean onPrepareMenu(NavigationMenu navigationMenu) {
                                            navigationMenu.findItem(R.id.tv_start_ryde).setVisible(true);
                                            navigationMenu.findItem(R.id.tv_cancel).setVisible(true);


                                            return true;
                                        }

                                        @Override
                                        public boolean onMenuItemSelected(MenuItem menuItem) {
                                            if (menuItem.getTitle().toString().contains(getString(R.string.res_start_ryde))) {
                                                startRyde();
                                                return true;
                                            }

                                            if (menuItem.getTitle().toString().contains(getString(R.string.res_cancel))) {
                                                cancelDialog();
                                                return true;
                                            }

                                            return true;
                                        }

                                        @Override
                                        public void onMenuClosed() {

                                        }
                                    });
                                }

                                Log.d(TAG, "tripAlert : " + tripAlert);
                                /*if (tvTitle.getText().toString().equalsIgnoreCase(getString(R.string.dashboard))) {
                                    ll_main_map_header.setVisibility(View.VISIBLE);
//                                    initMap();
                                    fab_speed_dial.setVisibility(View.VISIBLE);


                                    fab_speed_dial.setMenuListener(new FabSpeedDial.MenuListener() {
                                        @SuppressLint("RestrictedApi")
                                        @Override
                                        public boolean onPrepareMenu(NavigationMenu navigationMenu) {
                                            navigationMenu.findItem(R.id.tv_arrived_menu).setVisible(true);
                                            navigationMenu.findItem(R.id.tv_no_show).setVisible(false);
                                            navigationMenu.findItem(R.id.tv_cancel).setVisible(true);


                                            return true;
                                        }

                                        @Override
                                        public boolean onMenuItemSelected(MenuItem menuItem) {
                                            if (menuItem.getTitle().toString().contains(getString(R.string.res_arrived))) {

                                                myLatLng = new LatLng(latitude, longitude);
                                                LatLng frompickup = new LatLng(Double.parseDouble(SELECTED_PICK_LATITUDE), Double.parseDouble(SELECTED_PICK_LONGITUDE));

                                                distanceFromDestination = SphericalUtil.computeDistanceBetween(frompickup, myLatLng);

                                                Log.d(TAG, "total distance for distanceFromDestination: "+ distanceFromDestination);
                                                Log.d(TAG, "total distance for distanceFromDestination: "+ request_id);

                                                arrivedRide(accept, authorization, request_id);
                                                return true;
                                            }
                                            if (menuItem.getTitle().toString().contains(getString(R.string.res_no_show))) {
                                                noShowDialog();
                                                return true;
                                            }

                                            if (menuItem.getTitle().toString().contains(getString(R.string.res_cancel))) {
                                                cancelDialog();
                                                return true;
                                            }


                                            return true;
                                        }

                                        @Override
                                        public void onMenuClosed() {

                                        }
                                    });
                                } else {
                                    ll_main_map_header.setVisibility(View.GONE);
                                    fab_speed_dial.setVisibility(View.GONE);
                                }*/

                                return;
                            }


                            if (runningRideStatus.equalsIgnoreCase("arrived")) {
                                if (tvTitle.getText().toString().equalsIgnoreCase(getString(R.string.dashboard))) {
                                    ll_main_map_header.setVisibility(View.VISIBLE);
//                                    initMap();

                                    fab_speed_dial.setVisibility(View.VISIBLE);


                                    fab_speed_dial.setMenuListener(new FabSpeedDial.MenuListener() {
                                        @SuppressLint("RestrictedApi")
                                        @Override
                                        public boolean onPrepareMenu(NavigationMenu navigationMenu) {
                                            navigationMenu.findItem(R.id.begin_trip).setVisible(true);
                                            navigationMenu.findItem(R.id.tv_no_show).setVisible(true);
                                            navigationMenu.findItem(R.id.tv_cancel).setVisible(true);
                                            return true;
                                        }

                                        @Override
                                        public boolean onMenuItemSelected(MenuItem menuItem) {
                                            if (menuItem.getTitle().toString().contains(getString(R.string.res_begin_trip))) {
                                                startRide(accept, authorization, request_id);
                                                return true;
                                            }
                                            if (menuItem.getTitle().toString().contains(getString(R.string.res_no_show))) {
                                                noShowDialog();
                                                return true;
                                            }

                                            if (menuItem.getTitle().toString().contains(getString(R.string.res_cancel))) {
                                                cancelDialog();
                                                return true;
                                            }


                                            return true;
                                        }

                                        @Override
                                        public void onMenuClosed() {

                                        }
                                    });
                                } else {
                                    ll_main_map_header.setVisibility(View.GONE);
                                    fab_speed_dial.setVisibility(View.GONE);
                                }

                                return;
                            }

                            if (acceptRide.equalsIgnoreCase("Your ride has been accepted Successfully.")) {

                                return;
                            }

                            if (passmsg.contains("Your ride has been passed Successfully.")) {
                                hideCompleteHere();
                                ll_main_map_header.setVisibility(View.GONE);
                                fab_speed_dial.setVisibility(View.GONE);
//                                return;
                            } else {
                                fab_speed_dial.setVisibility(View.GONE);
                                ll_main_map_header.setVisibility(View.GONE);
                            }

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }

            @Override
            public void onProviderEnabled(String provider) {

            }

            @Override
            public void onProviderDisabled(String provider) {

            }
        };
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;
        root_main.setMinimumWidth(width);


        //on main map initial
        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapNearBy);
        mapFragment.getMapAsync(this);


        latitude = gps.getLatitude();
        longitude = gps.getLongitude();


        ll_main_map_header.setVisibility(View.GONE);
        fab_speed_dial.setVisibility(View.GONE);

        /*mapView = findViewById(R.id.mapView_main);
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(new com.mapbox.mapboxsdk.maps.OnMapReadyCallback() {
            @Override
            public void onMapReady(@NonNull MapboxMap map) {
                mapboxMap = map;
                mapboxMap.setStyle(getString(R.string.navigation_guidance_day), new Style.OnStyleLoaded() {
                    @Override
                    public void onStyleLoaded(@NonNull Style style) {
                        enableLocationComponent(style);

                        addDestinationIconSymbolLayer(style);

                    }
                });
            }
        });*/

        if (tvTitle.getText().toString().equals(getString(R.string.dashboard))) {
            /*if (options == null) {
                return;
            }*/
//            mapView.setVisibility(View.VISIBLE);
//            NavigationLauncher.startNavigation(MainActivity.this, options);
        } else {
//            mapView.setVisibility(View.GONE);
            fab_speed_dial.setVisibility(View.GONE);

        }

    }

    private void startRyde() {

        tripAlert = true;

        LatLng origin = new LatLng(Double.parseDouble("" + SELECTED_PICK_LATITUDE), Double.parseDouble("" + SELECTED_PICK_LONGITUDE));
        LatLng destination = new LatLng(Double.parseDouble("" + SELECTED_DROP_LATITUDE), Double.parseDouble("" + SELECTED_DROP_LONGITUDE));
        MapUtils.setDestinationMarkerForPickPoint(MainActivity.this, mMap, origin, "Customer Location ");
        MapUtils.setDestinationMarkerForDropPoint(MainActivity.this, mMap, destination, "Customer Location");
        try {
            DrawRouteMaps.getInstance(MainActivity.this, MainActivity.this, sessionManager, map_progress, true).draw(origin, destination, mMap);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (tvTitle.getText().toString().equalsIgnoreCase(getString(R.string.dashboard))) {
            ll_main_map_header.setVisibility(View.VISIBLE);
            fab_speed_dial.setVisibility(View.VISIBLE);


            fab_speed_dial.setMenuListener(new FabSpeedDial.MenuListener() {
                @SuppressLint("RestrictedApi")
                @Override
                public boolean onPrepareMenu(NavigationMenu navigationMenu) {
                    navigationMenu.findItem(R.id.tv_arrived_menu).setVisible(true);
                    navigationMenu.findItem(R.id.tv_no_show).setVisible(false);
                    navigationMenu.findItem(R.id.tv_cancel).setVisible(true);


                    return true;
                }

                @Override
                public boolean onMenuItemSelected(MenuItem menuItem) {
                    if (menuItem.getTitle().toString().contains(getString(R.string.res_arrived))) {

                        myLatLng = new LatLng(latitude, longitude);
                        LatLng frompickup = new LatLng(Double.parseDouble(SELECTED_PICK_LATITUDE), Double.parseDouble(SELECTED_PICK_LONGITUDE));

                        distanceFromDestination = SphericalUtil.computeDistanceBetween(frompickup, myLatLng);

                        Log.d(TAG, "total distance for distanceFromDestination: " + distanceFromDestination);
                        Log.d(TAG, "total distance for distanceFromDestination: " + request_id);

                        arrivedRide(accept, authorization, request_id, String.valueOf(distanceFromDestination));
                        return true;
                    }
                    if (menuItem.getTitle().toString().contains(getString(R.string.res_no_show))) {
                        noShowDialog();
                        return true;
                    }

                    if (menuItem.getTitle().toString().contains(getString(R.string.res_cancel))) {
                        cancelDialog();
                        return true;
                    }


                    return true;
                }

                @Override
                public void onMenuClosed() {

                }
            });
        } else {
            ll_main_map_header.setVisibility(View.GONE);
            fab_speed_dial.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_LOCATION_PERMISSION) {
            if (resultCode == RESULT_CANCELED) {
                Snackbar.make(rl_layout, "Please Enable your GSP Location from Setting..!!", Snackbar.LENGTH_LONG).show();

                donotupdate = false;
            }

            if (resultCode == RESULT_OK) {
                Intent i = getApplicationContext().getPackageManager()
                        .getLaunchIntentForPackage(getApplicationContext().getPackageName());

                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
                donotupdate = true;


            }
        }
    }


    private void enableLoc() {
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(MainActivity.this)
                    .addApi(LocationServices.API)

                    .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                        @Override
                        public void onConnected(Bundle bundle) {
                        }

                        @Override
                        public void onConnectionSuspended(int i) {
                            mGoogleApiClient.connect();
                        }
                    })
                    .addOnConnectionFailedListener(new GoogleApiClient.OnConnectionFailedListener() {
                        @Override
                        public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

                            Log.d("Location error", "Location error " + connectionResult.getErrorCode());
                        }
                    }).build();
            mGoogleApiClient.connect();

            LocationRequest locationRequest = LocationRequest.create();
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            locationRequest.setInterval(30 * 1000);
            locationRequest.setFastestInterval(5 * 1000);
            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);

            builder.setAlwaysShow(true);

            Task<LocationSettingsResponse> result =
                    LocationServices.getSettingsClient(this).checkLocationSettings(builder.build());
            result.addOnCompleteListener(new OnCompleteListener<LocationSettingsResponse>() {
                @Override
                public void onComplete(@NonNull Task<LocationSettingsResponse> task) {
                    try {
                        LocationSettingsResponse response = task.getResult(ApiException.class);

                    } catch (ApiException e) {
                        if (e.getStatusCode() == LocationSettingsStatusCodes.RESOLUTION_REQUIRED) {
                            try {
                                ResolvableApiException resolvable = (ResolvableApiException) e;

                                resolvable.startResolutionForResult(MainActivity.this, REQUEST_LOCATION_PERMISSION);
                            } catch (IntentSender.SendIntentException e1) {
                                // Ignore the error.
                            }
                        }
                        e.printStackTrace();
                    }

                }
            });
        }
    }

    private boolean hasGPSDevice(MainActivity mainActivity) {
        final LocationManager mgr = (LocationManager) mainActivity
                .getSystemService(Context.LOCATION_SERVICE);
        if (mgr == null)
            return false;
        final List<String> providers = mgr.getAllProviders();
        if (providers == null)
            return false;
        return providers.contains(LocationManager.GPS_PROVIDER);
    }


    private void registerForCallInvites() {
        RequestBody reqBody = RequestBody.create(new byte[0], null);
        new PostRequest(MainActivity.this, token, URL_GET_AUDIO_TOKEN, reqBody, new PostRequest.Callbacks() {
            @Override
            public void onFail(final String error) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(), error, Toast.LENGTH_LONG).show();
                    }
                });
            }

            @Override
            public void onSuccess(JSONObject response) {
                try {
                    JSONObject data = response.getJSONObject("data");
                    final String TWILIO_ACCESS_TOKEN = data.getString("token");
                    final String identity = data.getString("identity");
                    helper.setTwilioAuth(TWILIO_ACCESS_TOKEN);
                    helper.setIdentity(identity);

                    FirebaseInstanceId.getInstance().getInstanceId()
                            .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                                @Override
                                public void onComplete(@NonNull Task<InstanceIdResult> task) {
                                    if (!task.isSuccessful()) {
                                        return;
                                    }
                                    Voice.register(TWILIO_ACCESS_TOKEN, Voice.RegistrationChannel.FCM, task.getResult().getToken(), new RegistrationListener() {
                                        @Override
                                        public void onRegistered(String s, String s1) {

                                        }

                                        @Override
                                        public void onError(RegistrationException e, String s, String s1) {

                                        }
                                    });
                                }
                            });
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }).execute();
    }


    /*@SuppressWarnings({"MissingPermission"})
    private void enableLocationComponent(@NonNull Style loadedMapStyle) {
// Check if permissions are enabled and if not request
        if (PermissionsManager.areLocationPermissionsGranted(this)) {
// Activate the MapboxMap LocationComponent to show user location
// Adding in LocationComponentOptions is also an optional parameter
            locationComponent = mapboxMap.getLocationComponent();
            locationComponent.activateLocationComponent(this, loadedMapStyle);
            locationComponent.setLocationComponentEnabled(true);
// Set the component's camera mode
            locationComponent.setCameraMode(CameraMode.TRACKING);
        } else {
            permissionsManager = new PermissionsManager(this);
            permissionsManager.requestLocationPermissions(this);
        }
    }


    private void getRoute(Point origin, Point destination) {
        NavigationRoute.builder(this)
                .accessToken(Mapbox.getAccessToken())
                .origin(origin)
                .destination(destination)
                .build()
                .getRoute(new Callback<DirectionsResponse>() {
                    @Override
                    public void onResponse(Call<DirectionsResponse> call, Response<DirectionsResponse> response) {
                        progress.hideDialog();
// You can get the generic HTTP info about the response
                        Log.d(TAG, "Response code: " + response.code());
                        if (response.body() == null) {
                            Log.e(TAG, "No routes found, make sure you set the right user and access token.");
                            return;
                        } else if (response.body().routes().size() < 1) {
                            Log.e(TAG, "No routes found");
                            return;
                        }

                        currentRoute = response.body().routes().get(0);


                        boolean simulateRoute = false;
                        options = NavigationLauncherOptions.builder()
                                .directionsRoute(currentRoute)
                                .shouldSimulateRoute(simulateRoute)
                                .build();
// Call this method with Context from within an Activity

                    }

                    @Override
                    public void onFailure(Call<DirectionsResponse> call, Throwable throwable) {
                        progress.hideDialog();

                        Log.e(TAG, "Error: " + throwable.getMessage());
                    }
                });
    }

    private void addDestinationIconSymbolLayer(@NonNull Style loadedMapStyle) {
        loadedMapStyle.addImage("destination-icon-id",
                BitmapFactory.decodeResource(this.getResources(), R.drawable.mapbox_marker_icon_default));
        GeoJsonSource geoJsonSource = new GeoJsonSource("destination-source-id");
        loadedMapStyle.addSource(geoJsonSource);
        SymbolLayer destinationSymbolLayer = new SymbolLayer("destination-symbol-layer-id", "destination-source-id");
        destinationSymbolLayer.withProperties(
                iconImage("destination-icon-id"),
                iconAllowOverlap(true),
                iconIgnorePlacement(true)
        );
        loadedMapStyle.addLayer(destinationSymbolLayer);
    }*/


    @Override
    protected void onPause() {
        super.onPause();

        Log.d(TAG, "on pause :");
    }

    /*private void initMap() {


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                try {
                    Point destinationPoint = Point.fromLngLat(Double.parseDouble(SELECTED_DROP_LONGITUDE), Double.parseDouble(SELECTED_DROP_LATITUDE));

                    Point originPoint = Point.fromLngLat(gps.getLongitude(), gps.getLatitude());

//                    GeoJsonSource source = mapboxMap.getStyle().getSourceAs("destination-source-id");
//                    if (source != null) {
//                        source.setGeoJson(Feature.fromGeometry(destinationPoint));
//                    }


                    getRoute(originPoint, destinationPoint);

                    mapboxMap.setStyle(getString(R.string.navigation_guidance_day), new Style.OnStyleLoaded() {
                        @Override
                        public void onStyleLoaded(@NonNull Style style) {
                            enableLocationComponent(style);

                            addDestinationIconSymbolLayer(style);


                        }
                    });
                } catch (RuntimeException r) {
                    r.printStackTrace();
                }

            }
        }, 3000);
    }*/


    private void getDashboard(String accept, String authorization) {
        progress.createDialog(true);
        progress.DialogMessage(getString(R.string.please_wait));
        progress.hideDialog();

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        final Call<RespDashboardDriver> respDashboardDriverCall = apiService.DRIVER_DASHBOARD(accept, authorization);
        respDashboardDriverCall.enqueue(new Callback<RespDashboardDriver>() {

            @Override
            public void onResponse(Call<RespDashboardDriver> call, Response<RespDashboardDriver> response) {


                if (response.isSuccessful()) {

                    try {
                        try {
                            progress.hideDialog();

                            ll_main_map_header.setVisibility(View.GONE);
                            fab_speed_dial.setVisibility(View.GONE);
                            final RespDashboardDriver respDashboardDriver = response.body();


                            if (response.body().getMessage().contains("Unauthenticated")) {
                                progress.hideDialog();

//                                openAlertUnauthLogout();

                                Log.d(TAG, "alert :" + alert);
                                return;
                            } else if (response.body().getData().getUserDetails().getDocumentStatus().equalsIgnoreCase("pending")) {
                                Intent intent = new Intent(MainActivity.this, AddAddressActivity.class);
                                intent.putExtra(AppConfig.BUNDLE.Token, helper.LoadStringPref(Prefs.token, ""));
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);

                                return;
                            }


                            userUniqueCode = response.body().getData().getUserDetails().getUserUniqueCode();
                            address = response.body().getData().getUserDetails().getAddress();

                            userStatus = response.body().getData().getUserDetails().getUser_status();
                            Log.d(TAG, "user status :" + userStatus);

                            if (userStatus.equalsIgnoreCase("offline")) {
                                donotupdate = false;
                                ivStatus.setBackground(ContextCompat.getDrawable(MainActivity.this, R.drawable.dor_gray));
                            } else {
                                donotupdate = true;

                                ivStatus.setBackground(ContextCompat.getDrawable(MainActivity.this, R.drawable.dot_green));
                            }


                            firstname = response.body().getData().getUserDetails().getFirstName();
                            lastname = response.body().getData().getUserDetails().getLastName();
                            email = response.body().getData().getUserDetails().getEmail();
                            city = response.body().getData().getUserDetails().getCity();
                            supportnumber = response.body().getData().getUserDetails().getSupport_number();
                            userStatus = response.body().getData().getUserDetails().getUser_status();

                            payment_type = response.body().getData().getRunningRide().getPayment_type();
                            request_id = response.body().getData().getRunningRide().getRequest_id();
                            est_fare = response.body().getData().getRunningRide().getEst_fare();
                            eta = response.body().getData().getRunningRide().getEta();
                            source_location = response.body().getData().getRunningRide().getSource_location();
                            destination_location = response.body().getData().getRunningRide().getDestination_location();
                            passenger_unique_code = response.body().getData().getRunningRide().getPassenger_unique_code();

                            pick_lat = response.body().getData().getRunningRide().getSource_latitude();
                            pick_longg = response.body().getData().getRunningRide().getSource_longitude();
                            drop_lat = response.body().getData().getRunningRide().getDestination_latitude();
                            drop_longg = response.body().getData().getRunningRide().getDestination_longitude();
                            status = response.body().getData().getUserDetails().getStatus();
                            driverImage = AppConfig.URL.BASE_IMAGEURL + response.body().getData().getRunningRide().getDriver_picture();
                            passengerfirstname = response.body().getData().getRunningRide().getPassenger_first_name();
                            displaypassengername = response.body().getData().getRunningRide().getDisplay_passenger_name();
                            documentStatus = response.body().getData().getUserDetails().getDocumentStatus();
                            rideinstruction = response.body().getData().getRunningRide().getRide_instruction();
                            driver_id = response.body().getData().getRunningRide().getDriver_id();
                            user_unique_code = response.body().getData().getUserDetails().getUserUniqueCode();
                            vehicleInspectStatus = response.body().getData().getVehicleInspection();

                            if (vehicleInspectStatus.equalsIgnoreCase("false")) {
                                startActivity(new Intent(MainActivity.this, VehicleInspection.class));
                                return;
                            }


                            if (driverImage.contains("https://")) {

                                Picasso.with(MainActivity.this)
                                        .load(response.body().getData().getRunningRide().getDriver_picture())
                                        .placeholder(R.mipmap.ic_launcher)
                                        .error(R.mipmap.ic_launcher)
                                        .into(profile_pic);

                            } else {


                                Picasso.with(MainActivity.this)
                                        .load(driverImage)
                                        .placeholder(R.mipmap.ic_launcher)
                                        .error(R.mipmap.ic_launcher)
                                        .into(profile_pic);
                            }

                            runningRideStatus = response.body().getData().getRunningRide().getStatus();

                            if (runningRideStatus.contains("trip_completed")
                                    || response.body().getData().getRunningRide().getStatus().contains("trip_completed")) {
                                Log.d(TAG, "driver image : " + response.body().getData().getRunningRide().getDriver_picture());

                                payment_type = response.body().getData().getRunningRide().getPayment_type();
                                is_schedule = response.body().getData().getRunningRide().getIs_schedule();
                                request_id = response.body().getData().getRunningRide().getRequest_id();
                                est_fare = response.body().getData().getRunningRide().getEst_fare();
                                eta = response.body().getData().getRunningRide().getEta();
                                source_location = response.body().getData().getRunningRide().getSource_location();
                                destination_location = response.body().getData().getRunningRide().getDestination_location();
                                passenger_unique_code = response.body().getData().getRunningRide().getPassenger_unique_code();
                                pick_lat = response.body().getData().getRunningRide().getSource_latitude();
                                pick_longg = response.body().getData().getRunningRide().getSource_longitude();
                                drop_lat = response.body().getData().getRunningRide().getDestination_latitude();
                                drop_longg = response.body().getData().getRunningRide().getDestination_longitude();
                                passenger_rating = response.body().getData().getRunningRide().getRating();
                                passengerImage = response.body().getData().getRunningRide().getProfile_picture();

                                rideinstruction = response.body().getData().getRunningRide().getRide_instruction();
                                displaypassengername = response.body().getData().getRunningRide().getDisplay_passenger_name();
                                passengerphonenumber = response.body().getData().getRunningRide().getPassenger_phone_number();

                                if (passengerImage == null) {
                                    startActivity(new Intent(MainActivity.this, SignatureActivity.class)
                                            .putExtra(AppConfig.BUNDLE.est_fare, est_fare)
                                            .putExtra(AppConfig.BUNDLE.eta, eta)
                                            .putExtra(AppConfig.BUNDLE.payment_type, payment_type)
                                            .putExtra(AppConfig.BUNDLE.passenger_first_name, passengerfirstname)
                                            .putExtra(AppConfig.BUNDLE.display_passenger_name, displaypassengername)
                                            .putExtra(AppConfig.BUNDLE.passenger_image, "")
                                            .putExtra(AppConfig.BUNDLE.request_id, request_id));
                                } else if (passengerImage.contains("https://")) {

                                    startActivity(new Intent(MainActivity.this, SignatureActivity.class)
                                            .putExtra(AppConfig.BUNDLE.est_fare, est_fare)
                                            .putExtra(AppConfig.BUNDLE.eta, eta)
                                            .putExtra(AppConfig.BUNDLE.payment_type, payment_type)
                                            .putExtra(AppConfig.BUNDLE.passenger_first_name, passengerfirstname)
                                            .putExtra(AppConfig.BUNDLE.display_passenger_name, displaypassengername)
                                            .putExtra(AppConfig.BUNDLE.passenger_image, passengerImage)
                                            .putExtra(AppConfig.BUNDLE.request_id, request_id));


                                } else {

                                    startActivity(new Intent(MainActivity.this, SignatureActivity.class)
                                            .putExtra(AppConfig.BUNDLE.est_fare, est_fare)
                                            .putExtra(AppConfig.BUNDLE.eta, eta)
                                            .putExtra(AppConfig.BUNDLE.payment_type, payment_type)
                                            .putExtra(AppConfig.BUNDLE.passenger_first_name, passengerfirstname)
                                            .putExtra(AppConfig.BUNDLE.display_passenger_name, displaypassengername)
                                            .putExtra(AppConfig.BUNDLE.passenger_image, passengerImage)
                                            .putExtra(AppConfig.BUNDLE.request_id, request_id));
                                }
                                Log.d(TAG, "passengerImage completed :" + passengerImage);

                                fab_speed_dial.setVisibility(View.GONE);
                                ll_main_map_header.setVisibility(View.GONE);


                            } else if (runningRideStatus.contains("pending")
                                    || response.body().getData().getRunningRide().getStatus().contains("pending")) {


                                payment_type = response.body().getData().getRunningRide().getPayment_type();
                                is_schedule = response.body().getData().getRunningRide().getIs_schedule();
                                schedule_date = response.body().getData().getRunningRide().getSchedule_date();
                                request_id = response.body().getData().getRunningRide().getRequest_id();
                                est_fare = response.body().getData().getRunningRide().getEst_fare();
                                eta = response.body().getData().getRunningRide().getEta();
                                source_location = response.body().getData().getRunningRide().getSource_location();
                                destination_location = response.body().getData().getRunningRide().getDestination_location();
                                passenger_unique_code = response.body().getData().getRunningRide().getPassenger_unique_code();
                                pick_lat = response.body().getData().getRunningRide().getSource_latitude();
                                pick_longg = response.body().getData().getRunningRide().getSource_longitude();
                                drop_lat = response.body().getData().getRunningRide().getDestination_latitude();
                                drop_longg = response.body().getData().getRunningRide().getDestination_longitude();
                                passenger_rating = response.body().getData().getRunningRide().getRating();
                                passengerImage = response.body().getData().getRunningRide().getProfile_picture();

                                rideinstruction = response.body().getData().getRunningRide().getRide_instruction();
                                displaypassengername = response.body().getData().getRunningRide().getDisplay_passenger_name();
                                passengerphonenumber = response.body().getData().getRunningRide().getPassenger_phone_number();


                                startActivity(new Intent(MainActivity.this, RideConfirmDialogActivity.class)
                                        .putExtra(AppConfig.BUNDLE.payment_type, payment_type)
                                        .putExtra(AppConfig.BUNDLE.request_id, request_id)
                                        .putExtra(AppConfig.BUNDLE.est_fare, est_fare)
                                        .putExtra(AppConfig.BUNDLE.eta, eta)
                                        .putExtra(AppConfig.BUNDLE.ride_instruction, rideinstruction)
                                        .putExtra(AppConfig.BUNDLE.schedule_date, schedule_date)
                                        .putExtra(AppConfig.BUNDLE.source_location, source_location)
                                        .putExtra(AppConfig.BUNDLE.destination_location, destination_location)
                                        .putExtra(AppConfig.BUNDLE.passenger_unique_code, passenger_unique_code)
                                        .putExtra(AppConfig.BUNDLE.display_passenger_name, displaypassengername)
                                        .putExtra(AppConfig.BUNDLE.status, status)
                                        .putExtra(AppConfig.BUNDLE.passenger_rating, passenger_rating)
                                        .putExtra(AppConfig.BUNDLE.passenger_image, passengerImage)
                                        .putExtra(Config.IntentKeys.PICK_LATITUDE, "" + pick_lat)
                                        .putExtra(Config.IntentKeys.PICK_LONGITUDE, "" + pick_longg)
                                        .putExtra(Config.IntentKeys.PICK_LOCATION_TXT, "" + pick_location)
                                        .putExtra(Config.IntentKeys.DROP_LATITUDE, "" + drop_lat)
                                        .putExtra(Config.IntentKeys.DROP_LONGITUDE, "" + drop_longg)
                                        .putExtra(Config.IntentKeys.DROP_LOCATIOn_TXT, "" + drop_location));

                            } else if (runningRideStatus.contains("accepted")
                                    || response.body().getData().getRunningRide().getStatus().contains("accepted")) {
                                payment_type = response.body().getData().getRunningRide().getPayment_type();
                                is_schedule = response.body().getData().getRunningRide().getIs_schedule();
                                request_id = response.body().getData().getRunningRide().getRequest_id();
                                est_fare = response.body().getData().getRunningRide().getEst_fare();
                                eta = response.body().getData().getRunningRide().getEta();
                                source_location = response.body().getData().getRunningRide().getSource_location();
                                destination_location = response.body().getData().getRunningRide().getDestination_location();
                                passenger_unique_code = response.body().getData().getRunningRide().getPassenger_unique_code();
                                pick_lat = response.body().getData().getRunningRide().getSource_latitude();
                                pick_longg = response.body().getData().getRunningRide().getSource_longitude();
                                drop_lat = response.body().getData().getRunningRide().getDestination_latitude();
                                drop_longg = response.body().getData().getRunningRide().getDestination_longitude();
                                passenger_rating = response.body().getData().getRunningRide().getRating();
                                passengerImage = response.body().getData().getRunningRide().getProfile_picture();

                                rideinstruction = response.body().getData().getRunningRide().getRide_instruction();
                                displaypassengername = response.body().getData().getRunningRide().getDisplay_passenger_name();
                                passengerphonenumber = response.body().getData().getRunningRide().getPassenger_phone_number();

                                address = response.body().getData().getUserDetails().getAddress();

                                SELECTED_PICK_LATITUDE = pick_lat;
                                SELECTED_PICK_LONGITUDE = pick_longg;
                                SELECTED_DROP_LATITUDE = drop_lat;
                                SELECTED_DROP_LONGITUDE = drop_longg;

                                Log.d(TAG, "rideinstruction accepted :" + rideinstruction);
                                Log.d(TAG, "rideinstruction accepted tripalert:" + tripAlert);

                                /*if (rideinstruction.contains("") || rideinstruction.isEmpty()) {
                                    tv_details_instruction.setText("No Instructions");
                                } else {
                                    tv_details_instruction.setText(rideinstruction);

                                }*/

                                tv_details_instruction.setText(rideinstruction);
                                tvdestinationlocationarrived.setText(source_location);

                                if (displaypassengername.isEmpty())
                                    passengerName.setText("Anonymous");
                                else
                                    passengerName.setText(displaypassengername);

                                behaviorArrived.setState(BottomSheetBehavior.STATE_HALF_EXPANDED);
                                slideView.setText("");
                                slideView.setText(getString(R.string.res_arrived));

                               /* if (tripAlert) {
                                    finish();
                                    startActivity(getIntent());
                                    LatLng origin = new LatLng(Double.parseDouble("" + SELECTED_PICK_LATITUDE), Double.parseDouble("" + SELECTED_PICK_LONGITUDE));
                                    LatLng destination = new LatLng(Double.parseDouble("" + SELECTED_DROP_LATITUDE), Double.parseDouble("" + SELECTED_DROP_LONGITUDE));
                                    MapUtils.setDestinationMarkerForPickPoint(MainActivity.this, mMap, origin, "Customer Location ");
                                    MapUtils.setDestinationMarkerForDropPoint(MainActivity.this, mMap, destination, "Customer Location");
                                    try {
                                        DrawRouteMaps.getInstance(MainActivity.this, MainActivity.this, sessionManager, map_progress, true).draw(origin, destination, mMap);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                                else {
                                    LatLng origin = new LatLng(Double.parseDouble("" + latitude), Double.parseDouble("" + longitude));
                                    MapUtils.setDestinationMarkerForPickPoint(MainActivity.this, mGooglemap, origin, "" + source_location);
                                    mMap.animateCamera(CameraUpdateFactory.newCameraPosition(new CameraPosition.Builder().target(origin).zoom(Config.MapDefaultZoom).build()));

                                }*/


                                fab_speed_dial.setVisibility(View.VISIBLE);
                                ll_main_map_header.setVisibility(View.VISIBLE);
//                                initMap();
                                tvsourcelocationdetails.setText(source_location);


                                fab_speed_dial.setMenuListener(new FabSpeedDial.MenuListener() {
                                    @SuppressLint("RestrictedApi")
                                    @Override
                                    public boolean onPrepareMenu(NavigationMenu navigationMenu) {
                                        navigationMenu.findItem(R.id.tv_arrived_menu).setVisible(true);
                                        navigationMenu.findItem(R.id.tv_no_show).setVisible(false);
                                        navigationMenu.findItem(R.id.tv_cancel).setVisible(true);
                                        return true;
                                    }

                                    @Override
                                    public boolean onMenuItemSelected(MenuItem menuItem) {
                                        if (menuItem.getTitle().toString().contains(getString(R.string.res_arrived))) {
                                            arrivedRide(accept, authorization, request_id, String.valueOf(distanceFromDestination));
                                            return true;
                                        }
                                        if (menuItem.getTitle().toString().contains(getString(R.string.res_no_show))) {
                                            noShowDialog();
                                            return true;
                                        }

                                        if (menuItem.getTitle().toString().contains(getString(R.string.res_cancel))) {
                                            cancelDialog();
                                            return true;
                                        }


                                        return true;
                                    }

                                    @Override
                                    public void onMenuClosed() {

                                    }
                                });

                                if (passengerImage == null) {

                                    Picasso.with(MainActivity.this)
                                            .load(R.mipmap.ic_launcher)
                                            .placeholder(R.mipmap.ic_launcher)
                                            .error(R.mipmap.ic_launcher)
                                            .into(profile_pic);
                                } else if (passengerImage.contains("https://")) {


                                    Picasso.with(MainActivity.this)
                                            .load(passengerImage)
                                            .placeholder(R.mipmap.ic_launcher)
                                            .error(R.mipmap.ic_launcher)
                                            .into(profile_image_header);

                                } else {
                                    passengerImage = "" + response.body().getData().getRunningRide().getProfile_picture();


                                    Picasso.with(MainActivity.this)
                                            .load(passengerImage)
                                            .placeholder(R.mipmap.ic_launcher)
                                            .error(R.mipmap.ic_launcher)
                                            .into(profile_image_header);

                                }

                            } else if (runningRideStatus.contains("arrived")
                                    || response.body().getData().getRunningRide().getStatus().contains("arrived")) {

                                payment_type = response.body().getData().getRunningRide().getPayment_type();
                                is_schedule = response.body().getData().getRunningRide().getIs_schedule();
                                request_id = response.body().getData().getRunningRide().getRequest_id();
                                est_fare = response.body().getData().getRunningRide().getEst_fare();
                                eta = response.body().getData().getRunningRide().getEta();
                                source_location = response.body().getData().getRunningRide().getSource_location();
                                destination_location = response.body().getData().getRunningRide().getDestination_location();
                                passenger_unique_code = response.body().getData().getRunningRide().getPassenger_unique_code();
                                pick_lat = response.body().getData().getRunningRide().getSource_latitude();
                                pick_longg = response.body().getData().getRunningRide().getSource_longitude();
                                drop_lat = response.body().getData().getRunningRide().getDestination_latitude();
                                drop_longg = response.body().getData().getRunningRide().getDestination_longitude();
                                passenger_rating = response.body().getData().getRunningRide().getRating();

                                rideinstruction = response.body().getData().getRunningRide().getRide_instruction();
                                displaypassengername = response.body().getData().getRunningRide().getDisplay_passenger_name();
                                passengerImage = response.body().getData().getRunningRide().getProfile_picture();
                                runningRideStatus = response.body().getData().getRunningRide().getStatus();
                                passengerphonenumber = response.body().getData().getRunningRide().getPassenger_phone_number();

                                SELECTED_PICK_LATITUDE = pick_lat;
                                SELECTED_PICK_LONGITUDE = pick_longg;
                                SELECTED_DROP_LATITUDE = drop_lat;
                                SELECTED_DROP_LONGITUDE = drop_longg;

                                passengerName.setText(displaypassengername);

                                if (passengerImage == null) {

                                    Picasso.with(MainActivity.this)
                                            .load(R.mipmap.ic_launcher)
                                            .placeholder(R.mipmap.ic_launcher)
                                            .error(R.mipmap.ic_launcher)
                                            .into(profile_image_header);

                                } else if (passengerImage.contains("https://")) {

                                    Picasso.with(MainActivity.this)
                                            .load(R.mipmap.ic_launcher)
                                            .placeholder(R.mipmap.ic_launcher)
                                            .error(R.mipmap.ic_launcher)
                                            .into(profile_image_header);
                                } else {
                                    passengerImage = "" + response.body().getData().getRunningRide().getProfile_picture();


                                    Picasso.with(MainActivity.this)
                                            .load(passengerImage)
                                            .placeholder(R.mipmap.ic_launcher)
                                            .error(R.mipmap.ic_launcher)
                                            .into(profile_image_header);

                                }


                                Log.d(TAG, "rideinstruction arrived :" + rideinstruction);

                                behaviorArrived.setState(BottomSheetBehavior.STATE_HALF_EXPANDED);
                                slideView.setText("");
                                slideView.setText(getString(R.string.res_begin_trip));

                                fab_speed_dial.setVisibility(View.VISIBLE);
                                ll_main_map_header.setVisibility(View.VISIBLE);
//                                initMap();

                                tvsourcelocationdetails.setText(source_location);
                                tv_details_instruction.setText(rideinstruction);

                                LatLng origin = new LatLng(Double.parseDouble("" + SELECTED_PICK_LATITUDE), Double.parseDouble("" + SELECTED_PICK_LONGITUDE));
                                LatLng destination = new LatLng(Double.parseDouble("" + SELECTED_DROP_LATITUDE), Double.parseDouble("" + SELECTED_DROP_LONGITUDE));
                                MapUtils.setDestinationMarkerForPickPoint(MainActivity.this, mMap, origin, "Customer Location ");
                                MapUtils.setDestinationMarkerForDropPoint(MainActivity.this, mMap, destination, "Customer Location");
                                try {
                                    DrawRouteMaps.getInstance(MainActivity.this, MainActivity.this, sessionManager, map_progress, true).draw(origin, destination, mMap);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }


                                fab_speed_dial.setMenuListener(new FabSpeedDial.MenuListener() {
                                    @SuppressLint("RestrictedApi")
                                    @Override
                                    public boolean onPrepareMenu(NavigationMenu navigationMenu) {
                                        navigationMenu.findItem(R.id.begin_trip).setVisible(true);
                                        navigationMenu.findItem(R.id.tv_no_show).setVisible(true);
                                        navigationMenu.findItem(R.id.tv_cancel).setVisible(true);
                                        return true;
                                    }

                                    @Override
                                    public boolean onMenuItemSelected(MenuItem menuItem) {
                                        if (menuItem.getTitle().toString().contains(getString(R.string.res_begin_trip))) {
                                            startRide(accept, authorization, request_id);
                                            return true;
                                        }
                                        if (menuItem.getTitle().toString().contains(getString(R.string.res_no_show))) {
                                            noShowDialog();
                                            return true;
                                        }

                                        if (menuItem.getTitle().toString().contains(getString(R.string.res_cancel))) {
                                            cancelDialog();
                                            return true;
                                        }

                                        return true;
                                    }

                                    @Override
                                    public void onMenuClosed() {

                                    }
                                });

                            } else if (runningRideStatus.contains("start_riding")
                                    || response.body().getData().getRunningRide().getStatus().contains("start_riding")) {
//                    behaviorArrived.setState(BottomSheetBehavior.STATE_HALF_EXPANDED);

                                payment_type = response.body().getData().getRunningRide().getPayment_type();
                                is_schedule = response.body().getData().getRunningRide().getIs_schedule();
                                request_id = response.body().getData().getRunningRide().getRequest_id();
                                est_fare = response.body().getData().getRunningRide().getEst_fare();
                                eta = response.body().getData().getRunningRide().getEta();
                                source_location = response.body().getData().getRunningRide().getSource_location();
                                destination_location = response.body().getData().getRunningRide().getDestination_location();
                                passenger_unique_code = response.body().getData().getRunningRide().getPassenger_unique_code();
                                pick_lat = response.body().getData().getRunningRide().getSource_latitude();
                                pick_longg = response.body().getData().getRunningRide().getSource_longitude();
                                drop_lat = response.body().getData().getRunningRide().getDestination_latitude();
                                drop_longg = response.body().getData().getRunningRide().getDestination_longitude();
                                passenger_rating = response.body().getData().getRunningRide().getRating();
                                passengerImage = response.body().getData().getRunningRide().getProfile_picture();

                                rideinstruction = response.body().getData().getRunningRide().getRide_instruction();
                                displaypassengername = response.body().getData().getRunningRide().getDisplay_passenger_name();
                                passengerphonenumber = response.body().getData().getRunningRide().getPassenger_phone_number();


                                slideView.setText("");
                                slideView.setText(getString(R.string.res_complete_ride));

                                SELECTED_PICK_LATITUDE = pick_lat;
                                SELECTED_PICK_LONGITUDE = pick_longg;
                                SELECTED_DROP_LATITUDE = drop_lat;
                                SELECTED_DROP_LONGITUDE = drop_longg;

                                tvsourcelocationdetails.setText(destination_location);
                                ll_main_map_header.setVisibility(View.VISIBLE);
                                fab_speed_dial.setVisibility(View.VISIBLE);

                                passengerName.setText(displaypassengername);

                                tv_details_instruction.setText(rideinstruction);

                                LatLng origin = new LatLng(Double.parseDouble("" + SELECTED_PICK_LATITUDE), Double.parseDouble("" + SELECTED_PICK_LONGITUDE));
                                LatLng destination = new LatLng(Double.parseDouble("" + SELECTED_DROP_LATITUDE), Double.parseDouble("" + SELECTED_DROP_LONGITUDE));
                                MapUtils.setDestinationMarkerForPickPoint(MainActivity.this, mMap, origin, "Customer Location ");
                                MapUtils.setDestinationMarkerForDropPoint(MainActivity.this, mMap, destination, "Customer Location");
                                try {
                                    DrawRouteMaps.getInstance(MainActivity.this, MainActivity.this, sessionManager, map_progress, true).draw(origin, destination, mMap);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                                myLatLng = new LatLng(latitude, longitude);
                                LatLng from = new LatLng(Double.parseDouble(SELECTED_DROP_LATITUDE), Double.parseDouble(SELECTED_DROP_LONGITUDE));

                                distanceFromDestination = SphericalUtil.computeDistanceBetween(from, myLatLng);

                                Log.d(TAG, "rideinstruction arrived :" + rideinstruction);


                                fab_speed_dial.setVisibility(View.VISIBLE);
                                ll_main_map_header.setVisibility(View.VISIBLE);
//                                initMap();


                                if (passengerImage == null) {

                                    Picasso.with(MainActivity.this)
                                            .load(R.mipmap.ic_launcher)
                                            .placeholder(R.mipmap.ic_launcher)
                                            .error(R.mipmap.ic_launcher)
                                            .into(profile_image_header);

                                } else if (passengerImage.contains("https://")) {

                                    Picasso.with(MainActivity.this)
                                            .load(passengerImage)
                                            .placeholder(R.mipmap.ic_launcher)
                                            .error(R.mipmap.ic_launcher)
                                            .into(profile_image_header);
                                } else {
                                    passengerImage = "" + response.body().getData().getRunningRide().getProfile_picture();

                                    Picasso.with(MainActivity.this)
                                            .load(passengerImage)
                                            .placeholder(R.mipmap.ic_launcher)
                                            .error(R.mipmap.ic_launcher)
                                            .into(profile_image_header);
                                }

                                fab_speed_dial.setMenuListener(new FabSpeedDial.MenuListener() {
                                    @SuppressLint("RestrictedApi")
                                    @Override
                                    public boolean onPrepareMenu(NavigationMenu navigationMenu) {
                                        navigationMenu.findItem(R.id.complete_trip).setVisible(true);
                                        navigationMenu.findItem(R.id.tv_no_show).setVisible(false);
                                        navigationMenu.findItem(R.id.tv_cancel).setVisible(false);
                                        return true;
                                    }

                                    @Override
                                    public boolean onMenuItemSelected(MenuItem menuItem) {
                                        if (slideView.getTextView().getText().toString().contains(getString(R.string.res_complete_ride))) {
                                            completeRide(accept, authorization, distanceFromDestination, request_id);
                                            return true;
                                        }
                                        if (menuItem.getTitle().toString().contains(getString(R.string.res_no_show))) {
                                            noShowDialog();
                                            return true;
                                        }

                                        if (menuItem.getTitle().toString().contains(getString(R.string.res_cancel))) {
                                            cancelDialog();
                                            return true;
                                        }


                                        return true;
                                    }

                                    @Override
                                    public void onMenuClosed() {

                                    }
                                });

                            } else {
                                recreate();
                                hideCompleteHere();
//                    behaviorArrived.setState(BottomSheetBehavior.STATE_HALF_EXPANDED);
                            }


                            if (TextUtils.isEmpty(firstname)) {
                                tv_name.setText(firstname + " " + lastname);
                                tv_email.setText(email);
                                tv_driver_name.setText(firstname + " " + lastname);
                                driver_city_txt.setText(city);

                            } else {
                                tv_name.setText(response.body().getData().getUserDetails().getFirstName() + " " + response.body().getData().getUserDetails().getLastName());
                                tv_email.setText(response.body().getData().getUserDetails().getEmail());
                                tv_driver_name.setText(response.body().getData().getUserDetails().getFirstName() + " " + response.body().getData().getUserDetails().getLastName());
                                driver_city_txt.setText(response.body().getData().getUserDetails().getCity());

                                if (response.body().getData().getUserDetails().getProfilePicture() == null ||
                                        response.body().getData().getUserDetails().getProfilePicture().isEmpty()) {
                                    profile_pic.setImageResource(R.drawable.dummy_pic);
                                } else {

                                    myProfile = response.body().getData().getUserDetails().getProfilePicture();
                                    if (myProfile == null) {


                                        Picasso.with(MainActivity.this)
                                                .load(R.mipmap.ic_launcher)
                                                .placeholder(R.mipmap.ic_launcher)
                                                .error(R.mipmap.ic_launcher)
                                                .into(profile_pic);
                                    } else if (myProfile.contains("https://")) {

                                        Picasso.with(MainActivity.this)
                                                .load(myProfile)
                                                .placeholder(R.mipmap.ic_launcher)
                                                .error(R.mipmap.ic_launcher)
                                                .into(profile_pic);
//                                    Log.d(TAG, "profile pic activity :" + myProfile);

                                    } else {
                                        myProfile = "" + response.body().getData().getUserDetails().getProfilePicture();


                                        Picasso.with(MainActivity.this)
                                                .load(myProfile)
                                                .placeholder(R.mipmap.ic_launcher)
                                                .error(R.mipmap.ic_launcher)
                                                .into(profile_pic);
                                    }

                                }

                            }

                            /*if (rideinstruction.contains("") || rideinstruction.isEmpty()) {
                                tv_details_instruction.setText("No Instructions");
                            } else {
                                tv_details_instruction.setText(rideinstruction);

                            }*/

                            fab_speed_dial.setVisibility(View.GONE);
                            ll_main_map_header.setVisibility(View.GONE);
                        } catch (IllegalStateException e) {
                            e.printStackTrace();
                        }
                    } catch (NullPointerException e) {

                    }

                } else if (response.code() == 500) {
//                    openAlertUnauthLogout();
                } else {
                    Snackbar.make(rl_layout, "Something Went Wrong..!!", Snackbar.LENGTH_SHORT).show();
//                    openAlertUnauthLogout();
                }
            }

            @Override
            public void onFailure(Call<RespDashboardDriver> call, Throwable t) {
                progress.hideDialog();

//                Log.e(TAG, "get dashboard :" + t.getLocalizedMessage());

                getForceDashboard(accept, authorization);

            }
        });
    }

    private void getForceDashboard(String accept, String authorization) {

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        final Call<RespForceDashboard> respForceDashboardCall = apiService.DRIVER_DASHBOARD1(accept, authorization);

        respForceDashboardCall.enqueue(new Callback<RespForceDashboard>() {
            @Override
            public void onResponse(Call<RespForceDashboard> call, Response<RespForceDashboard> response) {
                try {
                    if (response.isSuccessful()) {

                        userStatus = response.body().getData().getUserDetails().getUserStatus();
                        vehicleInspectStatus = response.body().getData().getVehicleInspection();


                        if (vehicleInspectStatus.equalsIgnoreCase("false")) {
                            startActivity(new Intent(MainActivity.this, VehicleInspection.class));
                            return;
                        }


                        if (userStatus.equalsIgnoreCase("offline")) {

                            donotupdate = false;

                            ivStatus.setBackground(ContextCompat.getDrawable(MainActivity.this, R.drawable.dor_gray));
                        } else {

                            donotupdate = true;
                            ivStatus.setBackground(ContextCompat.getDrawable(MainActivity.this, R.drawable.dot_green));
                        }


                        if (TextUtils.isEmpty(firstname)) {
                            tv_name.setText(firstname + " " + lastname);
                            tv_email.setText(email);
                            tv_driver_name.setText(firstname + " " + lastname);
                            driver_city_txt.setText(city);

                        } else {
                            tv_name.setText(response.body().getData().getUserDetails().getFirstName() + " " + response.body().getData().getUserDetails().getLastName());
                            tv_email.setText(response.body().getData().getUserDetails().getEmail());
                            tv_driver_name.setText(response.body().getData().getUserDetails().getFirstName() + " " + response.body().getData().getUserDetails().getLastName());
                            driver_city_txt.setText(response.body().getData().getUserDetails().getCity());

                            if (response.body().getData().getUserDetails().getProfilePicture() == null ||
                                    response.body().getData().getUserDetails().getProfilePicture().isEmpty()) {
                                profile_pic.setImageResource(R.drawable.dummy_pic);
                            } else {

                                myProfile = response.body().getData().getUserDetails().getProfilePicture();
                                if (myProfile == null) {

                                    Picasso.with(MainActivity.this)
                                            .load(R.mipmap.ic_launcher)
                                            .placeholder(R.mipmap.ic_launcher)
                                            .error(R.mipmap.ic_launcher)
                                            .into(profile_pic);
                                } else if (myProfile.contains("https://")) {

//                                    Log.d(TAG, "profile pic activity :" + myProfile);
                                    Picasso.with(MainActivity.this)
                                            .load(myProfile)
                                            .placeholder(R.mipmap.ic_launcher)
                                            .error(R.mipmap.ic_launcher)
                                            .into(profile_pic);
                                } else {
                                    myProfile = "" + response.body().getData().getUserDetails().getProfilePicture();

                                    Picasso.with(MainActivity.this)
                                            .load(myProfile)
                                            .placeholder(R.mipmap.ic_launcher)
                                            .error(R.mipmap.ic_launcher)
                                            .into(profile_pic);
                                }

                            }
                        }

                    } else if (response.body().getMessage().contains("Unauthenticated")) {
                        progress.hideDialog();
//                        openAlertUnauthLogout();
                        Log.d(TAG, "alert :" + alert);
                        return;
                    } else {
                        Snackbar.make(rl_layout, "Something Went Wrong..!!", Snackbar.LENGTH_SHORT).show();
//                        openAlertUnauthLogout();
                    }
                } catch (NullPointerException e) {

                }


            }

            @Override
            public void onFailure(Call<RespForceDashboard> call, Throwable t) {
                Log.d(TAG, "force dashboard :" + t.getLocalizedMessage());
                Log.d(TAG, "force dashboard :" + t.getMessage());
            }
        });
    }


    //temporary location change
    private void updateInOurDb(String accept, String authorization, double latitude, double longitude) {

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        RequestBody latitude1 = RequestBody.create(MediaType.parse("multipart/form-data"), String.valueOf(latitude));
        RequestBody longitude1 = RequestBody.create(MediaType.parse("multipart/form-data"), String.valueOf(longitude));

        final Call<RespUpdateMyLocation> respUpdateMyLocationCall = apiService.UPDATE_MY_LOCATION(accept,
                authorization, latitude1, longitude1);
        respUpdateMyLocationCall.enqueue(new Callback<RespUpdateMyLocation>() {
            @Override
            public void onResponse(Call<RespUpdateMyLocation> call, Response<RespUpdateMyLocation> response) {
                try {

                    if (response.isSuccessful()) {
//                        Log.d(TAG, "update in our db latitude :" + latitude);
//                        Log.d(TAG, "update in our db longitude :" + longitude);
                    } else if (response.code() == 500) {
//                        openAlertUnauthLogout();
                    } else {
                        Snackbar.make(rl_layout, "Something Went Wrong..!!", Snackbar.LENGTH_SHORT).show();
//                        openAlertUnauthLogout();
                    }

                } catch (IllegalArgumentException e) {


                }
            }

            @Override
            public void onFailure(Call<RespUpdateMyLocation> call, Throwable t) {
                Log.d(TAG, "update in my db error :" + t.getMessage());
                Log.d(TAG, "update in my db error :" + t.getLocalizedMessage());
            }
        });
    }


    @Override
    public boolean onTouch(View view, MotionEvent event) {
        switch (event.getActionMasked()) {
            case MotionEvent.ACTION_DOWN:
                dX = view.getX() - event.getRawX();
                dY = view.getY() - event.getRawY();
                lastAction = MotionEvent.ACTION_DOWN;
                break;

            case MotionEvent.ACTION_MOVE:
                view.setY(event.getRawY() + dY);
                view.setX(event.getRawX() + dX);
                lastAction = MotionEvent.ACTION_MOVE;
                break;

            case MotionEvent.ACTION_UP:
                if (lastAction == MotionEvent.ACTION_DOWN)
                    break;

            default:
                return false;
        }
        return true;
    }


    private void openArriveRideNow() {
        behaviorArrived.setState(BottomSheetBehavior.STATE_HALF_EXPANDED);
    }

    private void closeRideNow() {
        behaviorArrived.setState(BottomSheetBehavior.STATE_COLLAPSED);
    }

    private List<LatLng> decodePoly(String encoded) {
        List<LatLng> poly = new ArrayList<LatLng>();
        int index = 0, len = encoded.length();
        int lat = 0, lng = 0;

        while (index < len) {
            int b, shift = 0, result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lat += dlat;

            shift = 0;
            result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lng += dlng;

            LatLng p = new LatLng((((double) lat / 1E5)),
                    (((double) lng / 1E5)));
            poly.add(p);
        }

        return poly;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case MY_PERMISSION_FINE_LOCATION:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                        mMap.setMyLocationEnabled(true);
                    }

                } else {
                    Toast.makeText(getApplicationContext(), "This app requires location permissions to be granted", Toast.LENGTH_LONG).show();
                    finish();
                }
                break;
        }
    }


    private void updateMyDb() {

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {

            @Override
            public void run() {
                //call function
                try {
                    latitude = gps.getLatitude();
                    longitude = gps.getLongitude();

                    updateInOurDb(accept, authorization, latitude, longitude);
//
                    handler.postDelayed(this, 30000);

//                    Snackbar.make(rl_layout, ""+ donotupdate, Snackbar.LENGTH_LONG).show();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }, 30000);
    }


    private void updateLocationRealtime() {

        /*final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {

            @Override
            public void run() {
//call function


            }
        }, 1000);*/

        String valueLattitude = myLat;
        String valueLongitude = myLang;

        if (TextUtils.isEmpty(myUserID) || myUserID.contains("") || myUserID.equals("")
                || myUserID.equalsIgnoreCase("") || myUserID.isEmpty()) {
            createUser(valueLattitude, valueLongitude);

//            Log.d(TAG, "create in real time db :" + valueLattitude);
//            Log.d(TAG, "create in real time db :" + valueLongitude);
        } else {
            updateUser(valueLattitude, valueLongitude);
//            handler.postDelayed(this, 1000);
//            Log.d(TAG, "update in real time db :" + valueLattitude);
//            Log.d(TAG, "update in real time db :" + valueLongitude);
        }

//        Snackbar.make(rl_layout, ""+ donotupdate, Snackbar.LENGTH_LONG).show();

    }

    private void updateUser(String lattitude, String longitude) {

//        Log.d(TAG, "my updated lat :" + lattitude);
//        Log.d(TAG, "my updated lang :" + longitude);
        // updating the user via child nodes
        if (!TextUtils.isEmpty(lattitude))
            mFirebaseDatabase.child(myUserID).child("lattitude").setValue(lattitude);

        if (!TextUtils.isEmpty(longitude))
            mFirebaseDatabase.child(myUserID).child("longitude").setValue(longitude);
    }


    private void cancel(String accept, String authorization, String request_id) {
        progress.createDialog(false);
        progress.DialogMessage(getString(R.string.please_wait));
        progress.showDialog();

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        RequestBody request_id1 = RequestBody.create(MediaType.parse("multipart/form-data"), request_id);
        final Call<RespCancelDriver> respCancelDriverCall = apiService.CANCEL_BY_DRIVER(accept, authorization, request_id1);
        respCancelDriverCall.enqueue(new Callback<RespCancelDriver>() {
            @Override
            public void onResponse(Call<RespCancelDriver> call, Response<RespCancelDriver> response) {
                progress.hideDialog();

                if (response.isSuccessful()) {

                    if (response.body().getMessage().equalsIgnoreCase("Your ride has been cancelled.")) {
//                        recreate();
                        finish();
                        startActivity(getIntent());
                    } else {
                        Snackbar.make(rl_layout, "" + response.body().getMessage(), Snackbar.LENGTH_SHORT).show();
                    }
                } else {
                    Snackbar.make(rl_layout, "" + response.body().getMessage(), Snackbar.LENGTH_SHORT).show();

                }

            }

            @Override
            public void onFailure(Call<RespCancelDriver> call, Throwable t) {

            }
        });
    }


    private void noShow(String accept, String authorization, String request_id) {
        progress.createDialog(false);
        progress.DialogMessage(getString(R.string.please_wait));
        progress.showDialog();

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        RequestBody request_id1 = RequestBody.create(MediaType.parse("multipart/form-data"), request_id);
        final Call<RespNoShow> respNoShowCall = apiService.NO_SHOW_CALL(accept, authorization, request_id1);
        respNoShowCall.enqueue(new Callback<RespNoShow>() {
            @Override
            public void onResponse(Call<RespNoShow> call, Response<RespNoShow> response) {
                progress.hideDialog();

                if (response.isSuccessful()) {

                    if (response.body().getMessage().equalsIgnoreCase("Your ride has been completed.")) {

                        finish();
                        startActivity(getIntent());
                    } else {
                        Snackbar.make(rl_layout, "You need to Arrived first for No Show..!!", Snackbar.LENGTH_SHORT).show();
                    }
                } else {
                    Snackbar.make(rl_layout, "" + response.body().getMessage(), Snackbar.LENGTH_SHORT).show();

                }

            }

            @Override
            public void onFailure(Call<RespNoShow> call, Throwable t) {

            }
        });
    }

    private void arrivedRide(String accept, String authorization, String request_id, String distanceFromDestination) {

        progress.createDialog(false);
        progress.DialogMessage(getString(R.string.please_wait));
        progress.showDialog();

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        RequestBody request_id1 = RequestBody.create(MediaType.parse("multipart/form-data"), request_id);
        RequestBody distanceFromDestination1 = RequestBody.create(MediaType.parse("multipart/form-data"), distanceFromDestination);

        final Call<RespArrived> respPassRequestCall = apiService.ARRIVED_CALL(accept, authorization,
                request_id1, distanceFromDestination1);
        respPassRequestCall.enqueue(new Callback<RespArrived>() {
            @Override
            public void onResponse(Call<RespArrived> call, Response<RespArrived> response) {
                progress.hideDialog();
//                Snackbar.make(rl_layout, "" + response.body().getMessage(), Snackbar.LENGTH_SHORT).show();

                if (response.body().getMessage().contains("Driver has been arrived Successfully.")) {
                    slideView.setText("");
                    slideView.setText(getString(R.string.res_begin_trip));

                    rideinstruction = response.body().getData().getRideInstruction();
                    distance_type = response.body().getData().getDistanceType();
                    payment_type = response.body().getData().getPaymentType();
                    displaypassengername = response.body().getData().getDisplayPassengerName();

//                    recreate();
                    finish();
                    startActivity(getIntent());
                } else {
//                    slideView.setText("");
//                    slideView.setText(getString(R.string.res_begin_trip));
                }
                if (response.body().getMessage().contains("Unauthenticated")) {
//                    openAlertUnauthLogout();
                    return;
                }

                openArriveRideNow();
            }

            @Override
            public void onFailure(Call<RespArrived> call, Throwable t) {
                progress.hideDialog();
                Toast.makeText(MainActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void startRide(String accept, String authorization, String request_id) {
        progress.createDialog(false);
        progress.DialogMessage(getString(R.string.please_wait));
        progress.showDialog();

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        RequestBody request_id1 = RequestBody.create(MediaType.parse("multipart/form-data"), request_id);

        final Call<RespStartRide> respPassRequestCall = apiService.START_RIDE_CALL(accept, authorization, request_id1);
        respPassRequestCall.enqueue(new Callback<RespStartRide>() {
            @Override
            public void onResponse(Call<RespStartRide> call, Response<RespStartRide> response) {
                progress.hideDialog();

//                Snackbar.make(rl_layout, "" + response.body().getMessage(), Snackbar.LENGTH_SHORT).show();

                if (response.body().getMessage().contains("Your ride has been started successfully.")) {
                    slideView.setText("");
                    slideView.setText(getString(R.string.res_complete_ride));

                    hideCompleteHere();

                    rideinstruction = response.body().getData().getRideInstruction();
                    distance_type = response.body().getData().getDistanceType();
                    payment_type = response.body().getData().getPaymentType();
                    displaypassengername = response.body().getData().getDisplayPassengerName();

//                    recreate();
                    finish();
                    startActivity(getIntent());
                } else {
//                    slideView.setText("");
//                    slideView.setText(getString(R.string.res_complete_ride));
                }

                if (response.body().getMessage().contains("Unauthenticated")) {
//                    openAlertUnauthLogout();
                    return;
                }


            }

            @Override
            public void onFailure(Call<RespStartRide> call, Throwable t) {
                progress.hideDialog();
                Toast.makeText(MainActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }


    /*private void hideArriveHere() {
        behaviorArrived.setState(BottomSheetBehavior.STATE_COLLAPSED);

        ll_main_map_header.setVisibility(View.GONE);
        fab_speed_dial.setVisibility(View.GONE);
    }*/


    private void hideCompleteHere() {
        behaviorArrived.setState(BottomSheetBehavior.STATE_COLLAPSED);

        fab_speed_dial.setVisibility(View.VISIBLE);
    }

    private void completeRide(String accept, String authorization, double distance, String request_id) {
        progress.createDialog(false);
        progress.DialogMessage(getString(R.string.please_wait));
        progress.showDialog();

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        RequestBody request_id1 = RequestBody.create(MediaType.parse("multipart/form-data"), request_id);
        RequestBody distance1 = RequestBody.create(MediaType.parse("multipart/form-data"), String.valueOf(distance));

        final Call<RespCompleteRide> respPassRequestCall = apiService.COMPLETE_CALL(accept,
                authorization, distance1, request_id1);
        respPassRequestCall.enqueue(new Callback<RespCompleteRide>() {
            @Override
            public void onResponse(Call<RespCompleteRide> call, Response<RespCompleteRide> response) {

                Log.d(TAG, "response complete ride : " + new Gson().toJson(response.body()));

                progress.hideDialog();

                actualcost = response.body().getData().getActualCost();
                actualdiscount = response.body().getData().getActualDiscount();
                basefare = response.body().getData().getBaseFare();
                extraDistancecost = response.body().getData().getExtraDistanceCost();

                est_fare = response.body().getData().getTotalCost();
                eta = response.body().getData().getTotalTime();
                payment_type = response.body().getData().getPaymentType();
                displaypassengername = response.body().getData().getDisplayPassengerName();
                distance_type = response.body().getData().getDistanceType();
                rideinstruction = response.body().getData().getRide_instruction();

                myLatLng = new LatLng(Double.parseDouble(myLat), Double.parseDouble(myLang));
                LatLng from = new LatLng(Double.parseDouble(SELECTED_DROP_LATITUDE), Double.parseDouble(SELECTED_DROP_LONGITUDE));

                //Calculating the distance in meters
                distanceFromDestination = SphericalUtil.computeDistanceBetween(from, myLatLng);


                if (distanceFromDestination.floatValue() < 200) {
                    openArriveRideNow();
                }

                if (response.body().getMessage().contains("Your ride has been completed.")) {
                    slideView.setText("");
                    slideView.setText(getString(R.string.res_complete_ride));

                    startActivity(new Intent(MainActivity.this, SignatureActivity.class)
                            .putExtra(AppConfig.BUNDLE.actual_cost, actualcost)
                            .putExtra(AppConfig.BUNDLE.actual_discount, actualdiscount)
                            .putExtra(AppConfig.BUNDLE.base_fare, basefare)
                            .putExtra(AppConfig.BUNDLE.extra_distance_cost, extraDistancecost)
                            .putExtra(AppConfig.BUNDLE.est_fare, est_fare)
                            .putExtra(AppConfig.BUNDLE.eta, eta)
                            .putExtra(AppConfig.BUNDLE.passenger_image, passengerImage)
                            .putExtra(AppConfig.BUNDLE.passenger_first_name, passengerfirstname)
                            .putExtra(AppConfig.BUNDLE.display_passenger_name, displaypassengername)
                            .putExtra(AppConfig.BUNDLE.payment_type, payment_type)
                            .putExtra(AppConfig.BUNDLE.request_id, request_id));
                } else {
                    slideView.setText(getString(R.string.res_complete_ride));

                }


                if (response.body().getMessage().contains("Unauthenticated")) {
//                    openAlertUnauthLogout();
                    return;
                } else {
                }

            }

            @Override
            public void onFailure(Call<RespCompleteRide> call, Throwable t) {
                progress.hideDialog();

                Log.d(TAG, "complete ride : " + t.getLocalizedMessage());
                Toast.makeText(MainActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }


    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Get extra data included in the Intent
            String message = intent.getStringExtra("message");

            Log.d(TAG, "broadcast  :" + message);
            if (message.contains("message")) {


                finish();
                startActivity(getIntent());
//                onRestart();
//                onResume();


            } else {
                request_id = intent.getStringExtra(AppConfig.BUNDLE.request_id);
                String payment_type = intent.getStringExtra(AppConfig.BUNDLE.payment_type);
                String user_id = intent.getStringExtra(AppConfig.BUNDLE.user_id);
                request_id = intent.getStringExtra(AppConfig.BUNDLE.request_id);
                body = intent.getStringExtra(AppConfig.EXTRA.body);
                maintitle = intent.getStringExtra(AppConfig.EXTRA.main_title);

                source_latitude = intent.getStringExtra(AppConfig.BUNDLE.source_latitude);
                source_longitude = intent.getStringExtra(AppConfig.BUNDLE.source_latitude);
                destination_latitude = intent.getStringExtra(AppConfig.BUNDLE.destination_latitude);
                destination_longitude = intent.getStringExtra(AppConfig.BUNDLE.destination_longitude);

                String source_location = intent.getStringExtra(AppConfig.BUNDLE.source_location);
                destination_location = intent.getStringExtra(AppConfig.BUNDLE.destination_location);
                is_schedule = intent.getStringExtra(AppConfig.BUNDLE.is_schedule);
                schedule_date = intent.getStringExtra(AppConfig.BUNDLE.schedule_date);
                passenger_first_name = intent.getStringExtra(AppConfig.BUNDLE.passenger_first_name);
                passenger_rating = intent.getStringExtra(AppConfig.BUNDLE.passenger_rating);
                status = intent.getStringExtra(AppConfig.BUNDLE.status);
                passenger_unique_code = intent.getStringExtra(AppConfig.BUNDLE.passenger_unique_code);
                displaypassengername = intent.getStringExtra(AppConfig.BUNDLE.display_passenger_name);
                rideinstruction = intent.getStringExtra(AppConfig.BUNDLE.ride_instruction);
                driver_id = intent.getStringExtra(AppConfig.BUNDLE.driver_id);

                String user_image = intent.getStringExtra(AppConfig.BUNDLE.user_image);
                String eta = intent.getStringExtra(AppConfig.BUNDLE.eta);
                String est_fare = intent.getStringExtra(AppConfig.BUNDLE.est_fare);

                tvopenbottomsheet.setVisibility(View.VISIBLE);
//                imgFcm = "http://159.203.105.17" + user_image;
                imgFcm = AppConfig.URL.BASE_URL_IMG + user_image;

                Picasso.with(MainActivity.this)
                        .load(imgFcm)
                        .placeholder(R.mipmap.ic_launcher)
                        .error(R.mipmap.ic_launcher)
                        .into(driverimage);

                tvTitle.setText(getString(R.string.dashboard));
                panel_payment_type.setText(payment_type);
                etaTime.setText(eta);
                tvetadetails.setText(eta);
                tvetafare.setText("$ " + est_fare);
                tvetafaredetails.setText(est_fare);
                tvdestinationlocationdetails.setText(destination_location);
                tvsourcelocationdetails.setText(source_location);
                Log.d(TAG, "broadcast Got request_id: " + request_id);
                Log.d(TAG, "broadcast Got source_latitude: " + source_latitude);
                Log.d(TAG, "broadcast Got source_longitude: " + source_longitude);
                Log.d(TAG, "broadcast Got destination_latitude: " + destination_latitude);
                Log.d(TAG, "broadcast Got destination_longitude: " + destination_longitude);

            }

        }
    };

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);

        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else if (doubleBackToExitPressedOnce) {
            finishAffinity();
            return;
        }
        this.doubleBackToExitPressedOnce = true;

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);

    }

    @Override
    protected void onResume() {
        super.onResume();

        accept = "application/json";
        authorization = "Bearer " + helper.LoadStringPref(Prefs.token, "");

        Log.d(TAG, "authorization : " + authorization);

        final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER) && hasGPSDevice(MainActivity.this)) {
            enableLoc();
        }

        tv_vehicle_status.setVisibility(View.GONE);
        if (tvTitle.getText().toString().equals(getString(R.string.dashboard))) {
            frameLayout.setVisibility(View.VISIBLE);
        } else {
            frameLayout.setVisibility(View.GONE);

        }

        getDashboard(accept, authorization);

//        updateInOurDb(accept, authorization, latitude, longitude);

        if (donotupdate) {
            updateMyDb();
        } else {

        }

        if (donotupdate) {
            updateLocationRealtime();
        } else {

        }
    }

    @OnClick({R.id.ll_user_status, R.id.btn_accept,
            R.id.tv_open_bottom_sheet, R.id.iv_close,
            R.id.tvTitle, R.id.btn_pass, R.id.btn_pass_details,
            R.id.navigation_map, R.id.ll_header,
            R.id.ll_main_map_header, R.id.ll_main,
            R.id.ll_call, R.id.tv_vehicle_status, R.id.ll_more})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ll_more:

                if (TextUtils.isEmpty(rideinstruction)) {
                    Snackbar.make(rl_layout, "No Instructions from Passenger..!!!", Snackbar.LENGTH_LONG).show();
                } else {
                    openMoreInstructions();
                }
                break;
            case R.id.tv_vehicle_status:
                startActivity(new Intent(MainActivity.this, DocumentActivity.class));
                break;

            case R.id.ll_user_status:


                if (!runningRideStatus.isEmpty()) {
                    List<LatLng> lstLatLngRoute = new ArrayList<>();
                    lstLatLngRoute.add(new LatLng(Double.parseDouble("" + SELECTED_PICK_LATITUDE), Double.parseDouble("" + SELECTED_PICK_LONGITUDE)));
                    lstLatLngRoute.add(new LatLng(Double.parseDouble("" + SELECTED_DROP_LATITUDE), Double.parseDouble("" + SELECTED_DROP_LONGITUDE)));

                    zoomRoute(mMap, lstLatLngRoute);
                } else {
                    latitude = gps.getLatitude();
                    longitude = gps.getLongitude();

                    mMap.animateCamera(CameraUpdateFactory.newCameraPosition(new CameraPosition.Builder().target(new LatLng(latitude, longitude)).zoom(15).build()));

                }

                Log.d(TAG, "runningRideStatus :" + runningRideStatus);

                break;
            case R.id.btn_accept:

                acceptRide(accept, authorization, request_id);
                break;
            case R.id.btn_accept_details:

                acceptRide(accept, authorization, request_id);
                break;

            case R.id.tv_open_bottom_sheet:

                behaviorOpen.setState(BottomSheetBehavior.STATE_EXPANDED);
                break;

            case R.id.tvTitle:
                break;

            case R.id.iv_close:
                behaviorOpen.setState(BottomSheetBehavior.STATE_COLLAPSED);
                break;

            case R.id.btn_pass:
//                passRide(accept, authorization, request_id);
                break;

            case R.id.btn_pass_details:
//                passRide(accept, authorization, request_id);
                break;
            case R.id.navigation_map:
//                onMapBox();
                googleMapExternal();
                break;

            case R.id.ll_main:
                behaviorOpen.setState(BottomSheetBehavior.STATE_EXPANDED);
                break;
            case R.id.ll_call:

                startActivity(new Intent(MainActivity.this, VoiceActivity.class)
                        .putExtra(AppConfig.BUNDLE.passenger_first_name, passengerfirstname)
                        .putExtra(AppConfig.BUNDLE.passenger_last_name, lastname)
                        .putExtra(AppConfig.BUNDLE.PassengerUniqueCode, passenger_unique_code)
                        .putExtra(AppConfig.BUNDLE.display_passenger_name, displaypassengername)
                        .putExtra(AppConfig.BUNDLE.UserUniqueCode, user_unique_code)
                        .putExtra(AppConfig.BUNDLE.payment_type, payment_type)
                        .putExtra(AppConfig.BUNDLE.passenger_phone_number, passengerphonenumber)
                        .putExtra(AppConfig.EXTRA.CHECKOUTGOING, 1));

                Log.d(TAG, "main call payment type : " + payment_type);
                break;

        }
    }

    private void googleMapExternal() {

        //working for navigation
        String url = "google.navigation:q=" + SELECTED_PICK_LATITUDE + "," + SELECTED_PICK_LONGITUDE + "&mode=d";
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        startActivity(intent);

    }

    public static void zoomRoute(GoogleMap googleMap, List<LatLng> lstLatLngRoute) {

        if (googleMap == null || lstLatLngRoute == null || lstLatLngRoute.isEmpty()) return;

        LatLngBounds.Builder boundsBuilder = new LatLngBounds.Builder();
        for (LatLng latLngPoint : lstLatLngRoute)
            boundsBuilder.include(latLngPoint);

        int routePadding = 100;
        LatLngBounds latLngBounds = boundsBuilder.build();

        googleMap.moveCamera(CameraUpdateFactory.newLatLngBounds(latLngBounds, routePadding));
    }


    private void createUser(String lattitude, String longitude) {
        // TODO
        // In real apps this userId should be fetched
        // by implementing firebase auth
        User user = new User(lattitude, longitude);


        mFirebaseDatabase.child(myUserID).setValue(user);

        addUserChangeListener(lattitude, longitude);
    }

    private void addUserChangeListener(final String lattitude, final String longitude) {
        // User data change listener
        mFirebaseDatabase.child(myUserID).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
//                Log.d(TAG, "data : " + lattitude);
//                Log.d(TAG, "data : " + longitude);
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.e(TAG, "Failed to read user", error.toException());
            }
        });
    }

    /*private void onMapBox() {

        if (options == null) {
            return;
        }
        mapView.setVisibility(View.VISIBLE);
        NavigationLauncher.startNavigation(MainActivity.this, options);

    }*/

    private void acceptRide(String accept, String authorization, String request_id) {

        progress.createDialog(false);
        progress.DialogMessage(getString(R.string.please_wait));
        progress.showDialog();

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        RequestBody request_id1 = RequestBody.create(MediaType.parse("multipart/form-data"), request_id);

        final Call<RespAcceptRide> respAcceptRideCall = apiService.ACCEPT_RIDE_CALL(accept, authorization, request_id1);
        respAcceptRideCall.enqueue(new Callback<RespAcceptRide>() {
            @Override
            public void onResponse(Call<RespAcceptRide> call, Response<RespAcceptRide> response) {
                progress.hideDialog();
                Toast.makeText(MainActivity.this, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                Log.e(TAG, "response acept: " + new Gson().toJson(response.body()));
                behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);

                tvdestinationlocationarrived.setText(destination_location);
//                ll_main_map_header.setVisibility(View.VISIBLE);
                mMap.clear();
            }

            @Override
            public void onFailure(Call<RespAcceptRide> call, Throwable t) {
                progress.hideDialog();
                Toast.makeText(MainActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        fragmentTransaction = manager.beginTransaction();

        if (tvTitle.getText().toString().equals(getString(R.string.dashboard))) {

        } else {
//            mapView.setVisibility(View.GONE);
            fab_speed_dial.setVisibility(View.GONE);
        }


        if (id == R.id.nav_dashboard) {
            frameLayout.setVisibility(View.VISIBLE);
            fragmentTransaction.replace(R.id.frame_main, DashboardFragment.newInstance());
            tvTitle.setText(getString(R.string.dashboard));
            llmap.setVisibility(View.VISIBLE);
            tv_vehicle_status.setVisibility(View.GONE);


        } else if (id == R.id.nav_settings) {

            llmap.setVisibility(View.GONE);
            tv_vehicle_status.setVisibility(View.GONE);
            frameLayout.setVisibility(View.GONE);

            //for fragment
            fragmentTransaction.replace(R.id.frame_main, ProfileFragment.newInstance());
            tvTitle.setText(getString(R.string.settings));


            behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            behaviorArrived.setState(BottomSheetBehavior.STATE_COLLAPSED);

        } else if (id == R.id.nav_change_duty) {

            frameLayout.setVisibility(View.GONE);

            if (TextUtils.isEmpty(userStatus)) {
//                llmap.setVisibility(View.GONE);
                tv_vehicle_status.setVisibility(View.GONE);


                Intent intent = new Intent(MainActivity.this, StatusChangeActivity.class);
                intent.putExtra(AppConfig.BUNDLE.display_passenger_name, displaypassengername);
                intent.putExtra(AppConfig.BUNDLE.UserStatus, userStatus);
                startActivity(intent);


                behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                behaviorArrived.setState(BottomSheetBehavior.STATE_COLLAPSED);


            } else {
//                frameLayout.setVisibility(View.VISIBLE);

                Intent intent = new Intent(MainActivity.this, StatusChangeActivity.class);
                intent.putExtra(AppConfig.BUNDLE.display_passenger_name, displaypassengername);
                intent.putExtra(AppConfig.BUNDLE.UserStatus, userStatus);
                startActivity(intent);


                behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                behaviorArrived.setState(BottomSheetBehavior.STATE_COLLAPSED);

//                llmap.setVisibility(View.GONE);
                tv_vehicle_status.setVisibility(View.GONE);

            }

            ll_main_map_header.setVisibility(View.GONE);
            fab_speed_dial.setVisibility(View.GONE);


        } else if (id == R.id.nav_history) {

            frameLayout.setVisibility(View.GONE);

            fragmentTransaction.replace(R.id.frame_main, HistoryFragment.newInstance());
            tvTitle.setText(getString(R.string.history));

            behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            behaviorArrived.setState(BottomSheetBehavior.STATE_COLLAPSED);

            llmap.setVisibility(View.GONE);
            tv_vehicle_status.setVisibility(View.GONE);


        } else if (id == R.id.nav_earnings) {
            llmap.setVisibility(View.GONE);
            frameLayout.setVisibility(View.GONE);

            fragmentTransaction.replace(R.id.frame_main, EarningsFragment.newInstance());
            tvTitle.setText(getString(R.string.earnings));

            behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            behaviorArrived.setState(BottomSheetBehavior.STATE_COLLAPSED);

            tv_vehicle_status.setVisibility(View.GONE);

            ll_main_map_header.setVisibility(View.GONE);
            fab_speed_dial.setVisibility(View.GONE);


        } else if (id == R.id.nav_pre_schedule_trip) {
            frameLayout.setVisibility(View.GONE);

            startActivity(new Intent(MainActivity.this, PreScheduleTripActivity.class));


        } else if (id == R.id.nav_report_issue) {

//            frameLayout.setVisibility(View.VISIBLE);
            frameLayout.setVisibility(View.GONE);


            Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts("mailto", "info@mtsryde.com", null));
            emailIntent.putExtra(Intent.EXTRA_SUBJECT, "" + MainActivity.this.getResources().getString(R.string.report_issue));
            emailIntent.putExtra(Intent.EXTRA_TEXT, "");
            startActivity(Intent.createChooser(emailIntent, "" + MainActivity.this.getResources().getString(R.string.send_email)));
            emailIntent.setType("text/plain");
            tvTitle.setText(getString(R.string.report));

            behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            behaviorArrived.setState(BottomSheetBehavior.STATE_COLLAPSED);

            llmap.setVisibility(View.GONE);
            tv_vehicle_status.setVisibility(View.GONE);

            ll_main_map_header.setVisibility(View.GONE);
            fab_speed_dial.setVisibility(View.GONE);


        } else if (id == R.id.nav_customer_support) {

            frameLayout.setVisibility(View.GONE);

            startActivity(new Intent(MainActivity.this, HelpSupportActivity.class)
                    .putExtra(AppConfig.BUNDLE.Support_Number, supportnumber)
                    .putExtra(AppConfig.BUNDLE.UserUniqueCode, user_unique_code));

            behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);


            ll_main_map_header.setVisibility(View.GONE);
            fab_speed_dial.setVisibility(View.GONE);


        } else if (id == R.id.nav_termscondition) {
//            frameLayout.setVisibility(View.GONE);

            startActivity(new Intent(MainActivity.this, TermsActivity.class));


            behaviorArrived.setState(BottomSheetBehavior.STATE_COLLAPSED);
//            llmap.setVisibility(View.GONE);
//            tv_vehicle_status.setVisibility(View.GONE);


//            ll_main_map_header.setVisibility(View.GONE);
//            fab_speed_dial.setVisibility(View.GONE);

        } else if (id == R.id.nav_logout) {
            logoutDialog();
        } else {
            ll_main_map_header.setVisibility(View.GONE);
            fab_speed_dial.setVisibility(View.GONE);
        }

        fragmentTransaction.commit();
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return false;
    }


    private void cancelDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setMessage(R.string.cancel_msg);
        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                cancel(accept, authorization, request_id);
            }
        });
        builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    private void noShowDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setMessage(R.string.no_show_msg);
        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                noShow(accept, authorization, request_id);

            }
        });
        builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    private void logoutDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setMessage(R.string.logout_msg);
        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            FirebaseInstanceId.getInstance().deleteInstanceId();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }).start();
                helper.clearAllPrefs();
                startActivity(new Intent(MainActivity.this, LoginActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                finish();
            }
        });
        builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;
        MapUtils.setMapTheme(this, mMap);
        mMap.clear();

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }

        mMap.setMyLocationEnabled(false);
        CameraUpdate yourLocation = CameraUpdateFactory.newLatLngZoom(new LatLng(latitude, longitude), 15);

        mMap.animateCamera(yourLocation);
        mMap.setOnCameraIdleListener(onCameraIdleListener);

        // Placing a marker on the touched position

        try {


            if (SELECTED_DROP_LONGITUDE.equals("")
                    || SELECTED_DROP_LONGITUDE.equalsIgnoreCase("null")
                    || SELECTED_DROP_LONGITUDE.contains("null")) {
                LatLng origin = new LatLng(Double.parseDouble("" + SELECTED_PICK_LATITUDE), Double.parseDouble("" + SELECTED_PICK_LONGITUDE));
                MapUtils.setDestinationMarkerForPickPoint(this, mGooglemap, origin, "" + source_location);
                googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(new CameraPosition.Builder().target(origin).zoom(Config.MapDefaultZoom).build()));

                map_progress.setVisibility(View.GONE);

                ll_main_map_header.setVisibility(View.GONE);
                fab_speed_dial.setVisibility(View.GONE);

            } else {


                LatLng origin = new LatLng(Double.parseDouble("" + SELECTED_PICK_LATITUDE), Double.parseDouble("" + SELECTED_PICK_LONGITUDE));
                LatLng destination = new LatLng(Double.parseDouble("" + SELECTED_DROP_LATITUDE), Double.parseDouble("" + SELECTED_DROP_LONGITUDE));
                MapUtils.setDestinationMarkerForPickPoint(this, mGooglemap, origin, "Customer Location ");
                MapUtils.setDestinationMarkerForDropPoint(this, mGooglemap, destination, "Customer Location");
                try {
                    DrawRouteMaps.getInstance(this, this, sessionManager, map_progress, true).draw(origin, destination, mGooglemap);
                } catch (Exception e) {

                }

                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(Double.parseDouble("" + SELECTED_PICK_LATITUDE), Double.parseDouble("" + SELECTED_PICK_LONGITUDE)), 12));
                mMap.animateCamera(CameraUpdateFactory.zoomTo(12), 2000, null);


                myLatLng = new LatLng(Double.parseDouble(myLat), Double.parseDouble(myLang));
                LatLng from = new LatLng(Double.parseDouble(SELECTED_PICK_LATITUDE), Double.parseDouble(SELECTED_PICK_LONGITUDE));


                //Calculating the distance in meters
                distanceFromDestination = SphericalUtil.computeDistanceBetween(from, myLatLng);


                if (distanceFromDestination.floatValue() < 200) {
                    openArriveRideNow();
                }


                ll_main_map_header.setVisibility(View.GONE);
                fab_speed_dial.setVisibility(View.GONE);


            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {
    }

    @Override
    public void onConnectionSuspended(int i) {
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    private void openMoreInstructions() {
        final Dialog mBottomSheetDialog = new Dialog(this, R.style.MaterialDialogSheet);
        mBottomSheetDialog.setContentView(R.layout.dialog_instruction);
        mBottomSheetDialog.setCancelable(true);
        mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        mBottomSheetDialog.getWindow().setGravity(Gravity.CENTER);
        mBottomSheetDialog.setCanceledOnTouchOutside(true);
        AppCompatTextView tv_instruction = mBottomSheetDialog.findViewById(R.id.tv_instruction_details);
        AppCompatTextView btn_ok = mBottomSheetDialog.findViewById(R.id.btn_ok);

        tv_instruction.setText(rideinstruction);


        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBottomSheetDialog.dismiss();

            }
        });
        mBottomSheetDialog.show();
    }

    private void openAlertUnauthLogout() {
        try {
            final Dialog mBottomSheetDialog = new Dialog(this, R.style.MaterialDialogSheet);
            mBottomSheetDialog.setContentView(R.layout.dialog_unauthorised);
            mBottomSheetDialog.setCancelable(false);
            mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            mBottomSheetDialog.getWindow().setGravity(Gravity.CENTER);
            mBottomSheetDialog.setCanceledOnTouchOutside(false);
            AppCompatTextView btn_ok = mBottomSheetDialog.findViewById(R.id.btn_ok);

            btn_ok.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    progress.createDialog(false);
                    progress.DialogMessage(getString(R.string.msg_logout));
                    progress.showDialog();
                    AsyncTask.execute(new Runnable() {
                        @Override
                        public void run() {

                            try {
                                FirebaseInstanceId.getInstance().deleteInstanceId();
                                helper.clearAllPrefs();
                                startActivity(new Intent(MainActivity.this, LoginActivity.class)
                                        .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                                progress.hideDialog();
                                MainActivity.this.finish();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    });


                }
            });
            mBottomSheetDialog.show();
        } catch (WindowManager.BadTokenException e) {
            e.printStackTrace();
        }

    }

    private void passRide(String accept, String authorization, String request_id) {
        progress.createDialog(false);
        progress.DialogMessage(getString(R.string.please_wait));
        progress.showDialog();

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        RequestBody request_id1 = RequestBody.create(MediaType.parse("multipart/form-data"), request_id);

        final Call<RespPassRequest> respPassRequestCall = apiService.PASS_RIDE_CALL(accept, authorization, request_id1);
        respPassRequestCall.enqueue(new Callback<RespPassRequest>() {
            @Override
            public void onResponse(Call<RespPassRequest> call, Response<RespPassRequest> response) {
                progress.hideDialog();
                Toast.makeText(MainActivity.this, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<RespPassRequest> call, Throwable t) {
                progress.hideDialog();
                Toast.makeText(MainActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    protected void onDestroy() {
        // Unregister since the activity is about to be closed.
        super.onDestroy();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);


        Log.d(TAG, "on destroy");
    }

    /*@Override
    public void onExplanationNeeded(List<String> permissionsToExplain) {
        Toast.makeText(this, R.string.user_location_permission_explanation, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onPermissionResult(boolean granted) {
        if (granted) {
//            enableLocationComponent(mapboxMap.getStyle());
        } else {
            Toast.makeText(this, R.string.user_location_permission_not_granted, Toast.LENGTH_LONG).show();
            finish();
        }
    }*/
}
