package com.rydedispatch.driver.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatRatingBar;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.CardView;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.maps.android.SphericalUtil;
import com.rydedispatch.driver.R;
import com.rydedispatch.driver.network.ApiClient;
import com.rydedispatch.driver.network.ApiInterface;
import com.rydedispatch.driver.response.RespAcceptRide;
import com.rydedispatch.driver.response.RespPassRequest;
import com.rydedispatch.driver.utils.AppConfig;
import com.rydedispatch.driver.utils.Config;
import com.rydedispatch.driver.utils.DrawRouteMaps;
import com.rydedispatch.driver.utils.MapUtils;
import com.rydedispatch.driver.utils.PreferenceHelper;
import com.rydedispatch.driver.utils.Prefs;
import com.rydedispatch.driver.utils.Progress;
import com.rydedispatch.driver.utils.SessionManager;
import com.rydedispatch.driver.utils.Utils;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RideConfirmDialogActivity extends FragmentActivity implements OnMapReadyCallback {
    private static final String TAG = RideConfirmDialogActivity.class.getSimpleName();

    public static Activity activity;
    //    GoogleMap mGooglemap;
    private GoogleMap mMap;


    GsonBuilder builder;
    Gson gson;

    SessionManager sessionManager;

    Handler mhandler;
    Runnable mRunnable;
    int PLACE_AUTOCOMPLETE_REQUEST_CODE = 1;

    @BindView(R.id.root)
    CardView root;
    @BindView(R.id.map_progress)
    ProgressBar map_progress;
    @BindView(R.id.panel_content)
    TextView panel_content;
    @BindView(R.id.btn_accept_on_other)
    Button btn_accept_on_other;
    @BindView(R.id.btn_pass_on_other)
    Button btn_pass_on_other;
    @BindView(R.id.panel_payment_type)
    TextView panel_payment_type;

    @BindView(R.id.tv_eta)
    AppCompatTextView etaTime;
    @BindView(R.id.tv_eta_fare)
    AppCompatTextView tvetafare;
    @BindView(R.id.ll_default)
    LinearLayout ll_default;
    @BindView(R.id.ll_schedule)
    LinearLayout ll_schedule;
    @BindView(R.id.tv_schedule)
    TextView tv_schedule;
    @BindView(R.id.tv_passenger)
    TextView tvPassenger;
    @BindView(R.id.tv_schedule_payment)
    TextView tv_schedule_payment;
    @BindView(R.id.tv_schedule_date)
    TextView tv_schedule_date;
    @BindView(R.id.tv_passenger_rating)
    AppCompatRatingBar tv_passenger_rating;
    @BindView(R.id.card_view_ride)
    CardView card_view_ride;
    @BindView(R.id.schedule_date)
    TextView tvschedule_date;
    @BindView(R.id.iv_passenger_image)
    CircleImageView iv_passenger_image;

    private String accept = "",
            authorization = "", user_id = "", source_latitude = "",
            source_longitude = "", destination_latitude = "",
            destination_longitude = "",
            est_fare = "", eta = "", payment_type = "", request_id = "", rideAccepted = "",
            destination_location = "", sourceLocation = "", is_schedule = "", schedule_date = "",
            passenger_first_name = "", passmsg = "", passengerRating = "",
            passenger_unique_code = "", status = "", userStatus = "", driver_id = "",
            displaypassengername = "", body = "", acceptRide = "", rideinstruction = "", passenger_image = "";

    private String SELECTED_PICK_LATITUDE;
    private String SELECTED_PICK_LONGITUDE;
    private String SELECTED_DROP_LATITUDE;
    private String SELECTED_DROP_LONGITUDE;


    private Bundle bundle;

    private Utils utils;
    private Progress progress;
    private PreferenceHelper helper;

    private MapFragment mapFragment;
    private SupportMapFragment mainMap;

    private Dialog mBottomSheetDialog;

    GoogleMap mDetailsMap;
    MapView mMapView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = this;
        mhandler = new Handler();
        builder = new GsonBuilder();
        gson = builder.create();
        setContentView(R.layout.activity_ride_confirm_dialog);
        ButterKnife.bind(this);

        utils = new Utils(this);
        progress = new Progress(this);

        helper = new PreferenceHelper(this, Prefs.PREF_FILE);
        accept = "application/json";
        authorization = "Bearer " + helper.LoadStringPref(Prefs.token, "");

        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver,
                new IntentFilter("custom-event-name"));

        mainMap = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map_frag_main_ride);
        mainMap.getMapAsync(this);

        bundle = getIntent().getExtras();
        if (bundle != null) {
            body = bundle.getString(AppConfig.EXTRA.body);
            sourceLocation = bundle.getString(AppConfig.BUNDLE.source_location);
            destination_location = bundle.getString(AppConfig.BUNDLE.destination_location);
            request_id = bundle.getString(AppConfig.BUNDLE.request_id);
            est_fare = bundle.getString(AppConfig.BUNDLE.est_fare);
            eta = bundle.getString(AppConfig.BUNDLE.eta);
            payment_type = bundle.getString(AppConfig.BUNDLE.payment_type);
            is_schedule = bundle.getString(AppConfig.BUNDLE.is_schedule);
            schedule_date = bundle.getString(AppConfig.BUNDLE.schedule_date);
            passenger_first_name = bundle.getString(AppConfig.BUNDLE.passenger_first_name);
            passengerRating = bundle.getString(AppConfig.BUNDLE.passenger_rating);
            passenger_unique_code = bundle.getString(AppConfig.BUNDLE.passenger_unique_code);
            status = bundle.getString(AppConfig.BUNDLE.status);
            userStatus = bundle.getString(AppConfig.BUNDLE.UserStatus);
            displaypassengername = bundle.getString(AppConfig.BUNDLE.display_passenger_name);
            rideinstruction = bundle.getString(AppConfig.BUNDLE.ride_instruction);
            driver_id = bundle.getString(AppConfig.BUNDLE.driver_id);
            passenger_image = bundle.getString(AppConfig.BUNDLE.passenger_image);
        }


        try {
            SELECTED_PICK_LATITUDE = "" + getIntent().getExtras().getString(Config.IntentKeys.PICK_LATITUDE);
            SELECTED_PICK_LONGITUDE = "" + getIntent().getExtras().getString(Config.IntentKeys.PICK_LONGITUDE);
            SELECTED_DROP_LATITUDE = "" + getIntent().getExtras().getString(Config.IntentKeys.DROP_LATITUDE);
            SELECTED_DROP_LONGITUDE = "" + getIntent().getExtras().getString(Config.IntentKeys.DROP_LONGITUDE);
        } catch (Exception e) {
            e.printStackTrace();
        }


        Log.d(TAG, "SELECTED_PICK_LATITUDE :" + SELECTED_PICK_LATITUDE);
        Log.d(TAG, "SELECTED_PICK_LONGITUDE :" + SELECTED_PICK_LONGITUDE);
        Log.d(TAG, "SELECTED_DROP_LATITUDE :" + SELECTED_DROP_LATITUDE);
        Log.d(TAG, "SELECTED_DROP_LONGITUDE :" + SELECTED_DROP_LONGITUDE);

        try {
            if (is_schedule.contains("0")) {
                ll_default.setVisibility(View.VISIBLE);
                ll_schedule.setVisibility(View.GONE);
                tvschedule_date.setText(schedule_date);
                if (payment_type.equalsIgnoreCase("none")) {
                    panel_payment_type.setText("Invoice");

                } else {
                    panel_payment_type.setText(payment_type);

                }
                etaTime.setText(eta + " MIN");
                tvetafare.setText("$ " + est_fare);
                tvPassenger.setText(displaypassengername);
                tv_passenger_rating.setRating(Float.parseFloat(passengerRating));


            } else {
                ll_default.setVisibility(View.GONE);
                ll_schedule.setVisibility(View.VISIBLE);

                tv_schedule.setText("Schedule Request");
                tv_schedule_date.setText(schedule_date);
                if (payment_type.equalsIgnoreCase("none")) {
                    panel_payment_type.setText("Invoice");

                } else {
                    panel_payment_type.setText(payment_type);

                }
                tvPassenger.setText(displaypassengername);
                tv_passenger_rating.setRating(Float.parseFloat(passengerRating));


            }

            if (payment_type.equalsIgnoreCase("none")) {
                panel_payment_type.setText("Invoice");
            } else {
                panel_payment_type.setText(payment_type);

            }
            etaTime.setText(eta + " MIN");
            tvetafare.setText("$ " + est_fare);
            tvPassenger.setText(displaypassengername);
            tv_passenger_rating.setRating(Float.parseFloat(passengerRating));

            if (schedule_date == null || schedule_date.contains("")) {
                tvschedule_date.setVisibility(View.INVISIBLE);
            } else {
                tvschedule_date.setText(schedule_date);

            }

        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        try {
            etaTime.setText(eta + " MIN");
            tvetafare.setText("$ " + est_fare);
            tvPassenger.setText(displaypassengername);
            tv_passenger_rating.setRating(Float.parseFloat(passengerRating));
            tvschedule_date.setText(schedule_date);

            if (payment_type.equalsIgnoreCase("none")) {
                panel_payment_type.setText("Invoice");
            } else {
                panel_payment_type.setText(payment_type);

            }
        } catch (NullPointerException e) {

        }

        if (passenger_image == null) {

            Picasso.with(RideConfirmDialogActivity.this)
                    .load(AppConfig.URL.BASE_IMAGEURL + passenger_image)
                    .placeholder(R.mipmap.ic_launcher)
                    .error(R.mipmap.ic_launcher)
                    .into(iv_passenger_image);
            return;
        }

        if (passenger_image.contains("https://")) {

            Picasso.with(RideConfirmDialogActivity.this)
                    .load(passenger_image)
                    .placeholder(R.mipmap.ic_launcher)
                    .error(R.drawable.dummy_pic)
                    .into(iv_passenger_image);

        } else {

            Picasso.with(RideConfirmDialogActivity.this)
                    .load(AppConfig.URL.BASE_IMAGEURL + passenger_image)
                    .placeholder(R.mipmap.ic_launcher)
                    .error(R.drawable.dummy_pic)
                    .into(iv_passenger_image);
        }

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;
        root.setMinimumWidth(width);

//        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map_frag_main);
//        mapFragment.getMapAsync(this);


    }


    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Get extra data included in the Intent
            String message = intent.getStringExtra("message");

            if (message.contains("message")) {
                finish();
            } else {
                recreate();
            }
        }
    };

    @Override
    protected void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();

    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        MapUtils.setMapTheme(this, mMap);
        mMap.clear();


        double latitude = Double.parseDouble(SELECTED_PICK_LATITUDE);
        double longitude = Double.parseDouble(SELECTED_PICK_LONGITUDE);

        LatLng location = new LatLng(latitude, longitude);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(location, 13));

        try {
            if (SELECTED_DROP_LONGITUDE.equals("")
                    || SELECTED_DROP_LONGITUDE.equalsIgnoreCase("null")
                    || SELECTED_DROP_LONGITUDE.contains("null")) {
                LatLng origin = new LatLng(Double.parseDouble("" + SELECTED_PICK_LATITUDE), Double.parseDouble("" + SELECTED_PICK_LONGITUDE));
                MapUtils.setDestinationMarkerForPickPoint(this, mMap, origin, "" + sourceLocation);
                mMap.animateCamera(CameraUpdateFactory.newCameraPosition(new CameraPosition.Builder().target(origin).zoom(Config.MapDefaultZoom).build()));

                map_progress.setVisibility(View.GONE);

                Log.d(TAG, "onMapReady if PICK_LATITUDE :" + SELECTED_PICK_LATITUDE);
                Log.d(TAG, "onMapReady if PICK_LONGITUDE :" + SELECTED_PICK_LONGITUDE);
                Log.d(TAG, "onMapReady if DROP_LATITUDE :" + SELECTED_DROP_LATITUDE);
                Log.d(TAG, "onMapReady if DROP_LONGITUDE :" + SELECTED_DROP_LONGITUDE);
            } else {
                googleMap.clear();

                LatLng origin1 = new LatLng(Double.parseDouble("" + SELECTED_PICK_LATITUDE), Double.parseDouble("" + SELECTED_PICK_LONGITUDE));
                LatLng destination1 = new LatLng(Double.parseDouble("" + SELECTED_DROP_LATITUDE), Double.parseDouble("" + SELECTED_DROP_LONGITUDE));
                MapUtils.setDestinationMarkerForPickPoint(this, googleMap, origin1, "Customer Location ");
                MapUtils.setDestinationMarkerForDropPoint(this, googleMap, destination1, "Customer Location");

                try {
                    DrawRouteMaps.getInstance(this, this, sessionManager, map_progress, true).draw(origin1, destination1, googleMap);
                } catch (Exception e) {
                    e.printStackTrace();
                }
//                mMap.animateCamera(CameraUpdateFactory.newCameraPosition(new CameraPosition.Builder().target(origin1).zoom(Config.MapDefaultZoom).build()));

                googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(new CameraPosition.Builder().target(origin1).zoom(Config.MapDefaultZoom).build()));
                List<LatLng> lstLatLngRoute = new ArrayList<>();
                lstLatLngRoute.add(origin1);
                lstLatLngRoute.add(destination1);

                MapUtils.zoomRoute(googleMap, lstLatLngRoute);

                Log.d(TAG, "onMapReady else PICK_LATITUDE :" + SELECTED_PICK_LATITUDE);
                Log.d(TAG, "onMapReady else PICK_LONGITUDE :" + SELECTED_PICK_LONGITUDE);
                Log.d(TAG, "onMapReady else DROP_LATITUDE :" + SELECTED_DROP_LATITUDE);
                Log.d(TAG, "onMapReady else DROP_LONGITUDE :" + SELECTED_DROP_LONGITUDE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @OnClick({R.id.btn_accept_on_other, R.id.btn_pass_on_other, R.id.card_view_ride})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_accept_on_other:


                acceptRide(accept, authorization, request_id);

                break;
            case R.id.btn_pass_on_other:
                passRide(accept, authorization, request_id);
                break;

            case R.id.card_view_ride:
                openBottomViewDetails();
                break;
        }
    }

    private void openBottomViewDetails() {
        mBottomSheetDialog = new Dialog(this, R.style.MaterialDialogSheet);
        mBottomSheetDialog.setContentView(R.layout.dialog_pick_up_request);
        mBottomSheetDialog.setCancelable(false);
        mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        mBottomSheetDialog.getWindow().setGravity(Gravity.CENTER);
        mBottomSheetDialog.setCanceledOnTouchOutside(false);
        RelativeLayout anchor_panel_open = mBottomSheetDialog.findViewById(R.id.anchor_panel_open);
        TextView panelOpen = mBottomSheetDialog.findViewById(R.id.panel_content_open);
        AppCompatImageView iv_close = mBottomSheetDialog.findViewById(R.id.iv_close);
        AppCompatTextView tv_name = mBottomSheetDialog.findViewById(R.id.tv_name_detail);
        AppCompatRatingBar tv_rating = mBottomSheetDialog.findViewById(R.id.tv_rating_details);
        AppCompatTextView tv_cash_credit = mBottomSheetDialog.findViewById(R.id.tv_cash_credit);
        AppCompatTextView tv_eta_details = mBottomSheetDialog.findViewById(R.id.tv_eta_details);
        AppCompatTextView tv_eta_fare_details = mBottomSheetDialog.findViewById(R.id.tv_eta_fare_details);
        AppCompatTextView tv_instruction = mBottomSheetDialog.findViewById(R.id.tv_instruction);
        AppCompatTextView tv_source_location_details = mBottomSheetDialog.findViewById(R.id.tv_source_location_details);
        AppCompatTextView tv_destination_location_details = mBottomSheetDialog.findViewById(R.id.tv_destination_location_details);
        CircleImageView iv_detail_passenger_image = mBottomSheetDialog.findViewById(R.id.iv_detail_passenger_image);
        Button btn_accept_details = mBottomSheetDialog.findViewById(R.id.btn_accept_details);
        Button btn_pass_details = mBottomSheetDialog.findViewById(R.id.btn_pass_details);

//        tv_instruction.setText("New Instructions");

        if (TextUtils.isEmpty(rideinstruction) || rideinstruction == null) {
            tv_instruction.setVisibility(View.INVISIBLE);
        } else {
            tv_instruction.setVisibility(View.VISIBLE);

            tv_instruction.setText(rideinstruction);
            tv_eta_details.setText(eta + " MIN");
            tv_eta_fare_details.setText("$ " + est_fare);
            tv_source_location_details.setText(sourceLocation);
            tv_destination_location_details.setText(destination_location);
            tv_name.setText(displaypassengername);
            tv_rating.setRating(Float.parseFloat(passengerRating));
            tv_cash_credit.setText(payment_type);
        }

        if (passenger_image == null) {

            Picasso.with(RideConfirmDialogActivity.this)
                    .load(AppConfig.URL.BASE_IMAGEURL + passenger_image)
                    .placeholder(R.mipmap.ic_launcher)
                    .error(R.drawable.dummy_pic)
                    .into(iv_detail_passenger_image);
            return;
        }

        if (passenger_image.contains("https://")) {

            Picasso.with(RideConfirmDialogActivity.this)
                    .load(passenger_image)
                    .placeholder(R.mipmap.ic_launcher)
                    .error(R.drawable.dummy_pic)
                    .into(iv_detail_passenger_image);

        } else {

            Picasso.with(RideConfirmDialogActivity.this)
                    .load(AppConfig.URL.BASE_IMAGEURL + passenger_image)
                    .placeholder(R.mipmap.ic_launcher)
                    .error(R.drawable.dummy_pic)
                    .into(iv_detail_passenger_image);
        }

        tv_instruction.setVisibility(View.VISIBLE);

        tv_instruction.setText(rideinstruction);
        tv_eta_details.setText(eta + " MIN");
        tv_eta_fare_details.setText("$ " + est_fare);
        tv_source_location_details.setText(sourceLocation);
        tv_destination_location_details.setText(destination_location);
        tv_name.setText(displaypassengername);
        tv_rating.setRating(Float.parseFloat(passengerRating));
        tv_cash_credit.setText(payment_type);

        iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBottomSheetDialog.dismiss();
            }
        });

        btn_accept_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                acceptRide(accept, authorization, request_id);
            }
        });

        btn_pass_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                passRide(accept, authorization, request_id);
            }
        });


        mBottomSheetDialog.show();

        mMapView = mBottomSheetDialog.findViewById(R.id.mapView);
        MapsInitializer.initialize(this);

        mMapView = (MapView) mBottomSheetDialog.findViewById(R.id.mapView);
        mMapView.onCreate(mBottomSheetDialog.onSaveInstanceState());
        mMapView.onResume();// needed to get the map to display immediately

        mMapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                mDetailsMap = googleMap;
                MapUtils.setMapTheme(RideConfirmDialogActivity.this, mDetailsMap);
                mDetailsMap.clear();


                double latitude = Double.parseDouble(SELECTED_PICK_LATITUDE);
                double longitude = Double.parseDouble(SELECTED_PICK_LONGITUDE);

                Log.d("RideConfirmDialog", "bottom sheet lat, lang:" + SELECTED_PICK_LATITUDE);
                Log.d("RideConfirmDialog", "bottom sheet lat, lang :" + SELECTED_PICK_LONGITUDE);
                LatLng location = new LatLng(latitude, longitude);
                mDetailsMap.moveCamera(CameraUpdateFactory.newLatLngZoom(location, 12));

                try {
                    if (SELECTED_DROP_LONGITUDE.equals("")) {
                        LatLng origin = new LatLng(Double.parseDouble("" + SELECTED_PICK_LATITUDE), Double.parseDouble("" + SELECTED_PICK_LONGITUDE));
                        MapUtils.setDestinationMarkerForPickPoint(RideConfirmDialogActivity.this, mDetailsMap, origin, "" + sourceLocation);
                        googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(new CameraPosition.Builder().target(origin).zoom(Config.MapDetailsDefaultZoom).build()));

                        map_progress.setVisibility(View.GONE);


                    } else {

                        LatLng origin = new LatLng(Double.parseDouble("" + SELECTED_PICK_LATITUDE), Double.parseDouble("" + SELECTED_PICK_LONGITUDE));
                        LatLng destination = new LatLng(Double.parseDouble("" + SELECTED_DROP_LATITUDE), Double.parseDouble("" + SELECTED_DROP_LONGITUDE));
                        MapUtils.setDestinationMarkerForPickPoint(RideConfirmDialogActivity.this, mDetailsMap, origin, "Customer Location ");
                        MapUtils.setDestinationMarkerForDropPoint(RideConfirmDialogActivity.this, mDetailsMap, destination, "Customer Location");
                        try {
                            DrawRouteMaps.getInstance(RideConfirmDialogActivity.this, RideConfirmDialogActivity.this, sessionManager, map_progress, true).draw(origin, destination, mDetailsMap);
                        } catch (Exception e) {

                        }
                        googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(new CameraPosition.Builder().target(origin).zoom(Config.MapDetailsDefaultZoom).build()));

                    }
                } catch (Exception e) {

                }
            }
        });
    }

    private void acceptRide(String accept, String authorization, String request_id) {

        progress.createDialog(false);
        progress.DialogMessage(getString(R.string.please_wait));
        progress.showDialog();

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        RequestBody request_id1 = RequestBody.create(MediaType.parse("multipart/form-data"), request_id);

        final Call<RespAcceptRide> respAcceptRideCall = apiService.ACCEPT_RIDE_CALL(accept, authorization, request_id1);
        respAcceptRideCall.enqueue(new Callback<RespAcceptRide>() {
            @Override
            public void onResponse(Call<RespAcceptRide> call, Response<RespAcceptRide> response) {
                progress.hideDialog();

                rideAccepted = response.body().getMessage();
                if (response.body().getMessage().contains("Your ride has been accepted Successfully.")) {
                    Toast.makeText(RideConfirmDialogActivity.this, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(RideConfirmDialogActivity.this, MainActivity.class)
                            .putExtra(AppConfig.BUNDLE.payment_type, payment_type)
                            .putExtra(AppConfig.BUNDLE.request_id, request_id)
                            .putExtra(AppConfig.BUNDLE.est_fare, est_fare)
                            .putExtra(AppConfig.BUNDLE.eta, eta)
                            .putExtra(AppConfig.BUNDLE.source_location, sourceLocation)
                            .putExtra(AppConfig.BUNDLE.destination_location, destination_location)
                            .putExtra(AppConfig.BUNDLE.passenger_first_name, passenger_first_name)
                            .putExtra(AppConfig.BUNDLE.is_schedule, is_schedule)
                            .putExtra(AppConfig.BUNDLE.passenger_unique_code, passenger_unique_code)
                            .putExtra(AppConfig.BUNDLE.display_passenger_name, displaypassengername)
                            .putExtra(AppConfig.BUNDLE.status, status)
                            .putExtra(AppConfig.BUNDLE.acceptRide, rideAccepted)
                            .putExtra(AppConfig.BUNDLE.passenger_image, passenger_image)
                            .putExtra(Config.IntentKeys.PICK_LATITUDE, "" + SELECTED_PICK_LATITUDE)
                            .putExtra(Config.IntentKeys.PICK_LONGITUDE, "" + SELECTED_PICK_LONGITUDE)
                            .putExtra(Config.IntentKeys.DROP_LATITUDE, "" + SELECTED_DROP_LATITUDE)
                            .putExtra(Config.IntentKeys.DROP_LONGITUDE, "" + SELECTED_DROP_LONGITUDE));
                    finish();

                } else {
                    finish();
                }

            }

            @Override
            public void onFailure(Call<RespAcceptRide> call, Throwable t) {
                progress.hideDialog();
                Toast.makeText(RideConfirmDialogActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void passRide(String accept, String authorization, String request_id) {
        progress.createDialog(false);
        progress.DialogMessage(getString(R.string.please_wait));
        progress.showDialog();

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        RequestBody request_id1 = RequestBody.create(MediaType.parse("multipart/form-data"), request_id);

        final Call<RespPassRequest> respPassRequestCall = apiService.PASS_RIDE_CALL(accept, authorization, request_id1);
        respPassRequestCall.enqueue(new Callback<RespPassRequest>() {
            @Override
            public void onResponse(Call<RespPassRequest> call, Response<RespPassRequest> response) {
                progress.hideDialog();

                passmsg = response.body().getMessage();
                Toast.makeText(RideConfirmDialogActivity.this, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(RideConfirmDialogActivity.this, MainActivity.class)
                        .putExtra(AppConfig.BUNDLE.pass_msg, passmsg);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);

            }

            @Override
            public void onFailure(Call<RespPassRequest> call, Throwable t) {
                progress.hideDialog();
                Toast.makeText(RideConfirmDialogActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }


    public String makeURL(double sourcelat, double sourcelog, double destlat, double destlog) {
        StringBuilder urlString = new StringBuilder();
        urlString.append("https://maps.googleapis.com/maps/api/directions/json");
        urlString.append("?origin=");// from
        urlString.append(Double.toString(sourcelat));
        urlString.append(",");
        urlString
                .append(Double.toString(sourcelog));
        urlString.append("&destination=");// to
        urlString
                .append(Double.toString(destlat));
        urlString.append(",");
        urlString.append(Double.toString(destlog));
        urlString.append("&sensor=false&mode=walking&alternatives=true");
        urlString.append("&key=AIzaSyBLlbs11nZRiYAEIOczQwLUrOsQsEUFV28");
        return urlString.toString();
    }

    public void drawPath(String result) {
        //Getting both the coordinates
        LatLng from = new LatLng(Double.valueOf(destination_latitude), Double.valueOf(destination_longitude));
        LatLng to = new LatLng(Double.valueOf(source_latitude), Double.valueOf(source_longitude));

        //Calculating the distance in meters
        Double distance = SphericalUtil.computeDistanceBetween(from, to);

        //Displaying the distance
//        Toast.makeText(this,String.valueOf(distance+" Meters"),Toast.LENGTH_SHORT).show();

        Log.d("RideConfirmDialog", "distance :" + distance);


        try {
            //Parsing json
            final JSONObject json = new JSONObject(result);
            JSONArray routeArray = json.getJSONArray("routes");
            JSONObject routes = routeArray.getJSONObject(0);
            JSONObject overviewPolylines = routes.getJSONObject("overview_polyline");
            String encodedString = overviewPolylines.getString("points");
            List<LatLng> list = decodePoly(encodedString);
            Polyline line = mMap.addPolyline(new PolylineOptions()
                    .addAll(list)
                    .width(10)
                    .color(Color.BLACK)
                    .geodesic(true)
            );


        } catch (JSONException e) {

        }
    }

    private List<LatLng> decodePoly(String encoded) {
        List<LatLng> poly = new ArrayList<LatLng>();
        int index = 0, len = encoded.length();
        int lat = 0, lng = 0;

        while (index < len) {
            int b, shift = 0, result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lat += dlat;

            shift = 0;
            result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lng += dlng;

            LatLng p = new LatLng((((double) lat / 1E5)),
                    (((double) lng / 1E5)));
            poly.add(p);
        }

        return poly;
    }


}
