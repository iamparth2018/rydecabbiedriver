package com.rydedispatch.driver.activity;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatTextView;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.hbb20.CountryCodePicker;
import com.rydedispatch.driver.R;
import com.rydedispatch.driver.network.ApiClient;
import com.rydedispatch.driver.network.ApiInterface;
import com.rydedispatch.driver.response.RespCreateDriver;
import com.rydedispatch.driver.response.RespGetCompany;
import com.rydedispatch.driver.response.RespSendOTP;
import com.rydedispatch.driver.utils.AppConfig;
import com.rydedispatch.driver.utils.PreferenceHelper;
import com.rydedispatch.driver.utils.Prefs;
import com.rydedispatch.driver.utils.Progress;
import com.rydedispatch.driver.utils.Utils;

import java.io.File;
import java.util.ArrayList;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegistersActivity extends AppCompatActivity {
    private String accept = "";
    private Progress progress;
    private static final String TAG = "RegistersActivity";

    @BindView(R.id.ll_register)
    LinearLayout linearLayout;
    @BindView(R.id.ccp)
    CountryCodePicker ccp;
    @BindView(R.id.edt_phone)
    EditText etNumber;
    @BindView(R.id.edt_first_name)
    EditText edt_first_name;
    @BindView(R.id.edt_last_name)
    EditText edt_last_name;
    @BindView(R.id.edt_email_signup)
    EditText etEmail;
    @BindView(R.id.btnClose)
    Button btnClose;
    @BindView(R.id.tv_policy)
    AppCompatTextView tv_policy;
    @BindView(R.id.spinner_get_company)
    Spinner spinnercompany;
    @BindView(R.id.ll_category)
    LinearLayout ll_category;
    @BindView(R.id.ll_company)
    LinearLayout ll_company;
    @BindView(R.id.edt_category)
    EditText etcategory;
    ArrayList<RespGetCompany.DataEntity> companyList1 = new ArrayList<>();
    ArrayList<String> companyList = new ArrayList<>();


    protected PreferenceHelper helper;

    private File mCurrentPhotoPath;

    private Utils utils;

    private String phoneNo = "", countryCode = "", enterEmail = "", etFirstName = "",
            etlastName = "", token = "", otp = "",
            deviceType = "android", gettoken = "", getDeviceToken = "", drivertype = "", companyId = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        setContentView(R.layout.activity_registers);
        ButterKnife.bind(this);

        btnClose = (Button) findViewById(R.id.btnClose);


        accept = "application/json";

        utils = new Utils(this);
        progress = new Progress(this);
        helper = new PreferenceHelper(this, Prefs.PREF_FILE);

        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Log.d(TAG, "getInstanceId failed " + Objects.requireNonNull(task.getException()).getLocalizedMessage());
                            return;
                        }
                        token = task.getResult().getToken();

                    }
                });

        edt_first_name.setRawInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
        edt_last_name.setRawInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);


        utils = new Utils(RegistersActivity.this);
        utils.hideKeyboard();
        ll_company.setVisibility(View.GONE);

    }


    @OnClick({R.id.ll_register, R.id.btnClose, R.id.tv_policy, R.id.ll_category, R.id.edt_category})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ll_register:
                register();
                break;
            case R.id.btnClose:
                finish();
                break;
            case R.id.tv_policy:
                startActivity(new Intent(RegistersActivity.this, TermsActivity.class));
                break;
            case R.id.ll_category:
                openAlertCategory();
                break;
            case R.id.edt_category:
                openAlertCategory();
                break;
        }
    }

    private void openAlertCategory() {
        openBottomSheet();
    }

    private void openBottomSheet() {
        final Dialog mBottomSheetDialog = new Dialog(RegistersActivity.this, R.style.MaterialDialogSheet);
        mBottomSheetDialog.setContentView(R.layout.dialog_bottom_sheet_category);
        mBottomSheetDialog.setCancelable(false);
        mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        mBottomSheetDialog.getWindow().setGravity(Gravity.BOTTOM);
        mBottomSheetDialog.setCanceledOnTouchOutside(false);
        AppCompatTextView take_individual = mBottomSheetDialog.findViewById(R.id.take_individual);
        AppCompatTextView take_company = mBottomSheetDialog.findViewById(R.id.take_company);

        take_individual.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ll_company.setVisibility(View.GONE);
                etcategory.setText(getString(R.string.res_individual));
                mBottomSheetDialog.dismiss();
                mBottomSheetDialog.cancel();


            }
        });

        take_company.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ll_company.setVisibility(View.VISIBLE);

                etcategory.setText(getString(R.string.res_company));
                mBottomSheetDialog.dismiss();
                mBottomSheetDialog.cancel();

                getCompanyNames();

            }
        });
        mBottomSheetDialog.show();
    }

    private void register() {
        etFirstName = edt_first_name.getText().toString();
        etlastName = edt_last_name.getText().toString();
        enterEmail = etEmail.getText().toString();
        phoneNo = etNumber.getText().toString();
        countryCode = ccp.getSelectedCountryCode();


        if (TextUtils.isEmpty(etFirstName)) {
            edt_first_name.setError("Enter First Name");
            utils.requestFocus(edt_first_name);
            return;
        }
        if (TextUtils.isEmpty(etlastName)) {
            edt_last_name.setError("Enter Last Name");
            utils.requestFocus(edt_last_name);
            return;
        }

        if (enterEmail.isEmpty() || !Utils.isValidEmail(enterEmail)) {
            etEmail.setError("Please Enter Valid Email ID");
            utils.requestFocus(etEmail);
            return;
        }
        if (TextUtils.isEmpty(phoneNo)) {
            etNumber.setError("Enter the Phone No");
            utils.requestFocus(etNumber);
            return;
        }
        if (countryCode.isEmpty()) {
            Toast.makeText(this, "Please select contry code", Toast.LENGTH_SHORT).show();
            return;
        }

        if (etcategory.getText().toString().equals(getString(R.string.res_category))) {
            Toast.makeText(this, "Please Select Category", Toast.LENGTH_SHORT).show();
        } else {
            if (etcategory.getText().toString().equalsIgnoreCase(getString(R.string.res_individual))) {
                drivertype = "individual";

            } else {
                drivertype = "company_driver";
                getCompanyNames();
            }

            Log.d(TAG, "etcategory :" + etcategory.getText().toString());
            Log.d(TAG, "category :" + drivertype);
//            Toast.makeText(this, "Send OTP", Toast.LENGTH_SHORT).show();
            sendOTP(accept);

        }


    }

    private void getCompanyNames() {

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        final Call<RespGetCompany> respGetCompanyCall = apiService.GET_COMPANY(accept);
        respGetCompanyCall.enqueue(new Callback<RespGetCompany>() {
            @Override
            public void onResponse(Call<RespGetCompany> call, Response<RespGetCompany> response) {

                if (response.isSuccessful()) {
                    if (response.body().getMessage().equalsIgnoreCase("Success to get company.")) {
                        companyList = new ArrayList<>();
                        companyList.add("Select Company");


                        for (RespGetCompany.DataEntity.CompanyEntity getcompany : response.body().getData().getCompany()) {
                            companyList.add(getcompany.getCompanyName()/* + " " +getcompany.getPhoneNumber()*/);
                        }
                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(RegistersActivity.this
                                , R.layout.row_auto_complete_category, companyList);

                        spinnercompany.setAdapter(adapter);

                        spinnercompany.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                                if (!parent.getItemAtPosition(position).toString().equalsIgnoreCase("Select Company")) {

                                    companyId = String.valueOf(response.body().getData().getCompany().get(position - 1).getId());

                                    Log.d(TAG, "if category company :" + drivertype);
                                    Log.d(TAG, "if cat :" + companyId);

                                } else {
                                    Log.d(TAG, "else category company :" + drivertype);

                                }
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });

                    } else {
                        Toast.makeText(RegistersActivity.this, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }

            }

            @Override
            public void onFailure(Call<RespGetCompany> call, Throwable t) {
                Log.d(TAG, "get company on failure :" + t.getMessage());
                Log.d(TAG, "get company on failure :" + t.getLocalizedMessage());
            }
        });
    }

    private void sendOTP(String accept) {
        progress.createDialog(false);
        progress.DialogMessage(getString(R.string.please_wait));
        progress.showDialog();

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        RequestBody enterEmail1 = RequestBody.create(MediaType.parse("multipart/form-data"), enterEmail);
        RequestBody phoneNo1 = RequestBody.create(MediaType.parse("multipart/form-data"), phoneNo);
        RequestBody countryCode1 = RequestBody.create(MediaType.parse("multipart/form-data"), countryCode);

        final Call<RespSendOTP> respSendOTPCall = apiService.SEND_OTP_CALL(accept, enterEmail1, phoneNo1, countryCode1);
        respSendOTPCall.enqueue(new Callback<RespSendOTP>() {
            @Override
            public void onResponse(Call<RespSendOTP> call, Response<RespSendOTP> response) {
                progress.hideDialog();

                if (response.isSuccessful()) {
                    if (response.body().getMessage().equalsIgnoreCase("You will receive an SMS with the verification code.")) {
                        progress.hideDialog();

                        openOTPAlert();
                    } else {
                        progress.hideDialog();
                        Toast.makeText(RegistersActivity.this, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<RespSendOTP> call, Throwable t) {
                progress.hideDialog();
                Toast.makeText(RegistersActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void openOTPAlert() {
        final Dialog mBottomSheetDialog = new Dialog(this, R.style.MaterialDialogSheet);
        mBottomSheetDialog.setContentView(R.layout.dialog_otp_alert);
        mBottomSheetDialog.setCancelable(false);
        mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        mBottomSheetDialog.getWindow().setGravity(Gravity.CENTER);
        mBottomSheetDialog.setCanceledOnTouchOutside(false);
        Button btn_resend = mBottomSheetDialog.findViewById(R.id.btn_resend);
        Button btn_submit = mBottomSheetDialog.findViewById(R.id.btn_submit);
        final EditText et_otp = mBottomSheetDialog.findViewById(R.id.et_otp);
        ImageButton ib_close = mBottomSheetDialog.findViewById(R.id.ib_close);
        otp = et_otp.getText().toString();


        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (et_otp.getText().toString().isEmpty()) {
                    et_otp.setError(getString(R.string.msg_error_phone));
                    return;
                }

                if (drivertype.equalsIgnoreCase("individual")) {
                    createDriver(accept, etFirstName, etlastName, etEmail.getText().toString(),
                            phoneNo, countryCode, et_otp.getText().toString(), deviceType, token, drivertype);
                } else {
                    createCompanyDriver(accept, etFirstName, etlastName, etEmail.getText().toString(),
                            phoneNo, countryCode, et_otp.getText().toString(), deviceType, token, drivertype, companyId);
                }

                Log.d(TAG, "category final : " + drivertype);
                Log.d(TAG, "category companyId : " + companyId);
                mBottomSheetDialog.dismiss();
            }
        });
        btn_resend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendOTP(accept);
                mBottomSheetDialog.dismiss();
            }
        });

        ib_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBottomSheetDialog.dismiss();
            }
        });
        mBottomSheetDialog.show();
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

    }

    private void createCompanyDriver(String accept, String etFirstName,
                                     String etlastName, String etEmail,
                                     String phoneNo, String countryCode, String et_otp,
                                     String deviceType, String token, String drivertype, String companyId) {

        progress.createDialog(false);
        progress.DialogMessage(getString(R.string.please_wait));
        progress.showDialog();


        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        RequestBody etFirstName1 = RequestBody.create(MediaType.parse("multipart/form-data"), etFirstName);
        RequestBody etLastName1 = RequestBody.create(MediaType.parse("multipart/form-data"), etlastName);
        RequestBody etEmail1 = RequestBody.create(MediaType.parse("multipart/form-data"), etEmail);
        RequestBody phoneNo1 = RequestBody.create(MediaType.parse("multipart/form-data"), phoneNo);
        RequestBody countryCode1 = RequestBody.create(MediaType.parse("multipart/form-data"), countryCode);
        RequestBody et_otp1 = RequestBody.create(MediaType.parse("multipart/form-data"), et_otp);
        RequestBody deviceType1 = RequestBody.create(MediaType.parse("multipart/form-data"), deviceType);
        RequestBody token1 = RequestBody.create(MediaType.parse("multipart/form-data"), token);
        RequestBody drivertype1 = RequestBody.create(MediaType.parse("multipart/form-data"), drivertype);
        RequestBody companyId1 = RequestBody.create(MediaType.parse("multipart/form-data"), companyId);


        final Call<RespCreateDriver> respSendOTPCall = apiService.CREATE_COMPANY_DRIVER(accept,
                etFirstName1, etLastName1, etEmail1, phoneNo1, countryCode1, et_otp1, deviceType1, token1,
                drivertype1, companyId1);
        respSendOTPCall.enqueue(new Callback<RespCreateDriver>() {
            @Override
            public void onResponse(Call<RespCreateDriver> call, Response<RespCreateDriver> response) {
                progress.hideDialog();

                if (response.body().getMessage().contains("Driver has been Registered successfully.")) {

                    gettoken = response.body().getData().getUserDetails().getToken();

                    startActivity(new Intent(RegistersActivity.this, AddAddressActivity.class)
                            .putExtra(AppConfig.BUNDLE.Token, gettoken));

                    helper.initPref();
                    helper.SaveBooleanPref(Prefs.USER_LOGIN, true);
                    helper.SaveStringPref(Prefs.token, response.body().getData().getUserDetails().getToken());
                    helper.ApplyPref();

                    Intent intent = new Intent(RegistersActivity.this, AddAddressActivity.class);
                    intent.putExtra(AppConfig.BUNDLE.Token, response.body().getData().getUserDetails().getToken());
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    finish();
                } else {
                    progress.hideDialog();

                    Toast.makeText(RegistersActivity.this, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<RespCreateDriver> call, Throwable t) {
                progress.hideDialog();
                Toast.makeText(RegistersActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void createDriver(String accept, String etFirstName,
                              String etlastName, String etEmail,
                              String phoneNo, String countryCode,
                              String et_otp, String deviceType, String token, String drivertype) {
        progress.createDialog(false);
        progress.DialogMessage(getString(R.string.please_wait));
        progress.showDialog();


        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        RequestBody etFirstName1 = RequestBody.create(MediaType.parse("multipart/form-data"), etFirstName);
        RequestBody etLastName1 = RequestBody.create(MediaType.parse("multipart/form-data"), etlastName);
        RequestBody etEmail1 = RequestBody.create(MediaType.parse("multipart/form-data"), etEmail);
        RequestBody phoneNo1 = RequestBody.create(MediaType.parse("multipart/form-data"), phoneNo);
        RequestBody countryCode1 = RequestBody.create(MediaType.parse("multipart/form-data"), countryCode);
        RequestBody et_otp1 = RequestBody.create(MediaType.parse("multipart/form-data"), et_otp);
        RequestBody deviceType1 = RequestBody.create(MediaType.parse("multipart/form-data"), deviceType);
        RequestBody token1 = RequestBody.create(MediaType.parse("multipart/form-data"), token);
        RequestBody drivertype1 = RequestBody.create(MediaType.parse("multipart/form-data"), drivertype);


        final Call<RespCreateDriver> respSendOTPCall = apiService.CREATE_DRIVER(accept,
                etFirstName1, etLastName1, etEmail1, phoneNo1, countryCode1, et_otp1, deviceType1, token1, drivertype1);
        respSendOTPCall.enqueue(new Callback<RespCreateDriver>() {
            @Override
            public void onResponse(Call<RespCreateDriver> call, Response<RespCreateDriver> response) {
                progress.hideDialog();

                if (response.body().getMessage().contains("Driver has been Registered successfully.")) {

                    gettoken = response.body().getData().getUserDetails().getToken();

                    startActivity(new Intent(RegistersActivity.this, AddAddressActivity.class)
                            .putExtra(AppConfig.BUNDLE.Token, gettoken));

                    helper.initPref();
                    helper.SaveBooleanPref(Prefs.USER_LOGIN, true);
                    helper.SaveStringPref(Prefs.id, String.valueOf(response.body().getData().getUserDetails().getId()));
                    helper.SaveStringPref(Prefs.parent_id, String.valueOf(response.body().getData().getUserDetails().getParentId()));
                    helper.SaveStringPref(Prefs.company_id, String.valueOf(response.body().getData().getUserDetails().getCompanyId()));
                    helper.SaveStringPref(Prefs.first_name, response.body().getData().getUserDetails().getFirstName());
                    helper.SaveStringPref(Prefs.last_name, response.body().getData().getUserDetails().getLastName());
                    helper.SaveStringPref(Prefs.company_name, response.body().getData().getUserDetails().getCompanyName());
                    helper.SaveStringPref(Prefs.company_image, String.valueOf(response.body().getData().getUserDetails().getCompanyImage()));
                    helper.SaveStringPref(Prefs.contact_person, response.body().getData().getUserDetails().getContactPerson());
                    helper.SaveStringPref(Prefs.profile_picture, String.valueOf(response.body().getData().getUserDetails().getProfilePicture()));
                    helper.SaveStringPref(Prefs.email, response.body().getData().getUserDetails().getEmail());
                    helper.SaveStringPref(Prefs.email_verified_at, String.valueOf(response.body().getData().getUserDetails().getEmailVerifiedAt()));
                    helper.SaveStringPref(Prefs.phone_number, response.body().getData().getUserDetails().getPhoneNumber());
                    helper.SaveStringPref(Prefs.address, response.body().getData().getUserDetails().getAddress());
                    helper.SaveStringPref(Prefs.country, response.body().getData().getUserDetails().getCountry());
                    helper.SaveStringPref(Prefs.state, response.body().getData().getUserDetails().getState());
                    helper.SaveStringPref(Prefs.city, response.body().getData().getUserDetails().getCity());
                    helper.SaveStringPref(Prefs.deviceToken, response.body().getData().getUserDetails().getDeviceToken());
                    helper.SaveStringPref(Prefs.devicetype, response.body().getData().getUserDetails().getDeviceType());
                    helper.SaveStringPref(Prefs.latitude, response.body().getData().getUserDetails().getLatitude());
                    helper.SaveStringPref(Prefs.longitude, response.body().getData().getUserDetails().getLongitude());
                    helper.SaveStringPref(Prefs.status, response.body().getData().getUserDetails().getStatus());
                    helper.SaveStringPref(Prefs.userstatus, response.body().getData().getUserDetails().getUserStatus());
                    helper.SaveStringPref(Prefs.authy_id, response.body().getData().getUserDetails().getAuthyId());
                    helper.SaveStringPref(Prefs.user_unique_code, response.body().getData().getUserDetails().getUserUniqueCode());
                    helper.SaveStringPref(Prefs.taxId, String.valueOf(response.body().getData().getUserDetails().getTaxId()));
                    helper.SaveStringPref(Prefs.commission_type, response.body().getData().getUserDetails().getCommissionType());
                    helper.SaveStringPref(Prefs.commission, String.valueOf(response.body().getData().getUserDetails().getCommission()));
                    helper.SaveStringPref(Prefs.role, response.body().getData().getUserDetails().getRole());
                    helper.SaveStringPref(Prefs.documentstatus, response.body().getData().getUserDetails().getDocumentStatus());
                    helper.SaveStringPref(Prefs.deleted_at, String.valueOf(response.body().getData().getUserDetails().getDeletedAt()));
                    helper.SaveStringPref(Prefs.created_at, response.body().getData().getUserDetails().getCreatedAt());
                    helper.SaveStringPref(Prefs.updated_at, response.body().getData().getUserDetails().getUpdatedAt());
                    helper.SaveStringPref(Prefs.token, response.body().getData().getUserDetails().getToken());
                    helper.ApplyPref();

                    Intent intent = new Intent(RegistersActivity.this, AddAddressActivity.class);
                    intent.putExtra(AppConfig.BUNDLE.Token, response.body().getData().getUserDetails().getToken());
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    finish();
                } else {
                    progress.hideDialog();

                    Toast.makeText(RegistersActivity.this, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<RespCreateDriver> call, Throwable t) {
                progress.hideDialog();
                Toast.makeText(RegistersActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }


}
