package com.rydedispatch.driver.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.rydedispatch.driver.R;
import com.rydedispatch.driver.network.ApiClient;
import com.rydedispatch.driver.network.ApiInterface;
import com.rydedispatch.driver.response.RespUpdateAvailability;
import com.rydedispatch.driver.utils.AppConfig;
import com.rydedispatch.driver.utils.PreferenceHelper;
import com.rydedispatch.driver.utils.Prefs;
import com.rydedispatch.driver.utils.Progress;
import com.rydedispatch.driver.utils.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class StatusChangeActivity extends AppCompatActivity {
    private static final String TAG = StatusChangeActivity.class.getSimpleName();

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.tvTitle)
    AppCompatTextView tvTitle;
    /*@BindView(R.id.switch1)
    Switch switch1;*/
    @BindView(R.id.iv_driver)
    AppCompatImageView ivDriver;
    @BindView(R.id.tv_on_duty)
    AppCompatTextView tvonduty;
    @BindView(R.id.ll_status)
    LinearLayout ll_status;
    @BindView(R.id.tv_status)
    AppCompatTextView tv_status;
    @BindView(R.id.tv_status_change)
    TextView tv_status_change;

    private String userStatus = "", accept = "",
            authorization = "", displaypassengername = "";
    private Bundle bundle;
    private boolean changeStatus;
    protected PreferenceHelper helper;

    protected Progress progress;
    private Utils utils;

    @SuppressLint({"RestrictedApi", "NewApi"})
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_status_change);
        ButterKnife.bind(this);

        tvTitle.setText(getString(R.string.res_status));

        bundle = getIntent().getExtras();
        if (bundle != null) {
            userStatus = bundle.getString(AppConfig.BUNDLE.UserStatus);
            displaypassengername = bundle.getString(AppConfig.BUNDLE.display_passenger_name);
        }

        Log.d(TAG, "user status :" + userStatus);

        helper = new PreferenceHelper(this, Prefs.PREF_FILE);
        progress = new Progress(this);
        utils = new Utils(this);
        accept = "application/json";
        authorization = "Bearer " + helper.LoadStringPref(Prefs.token, "");



        if (userStatus.equalsIgnoreCase("online")) {
            ivDriver.setBackgroundDrawable(getResources().getDrawable(R.drawable.online_driver));
            tvonduty.setText("Current Status");
            tvonduty.setTextColor(getResources().getColor(R.color.green1));

            tv_status.setText(userStatus);
            tv_status_change.setText("GO OFFLINE");
        }
        else {
            ivDriver.setBackgroundDrawable(getResources().getDrawable(R.drawable.offline_driver));
            tvonduty.setText("Current Status");
            tvonduty.setTextColor(getResources().getColor(R.color.icons_8_muted_red));

            tv_status.setText(userStatus);
            tv_status_change.setText("GO ONLINE");
        }

    }

    @OnClick({R.id.iv_back,R.id.ll_status})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
//                Intent intent = new Intent(StatusChangeActivity.this, MainActivity.class);
//                startActivity(intent);
                finish();
                break;
            case R.id.ll_status:

                if (userStatus.equalsIgnoreCase("online")) {
                    getUpdateAvailability(accept, authorization, "offline");
                } else {
                    getUpdateAvailability(accept, authorization, "online");

                }
                break;
        }
    }


    private void getUpdateAvailability(String accept, String authorization, String offline) {

        progress.createDialog(true);
        progress.DialogMessage(getString(R.string.please_wait));
        progress.showDialog();

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        RequestBody offline1 = RequestBody.create(MediaType.parse("multipart/form-data"), offline);
        final Call<RespUpdateAvailability> respDashboardDriverCall = apiService.UPDATE_AVAILABILITY_CALL(accept,
                authorization, offline1);
        respDashboardDriverCall.enqueue(new Callback<RespUpdateAvailability>() {
            @Override
            public void onResponse(Call<RespUpdateAvailability> call, Response<RespUpdateAvailability> response) {
                progress.hideDialog();

                if (response.body().getMessage().equalsIgnoreCase("Status has been updated successfully")) {

                    Toast.makeText(StatusChangeActivity.this, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(StatusChangeActivity.this, MainActivity.class);
                    startActivity(intent);

                    Log.e(TAG, "response driver: " + new Gson().toJson(response.body()));
                    Log.d(TAG, "response user status: " + userStatus);
                } else {
                    Toast.makeText(StatusChangeActivity.this, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<RespUpdateAvailability> call, Throwable t) {
                progress.hideDialog();

                Toast.makeText(StatusChangeActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
