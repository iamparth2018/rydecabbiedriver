package com.rydedispatch.driver.activity;

import android.Manifest;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.rydedispatch.driver.R;
import com.rydedispatch.driver.network.ApiClient;
import com.rydedispatch.driver.network.ApiInterface;
import com.rydedispatch.driver.response.RespForceDashboard;
import com.rydedispatch.driver.response.RespGetProfileDetails;
import com.rydedispatch.driver.response.RespLogoutDriver;
import com.rydedispatch.driver.response.RespUpdateProfile;
import com.rydedispatch.driver.utils.AppConfig;
import com.rydedispatch.driver.utils.PreferenceHelper;
import com.rydedispatch.driver.utils.Prefs;
import com.rydedispatch.driver.utils.Progress;
import com.rydedispatch.driver.utils.Utility;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;
import pl.aprilapps.easyphotopicker.EasyImageConfig;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileActivity extends AppCompatActivity {

    private static final String TAG = ProfileActivity.class.getSimpleName();

    @BindView(R.id.rl_main)
    RelativeLayout rlmain;
    @BindView(R.id.circular_progress_bar)
    ProgressBar pbar;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.iv_back)
    ImageView iv_back;
    @BindView(R.id.et_firstname)
    EditText edt_first_name;
    @BindView(R.id.et_lastname)
    EditText edt_last_name;
    @BindView(R.id.et_phone)
    EditText etphone;
    @BindView(R.id.et_email)
    EditText etemail;

    @BindView(R.id.profile_image)
    CircleImageView profilePic;
    @BindView(R.id.ib_edit)
    ImageView imageEdit;
    @BindView(R.id.ib_edit_number)
    ImageButton ibEditNumber;
    @BindView(R.id.ll_sign_out)
    LinearLayout llSignOut;
    @BindView(R.id.ll_save)
    LinearLayout llsave;
    @BindView(R.id.rl_layout)
    RelativeLayout rl_layout;
    private File mCurrentPhoto;
    private String accept = "", authorization = "",
            imageURL = "", firstname = "", lastname = "",
            email = "", picturePath = "", userChoosenTask = "", myProfile = "";
    protected PreferenceHelper helper;
    protected Progress progress;
    boolean clicked = false;
    private int REQUEST_CAMERA = 0, SELECT_FILE = 1;
    private Uri selectedImage;


    public static final int MY_PERMISSIONS_REQUEST_CAMERA = 100;
    public static final String ALLOW_KEY = "ALLOWED";
    public static final String CAMERA_PREF = "camera_pref";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        ButterKnife.bind(this);


        helper = new PreferenceHelper(this, Prefs.PREF_FILE);
        progress = new Progress(this);

        accept = "application/json";
        authorization = "Bearer " + helper.LoadStringPref(Prefs.token, "");

        edt_first_name.setRawInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
        edt_last_name.setRawInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);

        getProfileDetails(accept, authorization);

        profilePic = (CircleImageView) findViewById(R.id.profile_image);
    }


    @OnClick({R.id.iv_back, R.id.ib_edit,
            R.id.ll_sign_out, R.id.ll_save,
            R.id.ib_edit_number, R.id.profile_image, R.id.rl_main})
    public void onViewClicked(View view) {
        switch (view.getId()) {

            case R.id.rl_main:
                clicked = false;
                break;
            case R.id.iv_back:

//                startActivity(new Intent(ProfileActivity.this, MainActivity.class));
                finish();
                break;
            case R.id.ib_edit:

                if (ContextCompat.checkSelfPermission(this,
                        Manifest.permission.CAMERA)
                        != PackageManager.PERMISSION_GRANTED) {

                    if (getFromPref(this, ALLOW_KEY)) {

                        showSettingsAlert();

                    } else if (ContextCompat.checkSelfPermission(this,
                            Manifest.permission.CAMERA)
                            != PackageManager.PERMISSION_GRANTED) {
                        // Should we show an explanation?
                        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                                Manifest.permission.CAMERA)) {
                            showAlert();
                        } else {
                            // No explanation needed, we can request the permission.
                            ActivityCompat.requestPermissions(this,
                                    new String[]{Manifest.permission.CAMERA},
                                    MY_PERMISSIONS_REQUEST_CAMERA);
                        }
                    }
                } else {
                    openCamera();
//                    selectImage();
                }
//                selectImage();
//                    openCamera();
                break;
            case R.id.ll_sign_out:
                openAlertLogout();
                break;
            case R.id.ll_save:

                if (mCurrentPhoto == null) {

                    saveDetailsWithoutProfile(edt_first_name.getText().toString(), edt_last_name.getText().toString(),
                            etemail.getText().toString());
                } else {
                    saveDetails(edt_first_name.getText().toString(), edt_last_name.getText().toString(),
                            etemail.getText().toString(), mCurrentPhoto);
                }


                Log.d(TAG, "image upload" + mCurrentPhoto);
                break;
            case R.id.ib_edit_number:
                clicked = true;
                if (clicked) {
                    etphone.setClickable(true);
                    etphone.setEnabled(true);
                    etphone.setFocusable(true);
                } else {
                    etphone.setClickable(false);
                    etphone.setEnabled(false);
                    etphone.setFocusable(false);
                    etphone.setKeyListener(null);
                }
                myProfile = helper.LoadStringPref(Prefs.profile_picture, myProfile);

                Log.d(TAG, "get this image on dashboard: " + myProfile);
                break;
            case R.id.profile_image:
//                openCamera();
//                selectImage();

                if (ContextCompat.checkSelfPermission(this,
                        Manifest.permission.CAMERA)
                        != PackageManager.PERMISSION_GRANTED) {

                    if (getFromPref(this, ALLOW_KEY)) {

                        showSettingsAlert();

                    } else if (ContextCompat.checkSelfPermission(this,
                            Manifest.permission.CAMERA)
                            != PackageManager.PERMISSION_GRANTED) {
                        // Should we show an explanation?
                        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                                Manifest.permission.CAMERA)) {
                            showAlert();
                        } else {
                            // No explanation needed, we can request the permission.
                            ActivityCompat.requestPermissions(this,
                                    new String[]{Manifest.permission.CAMERA},
                                    MY_PERMISSIONS_REQUEST_CAMERA);
                        }
                    }
                } else {
                    openCamera();
                }
                break;
        }
    }


    private void openExternalCamera() {
//        Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
//        startActivity(intent);
        EasyImage.openCamera(ProfileActivity.this, EasyImageConfig.REQ_TAKE_PICTURE);

    }

    private void showAlert() {
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle("Alert");
        alertDialog.setMessage("App needs to access the Camera.");
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "DONT ALLOW",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        finish();
                    }
                });
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "ALLOW",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        ActivityCompat.requestPermissions(ProfileActivity.this,
                                new String[]{Manifest.permission.CAMERA},
                                MY_PERMISSIONS_REQUEST_CAMERA);
                        openCamera();

                    }
                });
        alertDialog.show();
    }

    @Override
    public void onRequestPermissionsResult
            (int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_CAMERA: {
                for (int i = 0, len = permissions.length; i < len; i++) {
                    String permission = permissions[i];
                    if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                        boolean showRationale =
                                ActivityCompat.shouldShowRequestPermissionRationale
                                        (this, permission);
                        if (showRationale) {
                            showAlert();
                        } else if (!showRationale) {
                            // user denied flagging NEVER ASK AGAIN
                            // you can either enable some fall back,
                            // disable features of your app
                            // or open another dialog explaining
                            // again the permission and directing to
                            // the app setting
                            saveToPreferences(ProfileActivity.this, ALLOW_KEY, true);
                        }
                    }
                }
            }
        }
    }


    public static void saveToPreferences(Context context, String key,
                                         Boolean allowed) {
        SharedPreferences myPrefs = context.getSharedPreferences
                (CAMERA_PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = myPrefs.edit();
        prefsEditor.putBoolean(key, allowed);
        prefsEditor.commit();
    }

    private void showSettingsAlert() {
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle("Alert");
        alertDialog.setMessage("App needs to access the Camera.");
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "DONT ALLOW",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        //finish();
                    }
                });
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "SETTINGS",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        startInstalledAppDetailsActivity(ProfileActivity.this);

                    }
                });
        alertDialog.show();
    }

    public static void startInstalledAppDetailsActivity(final Activity context) {
        if (context == null) {
            return;
        }
        final Intent i = new Intent();
        i.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        i.addCategory(Intent.CATEGORY_DEFAULT);
        i.setData(Uri.parse("package:" + context.getPackageName()));
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        i.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
        context.startActivity(i);
    }


    public static Boolean getFromPref(Context context, String key) {
        SharedPreferences myPrefs = context.getSharedPreferences
                (CAMERA_PREF, Context.MODE_PRIVATE);
        return (myPrefs.getBoolean(key, false));
    }


    private void openCamera() {
        openBottomSheet();
    }


    private void openBottomSheet() {
        final Dialog mBottomSheetDialog = new Dialog(ProfileActivity.this, R.style.MaterialDialogSheet);
        mBottomSheetDialog.setContentView(R.layout.dialog_bottom_sheet);
        mBottomSheetDialog.setCancelable(true);
        mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        mBottomSheetDialog.getWindow().setGravity(Gravity.BOTTOM);
        mBottomSheetDialog.setCanceledOnTouchOutside(true);
        AppCompatTextView take_picture = mBottomSheetDialog.findViewById(R.id.take_picture);
        AppCompatTextView Open_gallary = mBottomSheetDialog.findViewById(R.id.open_gallary);

        take_picture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                EasyImage.openCamera(ProfileActivity.this, EasyImageConfig.REQ_TAKE_PICTURE);
                mBottomSheetDialog.dismiss();
            }
        });

        Open_gallary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EasyImage.openGallery(ProfileActivity.this, EasyImageConfig.REQ_PICK_PICTURE_FROM_GALLERY);
                mBottomSheetDialog.dismiss();
            }
        });
        mBottomSheetDialog.show();
    }


    private void saveDetailsWithoutProfile(String firstname, String lastname, String email) {
        if (TextUtils.isEmpty(firstname)) {
            Toast.makeText(this, "Enter First Name", Toast.LENGTH_SHORT).show();
            return;
        }
        if (TextUtils.isEmpty(lastname)) {
            Toast.makeText(this, "Enter Last Name", Toast.LENGTH_SHORT).show();
            return;
        }

        if (TextUtils.isEmpty(email)) {
            Toast.makeText(this, "Enter Email", Toast.LENGTH_SHORT).show();
            return;
        }
        progress.createDialog(false);
        progress.DialogMessage(getString(R.string.save_details));
        progress.showDialog();

        Log.e(TAG, "saveProfile: " + picturePath);

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        RequestBody fname1 = RequestBody.create(MediaType.parse("multipart/form-data"), firstname);
        RequestBody l_name1 = RequestBody.create(MediaType.parse("multipart/form-data"), lastname);
        RequestBody email1 = RequestBody.create(MediaType.parse("multipart/form-data"), email);


        final Call<RespUpdateProfile> respGetProfileDetailsCall = apiService.UPDATE_PROFILE_CALL1(accept, authorization,
                fname1, l_name1, email1);
        respGetProfileDetailsCall.enqueue(new Callback<RespUpdateProfile>() {
            @Override
            public void onResponse(Call<RespUpdateProfile> call, Response<RespUpdateProfile> response) {
                progress.hideDialog();
                Log.d(TAG, "respUpdateProfile : " + new Gson().toJson(response.body().getData()));


                if (response.isSuccessful()) {

                    if (response.body().getMessage().equalsIgnoreCase("Profile updated successfully.")) {
                        Snackbar.make(rl_layout, "" + response.body().getMessage(),
                                Snackbar.LENGTH_SHORT).show();
                        helper.initPref();
                        helper.SaveStringPref(Prefs.profile_picture, response.body().getData().getProfilePicture());
                        helper.SaveStringPref(Prefs.first_name, response.body().getData().getFirstName());
                        helper.SaveStringPref(Prefs.last_name, response.body().getData().getLastName());
                        helper.SaveStringPref(Prefs.email, response.body().getData().getEmail());
                        helper.SaveStringPref(Prefs.city, response.body().getData().getCity());
                        helper.ApplyPref();
                    } else {
                        Snackbar.make(rl_layout, "" + response.body().getMessage(),
                                Snackbar.LENGTH_SHORT).show();
                    }

                }


            }

            @Override
            public void onFailure(Call<RespUpdateProfile> call, Throwable t) {
                progress.hideDialog();
                Snackbar.make(rl_layout, "" + t.getMessage(),
                        Snackbar.LENGTH_SHORT).show();
                Log.d(TAG, "response error :" + t.getMessage());
                Log.d(TAG, "response error :" + t.getLocalizedMessage());
            }
        });

    }

    private void saveDetails(String firstname, String lastname, String email, File mCurrentPhoto) {
        if (TextUtils.isEmpty(firstname)) {
            Toast.makeText(this, "Enter First Name", Toast.LENGTH_SHORT).show();
            return;
        }
        if (TextUtils.isEmpty(lastname)) {
            Toast.makeText(this, "Enter Last Name", Toast.LENGTH_SHORT).show();
            return;
        }

        if (TextUtils.isEmpty(email)) {
            Toast.makeText(this, "Enter Email", Toast.LENGTH_SHORT).show();
            return;
        }

        progress.createDialog(false);
        progress.DialogMessage(getString(R.string.save_details));
        progress.showDialog();


        //other upload image
//        MultipartBody.Part filePart = null;
//        if (!picturePath.isEmpty()) {
//            File file = new File(picturePath);
//            filePart = MultipartBody.Part.createFormData("profile_picture", file.getName(), RequestBody.create(MediaType.parse("image/*"), file));
//        }
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        RequestBody fname1 = RequestBody.create(MediaType.parse("multipart/form-data"), firstname);
        RequestBody l_name1 = RequestBody.create(MediaType.parse("multipart/form-data"), lastname);
        RequestBody email1 = RequestBody.create(MediaType.parse("multipart/form-data"), email);

        RequestBody fbody;
        String filename = "";
        if (mCurrentPhoto == null) {
            fbody = RequestBody.create(MediaType.parse("image/*"), "");
        } else {
            fbody = RequestBody.create(MediaType.parse("image/*"), mCurrentPhoto);
            filename = mCurrentPhoto.getName();
        }

        MultipartBody.Part body = MultipartBody.Part.createFormData("profile_picture", filename, fbody);

        final Call<RespUpdateProfile> respGetProfileDetailsCall = apiService.UPDATE_PROFILE_CALL(accept, authorization,
                fname1, l_name1, email1, body);
        respGetProfileDetailsCall.enqueue(new Callback<RespUpdateProfile>() {
            @Override
            public void onResponse(Call<RespUpdateProfile> call, Response<RespUpdateProfile> response) {
                progress.hideDialog();
                Log.d(TAG, "respUpdateProfile : " + new Gson().toJson(response.body().getData()));

                if (response.isSuccessful()) {
                    if (response.body().getMessage().equalsIgnoreCase("Profile updated successfully.")) {
                        Snackbar.make(rl_layout, "" + response.body().getMessage(),
                                Snackbar.LENGTH_SHORT).show();
                        helper.initPref();
                        helper.SaveStringPref(Prefs.profile_picture, response.body().getData().getProfilePicture());
                        helper.SaveStringPref(Prefs.first_name, response.body().getData().getFirstName());
                        helper.SaveStringPref(Prefs.last_name, response.body().getData().getLastName());
                        helper.SaveStringPref(Prefs.email, response.body().getData().getEmail());
                        helper.SaveStringPref(Prefs.city, response.body().getData().getCity());
                        helper.ApplyPref();
//                    finish();
//                    recreate();

                    } else {
                        Snackbar.make(rl_layout, "" + response.body().getMessage(),
                                Snackbar.LENGTH_SHORT).show();
                    }
                }
                else {
                    Snackbar.make(rl_layout, "" + response.body().getMessage(),
                            Snackbar.LENGTH_SHORT).show();
                }


            }

            @Override
            public void onFailure(Call<RespUpdateProfile> call, Throwable t) {
                progress.hideDialog();
                Snackbar.make(rl_layout, "" + t.getMessage(),
                        Snackbar.LENGTH_SHORT).show();
                Log.d(TAG, "response error :" + t.getMessage());
                Log.d(TAG, "response error :" + t.getLocalizedMessage());
            }
        });
    }


    private void openAlertLogout() {
        final Dialog mBottomSheetDialog = new Dialog(this, R.style.MaterialDialogSheet);
        mBottomSheetDialog.setContentView(R.layout.dialog_logout_alert);
        mBottomSheetDialog.setCancelable(false);
        mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        mBottomSheetDialog.getWindow().setGravity(Gravity.CENTER);
        mBottomSheetDialog.setCanceledOnTouchOutside(false);
        LinearLayout ll_no = mBottomSheetDialog.findViewById(R.id.ll_no);
        LinearLayout ll_yes = mBottomSheetDialog.findViewById(R.id.ll_yes);

        ll_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBottomSheetDialog.dismiss();

                logoutDriver(authorization);


            }
        });
        ll_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                mBottomSheetDialog.dismiss();
            }
        });
        mBottomSheetDialog.show();

    }

    private void logoutDriver(String authorization) {

        progress.createDialog(false);
        progress.DialogMessage(getString(R.string.msg_logout));
        progress.showDialog();


        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        final Call<RespLogoutDriver> respLogoutDriverCall = apiService.LOGOUT_DRIVER(authorization);
        respLogoutDriverCall.enqueue(new Callback<RespLogoutDriver>() {
            @Override
            public void onResponse(Call<RespLogoutDriver> call, Response<RespLogoutDriver> response) {

                AsyncTask.execute(new Runnable() {
                    @Override
                    public void run() {

                        try {
                            FirebaseInstanceId.getInstance().deleteInstanceId();
                            helper.clearAllPrefs();
                            startActivity(new Intent(ProfileActivity.this, LoginActivity.class)
                                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                            progress.hideDialog();
                            ProfileActivity.this.finish();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                });


            }

            @Override
            public void onFailure(Call<RespLogoutDriver> call, Throwable t) {
                progress.hideDialog();
                Toast.makeText(ProfileActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }


    /*@Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE) {
                selectedImage = data.getData();
                profilePic.setImageURI(selectedImage);
                Log.e(TAG, "onActivityResult: " + selectedImage);
                picturePath = getPath(ProfileActivity.this.getApplicationContext(), selectedImage);
//                onSelectFromGalleryResult(data);
                Log.d(TAG, "selectedImage :" + SELECT_FILE);
                Log.d(TAG, "selectedImage :" + selectedImage);
            } else if (requestCode == REQUEST_CAMERA) {

                onCaptureImageResult(data);

            }
        }

    }*/

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        EasyImage.handleActivityResult(requestCode, resultCode, data, ProfileActivity.this, new DefaultCallback() {
            @Override
            public void onImagePickerError(Exception e, EasyImage.ImageSource source, int type) {
                //Some error handling
                Toast.makeText(ProfileActivity.this, "Error " + e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();

                if (source == EasyImage.ImageSource.CAMERA) {
                    File photoFile = EasyImage.lastlyTakenButCanceledPhoto(ProfileActivity.this);
                    if (photoFile != null) photoFile.delete();
                }
            }

            @Override
            public void onImagePicked(File imageFile, EasyImage.ImageSource source, int type) {

                if (source == EasyImage.ImageSource.CAMERA) {

                    Picasso.with(ProfileActivity.this)
                            .load(imageFile)
                            .placeholder(R.mipmap.ic_launcher)
                            .error(R.drawable.dummy_pic)
                            .into(profilePic);
                    mCurrentPhoto = imageFile;

                } else if (source == EasyImage.ImageSource.GALLERY) {

                    Picasso.with(ProfileActivity.this)
                            .load(imageFile)
                            .placeholder(R.mipmap.ic_launcher)
                            .error(R.drawable.dummy_pic)
                            .into(profilePic);
                    mCurrentPhoto = imageFile;

                }
            }
        });
    }

    private String getPath(Context context, Uri selectedImage) {
        String result = null;
        String[] proj = {MediaStore.Images.Media.DATA};
        Cursor cursor = context.getContentResolver().query(selectedImage, proj, null, null, null);
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                int column_index = cursor.getColumnIndexOrThrow(proj[0]);
                result = cursor.getString(column_index);
            }
            cursor.close();
        }
        if (result == null) {
            result = "Not found";
        }
        return result;
    }

    private void onCaptureImageResult(Intent data) {
        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");

        selectedImage = getImageUri(ProfileActivity.this, thumbnail);
        profilePic.setImageURI(selectedImage);
        picturePath = getPath(ProfileActivity.this.getApplicationContext(), selectedImage);

    }

    private Uri getImageUri(Context context, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(context.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    private void selectImage() {
        final CharSequence[] items = {"Take Photo", "Choose from Library", "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(ProfileActivity.this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                boolean result = Utility.checkPermission(ProfileActivity.this);

                if (items[item].equals("Take Photo")) {
                    userChoosenTask = "Take Photo";
                    if (result)
                        cameraIntent();
                } else if (items[item].equals("Choose from Library")) {
                    userChoosenTask = "Choose from Library";
                    if (result)
                        galleryIntent();

                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void cameraIntent() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_CAMERA);
    }


    private void galleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
    }


    private void getProfileDetails(String accept, String authorization) {

//        progress.createDialog(false);
//        progress.DialogMessage(getString(R.string.please_wait));
//        progress.showDialog();

        pbar.setVisibility(View.VISIBLE);
        ObjectAnimator anim = ObjectAnimator.ofInt(pbar, "progress", 0, 100);
        anim.setDuration(15000);
        anim.setInterpolator(new DecelerateInterpolator());
        anim.start();

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        final Call<RespGetProfileDetails> respGetProfileDetailsCall = apiService.PROFILE_DASHBOARD(accept, authorization);
        respGetProfileDetailsCall.enqueue(new Callback<RespGetProfileDetails>() {
            @Override
            public void onResponse(Call<RespGetProfileDetails> call, Response<RespGetProfileDetails> response) {
//                progress.hideDialog();
                pbar.setVisibility(View.GONE);
                rlmain.setVisibility(View.VISIBLE);
                try {

                    Log.d(TAG, "profile :" + response.body().getMessage());
                    final RespGetProfileDetails profileDetailsResponce = response.body();


                    if (response.body().getMessage().contains("Unauthenticated")) {
                        openLogOut();
                        return;
                    }

                    if (response.body().getData().getUserDetails().getProfilePicture() == null ||
                            response.body().getData().getUserDetails().getProfilePicture().equals("")) {
                        profilePic.setImageResource(R.drawable.dummy_pic);
                    } else {

                        if (myProfile == null) {

                            Picasso.with(ProfileActivity.this)
                                    .load(R.mipmap.ic_launcher)
                                    .placeholder(R.mipmap.ic_launcher)
                                    .error(R.drawable.dummy_pic)
                                    .into(profilePic);

                        } else if (myProfile.contains("https://")) {

                            Picasso.with(ProfileActivity.this)
                                    .load(R.mipmap.ic_launcher)
                                    .placeholder(R.mipmap.ic_launcher)
                                    .error(R.drawable.dummy_pic)
                                    .into(profilePic);

                            helper.initPref();
                            helper.SaveStringPref(Prefs.profile_picture, myProfile);
                            helper.ApplyPref();
                            Log.d(TAG, "profile pic activity :" + myProfile);

                        } else {
//                            myProfile = AppConfig.URL.BASE_IMAGEURL + response.body().getData().getUserDetails().getProfilePicture();
                            myProfile = "" + response.body().getData().getUserDetails().getProfilePicture();


                            Picasso.with(ProfileActivity.this)
                                    .load(myProfile)
                                    .placeholder(R.mipmap.ic_launcher)
                                    .error(R.drawable.dummy_pic)
                                    .into(profilePic);

                            helper.initPref();
                            helper.SaveStringPref(Prefs.profile_picture, myProfile);
                            helper.ApplyPref();
                            Log.d(TAG, "profile pic activity :" + myProfile);
                        }


                    }


                    edt_first_name.setText(response.body().getData().getUserDetails().getFirstName());
                    edt_last_name.setText(response.body().getData().getUserDetails().getLastName());
                    etemail.setText(response.body().getData().getUserDetails().getEmail());
                    etphone.setText(response.body().getData().getUserDetails().getPhoneNumber());

                    firstname = edt_first_name.getText().toString();
                    lastname = edt_last_name.getText().toString();
                    email = etemail.getText().toString();
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<RespGetProfileDetails> call, Throwable t) {
//                progress.hideDialog();
                pbar.setVisibility(View.GONE);
                rlmain.setVisibility(View.VISIBLE);

//                Toast.makeText(ProfileActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
                getForceDashboard(accept, authorization);

            }
        });
    }

    private void getForceDashboard(String accept, String authorization) {
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        final Call<RespForceDashboard> respForceDashboardCall = apiService.DRIVER_DASHBOARD1(accept, authorization);

        respForceDashboardCall.enqueue(new Callback<RespForceDashboard>() {
            @Override
            public void onResponse(Call<RespForceDashboard> call, Response<RespForceDashboard> response) {
                if (response.isSuccessful()) {

                    if (response.body().getMessage().contains("Unauthenticated")) {
                        openLogOut();
                        return;
                    }

                    if (response.body().getData().getUserDetails().getProfilePicture() == null ||
                            response.body().getData().getUserDetails().getProfilePicture().equals("")) {
                        profilePic.setImageResource(R.drawable.dummy_pic);
                    } else {

                        if (myProfile == null) {

                            Picasso.with(ProfileActivity.this)
                                    .load(R.mipmap.ic_launcher)
                                    .placeholder(R.mipmap.ic_launcher)
                                    .error(R.drawable.dummy_pic)
                                    .into(profilePic);
                        } else if (myProfile.contains("https://")) {

                            Picasso.with(ProfileActivity.this)
                                    .load(myProfile)
                                    .placeholder(R.mipmap.ic_launcher)
                                    .error(R.drawable.dummy_pic)
                                    .into(profilePic);
                            helper.initPref();
                            helper.SaveStringPref(Prefs.profile_picture, myProfile);
                            helper.ApplyPref();
                            Log.d(TAG, "profile pic activity :" + myProfile);

                        } else {
//                            myProfile = AppConfig.URL.BASE_IMAGEURL + response.body().getData().getUserDetails().getProfilePicture();
                            myProfile = "" + response.body().getData().getUserDetails().getProfilePicture();

                            Picasso.with(ProfileActivity.this)
                                    .load(myProfile)
                                    .placeholder(R.mipmap.ic_launcher)
                                    .error(R.drawable.dummy_pic)
                                    .into(profilePic);
                            helper.initPref();
                            helper.SaveStringPref(Prefs.profile_picture, myProfile);
                            helper.ApplyPref();
                            Log.d(TAG, "profile pic activity :" + myProfile);
                        }


                    }


                    edt_first_name.setText(response.body().getData().getUserDetails().getFirstName());
                    edt_last_name.setText(response.body().getData().getUserDetails().getLastName());
                    etemail.setText(response.body().getData().getUserDetails().getEmail());
                    etphone.setText(response.body().getData().getUserDetails().getPhoneNumber());

                    firstname = edt_first_name.getText().toString();
                    lastname = edt_last_name.getText().toString();
                    email = etemail.getText().toString();
                }
            }

            @Override
            public void onFailure(Call<RespForceDashboard> call, Throwable t) {
                Log.d(TAG, "force dashboard :" + t.getLocalizedMessage());
                Log.d(TAG, "force dashboard :" + t.getMessage());
            }
        });
    }

    private void openLogOut() {
        final Dialog mBottomSheetDialog = new Dialog(this, R.style.MaterialDialogSheet);
        mBottomSheetDialog.setContentView(R.layout.dialog_unauthorised);
        mBottomSheetDialog.setCancelable(false);
        mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        mBottomSheetDialog.getWindow().setGravity(Gravity.CENTER);
        mBottomSheetDialog.setCanceledOnTouchOutside(false);
        AppCompatTextView btn_ok = mBottomSheetDialog.findViewById(R.id.btn_ok);

        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                progress.createDialog(false);
                progress.DialogMessage(getString(R.string.msg_logout));
                progress.showDialog();
                AsyncTask.execute(new Runnable() {
                    @Override
                    public void run() {

                        try {
                            FirebaseInstanceId.getInstance().deleteInstanceId();
                            helper.clearAllPrefs();
                            startActivity(new Intent(ProfileActivity.this, LoginActivity.class)
                                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                            progress.hideDialog();
                            ProfileActivity.this.finish();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        });
        mBottomSheetDialog.show();
    }

}
