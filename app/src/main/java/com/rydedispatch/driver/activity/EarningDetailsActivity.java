package com.rydedispatch.driver.activity;

import android.animation.ObjectAnimator;
import android.app.Dialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.google.firebase.iid.FirebaseInstanceId;
import com.rydedispatch.driver.R;
import com.rydedispatch.driver.adapter.EarningDetailsAdapter;
import com.rydedispatch.driver.network.ApiClient;
import com.rydedispatch.driver.network.ApiInterface;
import com.rydedispatch.driver.response.RespStatementDetails;
import com.rydedispatch.driver.utils.AppConfig;
import com.rydedispatch.driver.utils.PreferenceHelper;
import com.rydedispatch.driver.utils.Prefs;
import com.rydedispatch.driver.utils.Progress;
import com.rydedispatch.driver.utils.Utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EarningDetailsActivity extends AppCompatActivity {
    @BindView(R.id.circular_progress_bar)
    ProgressBar pbar;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.tvTitle)
    AppCompatTextView tvTitle;
    @BindView(R.id.tv_cash)
    AppCompatTextView tvcash;
    @BindView(R.id.tv_total_value)
    AppCompatTextView tv_total_value;
    @BindView(R.id.tv_spend_time_value)
    AppCompatTextView tv_spend_time_value;
    @BindView(R.id.tv_totalCompeleted)
    AppCompatTextView tv_totalCompeleted;

    @BindView(R.id.rv_earning_statements)
    RecyclerView rv_earning_statements;
    @BindView(R.id.rl_earning_details)
    RelativeLayout rl_earning_details;


    protected PreferenceHelper helper;
    private Bundle bundle;
    protected Progress progress;
    private Utils utils;

    private String accept = "", authorization = "", mins = "MIN",dollar = "$",
            spendtime =  "",totalearningvalue = "", status = "", earningID = "", requestId = "",payment_type = "";

    private LinearLayoutManager mLayoutManager;
    private EarningDetailsAdapter mEarningDetailAdapter;

    private List<RespStatementDetails.DataEntityX.StatementsDetailsEntity.DataEntity> listData = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_earning_details);
        ButterKnife.bind(this);


        bundle = getIntent().getExtras();
        if (bundle != null) {
            earningID = bundle.getString(AppConfig.BUNDLE.earningID);
            status = bundle.getString(AppConfig.BUNDLE.statusRide);
        }
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();
            }
        });


        Log.d("EarningDetails", "id :" + earningID);
        tvTitle.setText(getString(R.string.res_earning_details));

        utils = new Utils(this);
        progress = new Progress(this);
        helper = new PreferenceHelper(this, Prefs.PREF_FILE);

        accept = "application/json";
        authorization = "Bearer " + helper.LoadStringPref(Prefs.token, "");

        mLayoutManager = new LinearLayoutManager(EarningDetailsActivity.this);
        rv_earning_statements.setLayoutManager(mLayoutManager);
        rv_earning_statements.setItemAnimator(new DefaultItemAnimator());
        mEarningDetailAdapter = new EarningDetailsAdapter(EarningDetailsActivity.this, listData);
        rv_earning_statements.setAdapter(mEarningDetailAdapter);

        mEarningDetailAdapter.setListener(new EarningDetailsAdapter.customOnclickListener() {
            @Override
            public void onclick(int adapterPosition, RespStatementDetails.DataEntityX.StatementsDetailsEntity.DataEntity listhistories) {

                requestId = String.valueOf(listhistories.getRequestId());
                status = String.valueOf(listhistories.getStatus());
                payment_type = String.valueOf(listhistories.getPaymentType());

                if (status.equalsIgnoreCase("bonus")) {
                    Log.d("EarningDetails", "request id :" + listhistories.getRequestId());
                    Log.d("EarningDetails", "earning status :" + listhistories.getStatus());

                }
                else {
                    Log.d("EarningDetails", "request id :" + listhistories.getRequestId());
                    Log.d("EarningDetails", "earning status :" + listhistories.getStatus());

                    startActivity(new Intent(EarningDetailsActivity.this, HistoryDetails.class)
                            .putExtra(AppConfig.BUNDLE.request_id, requestId)
                            .putExtra(AppConfig.BUNDLE.statusRide, status));
                }


            }
        });

        getStatementDetails(accept, authorization, earningID);
    }

    private void getStatementDetails(String accept, String authorization, String earningID) {
//        progress.createDialog(false);
//        progress.DialogMessage(getString(R.string.please_wait));
//        progress.showDialog();
        pbar.setVisibility(View.VISIBLE);
        ObjectAnimator anim = ObjectAnimator.ofInt(pbar, "progress", 0, 100);
        anim.setDuration(15000);
        anim.setInterpolator(new DecelerateInterpolator());
        anim.start();

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        RequestBody earningID1 = RequestBody.create(MediaType.parse("multipart/form-data"), earningID);

        final Call<RespStatementDetails> respDashboardDriverCall = apiService.STATEMENT_DETAILS(accept, authorization, earningID1);
        respDashboardDriverCall.enqueue(new Callback<RespStatementDetails>() {
            @Override
            public void onResponse(Call<RespStatementDetails> call, Response<RespStatementDetails> response) {
//                progress.hideDialog();
                pbar.setVisibility(View.GONE);


                if (response.body().getData().getStatementsDetails().getData().isEmpty()) {
                    Snackbar.make(rl_earning_details, "No data available..!!", Snackbar.LENGTH_SHORT).show();
                } else if (response.message().contains("Unauthenticated")) {
                    openAlertUnauthLogout();
                } else {
                    listData.addAll(response.body().getData().getStatementsDetails().getData());
                    mEarningDetailAdapter.notifyDataSetChanged();

                    totalearningvalue = dollar + response.body().getData().getTotalEarnings();
                    spendtime =response.body().getData().getTotalSpendTime() + mins;
                    Log.d("Earning", "details :" + response.body().getData().getTotalEarnings());
                    Log.d("Earning", "totalearningvalue :" + totalearningvalue);

                    tv_total_value.setText(totalearningvalue);
                    tv_spend_time_value.setText(spendtime);
                    tv_totalCompeleted.setText(response.body().getData().getTotalCompeleted());
                }
            }

            @Override
            public void onFailure(Call<RespStatementDetails> call, Throwable t) {
//                progress.hideDialog();
                pbar.setVisibility(View.GONE);

//                Toast.makeText(MainActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }


    private void openAlertUnauthLogout() {
        final Dialog mBottomSheetDialog = new Dialog(this, R.style.MaterialDialogSheet);
        mBottomSheetDialog.setContentView(R.layout.dialog_unauthorised);
        mBottomSheetDialog.setCancelable(true);
        mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        mBottomSheetDialog.getWindow().setGravity(Gravity.CENTER);
        mBottomSheetDialog.setCanceledOnTouchOutside(true);
        AppCompatTextView btn_ok = mBottomSheetDialog.findViewById(R.id.btn_ok);

        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progress.createDialog(false);
                progress.DialogMessage(getString(R.string.msg_logout));
                progress.showDialog();
                AsyncTask.execute(new Runnable() {
                    @Override
                    public void run() {

                        try {
                            FirebaseInstanceId.getInstance().deleteInstanceId();
                            helper.clearAllPrefs();
                            startActivity(new Intent(EarningDetailsActivity.this, LoginActivity.class)
                                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                            progress.hideDialog();
                            EarningDetailsActivity.this.finish();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        });
        mBottomSheetDialog.show();

    }

    @OnClick({R.id.iv_back, R.id.tv_cash})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.tv_cash:
//                startActivity(new Intent(EarningDetailsActivity.this, HistoryDetails.class));
                break;
        }
    }
}
