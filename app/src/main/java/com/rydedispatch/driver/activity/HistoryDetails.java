package com.rydedispatch.driver.activity;

import android.animation.ObjectAnimator;
import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatTextView;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.RelativeLayout;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.rydedispatch.driver.R;
import com.rydedispatch.driver.network.ApiClient;
import com.rydedispatch.driver.network.ApiInterface;
import com.rydedispatch.driver.response.RespHistoryDetails;
import com.rydedispatch.driver.utils.AppConfig;
import com.rydedispatch.driver.utils.GPSTracker;
import com.rydedispatch.driver.utils.PreferenceHelper;
import com.rydedispatch.driver.utils.Prefs;
import com.rydedispatch.driver.utils.Progress;
import com.rydedispatch.driver.utils.Utils;
import com.squareup.picasso.Picasso;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HistoryDetails extends AppCompatActivity /*implements PermissionsListener*/ {

    private static final String TAG = HistoryDetails.class.getSimpleName();

    @BindView(R.id.circular_progress_bar)
    ProgressBar pbar;
    @BindView(R.id.tvTitle)
    AppCompatTextView tvTitle;
    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_passenger)
    AppCompatTextView tv_passenger;
    @BindView(R.id.rating_user)
    RatingBar ratinguser;
    @BindView(R.id.tv_total_time)
    AppCompatTextView tv_total_time;
    @BindView(R.id.tv_miles)
    AppCompatTextView tv_miles;
    @BindView(R.id.tv_booking_date)
    AppCompatTextView tvbookingtime;
    @BindView(R.id.tv_source_loc)
    AppCompatTextView tv_source_loc;
    @BindView(R.id.tv_destination_loc)
    AppCompatTextView tv_destination_loc;
    @BindView(R.id.tv_start_riding_time)
    AppCompatTextView tv_start_riding_time;
    @BindView(R.id.tv_completed_time)
    AppCompatTextView tv_completed_time;
    @BindView(R.id.tv_total_distance)
    AppCompatTextView tv_total_distance;
    @BindView(R.id.tv_base_fare)
    AppCompatTextView tv_base_fare;
    @BindView(R.id.tv_extra_ride_time_cost)
    AppCompatTextView tv_extra_ride_time_cost;
    @BindView(R.id.tv_total_cost)
    AppCompatTextView tv_total_cost;
    @BindView(R.id.tv_discount_value)
    AppCompatTextView tv_discount_value;
    @BindView(R.id.tv_sub_total_value)
    AppCompatTextView tv_sub_total_value;
    @BindView(R.id.tv_extra_distance_cost)
    AppCompatTextView tv_extra_distance_cost;
    @BindView(R.id.tv_wait_time_cost)
    AppCompatTextView tv_wait_time_cost;
    @BindView(R.id.tv_estimated_cost_without_base_fare)
    AppCompatTextView tv_estimated_cost_without_base_fare;
    @BindView(R.id.tv_status)
    AppCompatTextView tv_status;
    @BindView(R.id.tv_display_name_detail)
    AppCompatTextView tvdisplayname;
    @BindView(R.id.tv_commission_value)
    AppCompatTextView tv_commission_value;
    @BindView(R.id.tv_bonus_value)
    AppCompatTextView tv_bonus_value;
    @BindView(R.id.ll_bid_details)
    LinearLayout ll_bid_details;
    @BindView(R.id.tv_time_detail)
    AppCompatTextView tv_time_detail;
    @BindView(R.id.tv_est_to_trip)
    AppCompatTextView tv_est_to_trip;
    @BindView(R.id.tv_time_est_driver)
    AppCompatTextView tv_time_est_driver;
    @BindView(R.id.ll_schedule_trip)
    LinearLayout ll_schedule_trip;
    @BindView(R.id.ll_navigate_schedule)
    LinearLayout ll_navigate_schedule;
    @BindView(R.id.ll_call_schedule)
    LinearLayout ll_call_schedule;
    @BindView(R.id.tv_other_details)
    AppCompatTextView tv_other_details;
    @BindView(R.id.rl_details)
    RelativeLayout rl_details;
    @BindView(R.id.ll_navigate_call)
    LinearLayout ll_navigate_call;
    @BindView(R.id.profile_image_details)
    CircleImageView profile_image;
    @BindView(R.id.ll_detail)
    LinearLayout ll_detail;

    protected PreferenceHelper helper;
    private Bundle bundle;
    protected Progress progress;
    private Utils utils;

    private String accept = "", authorization = "", request_id = "",
            passengername = "", totaltime = "", source_loc = "", destination_loc = "",
            distance = "", km = "", bookingTime = "", startridingtime = "",
            completed_time = "", base_fare = "", extra_ride_time_cost = "",
            total_cost = "", discount = "", is_schedule = "",
            statusRide = "", extra_distance_cost = "", wait_time_cost = "",
            estimated_cost_without_base_fare = "", status = "", displayname = "", schedule = "",
            subuser = "", commission = "", actual_cost = "", estimatedtimetoarrival = "", uniqueCode = "", firstname = "", payment_type = "",
            lastname = "", imgDetails = "", passengerphonenumber = "", bonus = "";
    float ratinguserstr;

    GPSTracker gps;
    //    private MapView mapView;
//    MapboxMap mapboxMap;
//    NavigationLauncherOptions options;
//    private DirectionsRoute currentRoute;
//    private LocationComponent locationComponent;
//    private PermissionsManager permissionsManager;
    double destinationLat, destinationLang, sourceLat, sourceLang;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        Mapbox.getInstance(this, getString(R.string.map_box_token));

        setContentView(R.layout.activity_history_details);
        ButterKnife.bind(this);

        tvTitle.setText(getString(R.string.history_details));

        bundle = getIntent().getExtras();
        if (bundle != null) {
            request_id = bundle.getString(AppConfig.BUNDLE.request_id);
            statusRide = bundle.getString(AppConfig.BUNDLE.statusRide);
            schedule = bundle.getString(AppConfig.BUNDLE.is_schedule);
            payment_type = bundle.getString(AppConfig.BUNDLE.payment_type);
            passengerphonenumber = bundle.getString(AppConfig.BUNDLE.passenger_phone_number);
        }
        helper = new PreferenceHelper(this, Prefs.PREF_FILE);
        progress = new Progress(this);
        utils = new Utils(this);

        accept = "application/json";
        authorization = "Bearer " + helper.LoadStringPref(Prefs.token, "");
        getHistoryDetails(accept, authorization, request_id);

        /*if (statusRide.contains("inprogress")){
            ll_bid_details.setVisibility(View.GONE);
            tv_time_detail.setVisibility(View.VISIBLE);
        }
        else {
            ll_bid_details.setVisibility(View.VISIBLE);
        }*/

        gps = new GPSTracker(this);
        /*mapView = findViewById(R.id.mapView_main_detail);
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(new com.mapbox.mapboxsdk.maps.OnMapReadyCallback() {
            @Override
            public void onMapReady(@NonNull MapboxMap map) {
                mapboxMap = map;
                mapboxMap.setStyle(getString(R.string.navigation_guidance_day), new Style.OnStyleLoaded() {
                    @Override
                    public void onStyleLoaded(@NonNull Style style) {
                        enableLocationComponent(style);

                        addDestinationIconSymbolLayer(style);

                    }
                });
            }
        });*/
        Log.d("HistoryDetail", "request_id :" + request_id);
        Log.d("HistoryDetail", "schedule  :" + schedule);
        Log.d("HistoryDetail", "statusRide  :" + statusRide);
    }

    /*@SuppressWarnings({"MissingPermission"})
    private void enableLocationComponent(@NonNull Style loadedMapStyle) {
// Check if permissions are enabled and if not request
        if (PermissionsManager.areLocationPermissionsGranted(this)) {
// Activate the MapboxMap LocationComponent to show user location
// Adding in LocationComponentOptions is also an optional parameter
            locationComponent = mapboxMap.getLocationComponent();
            locationComponent.activateLocationComponent(this, loadedMapStyle);
            locationComponent.setLocationComponentEnabled(true);
// Set the component's camera mode
            locationComponent.setCameraMode(CameraMode.TRACKING);
        } else {
            permissionsManager = new PermissionsManager(this);
            permissionsManager.requestLocationPermissions(this);
        }
    }

    private void addDestinationIconSymbolLayer(@NonNull Style loadedMapStyle) {
        loadedMapStyle.addImage("destination-icon-id",
                BitmapFactory.decodeResource(this.getResources(), R.drawable.mapbox_marker_icon_default));
        GeoJsonSource geoJsonSource = new GeoJsonSource("destination-source-id");
        loadedMapStyle.addSource(geoJsonSource);
        SymbolLayer destinationSymbolLayer = new SymbolLayer("destination-symbol-layer-id", "destination-source-id");
        destinationSymbolLayer.withProperties(
                iconImage("destination-icon-id"),
                iconAllowOverlap(true),
                iconIgnorePlacement(true)
        );
        loadedMapStyle.addLayer(destinationSymbolLayer);
    }*/

    private void getHistoryDetails(String accept, String authorization, String request_id) {
//        progress.createDialog(false);
//        progress.DialogMessage(getString(R.string.please_wait));
//        progress.showDialog();

        pbar.setVisibility(View.VISIBLE);
        ObjectAnimator anim = ObjectAnimator.ofInt(pbar, "progress", 0, 100);
        anim.setDuration(15000);
        anim.setInterpolator(new DecelerateInterpolator());
        anim.start();

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        RequestBody request_id1 = RequestBody.create(MediaType.parse("multipart/form-data"), request_id);
        final Call<RespHistoryDetails> respDashboardDriverCall =
                apiService.HISTORY_DETAILS(accept, authorization, request_id1);
        respDashboardDriverCall.enqueue(new Callback<RespHistoryDetails>() {
            @Override
            public void onResponse(Call<RespHistoryDetails> call, Response<RespHistoryDetails> response) {
//                progress.hideDialog();
                pbar.setVisibility(View.GONE);
                ll_detail.setVisibility(View.VISIBLE);

                if (response.body().getMessage().contains("Unauthenticated")) {
                    openAlertUnauthLogout();
                    return;
                }

                if (response.isSuccessful()) {


                    passengername = response.body().getData().getPassengerFirstName();
                    ratinguserstr = Float.parseFloat(response.body().getData().getUserRating());
                    totaltime = response.body().getData().getTotalTime();
                    distance = response.body().getData().getTotalDistance();
                    km = response.body().getData().getDistanceType();
                    bookingTime = response.body().getData().getStart_at();
                    source_loc = response.body().getData().getSourceLocation();
                    destination_loc = response.body().getData().getDestinationLocation();
                    startridingtime = response.body().getData().getStartRidingTime();
                    completed_time = response.body().getData().getCompletedTime();
                    base_fare = response.body().getData().getBaseFare();
                    extra_ride_time_cost = response.body().getData().getExtraRideTimeCost();
                    estimated_cost_without_base_fare = response.body().getData().getEstimatedCostWithoutBaseFare();
                    status = response.body().getData().getStatus();
//                subuser = response.body().getData().getSub_user_id();
                    displayname = response.body().getData().getDisplay_passenger_name();
                    estimatedtimetoarrival = response.body().getData().getEstimated_time_to_arrival();
                    payment_type = response.body().getData().getPaymentType();


                    wait_time_cost = response.body().getData().getWaitTimeCost();
                    extra_distance_cost = response.body().getData().getExtraDistanceCost();
                    discount = response.body().getData().getActualDiscount();
                    total_cost = response.body().getData().getTotalCost();
                    actual_cost = response.body().getData().getActualCostWithoutCommission();
                    commission = response.body().getData().getCommission();
                    bonus = response.body().getData().getBonus();
                    sourceLat = Double.parseDouble(response.body().getData().getSourceLatitude());
                    sourceLang = Double.parseDouble(response.body().getData().getSourceLongitude());
                    uniqueCode = response.body().getData().getPassenger_unique_code();

                    imgDetails = response.body().getData().getProfilePicture();

                    Picasso.with(HistoryDetails.this)
                            .load(imgDetails)
                            .placeholder(R.mipmap.ic_launcher)
                            .error(R.mipmap.ic_launcher)
                            .into(profile_image);

                    tv_passenger.setText(passengername);


                    if (displayname.isEmpty() || displayname.matches("")) {

                    } else {

                    }

                    if (totaltime == null) {
                        tv_total_time.setText("0" + " MIN");
                    } else {
                        tv_total_time.setText(totaltime + " MIN");
                    }

                    if (distance == null) {
                        tv_miles.setText("0" + " " + km);
                    } else {
                        tv_miles.setText(distance + " " + km);
                    }
                    ratinguser.setRating(ratinguserstr);
                    tvbookingtime.setText(bookingTime);
                    tv_source_loc.setText(source_loc);
                    tv_destination_loc.setText(destination_loc);
                    tv_start_riding_time.setText(startridingtime);
                    tv_completed_time.setText(completed_time);
                    tv_total_distance.setText(distance + " " + km);

                    tv_base_fare.setText("$ " + base_fare);
                    tv_extra_ride_time_cost.setText("$ " + extra_ride_time_cost);

                    tv_estimated_cost_without_base_fare.setText("$ " + estimated_cost_without_base_fare);
                    tv_wait_time_cost.setText("$ " + wait_time_cost);
                    tv_extra_distance_cost.setText("$ " + extra_distance_cost);
                    tv_sub_total_value.setText("$ " + total_cost);
                    tv_discount_value.setText("$ " + discount);
                    tv_total_cost.setText("$ " + actual_cost);
                    tv_commission_value.setText("$ " + commission);
                    tv_bonus_value.setText("$ " + bonus);

                    /*if (subuser.contains("0") || subuser.length() == 0) {
                        tvdisplayname.setVisibility(View.GONE);
                    } else {
                        tvdisplayname.setVisibility(View.VISIBLE);
                        tvdisplayname.setText(displayname);
                    }*/

                    Log.d(TAG, "history details : " + new Gson().toJson(response.body()));

                    if (response.body().getData().getSub_user_id() != null) {
                        try
                        {
                            if (response.body().getData().getSub_user_id().length() == 0
                                    || response.body().getData().getSub_user_id().equals("0")) {
                                tvdisplayname.setVisibility(View.GONE);
                            } else {
                                tvdisplayname.setVisibility(View.VISIBLE);
                                tvdisplayname.setText(displayname);
                            }
                        }
                        catch (NullPointerException e) {
                            e.printStackTrace();
                        }

                    } else {
                        tvdisplayname.setVisibility(View.GONE);
                    }


                    if (status.equalsIgnoreCase("completed")) {
                        ll_bid_details.setVisibility(View.VISIBLE);
                        ll_schedule_trip.setVisibility(View.GONE);
                        tv_other_details.setVisibility(View.GONE);
                        ll_navigate_call.setVisibility(View.GONE);

                    } else if (statusRide.contains("cancelled_by_driver")) {
                        tv_status.setText(status.replace("_", " ").replace("_", " "));
                        tv_status.setTextColor(getResources().getColor(R.color.icons_8_muted_red));
                        tv_time_detail.setVisibility(View.GONE);
                        ll_schedule_trip.setVisibility(View.GONE);
                        tv_other_details.setVisibility(View.GONE);
                        ll_navigate_call.setVisibility(View.GONE);
                    } else {

                        if (status.contains("cancelled")) {
                            tv_status.setText(status);

                            tv_status.setTextColor(getResources().getColor(R.color.icons_8_muted_red));
                            tv_time_detail.setVisibility(View.GONE);
                            ll_schedule_trip.setVisibility(View.GONE);
                            tv_other_details.setVisibility(View.GONE);
                            ll_navigate_call.setVisibility(View.GONE);

                        } else if (statusRide.equalsIgnoreCase("no_show")) {
                            tv_status.setText(status.replace("_", " ").replace("_", " "));
                            tv_status.setTextColor(getResources().getColor(R.color.icons_8_muted_red));
                            tv_time_detail.setVisibility(View.GONE);
                            ll_schedule_trip.setVisibility(View.GONE);
                            tv_other_details.setVisibility(View.GONE);
                            ll_navigate_call.setVisibility(View.GONE);
                        } else {
                            tv_status.setText(status.replace("_", " ").replace("_", " "));
                            tv_status.setTextColor(getResources().getColor(R.color.green));

                            if (statusRide.equalsIgnoreCase("inprogress") || schedule.equalsIgnoreCase("1")) {
                                ll_bid_details.setVisibility(View.GONE);
                                tv_time_detail.setVisibility(View.VISIBLE);
                                tv_other_details.setVisibility(View.VISIBLE);
                                ll_navigate_call.setVisibility(View.VISIBLE);

                                if (totaltime == null || estimatedtimetoarrival == null) {
                                    tv_est_to_trip.setText("0" + " MIN");
                                    tv_time_est_driver.setText("0" + " MIN");
                                } else {
                                    tv_est_to_trip.setText(totaltime + " MIN");
                                    tv_time_est_driver.setText(estimatedtimetoarrival + " MIN");
                                }
                            } else {
                                ll_bid_details.setVisibility(View.VISIBLE);
                                ll_schedule_trip.setVisibility(View.GONE);
                                tv_time_detail.setVisibility(View.GONE);
                                tv_other_details.setVisibility(View.GONE);
                                ll_navigate_call.setVisibility(View.GONE);
                            }
                        }
                    }
                } else {

                }


            }

            @Override
            public void onFailure(Call<RespHistoryDetails> call, Throwable t) {
                pbar.setVisibility(View.GONE);
                ll_detail.setVisibility(View.VISIBLE);

            }
        });
    }

    @OnClick({R.id.iv_back, R.id.ll_navigate_schedule, R.id.ll_call_schedule})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.ll_navigate_schedule:
                Log.d(TAG, "source lat :" + sourceLat);
                Log.d(TAG, "source lang :" + sourceLang);
//                initMap(sourceLat, sourceLang);

//                onMapBox();
                googleMapExternal(sourceLat, sourceLang);

                break;

            case R.id.ll_call_schedule:
                Log.d(TAG, "voice call:" + displayname);
                Log.d(TAG, "voice payment_type:" + payment_type);
                Log.d(TAG, "voice uniqueCode:" + uniqueCode);
                Log.d(TAG, "voice passengerphonenumber:" + passengerphonenumber);

                startActivity(new Intent(HistoryDetails.this, VoiceActivity.class)
                        .putExtra(AppConfig.BUNDLE.passenger_first_name, firstname)
                        .putExtra(AppConfig.BUNDLE.passenger_last_name, lastname)
                        .putExtra(AppConfig.BUNDLE.PassengerUniqueCode, uniqueCode)
                        .putExtra(AppConfig.BUNDLE.display_passenger_name, displayname)
                        .putExtra(AppConfig.BUNDLE.payment_type, payment_type)
                        .putExtra(AppConfig.BUNDLE.passenger_phone_number, passengerphonenumber)
                        .putExtra(AppConfig.EXTRA.CHECKOUTGOING, 1));


                break;
        }
    }

    private void googleMapExternal(double sourceLat, double sourceLang) {
        String url = "google.navigation:q=" + sourceLat + "," + sourceLang + "&mode=d";
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        startActivity(intent);
    }



    /*private void onMapBox() {


        if (options == null) {
            Snackbar.make(rl_details, "Click again , No Routes Found..!!", Snackbar.LENGTH_SHORT).show();
            return;
        }
        mapView.setVisibility(View.GONE);
        NavigationLauncher.startNavigation(this, options);
    }*/


    /*private void initMap(double sourceLat, double sourceLang) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {


                try {
                    Point destinationPoint = Point.fromLngLat(sourceLang, sourceLat);

                    Point originPoint = Point.fromLngLat(gps.getLongitude(), gps.getLatitude());

                    getRoute(originPoint, destinationPoint);

                    mapboxMap.setStyle(getString(R.string.navigation_guidance_day), new Style.OnStyleLoaded() {
                        @Override
                        public void onStyleLoaded(@NonNull Style style) {

//                            progress.hideDialog();
                            enableLocationComponent(style);

                            addDestinationIconSymbolLayer(style);

                        }
                    });
                } catch (RuntimeException r) {
                    r.printStackTrace();
                }

            }
        }, 1000);
    }*/


    /*private void getRoute(Point origin, Point destination) {
        progress.createDialog(true);
        progress.DialogMessage(getString(R.string.please_wait));
        progress.showDialog();

        NavigationRoute.builder(this)
                .accessToken(Mapbox.getAccessToken())
                .origin(origin)
                .destination(destination)
                .build()
                .getRoute(new Callback<DirectionsResponse>() {
                    @Override
                    public void onResponse(Call<DirectionsResponse> call, Response<DirectionsResponse> response) {
// You can get the generic HTTP info about the response

                        progress.hideDialog();
                        Log.d(TAG, "Response code: " + response.code());
                        *//*if (response.code()==200) {
                        }*//*
                        if (response.body() == null) {
                            Log.e(TAG, "No routes found, make sure you set the right user and access token.");
                            return;
                        } else if (response.body().routes().size() < 1) {
                            Log.e(TAG, "No routes found");
                            return;
                        }

                        currentRoute = response.body().routes().get(0);

                        boolean simulateRoute = false;
                        options = NavigationLauncherOptions.builder()
                                .directionsRoute(currentRoute)
                                .shouldSimulateRoute(simulateRoute)
                                .build();


                    }

                    @Override
                    public void onFailure(Call<DirectionsResponse> call, Throwable throwable) {
                        progress.hideDialog();

                        Log.e(TAG, "Error: " + throwable.getMessage());


                    }
                });
    }*/

    private void openAlertUnauthLogout() {
        final Dialog mBottomSheetDialog = new Dialog(this, R.style.MaterialDialogSheet);
        mBottomSheetDialog.setContentView(R.layout.dialog_unauthorised);
        mBottomSheetDialog.setCancelable(true);
        mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        mBottomSheetDialog.getWindow().setGravity(Gravity.CENTER);
        mBottomSheetDialog.setCanceledOnTouchOutside(true);
        AppCompatTextView btn_ok = mBottomSheetDialog.findViewById(R.id.btn_ok);

        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progress.createDialog(false);
                progress.DialogMessage(getString(R.string.msg_logout));
                progress.showDialog();
                AsyncTask.execute(new Runnable() {
                    @Override
                    public void run() {

                        try {
                            FirebaseInstanceId.getInstance().deleteInstanceId();
                            helper.clearAllPrefs();
                            startActivity(new Intent(HistoryDetails.this, LoginActivity.class)
                                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                            progress.hideDialog();
                            HistoryDetails.this.finish();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        });
        mBottomSheetDialog.show();

    }

    /*@Override
    public void onExplanationNeeded(List<String> permissionsToExplain) {
        Toast.makeText(this, R.string.user_location_permission_explanation, Toast.LENGTH_LONG).show();

    }

    @Override
    public void onPermissionResult(boolean granted) {
        if (granted) {
//            enableLocationComponent(mapboxMap.getStyle());
        } else {
            Toast.makeText(this, R.string.user_location_permission_not_granted, Toast.LENGTH_LONG).show();
            finish();
        }
    }*/
}
