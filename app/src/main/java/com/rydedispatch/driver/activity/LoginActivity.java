package com.rydedispatch.driver.activity;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.gson.Gson;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.hbb20.CountryCodePicker;
import com.rydedispatch.driver.R;
import com.rydedispatch.driver.network.ApiClient;
import com.rydedispatch.driver.network.ApiInterface;
import com.rydedispatch.driver.response.RespLogin;
import com.rydedispatch.driver.response.RespSendLoginOTP;
import com.rydedispatch.driver.utils.AppConfig;
import com.rydedispatch.driver.utils.ConnectionDetector;
import com.rydedispatch.driver.utils.PreferenceHelper;
import com.rydedispatch.driver.utils.Prefs;
import com.rydedispatch.driver.utils.Progress;
import com.rydedispatch.driver.utils.Utils;

import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.fabric.sdk.android.Fabric;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {
    private static final String TAG = "LoginActivity";
    private static final int RESOLVE_HINT = 1000;

    @BindView(R.id.rl_login)
    RelativeLayout rl_login;
    @BindView(R.id.ll_sign_in)
    LinearLayout ll_sign_in;
    @BindView(R.id.ll_sign_up)
    LinearLayout ll_sign_up;
    @BindView(R.id.ccp)
    CountryCodePicker ccp;
    @BindView(R.id.edt_phone)
    EditText etNumber;
    private Utils utils;
    private String countryCode = "", phoneNo = "", token = "", accept = "", deviceType = "android";
    private Progress progress;
    protected PreferenceHelper helper;
    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    public static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 1;
    private ConnectionDetector connectionDetector;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        utils = new Utils(this);
        progress = new Progress(this);
        helper = new PreferenceHelper(this, Prefs.PREF_FILE);

        accept = "application/json";

        PermissionListener permissionlistener = new PermissionListener() {
            @Override
            public void onPermissionGranted() {

            }

            @Override
            public void onPermissionDenied(List<String> deniedPermissions) {
                Toast.makeText(LoginActivity.this, "Permission Denied\n" + deniedPermissions.toString(), Toast.LENGTH_SHORT).show();
            }
        };
        TedPermission.with(this)
                .setPermissionListener(permissionlistener)
                .setDeniedMessage("If you reject permission,you can not use this service\n\nPlease turn on permissions at [Setting] > [Permission]")
                .setPermissions(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.RECORD_AUDIO)
                .check();

        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Log.d(TAG, "getInstanceId failed " + Objects.requireNonNull(task.getException()).getLocalizedMessage());
                            return;
                        }
                        token = task.getResult().getToken();
                        Log.e(TAG, "onComplete: " + token);
                    }
                });

        Fabric fabric = new Fabric.Builder(this)
                .kits(new Crashlytics())
                .debuggable(true)           // Enables Crashlytics debugger
                .build();
        Fabric.with(this, new Crashlytics());


        connectionDetector = new ConnectionDetector(this);

    }


    @OnClick({R.id.ll_sign_in, R.id.ll_sign_up})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ll_sign_in:
                try {
                    signIn();

                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case R.id.ll_sign_up:
                signUp();
                break;
        }
    }

    private void signIn() {
        phoneNo = etNumber.getText().toString();
        countryCode = ccp.getSelectedCountryCode();
        if (phoneNo.isEmpty()) {
            etNumber.setError("Enter the Phone No");
            utils.requestFocus(etNumber);
            return;
        }
        if (connectionDetector.isConnectingToInternet()) {

            Log.e(TAG, "isConnectingToInternet : ");

            try {
                sendLoginOTP(accept, phoneNo, countryCode);
            } catch (Exception e) {
                e.printStackTrace();

            }

        } else {

            utils.hideKeyboard();
            Snackbar.make(rl_login, getString(R.string.conn_internet), Snackbar.LENGTH_LONG).show();

//            Intent intent = new Intent(Intent.ACTION_MAIN);
//            intent.setClassName("com.android.phone", "com.android.phone.NetworkSetting");
//            startActivity(intent);

            Log.e(TAG, "not connected : ");

        }
    }

    private void signUp() {
        startActivity(new Intent(LoginActivity.this, RegistersActivity.class));
    }

    private void openOtpAlert() {

        final Dialog mBottomSheetDialog = new Dialog(this, R.style.MaterialDialogSheet);
        mBottomSheetDialog.setContentView(R.layout.dialog_otp_alert);
        mBottomSheetDialog.setCancelable(false);
        mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        mBottomSheetDialog.getWindow().setGravity(Gravity.CENTER);
        mBottomSheetDialog.setCanceledOnTouchOutside(false);
        Button btn_resend = mBottomSheetDialog.findViewById(R.id.btn_resend);
        Button btn_submit = mBottomSheetDialog.findViewById(R.id.btn_submit);
        final EditText et_otp = mBottomSheetDialog.findViewById(R.id.et_otp);
        ImageButton ib_close = mBottomSheetDialog.findViewById(R.id.ib_close);


        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (et_otp.getText().toString().isEmpty()) {
                    et_otp.setError(getString(R.string.msg_error_phone));
                    return;
                }

                if (token == null || countryCode == null && token.isEmpty() || countryCode.isEmpty()) {
//                    Toast.makeText(LoginActivity.this, "Unable to Login Please Restart App", Toast.LENGTH_SHORT).show();
                    return;
                }

                login(phoneNo, countryCode, et_otp.getText().toString(), token, deviceType);


                mBottomSheetDialog.dismiss();
            }
        });
        btn_resend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                sendLoginOTP(accept, phoneNo, countryCode);
            }
        });

        ib_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBottomSheetDialog.dismiss();
            }
        });
        mBottomSheetDialog.show();
    }


    private void sendLoginOTP(String accept, String phoneNo, String countryCode) {
        progress.createDialog(false);
        progress.DialogMessage(getString(R.string.please_wait));
        progress.showDialog();

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        RequestBody phoneNo1 = RequestBody.create(MediaType.parse("multipart/form-data"), phoneNo);
        RequestBody countryCode1 = RequestBody.create(MediaType.parse("multipart/form-data"), countryCode);

        final Call<RespSendLoginOTP> respSendLoginOTPCall = apiService.LOGIN_OTP_CALL(accept, phoneNo1, countryCode1);
        respSendLoginOTPCall.enqueue(new Callback<RespSendLoginOTP>() {
            @Override
            public void onResponse(Call<RespSendLoginOTP> call, Response<RespSendLoginOTP> response) {

                /*if (response.body().getMessage().equalsIgnoreCase("You will receive an SMS with the verification code.")) {
                    progress.hideDialog();
                    openOtpAlert();
                } else {
                    progress.hideDialog();
                    Toast.makeText(LoginActivity.this, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }*/
                if (response.isSuccessful()) {
                    progress.hideDialog();
                    openOtpAlert();
                }
                else {
                    progress.hideDialog();
                    Toast.makeText(LoginActivity.this, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<RespSendLoginOTP> call, Throwable t) {
                progress.hideDialog();
                Toast.makeText(LoginActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }


    private void login(String phoneNo, String countryCode, String otp, String token, String deviceType) {
        progress.createDialog(false);
        progress.DialogMessage(getString(R.string.please_wait));
        progress.showDialog();

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        RequestBody phoneNo1 = RequestBody.create(MediaType.parse("multipart/form-data"), phoneNo);
        RequestBody countryCode1 = RequestBody.create(MediaType.parse("multipart/form-data"), countryCode);
        RequestBody otp1 = RequestBody.create(MediaType.parse("multipart/form-data"), otp);
        RequestBody deviceToken1 = RequestBody.create(MediaType.parse("multipart/form-data"), token);
        RequestBody deviceType1 = RequestBody.create(MediaType.parse("multipart/form-data"), deviceType);

        final Call<RespLogin> respLoginCall = apiService.LOGIN_CALL(phoneNo1, countryCode1,
                otp1, deviceToken1, deviceType1);
        respLoginCall.enqueue(new Callback<RespLogin>() {
            @Override
            public void onResponse(Call<RespLogin> call, Response<RespLogin> response) {
                Log.e(TAG, "login response : " + new Gson().toJson(response.body()));
                Log.d(TAG, "login response : " + new Gson().toJson(response.body()));

                if (response.body().getMessage().equalsIgnoreCase("Invalid OTP.")
                        || response.body().getData().getUserDetails().getRole().contains("Invalid OTP.")
                        || response.body().getData().getUserDetails().getRole().matches("Invalid OTP.")
                        || response.body().getData().getUserDetails().getRole().equals("Invalid OTP.")) {
                    progress.hideDialog();
                    openOtpAlert();
                } else if (response.body().getData().getUserDetails().getRole().equalsIgnoreCase("passenger")
                        || response.body().getData().getUserDetails().getRole().contains("passenger")
                        || response.body().getData().getUserDetails().getRole().matches("passenger")
                        || response.body().getData().getUserDetails().getRole().equals("passenger")) {
                    progress.hideDialog();
                    Toast.makeText(LoginActivity.this, "This account does not have proper role to login please login with other details...!!!", Toast.LENGTH_SHORT).show();
                } else if (response.body().getData().getUserDetails().getAddress().matches("")
                        || response.body().getData().getUserDetails().getAddress().equalsIgnoreCase("")
                        || response.body().getData().getUserDetails().getAddress().isEmpty()
                        || response.body().getData().getUserDetails().getAddress() == null) {
                    progress.hideDialog();
                    helper.initPref();
                    helper.SaveBooleanPref(Prefs.USER_LOGIN, true);
                    helper.SaveStringPref(Prefs.id, String.valueOf(response.body().getData().getUserDetails().getId()));
                    helper.SaveStringPref(Prefs.parent_id, String.valueOf(response.body().getData().getUserDetails().getParentId()));
                    helper.SaveStringPref(Prefs.company_id, String.valueOf(response.body().getData().getUserDetails().getCompanyId()));
                    helper.SaveStringPref(Prefs.first_name, response.body().getData().getUserDetails().getFirstName());
                    helper.SaveStringPref(Prefs.last_name, response.body().getData().getUserDetails().getLastName());
                    helper.SaveStringPref(Prefs.company_name, response.body().getData().getUserDetails().getCompanyName());
                    helper.SaveStringPref(Prefs.company_image, String.valueOf(response.body().getData().getUserDetails().getCompanyImage()));
                    helper.SaveStringPref(Prefs.contact_person, response.body().getData().getUserDetails().getContactPerson());
                    helper.SaveStringPref(Prefs.profile_picture, String.valueOf(response.body().getData().getUserDetails().getProfilePicture()));
                    helper.SaveStringPref(Prefs.email, response.body().getData().getUserDetails().getEmail());
                    helper.SaveStringPref(Prefs.email_verified_at, String.valueOf(response.body().getData().getUserDetails().getEmailVerifiedAt()));
                    helper.SaveStringPref(Prefs.phone_number, response.body().getData().getUserDetails().getPhoneNumber());
                    helper.SaveStringPref(Prefs.address, response.body().getData().getUserDetails().getAddress());
                    helper.SaveStringPref(Prefs.country, response.body().getData().getUserDetails().getCountry());
                    helper.SaveStringPref(Prefs.state, response.body().getData().getUserDetails().getState());
                    helper.SaveStringPref(Prefs.city, response.body().getData().getUserDetails().getCity());
                    helper.SaveStringPref(Prefs.deviceToken, response.body().getData().getUserDetails().getDeviceToken());
                    helper.SaveStringPref(Prefs.devicetype, response.body().getData().getUserDetails().getDeviceType());
                    helper.SaveStringPref(Prefs.latitude, response.body().getData().getUserDetails().getLatitude());
                    helper.SaveStringPref(Prefs.longitude, response.body().getData().getUserDetails().getLongitude());
                    helper.SaveStringPref(Prefs.status, response.body().getData().getUserDetails().getStatus());
                    helper.SaveStringPref(Prefs.userstatus, response.body().getData().getUserDetails().getUserStatus());
                    helper.SaveStringPref(Prefs.authy_id, response.body().getData().getUserDetails().getAuthyId());
                    helper.SaveStringPref(Prefs.user_unique_code, response.body().getData().getUserDetails().getUserUniqueCode());
                    helper.SaveStringPref(Prefs.taxId, String.valueOf(response.body().getData().getUserDetails().getTaxId()));
                    helper.SaveStringPref(Prefs.commission_type, response.body().getData().getUserDetails().getCommissionType());
                    helper.SaveStringPref(Prefs.commission, String.valueOf(response.body().getData().getUserDetails().getCommission()));
                    helper.SaveStringPref(Prefs.role, response.body().getData().getUserDetails().getRole());
                    helper.SaveStringPref(Prefs.documentstatus, response.body().getData().getUserDetails().getDocumentStatus());
                    helper.SaveStringPref(Prefs.deleted_at, String.valueOf(response.body().getData().getUserDetails().getDeletedAt()));
                    helper.SaveStringPref(Prefs.created_at, response.body().getData().getUserDetails().getCreatedAt());
                    helper.SaveStringPref(Prefs.updated_at, response.body().getData().getUserDetails().getUpdatedAt());
                    helper.SaveStringPref(Prefs.token, response.body().getData().getUserDetails().getToken());
                    helper.ApplyPref();

                    Intent intent = new Intent(LoginActivity.this, AddAddressActivity.class);
                    intent.putExtra(AppConfig.BUNDLE.Token, response.body().getData().getUserDetails().getToken());
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    finish();

                } else if (response.body().getData().getUserDetails().getRole().contains("driver")
                        || response.body().getData().getUserDetails().getRole().contains("driver")
                        || response.body().getData().getUserDetails().getRole().matches("driver")
                        || response.body().getData().getUserDetails().getRole().equals("driver")) {
                    try {
                        progress.hideDialog();
                        helper.initPref();
                        helper.SaveBooleanPref(Prefs.USER_LOGIN, true);
                        helper.SaveStringPref(Prefs.id, String.valueOf(response.body().getData().getUserDetails().getId()));
                        helper.SaveStringPref(Prefs.parent_id, String.valueOf(response.body().getData().getUserDetails().getParentId()));
                        helper.SaveStringPref(Prefs.company_id, String.valueOf(response.body().getData().getUserDetails().getCompanyId()));
                        helper.SaveStringPref(Prefs.first_name, response.body().getData().getUserDetails().getFirstName());
                        helper.SaveStringPref(Prefs.last_name, response.body().getData().getUserDetails().getLastName());
                        helper.SaveStringPref(Prefs.company_name, response.body().getData().getUserDetails().getCompanyName());
                        helper.SaveStringPref(Prefs.company_image, String.valueOf(response.body().getData().getUserDetails().getCompanyImage()));
                        helper.SaveStringPref(Prefs.contact_person, response.body().getData().getUserDetails().getContactPerson());
                        helper.SaveStringPref(Prefs.profile_picture, String.valueOf(response.body().getData().getUserDetails().getProfilePicture()));
                        helper.SaveStringPref(Prefs.email, response.body().getData().getUserDetails().getEmail());
                        helper.SaveStringPref(Prefs.email_verified_at, String.valueOf(response.body().getData().getUserDetails().getEmailVerifiedAt()));
                        helper.SaveStringPref(Prefs.phone_number, response.body().getData().getUserDetails().getPhoneNumber());
                        helper.SaveStringPref(Prefs.address, response.body().getData().getUserDetails().getAddress());
                        helper.SaveStringPref(Prefs.country, response.body().getData().getUserDetails().getCountry());
                        helper.SaveStringPref(Prefs.state, response.body().getData().getUserDetails().getState());
                        helper.SaveStringPref(Prefs.city, response.body().getData().getUserDetails().getCity());
                        helper.SaveStringPref(Prefs.deviceToken, response.body().getData().getUserDetails().getDeviceToken());
                        helper.SaveStringPref(Prefs.devicetype, response.body().getData().getUserDetails().getDeviceType());
                        helper.SaveStringPref(Prefs.latitude, response.body().getData().getUserDetails().getLatitude());
                        helper.SaveStringPref(Prefs.longitude, response.body().getData().getUserDetails().getLongitude());
                        helper.SaveStringPref(Prefs.status, response.body().getData().getUserDetails().getStatus());
                        helper.SaveStringPref(Prefs.userstatus, response.body().getData().getUserDetails().getUserStatus());
                        helper.SaveStringPref(Prefs.authy_id, response.body().getData().getUserDetails().getAuthyId());
                        helper.SaveStringPref(Prefs.user_unique_code, response.body().getData().getUserDetails().getUserUniqueCode());
                        helper.SaveStringPref(Prefs.taxId, String.valueOf(response.body().getData().getUserDetails().getTaxId()));
                        helper.SaveStringPref(Prefs.commission_type, response.body().getData().getUserDetails().getCommissionType());
                        helper.SaveStringPref(Prefs.commission, String.valueOf(response.body().getData().getUserDetails().getCommission()));
                        helper.SaveStringPref(Prefs.role, response.body().getData().getUserDetails().getRole());
                        helper.SaveStringPref(Prefs.documentstatus, response.body().getData().getUserDetails().getDocumentStatus());
                        helper.SaveStringPref(Prefs.deleted_at, String.valueOf(response.body().getData().getUserDetails().getDeletedAt()));
                        helper.SaveStringPref(Prefs.created_at, response.body().getData().getUserDetails().getCreatedAt());
                        helper.SaveStringPref(Prefs.updated_at, response.body().getData().getUserDetails().getUpdatedAt());
                        helper.SaveStringPref(Prefs.token, response.body().getData().getUserDetails().getToken());
                        helper.SaveStringPref(Prefs.support_number, response.body().getData().getUserDetails().getSupportNumber());
                        helper.ApplyPref();


                        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        finish();


                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    }

                } else {
                    progress.hideDialog();
                    Toast.makeText(LoginActivity.this, "Something Went Wrong..!!", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<RespLogin> call, Throwable t) {
                progress.hideDialog();

                Log.d(TAG, "error login :" + t.getLocalizedMessage());
                Log.d(TAG, "error login :" + t.getMessage());


            }
        });
    }

}
