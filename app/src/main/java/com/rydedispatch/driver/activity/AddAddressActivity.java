package com.rydedispatch.driver.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.rydedispatch.driver.R;
import com.rydedispatch.driver.adapter.GooglePlaceAdapter;
import com.rydedispatch.driver.network.ApiClient;
import com.rydedispatch.driver.network.ApiInterface;
import com.rydedispatch.driver.response.RespAddAddress;
import com.rydedispatch.driver.utils.AppConfig;
import com.rydedispatch.driver.utils.PreferenceHelper;
import com.rydedispatch.driver.utils.Prefs;
import com.rydedispatch.driver.utils.Progress;
import com.rydedispatch.driver.utils.Utils;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddAddressActivity extends AppCompatActivity {

    private static final String TAG = AddAddressActivity.class.getSimpleName();
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.tvTitle)
    AppCompatTextView tvTitle;

    @BindView(R.id.input_search)
    AutoCompleteTextView mSearchText;

    @BindView(R.id.ll_next)
    LinearLayout llnext;
    @BindView(R.id.ib_close)
    ImageButton ibclose;
    @BindView(R.id.tv_address)
    AppCompatTextView tvaddress;
    @BindView(R.id.skip)
    AppCompatTextView tvskip;

    private Utils utils;
    private Progress progress;
    protected PreferenceHelper helper;
    private String accept = "", address = "", country = "", state = "", city = "", token = "",
            authorization = "";
    private Double lat, lang;
    private Bundle bundle;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_address);
        ButterKnife.bind(this);

        tvTitle.setText(getString(R.string.res_adddress1));

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();
            }
        });


        bundle = getIntent().getExtras();
        if (bundle != null) {
            token = bundle.getString(AppConfig.BUNDLE.Token);
        }

        utils = new Utils(this);
        progress = new Progress(this);
        helper = new PreferenceHelper(this, Prefs.PREF_FILE);

        if(TextUtils.isEmpty(token)) {
            token = helper.LoadStringPref(Prefs.token, "");

            accept = "application/json";
            authorization = "Bearer " + token;
        }
        else {
            token = bundle.getString(AppConfig.BUNDLE.Token);

        }

        accept = "application/json";
        authorization = "Bearer " + helper.LoadStringPref(Prefs.token, "");

        mSearchText = (AutoCompleteTextView) findViewById(R.id.input_search);

        Log.d(TAG, "token  :" + token);
        Log.d(TAG, "authorization  :" + authorization);

        GooglePlaceAdapter adapter = new GooglePlaceAdapter(AddAddressActivity.this, R.layout.autocompletelistitem);
        mSearchText.setAdapter(adapter);


        mSearchText.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                in.hideSoftInputFromWindow(view.getApplicationWindowToken(), 0);
                String str = (String) adapterView.getItemAtPosition(i);
                String[] places = str.split("@");


                tvaddress.setVisibility(View.VISIBLE);

                String location = mSearchText.getText().toString();
                List<Address> addressList;


                if (!location.equals("")) {
                    Geocoder geocoder = new Geocoder(AddAddressActivity.this);

                    try {
                        addressList = geocoder.getFromLocationName(location, 5);

                        if (addressList != null) {
                            for (int j = 0; j < addressList.size(); j++) {

                                getAddressnew(addressList.get(j).getLatitude(), addressList.get(j).getLongitude());
                            }
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

            }
        });


    }


    private String getAddressnew(double latitude, double longitude) {
        Geocoder geocoder = new Geocoder(AddAddressActivity.this, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(latitude, longitude, 1);
            Address obj = addresses.get(0);
            String add = obj.getAddressLine(0);


            state = obj.getAdminArea();
            country = obj.getCountryName();
            lat = obj.getLatitude();
            lang = obj.getLongitude();

            if (city == null || city.equalsIgnoreCase("")) {
                city = obj.getSubAdminArea();
            } else {
                city = obj.getLocality();
            }

            return add;
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
        }
        return "";
    }


    @OnClick({R.id.ll_next, R.id.ib_close,
            R.id.iv_back, R.id.skip})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.skip:
                startActivity(new Intent(AddAddressActivity.this, AddVehicleActivity.class)
                        .putExtra(AppConfig.BUNDLE.LOGIN, false)
                        .putExtra(AppConfig.BUNDLE.SelectManually, false));
                break;
            case R.id.ll_next:
                llNex();
                break;
            case R.id.ib_close:
                mSearchText.setText("");
                tvaddress.setVisibility(View.GONE);
                break;
            case R.id.iv_back:
                finish();
                break;
        }
    }

    private void llNex() {


        if (TextUtils.isEmpty(country)) {
            Toast.makeText(this, "Please Select other address", Toast.LENGTH_SHORT).show();
            return;
        }
        if (TextUtils.isEmpty(state)) {
            Toast.makeText(this, "Please Select other address", Toast.LENGTH_SHORT).show();
            return;
        }

        if (TextUtils.isEmpty(city)) {
            Toast.makeText(this, "Please Select other address", Toast.LENGTH_SHORT).show();
            return;
        }

        if (lat == null) {
            Toast.makeText(this, "Please Select other address", Toast.LENGTH_SHORT).show();
            return;
        }

        if (lang == null) {
            Toast.makeText(this, "Please Select other address", Toast.LENGTH_SHORT).show();
            return;
        }


        hideSoftKeyboard();
        addAddress(accept, authorization, mSearchText.getText().toString());


//        startActivity(new Intent(AddAddressActivity.this, AddVehicleActivity.class));
    }


    private void hideSoftKeyboard() {
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }


    private void addAddress(String accept, String authorization, String address) {

        progress.createDialog(false);
        progress.DialogMessage(getString(R.string.please_wait));
        progress.showDialog();

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        RequestBody address1 = RequestBody.create(MediaType.parse("multipart/form-data"), address);
        RequestBody country1 = RequestBody.create(MediaType.parse("multipart/form-data"), country);
        RequestBody state1 = RequestBody.create(MediaType.parse("multipart/form-data"), state);
        RequestBody city1 = RequestBody.create(MediaType.parse("multipart/form-data"), city);
        RequestBody latitude1 = RequestBody.create(MediaType.parse("multipart/form-data"), String.valueOf(lat));
        RequestBody longitude1 = RequestBody.create(MediaType.parse("multipart/form-data"), String.valueOf(lang));

        final Call<RespAddAddress> respSendOTPCall = apiService.ADD_ADDRESS_CALL(accept, authorization,
                address1, country1, state1, city1, latitude1, longitude1);
        respSendOTPCall.enqueue(new Callback<RespAddAddress>() {
            @Override
            public void onResponse(Call<RespAddAddress> call, Response<RespAddAddress> response) {
                progress.hideDialog();

                if (response.body().getMessage().contains("Unauthenticated")) {
                    openUnauth();
                    return;
                }

                if (response.body().getMessage().contains("Address has been updated successfully.")) {
                    startActivity(new Intent(AddAddressActivity.this, AddVehicleActivity.class)
                            .putExtra(AppConfig.BUNDLE.LOGIN, false)
                            .putExtra(AppConfig.BUNDLE.SelectManually, false));
                } else {
                    Toast.makeText(AddAddressActivity.this, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();

                }

            }

            @Override
            public void onFailure(Call<RespAddAddress> call, Throwable t) {
                progress.hideDialog();
                Toast.makeText(AddAddressActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void openUnauth() {


        final Dialog mBottomSheetDialog = new Dialog(this, R.style.MaterialDialogSheet);
        mBottomSheetDialog.setContentView(R.layout.dialog_unauthorised);
        mBottomSheetDialog.setCancelable(false);
        mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        mBottomSheetDialog.getWindow().setGravity(Gravity.CENTER);
        mBottomSheetDialog.setCanceledOnTouchOutside(false);
        AppCompatTextView btn_ok = mBottomSheetDialog.findViewById(R.id.btn_ok);

        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                progress.createDialog(false);
                progress.DialogMessage(getString(R.string.msg_logout));
                progress.showDialog();
                AsyncTask.execute(new Runnable() {
                    @Override
                    public void run() {

                        try {
                            FirebaseInstanceId.getInstance().deleteInstanceId();
                            helper.clearAllPrefs();
                            startActivity(new Intent(AddAddressActivity.this, LoginActivity.class)
                                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                            progress.hideDialog();
                            AddAddressActivity.this.finish();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        });
        mBottomSheetDialog.show();

    }

}
