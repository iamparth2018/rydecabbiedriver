package com.rydedispatch.driver.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.rydedispatch.driver.R;
import com.rydedispatch.driver.network.PostRequest;
import com.rydedispatch.driver.response.Questions;
import com.rydedispatch.driver.utils.NonSwipeableViewPager;
import com.rydedispatch.driver.utils.PreferenceHelper;
import com.rydedispatch.driver.utils.Prefs;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Locale;

import okhttp3.FormBody;
import okhttp3.RequestBody;

import static com.rydedispatch.driver.utils.AppConfig.URL.URL_ADD_INSPECTION;
import static com.rydedispatch.driver.utils.AppConfig.URL.URL_GET_QUESTION;

public class VehicleInspection extends AppCompatActivity {

    private static final String TAG = VehicleInspection.class.getSimpleName();
    private NonSwipeableViewPager viewPager;
    private TextView tv_pages;
    private EditText edt_mileage;
    private String token;
    private ArrayList<Integer> ans = new ArrayList<>();
    private ArrayList<Questions> questions;
    private static final int MENU_ITEM_ITEM1 = 1;
    private String accept = "",
            authorization = "";
    PreferenceHelper helper;
    Menu MainMenu;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vehicle);

        viewPager = findViewById(R.id.view_pager);
        Button btnFault = findViewById(R.id.btnFault);
        Button btnOk = findViewById(R.id.btnOk);
        edt_mileage = findViewById(R.id.edt_mileage);
        tv_pages = findViewById(R.id.tv_pages);
//        question = getResources().getStringArray(R.array.question);

        helper = new PreferenceHelper(this, Prefs.PREF_FILE);
        token = helper.LoadStringPref(Prefs.token, "");

        viewPager.disableScroll(true);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int i) {
                tv_pages.setText(String.format(Locale.US, "Pages %d of %d", i + 1, questions.size()));
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });

        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nextPage();

                if (ans.size() == questions.size()) {
                    ans.remove(ans.size() - 1);
                    ans.add(0);
                } else {
                    ans.add(0);
                }
//                ans.add(0);
            }
        });

        btnFault.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nextPage();
//                ans.add(1);
                if (ans.size() == questions.size()) {
                    ans.remove(ans.size() - 1);
                    ans.add(1);
                } else {
                    ans.add(1);
                }
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        accept = "application/json";
        authorization = "Bearer " + helper.LoadStringPref(Prefs.token, "");

        Log.d(TAG, "auth :" + authorization);
        try {
            getQuestion();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        }

    }

    public void nextPage() {
        int current = viewPager.getCurrentItem() + 1;
        if (current < questions.size()) {
            viewPager.setCurrentItem(current);
        }

        if (current == questions.size()) {
//            MainMenu.findItem(MENU_ITEM_ITEM1).setVisible(true);
//            btnFault.setEnabled(false);
//            btnOk.setEnabled(false);

            if (!validateMileage()) {
                return;
            }

            submitAns();
        }
    }

    private boolean validateMileage() {
        if (edt_mileage.getText().toString().trim().isEmpty()) {
            edt_mileage.requestFocus();
            Toast.makeText(getApplicationContext(), "Enter Mileage", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    class CustomPagerAdapter extends PagerAdapter {
        LayoutInflater mLayoutInflater;

        CustomPagerAdapter() {
            mLayoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            notifyDataSetChanged();
        }

        @Override
        public int getCount() {
            return questions.size();
        }

        @Override
        public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
            return view == object;
        }

        @NotNull
        @Override
        public Object instantiateItem(@NonNull ViewGroup container, int position) {
            View itemView = mLayoutInflater.inflate(R.layout.item_inspection, container, false);

            TextView que_no = itemView.findViewById(R.id.que_no);
            TextView question_name = itemView.findViewById(R.id.question_name);
            LinearLayout ll_options = itemView.findViewById(R.id.options);

            que_no.setText("Question " + (position + 1));
            question_name.setText(questions.get(position).getQuestion());

            if (questions.get(position).getOptions().size() <= 0) {
                ll_options.setVisibility(View.GONE);
            } else {
                ll_options.setVisibility(View.VISIBLE);
            }

            TextView[] textViews = new TextView[questions.get(position).getOptions().size()];

            for (int i = 0; i < textViews.length; i++) {
                textViews[i] = new TextView(VehicleInspection.this);
                textViews[i].setText(questions.get(position).getOptions().get(i));
                textViews[i].setId(i + 100);
                textViews[i].setTextColor(Color.BLACK);
                textViews[i].setGravity(View.TEXT_ALIGNMENT_CENTER);
                textViews[i].setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.ic_done, 0, 0, 0);
                textViews[i].setPadding(10, 10, 10, 10);
                textViews[i].setCompoundDrawablePadding(15);

                Typeface typeface = ResourcesCompat.getFont(VehicleInspection.this, R.font.helveticaneuemedium);
                textViews[i].setTypeface(typeface, Typeface.BOLD);

                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                params.setMargins(20, 20, 20, 20);
                textViews[i].setLayoutParams(params);

                ll_options.addView(textViews[i]);
            }

            container.addView(itemView);
            return itemView;
        }

        @Override
        public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
            container.removeView((RelativeLayout) object);
        }
    }

    private void getQuestion() {
        questions = new ArrayList<>();
        new PostRequest(VehicleInspection.this, token, URL_GET_QUESTION, new PostRequest.Callbacks() {
            @Override
            public void onFail(final String error) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(VehicleInspection.this, error, Toast.LENGTH_LONG).show();
                    }
                });
            }

            @Override
            public void onSuccess(JSONObject response) {

                try {
                    JSONArray jsonArray = response.getJSONArray("data");
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String question = jsonObject.getString("question");
                        ArrayList<String> optionsStrings = new ArrayList<>();
                        JSONArray options = jsonObject.optJSONArray("options");
                        if (options != null && options.length() > 0) {
                            for (int j = 0; j < options.length(); j++) {
                                optionsStrings.add(options.getString(j));
                            }
                        }
                        questions.add(new Questions(question, optionsStrings));
                    }

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            viewPager.setAdapter(new CustomPagerAdapter());
                        }
                    });


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }).execute();
    }

    private void submitAns() {

        Integer[] ints = new Integer[ans.size()];
        ans.toArray(ints);

        Log.e("ans", Arrays.toString(ints));

        RequestBody formBody = new FormBody.Builder()
                .add("questions", Arrays.toString(ints))
                .add("mileage", edt_mileage.getText().toString())
                .build();
        new PostRequest(VehicleInspection.this, token, URL_ADD_INSPECTION, formBody, new PostRequest.Callbacks() {
            @Override
            public void onFail(final String error) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(VehicleInspection.this, error, Toast.LENGTH_LONG).show();
                    }
                });
            }

            @Override
            public void onSuccess(JSONObject response) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(VehicleInspection.this, "Vehicle Inspection Done..!!", Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(VehicleInspection.this, MainActivity.class));
                        finish();
                    }
                });
            }
        }).execute();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(Menu.NONE, MENU_ITEM_ITEM1, Menu.NONE, "Submit");
        menu.findItem(MENU_ITEM_ITEM1).setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM | MenuItem.SHOW_AS_ACTION_WITH_TEXT);
        MainMenu = menu;
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MainMenu.findItem(MENU_ITEM_ITEM1).setVisible(false);
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == MENU_ITEM_ITEM1) {
            submitAns();
            return true;
        }
        return false;
    }

}
