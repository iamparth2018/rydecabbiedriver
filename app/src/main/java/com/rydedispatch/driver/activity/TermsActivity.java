package com.rydedispatch.driver.activity;

import android.animation.ObjectAnimator;
import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.DecelerateInterpolator;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.rydedispatch.driver.R;

public class TermsActivity extends AppCompatActivity {
    ProgressDialog pd;
    AppCompatTextView iv_back;
    AppCompatTextView tv_title;
    ProgressBar pbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_terms);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_custom);
        setSupportActionBar(toolbar);

        iv_back = findViewById(R.id.iv_back);
        tv_title = findViewById(R.id.tv_title);
        tv_title.setText(R.string.res_terms);

        WebView htmlWebView = (WebView) findViewById(R.id.webView);
        pbar = (ProgressBar) findViewById(R.id.circular_progress_bar);
        htmlWebView.setWebViewClient(new CustomWebViewClient());
        WebSettings webSetting = htmlWebView.getSettings();
        webSetting.setJavaScriptEnabled(true);
        webSetting.setDisplayZoomControls(true);

//        pd = new ProgressDialog(TermsActivity.this);
//        pd.setMessage("Loading...");
//        pd.show();

        pbar.setVisibility(View.VISIBLE);
        ObjectAnimator anim = ObjectAnimator.ofInt(pbar, "progress", 0, 100);
        anim.setDuration(15000);
        anim.setInterpolator(new DecelerateInterpolator());
        anim.start();

        htmlWebView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                pbar.setVisibility(View.GONE);
                htmlWebView.setVisibility(View.VISIBLE);
            }

            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                Toast.makeText(TermsActivity.this, "Error:" + description, Toast.LENGTH_SHORT).show();
                pbar.setVisibility(View.GONE);
                htmlWebView.setVisibility(View.VISIBLE);
            }
        });

        htmlWebView.loadUrl("http://167.172.247.117/termsandCondition");
//        htmlWebView.loadUrl("https://portal.rydedispatch.com/termsandCondition");

    }

    public void onBack(View view) {
        finish();
    }

    private class CustomWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
    }

}
