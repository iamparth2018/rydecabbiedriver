package com.rydedispatch.driver.activity;

import android.animation.ObjectAnimator;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.rydedispatch.driver.R;
import com.rydedispatch.driver.adapter.DocumentAdapter;
import com.rydedispatch.driver.network.ApiClient;
import com.rydedispatch.driver.network.ApiInterface;
import com.rydedispatch.driver.response.RespGetDocumentList;
import com.rydedispatch.driver.utils.AppConfig;
import com.rydedispatch.driver.utils.PreferenceHelper;
import com.rydedispatch.driver.utils.Prefs;
import com.rydedispatch.driver.utils.Progress;
import com.rydedispatch.driver.utils.Utils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class DocumentActivity extends AppCompatActivity {

    private static final String TAG = DocumentActivity.class.getSimpleName();

    @BindView(R.id.circular_progress_bar)
    ProgressBar pbar;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.tvTitle)
    AppCompatTextView tvTitle;
    @BindView(R.id.tv_source)
    AppCompatTextView tvSource;
    @BindView(R.id.rv_documents)
    RecyclerView rvdocuments;
    @BindView(R.id.btn_submit)
    Button btnSubmit;


    private String accept = "", authorization = "", id = "", user_id = "", documentName = "", unique_id = "",
            frontdocumentUrl = "", backdocumenturl = "", documentType = "", issuedDate = "", expiryDate = "",
            status ="", deleteAt = "", createdAt = "", updatedAt = "";
    private Utils utils;
    private File mCurrentPhoto;

    protected PreferenceHelper helper;
    protected Progress progress;
    private LinearLayoutManager mLayoutManager;
    private DocumentAdapter mDocumentAdapter;
    private List<RespGetDocumentList.DataEntity> listData = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_document);
        ButterKnife.bind(this);

        tvTitle.setText(getString(R.string.res_document));

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();
            }
        });

        utils = new Utils(DocumentActivity.this);
        progress = new Progress(DocumentActivity.this);
        helper = new PreferenceHelper(DocumentActivity.this, Prefs.PREF_FILE);

        accept = "application/json";
        authorization = "Bearer " + helper.LoadStringPref(Prefs.token, "");

        mLayoutManager = new LinearLayoutManager(DocumentActivity.this);
        rvdocuments.setLayoutManager(mLayoutManager);
        rvdocuments.setItemAnimator(new DefaultItemAnimator());
        mDocumentAdapter = new DocumentAdapter(DocumentActivity.this, listData);
        rvdocuments.setAdapter(mDocumentAdapter);

        mDocumentAdapter.setListener(new DocumentAdapter.customOnclickListener() {
            @Override
            public void onclick(int adapterPosition, RespGetDocumentList.DataEntity documentsEntityList) {
                id = String.valueOf(documentsEntityList.getId());
                Log.d(TAG, "id :" + documentsEntityList.getId());
                Log.d(TAG, "name :" + documentsEntityList.getDocumentName());

                id = String.valueOf(documentsEntityList.getId());
                user_id = String.valueOf(documentsEntityList.getUserId());
                unique_id = documentsEntityList.getUniqueId();
                documentName = documentsEntityList.getDocumentName();
                frontdocumentUrl = documentsEntityList.getFrontDocumentUrl();
                backdocumenturl = String.valueOf(documentsEntityList.getBackDocumentUrl());
                documentType = documentsEntityList.getDocumentType();
                issuedDate = documentsEntityList.getIssueDate();
                expiryDate = documentsEntityList.getExpiryDate();
                status = documentsEntityList.getStatus();
                deleteAt = String.valueOf(documentsEntityList.getDeletedAt());
                createdAt = documentsEntityList.getCreatedAt();
                updatedAt = documentsEntityList.getUpdatedAt();

                if (documentName.contains("Driving License")) {
                    startActivity(new Intent(DocumentActivity.this, DocumentDetailsActivity.class)
                            .putExtra(AppConfig.BUNDLE.id, id)
                            .putExtra(AppConfig.BUNDLE.user_id, user_id)
                            .putExtra(AppConfig.BUNDLE.unique_id, unique_id)
                            .putExtra(AppConfig.BUNDLE.document_name, documentName)
                            .putExtra(AppConfig.BUNDLE.front_document_url, frontdocumentUrl)
                            .putExtra(AppConfig.BUNDLE.back_document_url, backdocumenturl)
                            .putExtra(AppConfig.BUNDLE.document_type, documentType)
                            .putExtra(AppConfig.BUNDLE.issue_date, issuedDate)
                            .putExtra(AppConfig.BUNDLE.expiry_date, expiryDate)
                            .putExtra(AppConfig.BUNDLE.status, status)
                            .putExtra(AppConfig.BUNDLE.deleted_at, deleteAt)
                            .putExtra(AppConfig.BUNDLE.created_at, createdAt)
                            .putExtra(AppConfig.BUNDLE.updated_at, updatedAt));

                }
                else {
                    startActivity(new Intent(DocumentActivity.this, DocumentDetailsActivity.class)
                            .putExtra(AppConfig.BUNDLE.id, id)
                            .putExtra(AppConfig.BUNDLE.user_id, user_id)
                            .putExtra(AppConfig.BUNDLE.unique_id, unique_id)
                            .putExtra(AppConfig.BUNDLE.document_name, documentName)
                            .putExtra(AppConfig.BUNDLE.front_document_url, frontdocumentUrl)
                            .putExtra(AppConfig.BUNDLE.back_document_url, backdocumenturl)
                            .putExtra(AppConfig.BUNDLE.document_type, documentType)
                            .putExtra(AppConfig.BUNDLE.issue_date, issuedDate)
                            .putExtra(AppConfig.BUNDLE.expiry_date, expiryDate)
                            .putExtra(AppConfig.BUNDLE.status, status)
                            .putExtra(AppConfig.BUNDLE.deleted_at, deleteAt)
                            .putExtra(AppConfig.BUNDLE.created_at, createdAt)
                            .putExtra(AppConfig.BUNDLE.updated_at, updatedAt));
                }

            }
        });


        getDocumentList(accept, authorization);
    }

    /*@Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        EasyImage.handleActivityResult(requestCode, resultCode, data, DocumentActivity.this, new DefaultCallback() {


            @Override
            public void onImagePickerError(Exception e, EasyImage.ImageSource source, int type) {
                //Some error handling
                Toast.makeText(DocumentActivity.this, "Error " + e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();

                if (source == EasyImage.ImageSource.CAMERA) {
                    File photoFile = EasyImage.lastlyTakenButCanceledPhoto(DocumentActivity.this);
                    if (photoFile != null) photoFile.delete();
                }

            }

            @Override
            public void onImagePicked(File imageFile, EasyImage.ImageSource source, int type) {

                if (requestCode == REQ_PICK_PICTURE_FROM_DOCUMENTS ||
                        source == EasyImage.ImageSource.DOCUMENTS) {
                    mCurrentPhoto = imageFile;
                    tvSource.setText("" + imageFile.getName());

                    Log.d(TAG, "image document :" + imageFile.getName());
                    return;
                }

                if (source == EasyImage.ImageSource.CAMERA) {

                    mCurrentPhoto = imageFile;
                    tvSource.setText("" + imageFile.getName());
                    return;
                }
                if (source == EasyImage.ImageSource.GALLERY) {
                    mCurrentPhoto = imageFile;
                    tvSource.setText("" + imageFile.getName());

                    return;
                }
            }

        });

    }*/


    private void getDocumentList(String accept, String authorization) {
//        progress.createDialog(false);
//        progress.DialogMessage(getString(R.string.please_wait));
//        progress.showDialog();
        pbar.setVisibility(View.VISIBLE);
        ObjectAnimator anim = ObjectAnimator.ofInt(pbar, "progress", 0, 100);
        anim.setDuration(15000);
        anim.setInterpolator(new DecelerateInterpolator());
        anim.start();

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        final Call<RespGetDocumentList> respDashboardDriverCall = apiService.GET_DOCUMENT_LIST_CALL(accept, authorization);
        respDashboardDriverCall.enqueue(new Callback<RespGetDocumentList>() {
            @Override
            public void onResponse(Call<RespGetDocumentList> call, Response<RespGetDocumentList> response) {
//                progress.hideDialog();
                pbar.setVisibility(View.GONE);
                rvdocuments.setVisibility(View.VISIBLE);


                listData.addAll(response.body().getData());
                mDocumentAdapter.notifyDataSetChanged();

                if (response.body().getMessage().contains("Unauthenticated")) {
                    openAlertUnauthLogout();
                    return;
                }
            }

            @Override
            public void onFailure(Call<RespGetDocumentList> call, Throwable t) {
//                progress.hideDialog();
                pbar.setVisibility(View.GONE);
                rvdocuments.setVisibility(View.VISIBLE);
//                Toast.makeText(MainActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @OnClick({R.id.iv_back, R.id.btn_submit})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.btn_submit:
                if (mCurrentPhoto == null) {
                    Toast.makeText(this, "" + getString(R.string.res_doc), Toast.LENGTH_SHORT).show();
                    return;
                }

                Log.d(TAG, "id do post:" + id);
                Log.d(TAG, "mCurrentPhoto do post:" + mCurrentPhoto);

                break;
        }
    }


    private void openAlertUnauthLogout() {
        final Dialog mBottomSheetDialog = new Dialog(this, R.style.MaterialDialogSheet);
        mBottomSheetDialog.setContentView(R.layout.dialog_unauthorised);
        mBottomSheetDialog.setCancelable(true);
        mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        mBottomSheetDialog.getWindow().setGravity(Gravity.CENTER);
        mBottomSheetDialog.setCanceledOnTouchOutside(true);
        AppCompatTextView btn_ok = mBottomSheetDialog.findViewById(R.id.btn_ok);

        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                helper.clearAllPrefs();
                startActivity(new Intent(DocumentActivity.this, LoginActivity.class));
                finish();
            }
        });
        mBottomSheetDialog.show();


    }


}
