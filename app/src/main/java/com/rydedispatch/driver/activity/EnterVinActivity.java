package com.rydedispatch.driver.activity;

import android.content.Intent;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

import com.rydedispatch.driver.R;
import com.rydedispatch.driver.utils.AppConfig;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class EnterVinActivity extends AppCompatActivity {
    private static final String TAG = EnterVinActivity.class.getSimpleName();

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.tvTitle)
    AppCompatTextView tvTitle;

    @BindView(R.id.btn_next)
    Button btnnext;
    @BindView(R.id.et_enter_vin)
    TextInputEditText editText;
    private String etentervin = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enter_vin);
        ButterKnife.bind(this);

        tvTitle.setText(getString(R.string.enter_vin));

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();
            }
        });

//        editText.setText("5UXTR9C51KLP89032");
    }

    @OnClick({R.id.btn_next,R.id.iv_back})
    public void onViewClicked(View view) {
        switch (view.getId()) {

            case R.id.btn_next:
                next();
                break;

            case R.id.iv_back:
                finish();
                break;
        }
    }

    private void next() {
        etentervin = Objects.requireNonNull(editText.getText()).toString();

        if (etentervin.isEmpty() || TextUtils.isEmpty(etentervin)) {
            Toast.makeText(this, R.string.res_enter_vin_empty, Toast.LENGTH_SHORT).show();
            return;
        }


        startActivity(new Intent(EnterVinActivity.this, SelectManuallyActivity.class)
                .putExtra(AppConfig.BUNDLE.barcode, etentervin)
                .putExtra(AppConfig.BUNDLE.SelectManually, true));
    }
}