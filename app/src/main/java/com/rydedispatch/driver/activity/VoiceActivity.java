package com.rydedispatch.driver.activity;

import android.Manifest;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.media.AudioAttributes;
import android.media.AudioFocusRequest;
import android.media.AudioManager;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.os.Vibrator;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Chronometer;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.google.firebase.iid.FirebaseInstanceId;
import com.rydedispatch.driver.R;
import com.rydedispatch.driver.network.PostRequest;
import com.rydedispatch.driver.utils.AppConfig;
import com.rydedispatch.driver.utils.ConnectionDetector;
import com.rydedispatch.driver.utils.PreferenceHelper;
import com.rydedispatch.driver.utils.Prefs;
import com.twilio.voice.Call;
import com.twilio.voice.CallException;
import com.twilio.voice.CallInvite;
import com.twilio.voice.ConnectOptions;
import com.twilio.voice.RegistrationException;
import com.twilio.voice.RegistrationListener;
import com.twilio.voice.Voice;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Locale;

import okhttp3.RequestBody;

import static com.rydedispatch.driver.utils.AppConfig.URL_GET_AUDIO_TOKEN;


public class VoiceActivity extends AppCompatActivity {

    private static final String TAG = "VoiceActivity";

    private static final int MIC_PERMISSION_REQUEST_CODE = 1;

    private String identity = "",accessToken = "", token = "", phoneNumber = "", client = "",
            payment_type = "", fromNumber = "", user_unique_code = "", passengerUniqueCode = "",
            username = "", passengerName = "", displaypassengername = "", support_number = "", passengerphoneNumber = "";
    private AudioManager audioManager;
    private int savedAudioMode = AudioManager.MODE_INVALID;
    LinearLayout incoming;
    RelativeLayout call;
    private NotificationManager notificationManager;
    private boolean isReceiverRegistered = false;
    private VoiceBroadcastReceiver voiceBroadcastReceiver;
    private int activeCallNotificationId;

    // Empty HashMap, never populated for the Quickstart
    HashMap<String, String> twiMLParams = new HashMap<>();
    private ConnectionDetector connectionDetector;

    private ToggleButton muteActionFab;
    private ToggleButton speakerActionFab;
    private Chronometer chronometer;
    private TextView txt_clinetname;
    private TextView txt_readername;

    public static final String INCOMING_CALL_INVITE = "INCOMING_CALL_INVITE";
    public static final String CANCELLED_CALL_INVITE = "CANCELLED_CALL_INVITE";
    public static final String INCOMING_CALL_NOTIFICATION_ID = "INCOMING_CALL_NOTIFICATION_ID";
    public static final String ACTION_INCOMING_CALL = "ACTION_INCOMING_CALL";
    public static final String ACTION_CANCEL_CALL = "ACTION_CANCEL_CALL";
    public static final String ACTION_FCM_TOKEN = "ACTION_FCM_TOKEN";

    private CallInvite activeCallInvite;
    private Call activeCall;

    RegistrationListener registrationListener = registrationListener();
    Call.Listener callListener = callListener();

    PreferenceHelper helper;
    private int checkoutgoing = 0;
    private Bundle bundle;
    private boolean isConnect = false;
    private Ringtone mRingtone;
    private Vibrator vibrator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_outgoing);

//        helper = new PreferenceHelper(this);
        helper = new PreferenceHelper(this, Prefs.PREF_FILE);
        notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);


        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
                | WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON
                | WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        Intent i = getIntent();
        checkoutgoing = i.getIntExtra("checkOutgoing", 0);

        accessToken = helper.getTwilioAuth();
        identity = helper.getIdentity();
//        token = helper.LoadStringPref(Prefs.token, "");
        connectionDetector = new ConnectionDetector(this);

        incoming = findViewById(R.id.incoming);
        call = findViewById(R.id.call);

        ImageButton hangupActionFab = findViewById(R.id.hangup_action_fab);
        ImageButton connect_call = findViewById(R.id.connect_call);
        ImageButton connect_disconnect = findViewById(R.id.connect_disconnect);
        muteActionFab = findViewById(R.id.tb_mute);
        speakerActionFab = findViewById(R.id.tb_speaker);
        chronometer = findViewById(R.id.chronometer);
        txt_clinetname = findViewById(R.id.txt_clinetname);
        txt_readername = findViewById(R.id.txt_readername);


        bundle = getIntent().getExtras();
        if (bundle != null) {
            user_unique_code = bundle.getString(AppConfig.BUNDLE.UserUniqueCode);
            passengerUniqueCode = bundle.getString(AppConfig.BUNDLE.PassengerUniqueCode);
            passengerName = bundle.getString(AppConfig.BUNDLE.passenger_first_name);
            displaypassengername = bundle.getString(AppConfig.BUNDLE.display_passenger_name);
            username = bundle.getString(AppConfig.BUNDLE.User_Name);
            support_number = bundle.getString(AppConfig.BUNDLE.Support_Number);
            payment_type = bundle.getString(AppConfig.BUNDLE.payment_type);
            passengerphoneNumber = bundle.getString(AppConfig.BUNDLE.passenger_phone_number);

        }
        /*Log.d(TAG, "voice passengerName:" + passengerName);
        Log.d(TAG, "checkoutgoing:" + checkoutgoing);
        Log.d(TAG, "displaypassengername:" + displaypassengername);
        Log.d(TAG, "user_unique_code:" + user_unique_code);
        Log.d(TAG, "passengerUniqueCode:" + passengerUniqueCode);
        Log.d(TAG, "username:" + username);
        Log.d(TAG, "payment_type:" + payment_type);
        Log.d(TAG, "passengerphoneNumber:" + passengerphoneNumber);*/
        Log.d(TAG, "identity : "+ identity);


        hangupActionFab.setOnClickListener(hangupActionFabClickListener());
        connect_call.setOnClickListener(answerCallClickListener());
        connect_disconnect.setOnClickListener(cancelCallClickListener());

        muteActionFab.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (activeCall != null) {
                    boolean mute = !activeCall.isMuted();
                    activeCall.mute(mute);
                    muteActionFab.setChecked(mute);
                }
            }
        });

        voiceBroadcastReceiver = new VoiceBroadcastReceiver();
        registerReceiver();

        audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        audioManager.setSpeakerphoneOn(true);

        speakerActionFab.setChecked(audioManager.isSpeakerphoneOn());
        speakerActionFab.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                speakerActionFab.setChecked(isChecked);
                audioManager.setSpeakerphoneOn(isChecked);
            }
        });


        setVolumeControlStream(AudioManager.STREAM_VOICE_CALL);

        handleIncomingCallIntent(getIntent());


        if (!checkPermissionForMicrophone()) {
            requestPermissionForMicrophone();
        } else {
            registerForCallInvites();
        }

        txt_readername.setText("Connecting");

        Log.d(TAG, "accessToken :" + accessToken);

    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        handleIncomingCallIntent(intent);
    }

    private RegistrationListener registrationListener() {
        return new RegistrationListener() {
            @Override
            public void onRegistered(@NonNull String accessToken, @NonNull String fcmToken) {
                Log.d(TAG, "Successfully registered FCM " + fcmToken + " " + checkoutgoing);

                if (checkoutgoing == 1) {
                    makecall();
                } else if (checkoutgoing == 3) {
                    makecallSupport();
                } else {
                }
            }

            @Override
            public void onError(@NonNull RegistrationException error, @NonNull String accessToken, @NonNull String fcmToken) {
                String message = String.format(Locale.US, "Registration Error: %d, %s", error.getErrorCode(), error.getMessage());
                Log.e(TAG, message);
            }
        };
    }


    private void makecallSupport() {
        if (connectionDetector.isConnectingToInternet()) {

            Log.e(TAG, "support make call : " + "my :" + user_unique_code + "   to:" + support_number);

            twiMLParams.put("PhoneNumber", "+16514139117");
            twiMLParams.put("fromnumber", "+16514139117");
            twiMLParams.put("call_type", "support");

            txt_readername.setText("+16514139117");

            ConnectOptions connectOptions = new ConnectOptions.Builder(accessToken)
                    .params(twiMLParams)
                    .build();
            activeCall = Voice.connect(VoiceActivity.this, connectOptions, callListener);
            setCallUI();

        } else {

            Toast.makeText(VoiceActivity.this, getResources().getString(R.string.conn_internet), Toast.LENGTH_SHORT).show();

        }

    }

    private void makecall() {

        if (connectionDetector.isConnectingToInternet()) {

            if (payment_type.equalsIgnoreCase("none")) {
                Log.e(TAG, "makecall none: " + "my :" + phoneNumber + "   to:" + fromNumber + "passengerphoneNumber :" + passengerphoneNumber);
                //uncomment this for static call
                twiMLParams.put("PhoneNumber", passengerphoneNumber);
//                twiMLParams.put("fromnumber", fromNumber);
                twiMLParams.put("fromnumber", helper.LoadStringPref(Prefs.user_unique_code, ""));
                twiMLParams.put("call_type", "normal");

                txt_readername.setText(displaypassengername);

                ConnectOptions connectOptions = new ConnectOptions.Builder(accessToken)
                        .params(twiMLParams)
                        .build();
                activeCall = Voice.connect(VoiceActivity.this, connectOptions, callListener);
                setCallUI();
            } else {
                Log.e(TAG, "makecall else : " + "my :" + passengerUniqueCode + "   to:" + fromNumber);
                Log.d(TAG, "identity to call : " + identity);
                //uncomment this for static call
                twiMLParams.put("PhoneNumber", passengerUniqueCode);
//                twiMLParams.put("fromnumber", fromNumber);
                twiMLParams.put("fromnumber", helper.LoadStringPref(Prefs.user_unique_code, ""));
                twiMLParams.put("call_type", "normal");

                txt_readername.setText(displaypassengername);

                ConnectOptions connectOptions = new ConnectOptions.Builder(accessToken)
                        .params(twiMLParams)
                        .build();
                activeCall = Voice.connect(VoiceActivity.this, connectOptions, callListener);
                setCallUI();
            }


        } else {

            Toast.makeText(VoiceActivity.this, getResources().getString(R.string.conn_internet), Toast.LENGTH_SHORT).show();

        }
    }


    private Call.Listener callListener() {
        return new Call.Listener() {
            @Override
            public void onRinging(@NonNull Call call) {
                txt_readername.setText("Ringing");
                Log.d(TAG, "Ringing");
            }

            @Override
            public void onConnectFailure(@NonNull Call call, @NonNull CallException error) {
                setAudioFocus(false);
                Log.d(TAG, "Connect failure");
                String message = String.format(Locale.US, "Call Error: %d, %s", error.getErrorCode(), error.getMessage());
                Log.e(TAG, message);
                resetUI();

                if (error.getErrorCode() == 31005) {
                    Toast.makeText(VoiceActivity.this, "please try again", Toast.LENGTH_SHORT).show();
                    finish();
                }
            }

            @Override
            public void onConnected(@NonNull Call call) {
                setAudioFocus(true);
                Log.d(TAG, "Connected");
                activeCall = call;
                if (checkoutgoing == 1) {
                    txt_readername.setText(displaypassengername);
                } else if (checkoutgoing == 3) {
//                    txt_readername.setText(helper.getDriverName());
                    txt_readername.setText("+16514139117");

                } else if (displaypassengername == null) {
                    txt_readername.setText(activeCall.getFrom());
                } else {
                    txt_readername.setText(activeCall.getFrom());

                }

                chronometer.setVisibility(View.VISIBLE);
                chronometer.setBase(SystemClock.elapsedRealtime());
                chronometer.start();
            }

            @Override
            public void onReconnecting(@NonNull Call call, @NonNull CallException callException) {
                Log.d(TAG, "onReconnecting");
                txt_readername.setText("Reconnecting");
            }

            @Override
            public void onReconnected(@NonNull Call call) {
                Log.d(TAG, "onReconnected");
                txt_readername.setText("Reconnected");
            }

            @Override
            public void onDisconnected(@NonNull Call call, CallException error) {
                setAudioFocus(false);
                Log.d(TAG, "Disconnected");
                if (error != null) {
                    String message = String.format(Locale.US, "Call Error: %d, %s", error.getErrorCode(), error.getMessage());
                    Log.e(TAG, message);
                }
                resetUI();
            }
        };
    }

    private void setCallUI() {
        incoming.setVisibility(View.GONE);
        call.setVisibility(View.VISIBLE);
        chronometer.setVisibility(View.VISIBLE);
        chronometer.setBase(SystemClock.elapsedRealtime());
        chronometer.start();
    }

    /*
     * Reset UI elements
     */
    private void resetUI() {
        finish();
        incoming.setVisibility(View.GONE);
        call.setVisibility(View.VISIBLE);
        chronometer.setVisibility(View.INVISIBLE);
        chronometer.stop();
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver();
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver();
    }

    private void handleIncomingCallIntent(Intent intent) {
        if (intent != null && intent.getAction() != null) {
            Log.e(TAG, intent.getAction());
            if (intent.getAction().equals(ACTION_INCOMING_CALL)) {
                activeCallInvite = intent.getParcelableExtra(INCOMING_CALL_INVITE);
                if (activeCallInvite != null) {
                    incoming.setVisibility(View.VISIBLE);
                    call.setVisibility(View.GONE);
                    activeCallNotificationId = intent.getIntExtra(INCOMING_CALL_NOTIFICATION_ID, 0);
//                    txt_clinetname.setText(activeCallInvite.getFrom() + " is calling");
                    client = activeCallInvite.getTo().replace("client:", "");
                    txt_clinetname.setText(client);

                }
                startRingtone();

            } else if (intent.getAction().equals(ACTION_CANCEL_CALL)) {

                if (!isConnect) {
                    incoming.setVisibility(View.GONE);
                    call.setVisibility(View.VISIBLE);
                    resetUI();
                }
//                incoming.setVisibility(View.GONE);
//                call.setVisibility(View.VISIBLE);
                dismissVibrate();

            } else if (intent.getAction().equals(ACTION_FCM_TOKEN)) {
                registerForCallInvites();
            }

        }
    }

    private void startRingtone() {
        Uri defaultRintoneUri = RingtoneManager.getActualDefaultRingtoneUri(this
                , RingtoneManager.TYPE_RINGTONE);
        vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        mRingtone = RingtoneManager.getRingtone(this, defaultRintoneUri);
        long pattern[] = {0, 100, 200, 300, 400};
        if (mRingtone != null)
            mRingtone.play();
        vibrator.vibrate(pattern, 0);
    }

    private void dismissVibrate() {
        if (vibrator != null) {
            vibrator.cancel();
            vibrator = null;
        }

        if (mRingtone != null) {
            mRingtone.stop();
            mRingtone = null;
        }

    }

    private void registerReceiver() {
        if (!isReceiverRegistered) {
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction(ACTION_INCOMING_CALL);
            intentFilter.addAction(ACTION_CANCEL_CALL);
            intentFilter.addAction(ACTION_FCM_TOKEN);
            LocalBroadcastManager.getInstance(this).registerReceiver(
                    voiceBroadcastReceiver, intentFilter);
            isReceiverRegistered = true;
        }
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        Toast.makeText(this, "Can't Go Back...!!!", Toast.LENGTH_SHORT).show();
    }

    private void unregisterReceiver() {
        if (isReceiverRegistered) {
            LocalBroadcastManager.getInstance(this).unregisterReceiver(voiceBroadcastReceiver);
            isReceiverRegistered = false;
        }
    }

    private class VoiceBroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(ACTION_INCOMING_CALL) || action.equals(ACTION_CANCEL_CALL)) {
                handleIncomingCallIntent(intent);
            }
        }
    }


    private void registerForCallInvites() {
        final String fcmToken = FirebaseInstanceId.getInstance().getToken();
        if (fcmToken != null && accessToken != null && accessToken.length() > 0) {
            Log.i(TAG, "Registering with FCM");
            Voice.register(accessToken, Voice.RegistrationChannel.FCM, fcmToken, registrationListener);
        } else {
            getToken();
        }
    }

    private void getToken() {
        RequestBody reqBody = RequestBody.create(new byte[0], null);

        new PostRequest(VoiceActivity.this, token, URL_GET_AUDIO_TOKEN, reqBody, new PostRequest.Callbacks() {
            @Override
            public void onFail(final String error) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(), error, Toast.LENGTH_LONG).show();
                    }
                });
            }

            @Override
            public void onSuccess(JSONObject response) {
                try {
                    JSONObject data = response.getJSONObject("data");
                    accessToken = data.getString("token");
                    identity = data.getString("identity");

                    Log.d(TAG , "URL_GET_AUDIO_TOKEN token :" + token);
                    Log.d(TAG , "URL_GET_AUDIO_TOKEN identity :" + identity);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            registerForCallInvites();
                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }).execute();
    }

    public void makeCall() {
        if (checkoutgoing == 3) {
//            twiMLParams.put("PhoneNumber", "+16514139117");
//            twiMLParams.put("fromnumber", "+16514139117");
            twiMLParams.put("PhoneNumber", "+16514139117");
            twiMLParams.put("to", user_unique_code);
            txt_readername.setText(displaypassengername);
        } else if (checkoutgoing == 1) {
//            twiMLParams.put("PhoneNumber", helper.getDriverUniqueCode());
//            twiMLParams.put("fromnumber", helper.getDriverUniqueCode());
            twiMLParams.put("PhoneNumber", passengerUniqueCode);
            twiMLParams.put("fromnumber", user_unique_code);
//            twiMLParams.put("to", fromNumber);
            txt_readername.setText(displaypassengername);
        }

        ConnectOptions connectOptions = new ConnectOptions.Builder(accessToken)
                .params(twiMLParams)
                .build();
        activeCall = Voice.connect(VoiceActivity.this, connectOptions, callListener);
        setCallUI();
    }

    private View.OnClickListener hangupActionFabClickListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetUI();
                disconnect();
            }
        };
    }

    private View.OnClickListener answerCallClickListener() {
        return new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                isConnect = true;
                answer();
                setCallUI();
            }
        };
    }

    private View.OnClickListener cancelCallClickListener() {
        return new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (activeCallInvite != null) {
                    activeCallInvite.reject(VoiceActivity.this);
                    notificationManager.cancel(activeCallNotificationId);
                }
                resetUI();
                dismissVibrate();
            }
        };
    }


    private void answer() {
        activeCallInvite.accept(this, callListener);
        notificationManager.cancel(activeCallNotificationId);

        dismissVibrate();
    }

    private void disconnect() {
        if (activeCall != null) {
            activeCall.disconnect();
            activeCall = null;
        }
    }

    private void setAudioFocus(boolean setFocus) {
        if (audioManager != null) {
            if (setFocus) {
                savedAudioMode = audioManager.getMode();
                // Request audio focus before making any device switch.
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    AudioAttributes playbackAttributes = new AudioAttributes.Builder()
                            .setUsage(AudioAttributes.USAGE_VOICE_COMMUNICATION)
                            .setContentType(AudioAttributes.CONTENT_TYPE_SPEECH)
                            .build();
                    AudioFocusRequest focusRequest = new AudioFocusRequest.Builder(AudioManager.AUDIOFOCUS_GAIN_TRANSIENT)
                            .setAudioAttributes(playbackAttributes)
                            .setAcceptsDelayedFocusGain(true)
                            .setOnAudioFocusChangeListener(new AudioManager.OnAudioFocusChangeListener() {
                                @Override
                                public void onAudioFocusChange(int i) {
                                }
                            })
                            .build();
                    audioManager.requestAudioFocus(focusRequest);
                }
                audioManager.setMode(AudioManager.MODE_IN_COMMUNICATION);
            } else {
                audioManager.setMode(savedAudioMode);
                audioManager.abandonAudioFocus(null);
            }
        }
    }

    private boolean checkPermissionForMicrophone() {
        int resultMic = ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO);
        return resultMic == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermissionForMicrophone() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.RECORD_AUDIO)) {
            Toast.makeText(VoiceActivity.this,
                    "Microphone permissions needed. Please allow in your application settings.",
                    Toast.LENGTH_SHORT).show();
        } else {
            ActivityCompat.requestPermissions(
                    this,
                    new String[]{Manifest.permission.RECORD_AUDIO},
                    MIC_PERMISSION_REQUEST_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == MIC_PERMISSION_REQUEST_CODE && permissions.length > 0) {
            if (grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(VoiceActivity.this,
                        "Microphone permissions needed. Please allow in your application settings.",
                        Toast.LENGTH_SHORT).show();
            } else {
                registerForCallInvites();
            }
        }
    }

}
