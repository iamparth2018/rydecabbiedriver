package com.rydedispatch.driver.activity;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.rydedispatch.driver.R;
import com.rydedispatch.driver.adapter.HistoryAdapter;
import com.rydedispatch.driver.network.ApiClient;
import com.rydedispatch.driver.network.ApiInterface;
import com.rydedispatch.driver.response.RespHistory;
import com.rydedispatch.driver.utils.AppConfig;
import com.rydedispatch.driver.utils.GPSTracker;
import com.rydedispatch.driver.utils.PreferenceHelper;
import com.rydedispatch.driver.utils.Prefs;
import com.rydedispatch.driver.utils.Progress;
import com.rydedispatch.driver.utils.Utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class PreScheduleTripActivity extends AppCompatActivity /*implements PermissionsListener*/ {

    private static final String TAG = PreScheduleTripActivity.class.getSimpleName();
    @BindView(R.id.iv_back)
    ImageView iv_back;
    @BindView(R.id.tvTitle)
    AppCompatTextView tvTitle;
    @BindView(R.id.tv_list)
    AppCompatTextView tv_list;
    @BindView(R.id.rl_schedule)
    RelativeLayout rl_schedule;

    @BindView(R.id.rv_history)
    RecyclerView rvHistory;

    private Utils utils;
    protected PreferenceHelper helper;
    protected Progress progress;
    private String accept = "", authorization = "", status = "", displaypassengername = "",
            schedule = "", requestId = "", runningRideStatus = "", uniqueCode = "",
            firstname = "", lastname = "", payment_type = "", passengerphonenumber = "";
    private LinearLayoutManager mLayoutManager;
    private HistoryAdapter mHistoryAdapter;
    private List<RespHistory.DataEntityX.HistoryEntity.DataEntity> listData = new ArrayList<>();
    double destinationLat, destinationLang, sourceLat, sourceLang;

    GPSTracker gps;
//    private MapView mapView;
//    MapboxMap mapboxMap;
//    NavigationLauncherOptions options;
//    private DirectionsRoute currentRoute;
//    private LocationComponent locationComponent;
//    private PermissionsManager permissionsManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        Mapbox.getInstance(this, getString(R.string.map_box_token));

        setContentView(R.layout.activity_pre_schedule_trip);
        ButterKnife.bind(this);

        tvTitle.setText(getString(R.string.pre_schedule_trips));

        gps = new GPSTracker(this);

        utils = new Utils(this);
        progress = new Progress(this);
        helper = new PreferenceHelper(this, Prefs.PREF_FILE);

        accept = "application/json";
        authorization = "Bearer " + helper.LoadStringPref(Prefs.token, "");

        /*mapView = findViewById(R.id.mapView_main_history);
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(new com.mapbox.mapboxsdk.maps.OnMapReadyCallback() {
            @Override
            public void onMapReady(@NonNull MapboxMap map) {
                mapboxMap = map;
                mapboxMap.setStyle(getString(R.string.navigation_guidance_day), new Style.OnStyleLoaded() {
                    @Override
                    public void onStyleLoaded(@NonNull Style style) {
                        enableLocationComponent(style);

                        addDestinationIconSymbolLayer(style);

                    }
                });
            }
        });*/

        mLayoutManager = new LinearLayoutManager(this);
        rvHistory.setLayoutManager(mLayoutManager);
        rvHistory.setItemAnimator(new DefaultItemAnimator());
        mHistoryAdapter = new HistoryAdapter(this, listData);
        rvHistory.setAdapter(mHistoryAdapter);

        mHistoryAdapter.setListener(new HistoryAdapter.customOnclickListener() {
            @Override
            public void onclick(int adapterPosition, RespHistory.DataEntityX.HistoryEntity.DataEntity listhistories) {

                requestId = String.valueOf(listhistories.getId());
                status = String.valueOf(listhistories.getStatus());
                schedule = String.valueOf(listhistories.getIsSchedule());
                displaypassengername = String.valueOf(listhistories.getDisplay_passenger_name());
                firstname = String.valueOf(listhistories.getFirstName());
                lastname = String.valueOf(listhistories.getLastName());
                uniqueCode = String.valueOf(listhistories.getUser_unique_code());
                payment_type = String.valueOf(listhistories.getPayment_type());
                passengerphonenumber = String.valueOf(listhistories.getPassenger_phone_number());

                Log.d("HistoryFragment", "user unique code :" + listhistories.getUser_unique_code());
                Log.d("HistoryFragment", "user unique code :" + listhistories.getStatus());
                startActivity(new Intent(PreScheduleTripActivity.this, HistoryDetails.class)
                        .putExtra(AppConfig.BUNDLE.payment_type, payment_type)
                        .putExtra(AppConfig.BUNDLE.passenger_phone_number, passengerphonenumber)
                        .putExtra(AppConfig.BUNDLE.request_id, requestId)
                        .putExtra(AppConfig.BUNDLE.statusRide, status)
                        .putExtra(AppConfig.BUNDLE.is_schedule, schedule));
            }
        });

        mHistoryAdapter.setCalllistener(new HistoryAdapter.callListener() {
            @Override
            public void oncall(int adapterPosition, RespHistory.DataEntityX.HistoryEntity.DataEntity listhistories) {
                displaypassengername = String.valueOf(listhistories.getDisplay_passenger_name());
                firstname = String.valueOf(listhistories.getFirstName());
                lastname = String.valueOf(listhistories.getLastName());
                uniqueCode = String.valueOf(listhistories.getUser_unique_code());
                payment_type = String.valueOf(listhistories.getPayment_type());
                passengerphonenumber = String.valueOf(listhistories.getPassenger_phone_number());

                startActivity(new Intent(PreScheduleTripActivity.this, VoiceActivity.class)
                        .putExtra(AppConfig.BUNDLE.passenger_first_name, firstname)
                        .putExtra(AppConfig.BUNDLE.passenger_last_name, lastname)
                        .putExtra(AppConfig.BUNDLE.PassengerUniqueCode, uniqueCode)
                        .putExtra(AppConfig.BUNDLE.display_passenger_name, displaypassengername)
                        .putExtra(AppConfig.BUNDLE.payment_type, payment_type)
                        .putExtra(AppConfig.BUNDLE.passenger_phone_number, passengerphonenumber)
                        .putExtra(AppConfig.EXTRA.CHECKOUTGOING, 1));

            }
        });


        mHistoryAdapter.setNavilistener(new HistoryAdapter.naviListener() {
            @Override
            public void navigate(int adapterPosition, RespHistory.DataEntityX.HistoryEntity.DataEntity listhistories) {
                destinationLat = Double.parseDouble(listhistories.getDestination_latitude());
                destinationLang = Double.parseDouble(listhistories.getDestination_longitude());
                sourceLat = Double.parseDouble(listhistories.getSource_latitude());
                sourceLang = Double.parseDouble(listhistories.getSource_longitude());

                Log.d(TAG, "destination lat :" + destinationLat);
                Log.d(TAG, "destination lang :" + destinationLang);
                Log.d(TAG, "sourceLat :" + sourceLat);
                Log.d(TAG, "sourceLang :" + sourceLang);

//                initMap(sourceLat, sourceLang);
//                onMapBox();
                googleMapExternal(sourceLat, sourceLang);

            }
        });


        getHistoryListUpcoming(accept, authorization, "", "", "", "1" , "all","1");


    }

    private void googleMapExternal(double sourceLat, double sourceLang) {
        String url = "google.navigation:q=" + sourceLat + "," + sourceLang + "&mode=d";
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        startActivity(intent);
    }


    private void getHistoryListUpcoming(String accept, String authorization,
                                        String search, String startDate,
                                        String endDate, String isschedule,
                                        String status, String page) {
        progress.createDialog(false);
        progress.DialogMessage(getString(R.string.please_wait));
        progress.showDialog();

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        RequestBody search1 = RequestBody.create(MediaType.parse("multipart/form-data"), search);
        RequestBody startDate1 = RequestBody.create(MediaType.parse("multipart/form-data"), startDate);
        RequestBody endDate1 = RequestBody.create(MediaType.parse("multipart/form-data"), endDate);
        RequestBody isschedule1 = RequestBody.create(MediaType.parse("multipart/form-data"), isschedule);
        RequestBody status1 = RequestBody.create(MediaType.parse("multipart/form-data"), status);
        RequestBody page1 = RequestBody.create(MediaType.parse("multipart/form-data"), page);

        final Call<RespHistory> respHistoryCall = apiService.HISTORY_CALL(accept, authorization,
                search1, startDate1, endDate1, isschedule1, status1, page1);
        respHistoryCall.enqueue(new Callback<RespHistory>() {
            @Override
            public void onResponse(Call<RespHistory> call, Response<RespHistory> response) {

                progress.hideDialog();

                if (response.isSuccessful()) {

                    if (response.body().getMessage().contains("Unauthenticated")) {
                        openAlertUnauthLogout();
                    } else if (response.body().getMessage().equalsIgnoreCase("Your history has been get successfully.")) {


                        if (!response.body().getData().getHistory().getData().isEmpty()) {
                            listData.clear();
                            listData.addAll(response.body().getData().getHistory().getData());
                            mHistoryAdapter.notifyDataSetChanged();
                            return;
                        }
                        else {
                            Snackbar.make(rl_schedule, "No data available..!!", Snackbar.LENGTH_SHORT).show();

                        }
                    } else if (response.body().getData().getHistory().getData().isEmpty() || response.body().getData().getHistory().getData().contains("")) {
                        Snackbar.make(rl_schedule, "No data available..!!", Snackbar.LENGTH_SHORT).show();
                    } else {
                        Snackbar.make(rl_schedule, "Something Went Wrong..!!", Snackbar.LENGTH_SHORT).show();

                    }


                } else if (response.code() == 500) {
                    Snackbar.make(rl_schedule, "Internal Server Error..!!", Snackbar.LENGTH_SHORT).show();
                } else {
                    Snackbar.make(rl_schedule, "" + response.message(), Snackbar.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<RespHistory> call, Throwable t) {
                progress.hideDialog();
//                Toast.makeText(MainActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
                Snackbar.make(rl_schedule, "" + t.getLocalizedMessage(), Snackbar.LENGTH_SHORT).show();

            }
        });
    }

    private void openAlertUnauthLogout() {
        final Dialog mBottomSheetDialog = new Dialog(this, R.style.MaterialDialogSheet);
        mBottomSheetDialog.setContentView(R.layout.dialog_unauthorised);
        mBottomSheetDialog.setCancelable(false);
        mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        mBottomSheetDialog.getWindow().setGravity(Gravity.CENTER);
        mBottomSheetDialog.setCanceledOnTouchOutside(false);
        AppCompatTextView btn_ok = mBottomSheetDialog.findViewById(R.id.btn_ok);

        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progress.createDialog(false);
                progress.DialogMessage(getString(R.string.msg_logout));
                progress.showDialog();
                AsyncTask.execute(new Runnable() {
                    @Override
                    public void run() {

                        try {
                            FirebaseInstanceId.getInstance().deleteInstanceId();
                            helper.clearAllPrefs();
                            startActivity(new Intent(PreScheduleTripActivity.this, LoginActivity.class)
                                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                            progress.hideDialog();
                            finish();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        });
        mBottomSheetDialog.show();
    }

    /*@SuppressWarnings({"MissingPermission"})
    private void enableLocationComponent(@NonNull Style loadedMapStyle) {
// Check if permissions are enabled and if not request
        if (PermissionsManager.areLocationPermissionsGranted(this)) {
// Activate the MapboxMap LocationComponent to show user location
// Adding in LocationComponentOptions is also an optional parameter
            locationComponent = mapboxMap.getLocationComponent();
            locationComponent.activateLocationComponent(this, loadedMapStyle);
            locationComponent.setLocationComponentEnabled(true);
// Set the component's camera mode
            locationComponent.setCameraMode(CameraMode.TRACKING);
        } else {
            permissionsManager = new PermissionsManager(this);
            permissionsManager.requestLocationPermissions(this);
        }
    }

    private void initMap(double sourceLat, double sourceLang) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                try {
                    Point destinationPoint = Point.fromLngLat(sourceLang, sourceLat);

                    Point originPoint = Point.fromLngLat(gps.getLongitude(), gps.getLatitude());

                    getRoute(originPoint, destinationPoint);

                    mapboxMap.setStyle(getString(R.string.navigation_guidance_day), new Style.OnStyleLoaded() {
                        @Override
                        public void onStyleLoaded(@NonNull Style style) {

//                            progress.hideDialog();
                            enableLocationComponent(style);

                            addDestinationIconSymbolLayer(style);

                        }
                    });
                } catch (RuntimeException r) {
                    r.printStackTrace();
                }

            }
        }, 1000);
    }

    private void addDestinationIconSymbolLayer(@NonNull Style loadedMapStyle) {
        loadedMapStyle.addImage("destination-icon-id",
                BitmapFactory.decodeResource(this.getResources(), R.drawable.mapbox_marker_icon_default));
        GeoJsonSource geoJsonSource = new GeoJsonSource("destination-source-id");
        loadedMapStyle.addSource(geoJsonSource);
        SymbolLayer destinationSymbolLayer = new SymbolLayer("destination-symbol-layer-id", "destination-source-id");
        destinationSymbolLayer.withProperties(
                iconImage("destination-icon-id"),
                iconAllowOverlap(true),
                iconIgnorePlacement(true)
        );
        loadedMapStyle.addLayer(destinationSymbolLayer);
    }


    private void getRoute(Point origin, Point destination) {
        progress.createDialog(true);
        progress.DialogMessage(getString(R.string.please_wait));
        progress.showDialog();

        NavigationRoute.builder(this)
                .accessToken(Mapbox.getAccessToken())
                .origin(origin)
                .destination(destination)
                .build()
                .getRoute(new Callback<DirectionsResponse>() {
                    @Override
                    public void onResponse(Call<DirectionsResponse> call, Response<DirectionsResponse> response) {
// You can get the generic HTTP info about the response

                        progress.hideDialog();

                        if (response.code()==200) {
                            if (response.body() == null) {
                                Log.d(TAG, "No routes found, make sure you set the right user and access token.");
                            } else if (response.body().routes().size() < 1) {
                                Log.d(TAG, "No routes found");

                                currentRoute = response.body().routes().get(0);
                                boolean simulateRoute = false;
                                options = NavigationLauncherOptions.builder()
                                        .directionsRoute(currentRoute)
                                        .shouldSimulateRoute(simulateRoute)
                                        .build();


                            }
                            else {
                                currentRoute = response.body().routes().get(0);
                                boolean simulateRoute = false;
                                options = NavigationLauncherOptions.builder()
                                        .directionsRoute(currentRoute)
                                        .shouldSimulateRoute(simulateRoute)
                                        .build();

                                Log.d(TAG, "Response code: " + response.code());
                                Log.d(TAG, "Response code: " + response.body().routes().get(0));
                                Log.d(TAG, "Response code: " + currentRoute);
                            }

                            NavigationLauncher.startNavigation(PreScheduleTripActivity.this, options);

                        }
                        else {
                            Log.d(TAG, "response error : "+ response.message());
                            Log.d(TAG, "response error : "+ response.code());
                            Snackbar.make(rl_schedule, "" + response.message(), Snackbar.LENGTH_SHORT).show();

                        }
                    }

                    @Override
                    public void onFailure(Call<DirectionsResponse> call, Throwable throwable) {
                        progress.hideDialog();

                        Log.e(TAG, "Error: " + throwable.getMessage());
                    }
                });
    }*/


    /*private void onMapBox() {

        if (options == null) {
            return;
        }
        mapView.setVisibility(View.GONE);
        NavigationLauncher.startNavigation(this, options);
    }*/

    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }

    @OnClick({R.id.iv_back})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
        }
    }

    /*@Override
    public void onExplanationNeeded(List<String> permissionsToExplain) {
        Toast.makeText(this, R.string.user_location_permission_explanation, Toast.LENGTH_LONG).show();

    }

    @Override
    public void onPermissionResult(boolean granted) {
        if (granted) {
//            enableLocationComponent(mapboxMap.getStyle());
        } else {
            Toast.makeText(this, R.string.user_location_permission_not_granted, Toast.LENGTH_LONG).show();
            finish();
        }
    }*/
}
