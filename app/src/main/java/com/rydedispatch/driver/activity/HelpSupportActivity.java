package com.rydedispatch.driver.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import com.rydedispatch.driver.R;
import com.rydedispatch.driver.utils.AppConfig;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class HelpSupportActivity extends AppCompatActivity {

    private static final String TAG = HelpSupportActivity.class.getSimpleName();

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.tvTitle)
    AppCompatTextView tvTitle;
    @BindView(R.id.ll_call)
    LinearLayout llcall;
    @BindView(R.id.ll_chat)
    LinearLayout llchat;
    private String supportcode = "", userUniqueCode = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help_support);
        ButterKnife.bind(this);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            supportcode = bundle.getString(AppConfig.BUNDLE.Support_Number);
            userUniqueCode = bundle.getString(AppConfig.BUNDLE.UserUniqueCode);
        }

        tvTitle.setText(getString(R.string.res_help_support));

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        Log.d(TAG, "support number :" + supportcode);
    }

    @OnClick({R.id.iv_back, R.id.ll_chat, R.id.ll_call})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
//                startActivity(new Intent(HelpSupportActivity.this, MainActivity.class));
                finish();
                break;
            case R.id.ll_chat:
                startActivity(new Intent(HelpSupportActivity.this, ChatActivity.class));
                break;
            case R.id.ll_call:

                if (TextUtils.isEmpty(supportcode)) {
                    supportcode = "+16514139117";

                    startActivity(new Intent(HelpSupportActivity.this, VoiceActivity.class)
                            .putExtra(AppConfig.BUNDLE.Support_Number, supportcode)
                            .putExtra(AppConfig.BUNDLE.UserUniqueCode, userUniqueCode)
                            .putExtra(AppConfig.EXTRA.CHECKOUTGOING, 3));
                }else {
                    startActivity(new Intent(HelpSupportActivity.this, VoiceActivity.class)
                            .putExtra(AppConfig.BUNDLE.Support_Number, supportcode)
                            .putExtra(AppConfig.BUNDLE.UserUniqueCode, userUniqueCode)
                            .putExtra(AppConfig.EXTRA.CHECKOUTGOING, 3));
                }

                Log.d(TAG, "userUniqueCode :" + userUniqueCode);
                break;
        }
    }
}
