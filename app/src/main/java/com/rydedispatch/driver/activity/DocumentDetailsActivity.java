package com.rydedispatch.driver.activity;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.rydedispatch.driver.R;
import com.rydedispatch.driver.network.ApiClient;
import com.rydedispatch.driver.network.ApiInterface;
import com.rydedispatch.driver.response.RespDashboardDriver;
import com.rydedispatch.driver.response.RespUploadDocument;
import com.rydedispatch.driver.utils.AppConfig;
import com.rydedispatch.driver.utils.PreferenceHelper;
import com.rydedispatch.driver.utils.Prefs;
import com.rydedispatch.driver.utils.Progress;
import com.rydedispatch.driver.utils.Utils;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;
import pl.aprilapps.easyphotopicker.EasyImageConfig;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DocumentDetailsActivity extends AppCompatActivity {
    private static final String TAG = DocumentDetailsActivity.class.getSimpleName();
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.tvTitle)
    AppCompatTextView tvTitle;
    @BindView(R.id.tv_rear)
    AppCompatTextView tvrear;
    @BindView(R.id.tv_front)
    AppCompatTextView tvfront;
    @BindView(R.id.llfrontrear)
    LinearLayout llfrontrear;
    @BindView(R.id.ib_edit)
    ImageView ibEdit;
    @BindView(R.id.ib_edit_rear)
    ImageView ibeditrear;
    @BindView(R.id.profile_image)
    ImageView profilePic;
    @BindView(R.id.profile_image_rear)
    ImageView profileimagerear;
    @BindView(R.id.frame_layout_rear)
    FrameLayout framelayoutrear;
    @BindView(R.id.frame_layout_front)
    FrameLayout framelayoutfront;
    @BindView(R.id.ll_save)
    LinearLayout llsave;
    @BindView(R.id.et_driving_lic)
    EditText etdrivinglic;
    @BindView(R.id.et_vehicle_type)
    EditText etvehicletype;
    @BindView(R.id.et_issued_on)
    EditText etissuedOn;
    @BindView(R.id.et_expired_date)
    EditText etExpirydate;
    @BindView(R.id.ll_driving)
    LinearLayout ll_driving;
    @BindView(R.id.ll_inspection)
    LinearLayout ll_inspection;
    @BindView(R.id.et_inspection)
    EditText et_inspection;
    @BindView(R.id.et_inspection_expired_date)
    EditText et_inspection_expired_date;
    @BindView(R.id.rl_layout_document)
    RelativeLayout rl_layout_document;

    private File mCurrentPhoto, mCurrentRearPhoto, mCurrentUpdatePhoto, mCurrentRearUpdatePhoto;
    private Utils utils;
    boolean frontSelected = true;
    boolean rearSelected = true;
    private String accept = "", authorization = "", id = "", user_id = "", unique_id = "",
            documentname = "", frontdocumenturl = "", backdocumenturl = "", documentType = "",
            drivinglic = "", vehicleType = "", issuedate = "", status = "", deletedat = "", createdat = "",
            issuedOn = "", expirydate = "", documentId = "", uniqueid = "", updatedat = "", myDocument = "";
    protected PreferenceHelper helper;

    private Bundle bundle;
    protected Progress progress;

    DatePickerDialog datePickerDialog;
    int year;
    int month;
    int dayOfMonth;
    private Calendar calendar;
    private Calendar calendarexpiry;

    public static final int MY_PERMISSIONS_REQUEST_CAMERA = 100;
    public static final String ALLOW_KEY = "ALLOWED";
    public static final String CAMERA_PREF = "camera_pref";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_document_details);
        ButterKnife.bind(this);


        bundle = getIntent().getExtras();
        if (bundle != null) {
            id = bundle.getString(AppConfig.BUNDLE.id);
            user_id = bundle.getString(AppConfig.BUNDLE.user_id);
            unique_id = bundle.getString(AppConfig.BUNDLE.unique_id);
            documentname = bundle.getString(AppConfig.BUNDLE.document_name);
            frontdocumenturl = bundle.getString(AppConfig.BUNDLE.front_document_url);
            backdocumenturl = bundle.getString(AppConfig.BUNDLE.back_document_url);
            documentType = bundle.getString(AppConfig.BUNDLE.document_type);
            issuedate = bundle.getString(AppConfig.BUNDLE.issue_date);
            expirydate = bundle.getString(AppConfig.BUNDLE.expiry_date);
            status = bundle.getString(AppConfig.BUNDLE.status);
            deletedat = bundle.getString(AppConfig.BUNDLE.deleted_at);
            createdat = bundle.getString(AppConfig.BUNDLE.created_at);
            updatedat = bundle.getString(AppConfig.BUNDLE.updated_at);
        }

        helper = new PreferenceHelper(this, Prefs.PREF_FILE);
        progress = new Progress(this);
        utils = new Utils(this);

        ibeditrear.setScaleType(ImageView.ScaleType.CENTER_CROP);

        Log.d(TAG, "documentName :" + documentname);
        Log.d(TAG, "doc id :" + id);

        if (documentname.contains("Driving License")) {
            ll_driving.setVisibility(View.VISIBLE);
            ll_inspection.setVisibility(View.GONE);

            etdrivinglic.setText(unique_id);
            etvehicletype.setText(documentType);
            etissuedOn.setText(issuedate);
            etExpirydate.setText(expirydate);

            tvTitle.setText(documentname);


            Log.d(TAG, "frontdocumenturl :" + frontdocumenturl);

            if (frontdocumenturl == null) {

                Picasso.with(this)
                        .load(R.mipmap.ic_launcher)
                        .placeholder(R.mipmap.ic_launcher)
                        .error(R.mipmap.ic_launcher)
                        .into(profilePic);

            } else if (frontdocumenturl.contains("https://")) {

                Picasso.with(this)
                        .load(frontdocumenturl)
                        .placeholder(R.mipmap.ic_launcher)
                        .error(R.mipmap.ic_launcher)
                        .into(profilePic);
            } else {

                Picasso.with(this)
                        .load(AppConfig.URL.BASE_IMAGEURL + frontdocumenturl)
                        .placeholder(R.mipmap.ic_launcher)
                        .error(R.mipmap.ic_launcher)
                        .into(profilePic);
            }

            if (backdocumenturl == null) {

                Picasso.with(this)
                        .load(R.mipmap.ic_launcher)
                        .placeholder(R.mipmap.ic_launcher)
                        .error(R.mipmap.ic_launcher)
                        .into(profilePic);
            } else if (backdocumenturl.contains("https://")) {

                Picasso.with(this)
                        .load(frontdocumenturl)
                        .placeholder(R.mipmap.ic_launcher)
                        .error(R.mipmap.ic_launcher)
                        .into(profilePic);
            } else {

                Picasso.with(this)
                        .load(AppConfig.URL.BASE_IMAGEURL + backdocumenturl)
                        .placeholder(R.mipmap.ic_launcher)
                        .error(R.mipmap.ic_launcher)
                        .into(profilePic);
            }


        } else {
            tvTitle.setText(documentname);

            ll_driving.setVisibility(View.GONE);
            ll_inspection.setVisibility(View.VISIBLE);

            et_inspection.setText(unique_id);
            et_inspection_expired_date.setText(expirydate);

            Log.d(TAG, "frontdocumenturl else:" + frontdocumenturl);

            if (frontdocumenturl == null) {

                Picasso.with(this)
                        .load(R.mipmap.ic_launcher)
                        .placeholder(R.mipmap.ic_launcher)
                        .error(R.mipmap.ic_launcher)
                        .into(profilePic);
            } else if (frontdocumenturl.contains("https://")) {

                Picasso.with(this)
                        .load(frontdocumenturl)
                        .placeholder(R.mipmap.ic_launcher)
                        .error(R.mipmap.ic_launcher)
                        .into(profilePic);
            } else {

                Picasso.with(this)
                        .load(AppConfig.URL.BASE_IMAGEURL + frontdocumenturl)
                        .placeholder(R.mipmap.ic_launcher)
                        .error(R.mipmap.ic_launcher)
                        .into(profilePic);
            }
            if (backdocumenturl == null) {

                Picasso.with(this)
                        .load(R.mipmap.ic_launcher)
                        .placeholder(R.mipmap.ic_launcher)
                        .error(R.mipmap.ic_launcher)
                        .into(profilePic);
            } else if (backdocumenturl.contains("https://")) {

                Picasso.with(this)
                        .load(backdocumenturl)
                        .placeholder(R.mipmap.ic_launcher)
                        .error(R.mipmap.ic_launcher)
                        .into(profilePic);
            } else {

                Picasso.with(this)
                        .load(AppConfig.URL.BASE_IMAGEURL + backdocumenturl)
                        .placeholder(R.mipmap.ic_launcher)
                        .error(R.mipmap.ic_launcher)
                        .into(profilePic);
            }

        }

        etissuedOn.setFocusable(false);
        etExpirydate.setFocusable(false);
        et_inspection_expired_date.setFocusable(false);
    }

    @Override
    protected void onResume() {
        super.onResume();

        accept = "application/json";
        authorization = "Bearer " + helper.LoadStringPref(Prefs.token, "");

        getDocumentItem(accept, authorization);

    }

    private void getDocumentItem(String accept, String authorization) {
        progress.createDialog(true);
        progress.DialogMessage(getString(R.string.please_wait));
        progress.hideDialog();

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        final Call<RespDashboardDriver> respDashboardDriverCall = apiService.DRIVER_DASHBOARD(accept, authorization);
        respDashboardDriverCall.enqueue(new Callback<RespDashboardDriver>() {
            @Override
            public void onResponse(Call<RespDashboardDriver> call, Response<RespDashboardDriver> response) {
//                Toast.makeText(DocumentDetailsActivity.this, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<RespDashboardDriver> call, Throwable t) {

            }
        });
    }

    @OnClick({R.id.iv_back, R.id.ib_edit, R.id.ib_edit_rear,
            R.id.tv_front, R.id.tv_rear, R.id.ll_save, R.id.et_issued_on, R.id.et_expired_date,
            R.id.et_inspection_expired_date, R.id.profile_image, R.id.profile_image_rear})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.ib_edit:
                if (ContextCompat.checkSelfPermission(this,
                        Manifest.permission.CAMERA)
                        != PackageManager.PERMISSION_GRANTED) {

                    if (getFromPref(this, ALLOW_KEY)) {

                        showSettingsAlert();

                    } else if (ContextCompat.checkSelfPermission(this,
                            Manifest.permission.CAMERA)
                            != PackageManager.PERMISSION_GRANTED) {
                        // Should we show an explanation?
                        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                                Manifest.permission.CAMERA)) {
                            showAlert();
                        } else {
                            // No explanation needed, we can request the permission.
                            ActivityCompat.requestPermissions(this,
                                    new String[]{Manifest.permission.CAMERA},
                                    MY_PERMISSIONS_REQUEST_CAMERA);
                        }
                    }
                } else {
                    openCamera();
                }
                break;
            case R.id.tv_front:
                frontSelected = true;
                hideRear();
                break;
            case R.id.tv_rear:
                frontSelected = false;
                hideFront();
                break;
            case R.id.ib_edit_rear:
                openCameraRear();
                break;
            case R.id.et_issued_on:
                openIssueOn();
                break;
            case R.id.et_expired_date:
                openExpiryOn();

                break;
            case R.id.et_inspection_expired_date:
                openInspectionExpiryOn();
                break;

            case R.id.profile_image:
                openCamera();
                break;

            case R.id.profile_image_rear:
                openCameraRear();
                break;
            case R.id.ll_save:


                Log.d(TAG, "status :" + status);
                Log.d(TAG, "documentname :" + documentname);
                Log.d(TAG, "doc id :" + id);


                if (documentname.equalsIgnoreCase("Driving License")) {
                    drivinglic = etdrivinglic.getText().toString();
                    vehicleType = etvehicletype.getText().toString();
                    issuedOn = etissuedOn.getText().toString();
                    expirydate = etExpirydate.getText().toString();

                    /*if (TextUtils.isEmpty(drivinglic)) {
                        etdrivinglic.setEnabled(true);
                        etdrivinglic.setFocusable(true);
                        Toast.makeText(this, "Please Enter Valid Driving Licence Number", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    if (TextUtils.isEmpty(vehicleType)) {
                        etvehicletype.setEnabled(true);
                        etvehicletype.setFocusable(true);
                        Toast.makeText(this, "Please Enter Vehicle Type", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    if (TextUtils.isEmpty(issuedOn)) {
                        Toast.makeText(this, "Please Enter Issued On", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    if (TextUtils.isEmpty(expirydate)) {
                        Toast.makeText(this, "Please Enter Expiry Date", Toast.LENGTH_SHORT).show();
                        return;
                    }*/


                    uploadDocument(accept, authorization, id,
                            drivinglic, vehicleType, issuedOn,
                            expirydate, mCurrentPhoto, mCurrentRearPhoto);


                } else if (documentname.equalsIgnoreCase("Police Clearance Certificate")) {
                    unique_id = et_inspection.getText().toString();
                    expirydate = et_inspection_expired_date.getText().toString();

                    Log.d(TAG, "id :" + id);
                    Log.d(TAG, "expirydate :" + expirydate);
                    Log.d(TAG, "mCurrentPhoto :" + mCurrentPhoto);
                    Log.d(TAG, "mCurrentRearPhoto :" + mCurrentRearPhoto);

                    uploadDocument(accept, authorization, id,
                            unique_id, documentType, issuedOn, expirydate,
                            mCurrentPhoto, mCurrentRearPhoto);

                } else if (documentname.equalsIgnoreCase("Insurance")) {
                    unique_id = et_inspection.getText().toString();
                    expirydate = et_inspection_expired_date.getText().toString();

                    Log.d(TAG, "id :" + id);
                    Log.d(TAG, "expirydate :" + expirydate);
                    Log.d(TAG, "mCurrentPhoto :" + mCurrentPhoto);
                    Log.d(TAG, "mCurrentRearPhoto :" + mCurrentRearPhoto);

                    uploadDocument(accept, authorization, id,
                            unique_id, documentType, issuedOn, expirydate,
                            mCurrentPhoto, mCurrentRearPhoto);

                    return;
                } else {
                    unique_id = et_inspection.getText().toString();
                    expirydate = et_inspection_expired_date.getText().toString();

                    uploadDocumentnew(accept, authorization, id, unique_id, expirydate, mCurrentPhoto, mCurrentRearPhoto);
                }

                break;
        }
    }


    private void showAlert() {
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle("Alert");
        alertDialog.setMessage("App needs to access the Camera.");
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "DONT ALLOW",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        finish();
                    }
                });
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "ALLOW",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        ActivityCompat.requestPermissions(DocumentDetailsActivity.this,
                                new String[]{Manifest.permission.CAMERA},
                                MY_PERMISSIONS_REQUEST_CAMERA);
                        openCamera();

                    }
                });
        alertDialog.show();
    }

    @Override
    public void onRequestPermissionsResult
            (int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_CAMERA: {
                for (int i = 0, len = permissions.length; i < len; i++) {
                    String permission = permissions[i];
                    if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                        boolean showRationale =
                                ActivityCompat.shouldShowRequestPermissionRationale
                                        (this, permission);
                        if (showRationale) {
                            showAlert();
                        } else if (!showRationale) {
                            // user denied flagging NEVER ASK AGAIN
                            // you can either enable some fall back,
                            // disable features of your app
                            // or open another dialog explaining
                            // again the permission and directing to
                            // the app setting
                            saveToPreferences(DocumentDetailsActivity.this, ALLOW_KEY, true);
                        }
                    }
                }
            }
        }
    }


    public static void saveToPreferences(Context context, String key,
                                         Boolean allowed) {
        SharedPreferences myPrefs = context.getSharedPreferences
                (CAMERA_PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = myPrefs.edit();
        prefsEditor.putBoolean(key, allowed);
        prefsEditor.commit();
    }

    private void showSettingsAlert() {
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle("Alert");
        alertDialog.setMessage("App needs to access the Camera.");
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "DONT ALLOW",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        //finish();
                    }
                });
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "SETTINGS",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        startInstalledAppDetailsActivity(DocumentDetailsActivity.this);

                    }
                });
        alertDialog.show();
    }

    public static void startInstalledAppDetailsActivity(final Activity context) {
        if (context == null) {
            return;
        }
        final Intent i = new Intent();
        i.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        i.addCategory(Intent.CATEGORY_DEFAULT);
        i.setData(Uri.parse("package:" + context.getPackageName()));
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        i.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
        context.startActivity(i);
    }


    public static Boolean getFromPref(Context context, String key) {
        SharedPreferences myPrefs = context.getSharedPreferences
                (CAMERA_PREF, Context.MODE_PRIVATE);
        return (myPrefs.getBoolean(key, false));
    }

    private void openExpiryOn() {
        calendarexpiry = Calendar.getInstance();
        year = calendarexpiry.get(Calendar.YEAR);
        month = calendarexpiry.get(Calendar.MONTH);
        dayOfMonth = calendarexpiry.get(Calendar.DAY_OF_MONTH);
        datePickerDialog = new DatePickerDialog(DocumentDetailsActivity.this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int year, int month, int day) {

                        etExpirydate.setText(day + "-" + (month + 1) + "-" + year);

                    }
                }, year, month, dayOfMonth);
//        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
        datePickerDialog.show();

    }

    private void openInspectionExpiryOn() {
        calendarexpiry = Calendar.getInstance();
        year = calendarexpiry.get(Calendar.YEAR);
        month = calendarexpiry.get(Calendar.MONTH);
        dayOfMonth = calendarexpiry.get(Calendar.DAY_OF_MONTH);
        datePickerDialog = new DatePickerDialog(DocumentDetailsActivity.this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int year, int month, int day) {

                        et_inspection_expired_date.setText(day + "-" + (month + 1) + "-" + year);

                    }
                }, year, month, dayOfMonth);
//        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
        datePickerDialog.show();

    }

    private void openIssueOn() {
        calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
        calendar.add(Calendar.DATE, 3);
        datePickerDialog = new DatePickerDialog(DocumentDetailsActivity.this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int year, int month, int day) {

                        etissuedOn.setText(day + "-" + (month + 1) + "-" + year);
                    }
                }, year, month, dayOfMonth);
//        datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
        datePickerDialog.show();
    }

    private void uploadDocumentnew(String accept, String authorization,
                                   String documentId, String unique_id,
                                   String expirydate, File mCurrentPhoto, File mCurrentRearPhoto) {

        progress.createDialog(false);
        progress.DialogMessage(getString(R.string.please_wait));
        progress.showDialog();

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        RequestBody documentId1 = RequestBody.create(MediaType.parse("multipart/form-data"), documentId);
        RequestBody uniqueid1 = RequestBody.create(MediaType.parse("multipart/form-data"), unique_id);
        RequestBody expirydate1 = RequestBody.create(MediaType.parse("multipart/form-data"), expirydate);

        RequestBody fbody;
        String filename = "";
        if (mCurrentPhoto == null) {
            fbody = RequestBody.create(MediaType.parse("image/*"), "");
        } else {
            fbody = RequestBody.create(MediaType.parse("image/*"), mCurrentPhoto);
            filename = mCurrentPhoto.getName();
        }

        MultipartBody.Part bodyfront = MultipartBody.Part.createFormData("front_document_url", filename, fbody);

        RequestBody rearbody;
        String filerearname = "";
        if (mCurrentRearPhoto == null) {
            rearbody = RequestBody.create(MediaType.parse("image/*"), "");
        } else {
            rearbody = RequestBody.create(MediaType.parse("image/*"), mCurrentRearPhoto);
            filerearname = mCurrentRearPhoto.getName();
        }

        MultipartBody.Part bodyrear = MultipartBody.Part.createFormData("back_document_url", filerearname, rearbody);

        final Call<RespUploadDocument> respUploadDocumentCall = apiService.UPLOAD_NEW_DOCUMENT(accept, authorization,
                documentId1, uniqueid1, expirydate1, bodyfront, bodyrear);

        respUploadDocumentCall.enqueue(new Callback<RespUploadDocument>() {
            @Override
            public void onResponse(Call<RespUploadDocument> call, Response<RespUploadDocument> response) {
                progress.hideDialog();

                if (response.isSuccessful()) {
                    if (response.body().getMessage().equalsIgnoreCase("Document has been uploaded successfully")) {
                        Toast.makeText(DocumentDetailsActivity.this, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();

                        startActivity(new Intent(DocumentDetailsActivity.this, DocumentActivity.class));
                        finish();
                    } else {

                        Snackbar.make(rl_layout_document, "" + response.body().getMessage(), Snackbar.LENGTH_SHORT).show();

                    }
                } else {
                    Snackbar.make(rl_layout_document, "" + response.body().getMessage(), Snackbar.LENGTH_SHORT).show();

                }

            }

            @Override
            public void onFailure(Call<RespUploadDocument> call, Throwable t) {
                progress.hideDialog();
                Toast.makeText(DocumentDetailsActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }


    private void uploadDocument(String accept, String authorization, String documentId,
                                String uniqueid, String vehicleType,
                                String issuedOn, String expirydate,
                                File mCurrentPhoto, File mCurrentRearPhoto) {
        progress.createDialog(false);
        progress.DialogMessage(getString(R.string.please_wait));
        progress.showDialog();

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        RequestBody documentId1 = RequestBody.create(MediaType.parse("multipart/form-data"), documentId);
        RequestBody uniqueid1 = RequestBody.create(MediaType.parse("multipart/form-data"), uniqueid);
        RequestBody vehicleType1 = RequestBody.create(MediaType.parse("multipart/form-data"), vehicleType);
        RequestBody issuedOn1 = RequestBody.create(MediaType.parse("multipart/form-data"), issuedOn);
        RequestBody expirydate1 = RequestBody.create(MediaType.parse("multipart/form-data"), expirydate);

        RequestBody fbody;
        String filename = "";
        if (mCurrentPhoto == null) {
            fbody = RequestBody.create(MediaType.parse("image/*"), "");
        } else {
            fbody = RequestBody.create(MediaType.parse("image/*"), mCurrentPhoto);
            filename = mCurrentPhoto.getName();
        }

        MultipartBody.Part bodyfront = MultipartBody.Part.createFormData("front_document_url", filename, fbody);

        RequestBody rearbody;
        String filerearname = "";
        if (mCurrentRearPhoto == null) {
            rearbody = RequestBody.create(MediaType.parse("image/*"), "");
        } else {
            rearbody = RequestBody.create(MediaType.parse("image/*"), mCurrentRearPhoto);
            filerearname = mCurrentRearPhoto.getName();
        }

        MultipartBody.Part bodyrear = MultipartBody.Part.createFormData("back_document_url", filerearname, rearbody);

        final Call<RespUploadDocument> respUploadDocumentCall = apiService.UPLOAD_DOCUMENT(accept, authorization,
                documentId1, uniqueid1, vehicleType1, issuedOn1, expirydate1, bodyfront, bodyrear);

        respUploadDocumentCall.enqueue(new Callback<RespUploadDocument>() {
            @Override
            public void onResponse(Call<RespUploadDocument> call, Response<RespUploadDocument> response) {
                progress.hideDialog();

                if (response.isSuccessful()) {
                    if (response.body().getMessage().equalsIgnoreCase("Document has been uploaded successfully")) {
                        Toast.makeText(DocumentDetailsActivity.this, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();

                        startActivity(new Intent(DocumentDetailsActivity.this, DocumentActivity.class));
                        finish();
                    } else {

                        Snackbar.make(rl_layout_document, "" + response.body().getMessage(), Snackbar.LENGTH_SHORT).show();

                    }
                } else {
                    Snackbar.make(rl_layout_document, "" + response.body().getMessage(), Snackbar.LENGTH_SHORT).show();

                }

            }

            @Override
            public void onFailure(Call<RespUploadDocument> call, Throwable t) {
                progress.hideDialog();
                Toast.makeText(DocumentDetailsActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void openCameraRear() {
        openBottomSheetRear();
//        openBottomSheetRearnew();
    }


    private void openBottomSheetRear() {
        final Dialog mBottomSheetDialog = new Dialog(DocumentDetailsActivity.this, R.style.MaterialDialogSheet);
        mBottomSheetDialog.setContentView(R.layout.dialog_bottom_sheet);
        mBottomSheetDialog.setCancelable(true);
        mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        mBottomSheetDialog.getWindow().setGravity(Gravity.BOTTOM);
        mBottomSheetDialog.setCanceledOnTouchOutside(true);
        AppCompatTextView take_picture = mBottomSheetDialog.findViewById(R.id.take_picture);
        AppCompatTextView Open_gallary = mBottomSheetDialog.findViewById(R.id.open_gallary);

        take_picture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EasyImage.openCamera(DocumentDetailsActivity.this, EasyImageConfig.REQ_TAKE_PICTURE);
                mBottomSheetDialog.dismiss();
            }
        });

        Open_gallary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EasyImage.openGallery(DocumentDetailsActivity.this, EasyImageConfig.REQ_PICK_PICTURE_FROM_GALLERY);
                mBottomSheetDialog.dismiss();
            }
        });
        mBottomSheetDialog.show();
    }

    private void hideRear() {
        tvfront.setTextColor(getResources().getColor(R.color.colorWhite));
        tvfront.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        tvrear.setTextColor(getResources().getColor(R.color.colorPrimary));
        tvrear.setBackgroundColor(getResources().getColor(R.color.colorWhite));

        framelayoutrear.setVisibility(View.GONE);
        framelayoutfront.setVisibility(View.VISIBLE);
    }

    private void hideFront() {
        tvrear.setTextColor(getResources().getColor(R.color.colorWhite));
        tvrear.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        tvfront.setTextColor(getResources().getColor(R.color.colorPrimary));
        tvfront.setBackgroundColor(getResources().getColor(R.color.colorWhite));

        framelayoutrear.setVisibility(View.VISIBLE);
        framelayoutfront.setVisibility(View.GONE);
    }

    private void openCamera() {
        openBottomSheet();
    }

    private void openBottomSheet() {
        final Dialog mBottomSheetDialog = new Dialog(DocumentDetailsActivity.this, R.style.MaterialDialogSheet);
        mBottomSheetDialog.setContentView(R.layout.dialog_bottom_sheet);
        mBottomSheetDialog.setCancelable(true);
        mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        mBottomSheetDialog.getWindow().setGravity(Gravity.BOTTOM);
        mBottomSheetDialog.setCanceledOnTouchOutside(true);
        AppCompatTextView take_picture = mBottomSheetDialog.findViewById(R.id.take_picture);
        AppCompatTextView Open_gallary = mBottomSheetDialog.findViewById(R.id.open_gallary);

        take_picture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EasyImage.openCamera(DocumentDetailsActivity.this, EasyImageConfig.REQ_TAKE_PICTURE);
                mBottomSheetDialog.dismiss();
            }
        });

        Open_gallary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EasyImage.openGallery(DocumentDetailsActivity.this, EasyImageConfig.REQ_PICK_PICTURE_FROM_GALLERY);
                mBottomSheetDialog.dismiss();
            }
        });
        mBottomSheetDialog.show();
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        EasyImage.handleActivityResult(requestCode, resultCode, data, DocumentDetailsActivity.this, new DefaultCallback() {
            @Override
            public void onImagePickerError(Exception e, EasyImage.ImageSource source, int type) {
                //Some error handling
                Toast.makeText(DocumentDetailsActivity.this, "Error " + e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();

                if (source == EasyImage.ImageSource.CAMERA) {
                    File photoFile = EasyImage.lastlyTakenButCanceledPhoto(DocumentDetailsActivity.this);
                    if (photoFile != null) photoFile.delete();
                }
            }

            @Override
            public void onImagePicked(File imageFile, EasyImage.ImageSource source, int type) {

                if (source == EasyImage.ImageSource.CAMERA) {


                    if (frontSelected) {
                        Picasso.with(DocumentDetailsActivity.this)
                                .load(imageFile)
                                .placeholder(R.mipmap.ic_launcher)
                                .error(R.mipmap.ic_launcher)
                                .into(profilePic);

                        mCurrentPhoto = imageFile;
                        Log.d(TAG, "front selected camera :" + frontSelected);
                    } else {

                        Picasso.with(DocumentDetailsActivity.this)
                                .load(R.mipmap.ic_launcher)
                                .placeholder(R.mipmap.ic_launcher)
                                .error(R.mipmap.ic_launcher)
                                .into(profilePic);
                        mCurrentRearPhoto = imageFile;
                        Log.d(TAG, "rear selected camera :" + rearSelected);
                    }


                } else if (source == EasyImage.ImageSource.GALLERY) {

                    if (frontSelected) {

                        Picasso.with(DocumentDetailsActivity.this)
                                .load(imageFile)
                                .placeholder(R.mipmap.ic_launcher)
                                .error(R.mipmap.ic_launcher)
                                .into(profilePic);
                        mCurrentPhoto = imageFile;

                        Log.d(TAG, "front selected gallery :" + frontSelected);

                    } else {

                        Picasso.with(DocumentDetailsActivity.this)
                                .load(imageFile)
                                .placeholder(R.mipmap.ic_launcher)
                                .error(R.mipmap.ic_launcher)
                                .into(profilePic);

                        mCurrentRearPhoto = imageFile;
                        Log.d(TAG, "rear selected gallery :" + rearSelected);

                    }

                }

            }
        });


    }

}
