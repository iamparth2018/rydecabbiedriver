package com.rydedispatch.driver.activity;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.gson.Gson;
import com.rydedispatch.driver.R;
import com.rydedispatch.driver.adapter.ChatAdapter;
import com.rydedispatch.driver.network.ApiClient;
import com.rydedispatch.driver.network.ApiInterface;
import com.rydedispatch.driver.response.RespGetMessageList;
import com.rydedispatch.driver.response.RespSendMessage;
import com.rydedispatch.driver.utils.AppConfig;
import com.rydedispatch.driver.utils.PreferenceHelper;
import com.rydedispatch.driver.utils.Prefs;
import com.rydedispatch.driver.utils.Progress;
import com.rydedispatch.driver.utils.Utils;

import java.util.Collections;
import java.util.LinkedList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChatActivity extends AppCompatActivity {
    @BindView(R.id.rl_chat)
    RelativeLayout rl_chat;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.tvTitle)
    AppCompatTextView tvTitle;
    @BindView(R.id.rv_message_list)
    RecyclerView rv_message_list;
    @BindView(R.id.iv_back)
    ImageView ivback;
    @BindView(R.id.btn_send)
    Button btn_send;

    protected PreferenceHelper helper;
    protected Progress progress;
    private LinearLayoutManager mLayoutManager;
    private ChatAdapter mChatAdapter;
    private LinkedList<RespGetMessageList.DataEntityX.ChatsEntity.DataEntity> listData = new LinkedList<RespGetMessageList.DataEntityX.ChatsEntity.DataEntity>();
    private String accept = "", authorization = "", message = "", resMessage = "", firstString = "";
    private Utils utils;
    private AppCompatEditText edittext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        ButterKnife.bind(this);

        tvTitle.setText(getString(R.string.res_support_center));

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        utils = new Utils(ChatActivity.this);
        progress = new Progress(ChatActivity.this);
        helper = new PreferenceHelper(ChatActivity.this, Prefs.PREF_FILE);

        accept = "application/json";
        authorization = "Bearer " + helper.LoadStringPref(Prefs.token, "");

        mLayoutManager = new LinearLayoutManager(ChatActivity.this);
        rv_message_list.setLayoutManager(mLayoutManager);
        rv_message_list.setItemAnimator(new DefaultItemAnimator());
        mChatAdapter = new ChatAdapter(ChatActivity.this, listData, 1);
        rv_message_list.setAdapter(mChatAdapter);

        //get message list from here
        getChatList(accept, authorization);

        edittext = (AppCompatEditText) findViewById(R.id.et_message);


        AppCompatEditText editor = new AppCompatEditText(this);
        editor.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);

        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver,
                new IntentFilter("custom-event-name-chat"));

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
    }

    private void sendMessageNew(String accept, String authorization, String message, String s) {
        utils.hideKeyboard();
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<RespGetMessageList> responceChathistoryCall = apiService.SEND_NEW_MESSAGE(accept, authorization, message, s);
        responceChathistoryCall.enqueue(new Callback<RespGetMessageList>() {
            @Override
            public void onResponse(Call<RespGetMessageList> call, Response<RespGetMessageList> response) {
                final RespGetMessageList responceChathistory = response.body();
                if (response.code() == AppConfig.URL.SUCCESS) {
                    Log.e("ChatActivity", "onResponse: " + response.body().toString());
                    Log.e("ChatActivity", "response 33: " + new Gson().toJson(response.body()));
                    if (!response.body().isError()) {

//                        lastPage = responceChathistory.getData().getChats().getLastPage();
//                        currentPage = responceChathistory.getData().getChats().getCurrentPage();
//                        totalpage = responceChathistory.getData().getChats().getTotal();
                        edittext.setText("");

                        if (responceChathistory.getData().getChats().getData().size() == 0) {
                            Toast.makeText(ChatActivity.this, "Empty History", Toast.LENGTH_SHORT).show();
                            return;
                        }
                        listData.clear();

                        Log.e("ChatActivity", "onResponse: size : " + responceChathistory.getData().getChats().getData().size());
                        listData.addAll(responceChathistory.getData().getChats().getData());
                        mChatAdapter.notifyDataSetChanged();
                        rv_message_list.scrollToPosition(mChatAdapter.getItemCount() - 1);
                        Collections.reverse(listData);

                    } else {
                        resMessage = responceChathistory.getMessage();
                        Toast.makeText(ChatActivity.this, resMessage, Toast.LENGTH_SHORT).show();

                        if (response.body().getMessage().contains("Unauthenticated")) {
                            openAlertUnauthLogout();
                            return;
                        }
                    }
                } else {

                    Toast.makeText(ChatActivity.this, getString(R.string.msg_unexpected_error), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<RespGetMessageList> call, Throwable t) {
                progress.hideDialog();

            }
        });
    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Get extra data included in the Intent
            String message = intent.getStringExtra("message");

            Log.d("RideConfirmDialog", "broadcast  :" + message);
            if (message.contains("message")) {
                finish();
                startActivity(getIntent());
                Log.d("RideConfirmDialog" , "RideConfirmDialog :" + message);
            } else {
                Log.d("RideConfirmDialog" , "RideConfirmDialog :" + message);
            }
        }
    };

    private void sendMessageNow(String accept, String authorization, String message, String page) {

//        Log.d("ChatMessage" , "msg :" + accept);
//        Log.d("ChatMessage" , "msg :" + authorization);
        Log.d("ChatMessage", "msg :" + message);
        Log.d("ChatMessage", "msg :" + page);


        progress.createDialog(false);
        progress.DialogMessage(getString(R.string.please_wait));
        progress.showDialog();

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        RequestBody message1 = RequestBody.create(MediaType.parse("multipart/form-data"), message);
        RequestBody page1 = RequestBody.create(MediaType.parse("multipart/form-data"), page);
        final Call<RespSendMessage> respDashboardDriverCall =
                apiService.SEND_MESSAGE(accept, authorization, message1, page1);
        respDashboardDriverCall.enqueue(new Callback<RespSendMessage>() {
            @Override
            public void onResponse(Call<RespSendMessage> call, Response<RespSendMessage> response) {
                progress.hideDialog();


//                listData.addAll(response.body().getData().getChats().getData());

                edittext.setText("");

                mChatAdapter.notifyDataSetChanged();
                rv_message_list.scrollToPosition(mChatAdapter.getItemCount() - 1);
                Collections.reverse(listData);


                if (response.body().getMessage().contains("Unauthenticated")) {
                    openAlertUnauthLogout();
                    return;
                }

            }

            @Override
            public void onFailure(Call<RespSendMessage> call, Throwable t) {
                progress.hideDialog();
//                Toast.makeText(MainActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void getChatList1(String accept, String authorization) {
        progress.createDialog(false);
        progress.DialogMessage(getString(R.string.please_wait));
        progress.hideDialog();

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        final Call<RespGetMessageList> respDashboardDriverCall = apiService.GET_MESSAGE_LIST(accept, authorization);
        respDashboardDriverCall.enqueue(new Callback<RespGetMessageList>() {
            @Override
            public void onResponse(Call<RespGetMessageList> call, Response<RespGetMessageList> response) {
                progress.hideDialog();

                listData.clear();
                listData.addAll(response.body().getData().getChats().getData());
                mChatAdapter.notifyDataSetChanged();
                rv_message_list.scrollToPosition(mChatAdapter.getItemCount() - 1);

                Collections.reverse(listData);

                if (response.body().getMessage().contains("Unauthenticated")) {
                    openAlertUnauthLogout();
                    return;
                }
            }

            @Override
            public void onFailure(Call<RespGetMessageList> call, Throwable t) {
                progress.hideDialog();
//                Toast.makeText(MainActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }


    private void getChatList(String accept, String authorization) {
        progress.createDialog(false);
        progress.DialogMessage(getString(R.string.please_wait));
        progress.hideDialog();

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        final Call<RespGetMessageList> respDashboardDriverCall = apiService.GET_MESSAGE_LIST(accept, authorization);
        respDashboardDriverCall.enqueue(new Callback<RespGetMessageList>() {
            @Override
            public void onResponse(Call<RespGetMessageList> call, Response<RespGetMessageList> response) {
                progress.hideDialog();


                listData.addAll(response.body().getData().getChats().getData());
                mChatAdapter.notifyDataSetChanged();
                rv_message_list.scrollToPosition(mChatAdapter.getItemCount() - 1);

                Collections.reverse(listData);

                if (response.body().getMessage().contains("Unauthenticated")) {
                    openAlertUnauthLogout();
                    return;
                }


            }

            @Override
            public void onFailure(Call<RespGetMessageList> call, Throwable t) {
                progress.hideDialog();
//                Toast.makeText(MainActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void openAlertUnauthLogout() {
        final Dialog mBottomSheetDialog = new Dialog(this, R.style.MaterialDialogSheet);
        mBottomSheetDialog.setContentView(R.layout.dialog_unauthorised);
        mBottomSheetDialog.setCancelable(true);
        mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        mBottomSheetDialog.getWindow().setGravity(Gravity.CENTER);
        mBottomSheetDialog.setCanceledOnTouchOutside(true);
        AppCompatTextView btn_ok = mBottomSheetDialog.findViewById(R.id.btn_ok);

        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                helper.clearAllPrefs();
                startActivity(new Intent(ChatActivity.this, LoginActivity.class));
                finish();
            }
        });
        mBottomSheetDialog.show();

    }

    @OnClick({R.id.iv_back, R.id.btn_send})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.btn_send:
                message = edittext.getText().toString();
//                sendMessageNow(accept, authorization, message , "1");
                if (TextUtils.isEmpty(message)) {
                    Snackbar.make(rl_chat, "Can't send Blank Message...!!!", Snackbar.LENGTH_SHORT).show();
                    return;
                }

                sendMessageNew(accept, authorization, message, "1");
                break;
        }
    }
}
