package com.rydedispatch.driver.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class AppConfig {

    public static final String URL_GET_AUDIO_TOKEN = "generateAudioToken";

    public static final class URL {

//        public static final String BASE_URL = "https://portal.rydedispatch.com/api/";
        public static final String BASE_URL = "http://167.172.247.117/api/";
//        public static final String BASE_URL_IMG = "https://portal.rydedispatch.com";
        public static final String BASE_URL_IMG = "http://167.172.247.117";

//        public static final String BASE_URL1 = "https://portal.rydedispatch.com/api";
//        public static final String BASE_IMAGEURL = "http://159.203.105.17";

//        public static final String BASE_IMAGEURL = "https://portal.rydedispatch.com";
        public static final String BASE_IMAGEURL = "http://167.172.247.117";



        public static final String URL_LOGIN = "login";
        public static final String URL_GET_YEAR_OF_CAR = "getYearofCar";
        public static final String URL_GET_MAKE_OF_CAR = "getMakeofCar";
        public static final String URL_MODEL_OF_CAR = "getModelofCar";
        public static final String URL_SEND_OTP = "sendOTP";
        public static final String URL_CREATE_DRIVER = "createDriver";
        public static final String URL_SEND_LOGIN_OTP = "sendLoginOTP";
        public static final String URL_PROFILE_DETAILS = "driverDashboard";
        public static final String URL_UPDATE_PROFILE = "updateProfile";
        public static final String URL_LOGOUT = "logOut";
        public static final String URL_DASHBOARD_DRIVER = "driverDashboard";
        public static final String URL_ADD_VEHICLE = "addVehicle";
        public static final String URL_ADD_ADDRESS = "addAddress";
        public static final String URL_GENERATE_AUDIO_TOKEN = "generateAudioToken";
        public static final String URL_UPDATE_AVAILABILITY = "updateAvailability";
        public static final String URL_UPLOAD_DOCUMENT = "uploadDocument";
        public static final String URL_CAR_TYPE = "getCarTypes";
        public static final String URL_GET_DOCUMENT = "getDocuments";
        public static final String URL_ACCEPT_RIDE = "acceptRide";
        public static final String URL_NO_SHOW = "noShow";

        public static final String URL_PASS_RIDE = "passRequest";
        public static final String URL_ARRIVED = "arrived";
        public static final String URL_START_RIDE = "startRide";
        public static final String URL_COMPLETE_RIDE = "completeRide";
        public static final String URL_SUBMIT_RATING = "submitRating";
        public static final String URL_UPLOADSIGNATURE = "uploadSignature";
        public static final String URL_HISTORY_LIST = "driverHistory";
        public static final String URL_GET_MESSAGE_LIST = "getMessages";
        public static final String URL_HISTORY_DETAILS = "historyDetails";
        public static final String URL_SEND_MESSAGE = "sendMessage";
        public static final String URL_UPDATE_MY_LOCATION = "updateLocation";
        public static final String URL_GET_STATEMENTS = "getStatements";
        public static final String URL_STATEMENT_DETAILS = "statementDetails";
        public static final String URL_GET_VEHICLE = "getVehicle";
        public static final String URL_GET_VEHICLE_DETAILS = "getVehicleDetails";
        public static final String URL_CANCEL_BY_DRIVER = "cancelRide";
        public static final String URL_GET_COMPANY = "getCompany";
        public static final String URL_ADD_INSPECTION = "addInspection";
        public static final String URL_GET_QUESTION = "getQuestions";


        public static final int SUCCESS = 200;

        public static final String URL_PASSENGER_DASHBOARD = "dashboard";

        public static final String TERMS_OF_SERVICE ="termsOfServices";

    }

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static final class EXTRA {
        public static final String FCM_GROUP = "RydeDriver";
        public static final String CHECKOUTGOING = "checkOutgoing";

        public static final int NOTIFICATION_LINK = 101;
        public static final String body = "body";
        public static final String flag = "flag";
        public static final String title = "title";
        public static final String main_title = "main_title";
        public static final String subtitle = "subtitle";
        public static final String url = "url";
        public static final String EMPTY = "";
        public static final String NotificationTitle = "Rydemate";

        public static final String CLIENTLATITUDE = "clatitiude";
        public static final String CLIENTLONGITUDE = "clatlongitude";
    }

    public static final class BUNDLE {
        public static final String barcode = "barcode";
        public static final String SelectManually = "selectManually";
        public static final String LOGIN = "login";
        public static final String Token = "token";
        public static final String UserUniqueCode = "user_unique_code";
        public static final String PassengerUniqueCode = "passenger_unique_code";
        public static final String documentid = "document_id";
        public static final String UserStatus = "user_status";
        public static final String AlertInspection = "alertinspect";

        public static final String RING = "Ring";
        public static final String User_Name = "User_Name";
        public static final String END = "end";
        public static final String CALL_END = "call_end";


        public static final String destination_latitude = "destination_latitude";
        public static final String destination_longitude = "destination_longitude";

        public static final String payment_type = "payment_type";
        public static final String user_id = "user_id";
        public static final String driver_id = "driver_id";
        public static final String request_id = "request_id";
        public static final String TotalTime = "total_time";
        public static final String TotalCost = "total_cost";
        public static final String source_longitude = "source_longitude";
        public static final String source_location = "source_location";
        public static final String source_latitude = "source_latitude";
        public static final String destination_location = "destination_location";
        public static final String user_image = "user_image";
        public static final String est_fare = "est_fare";
        public static final String is_schedule = "is_schedule";
        public static final String schedule_date = "schedule_date";
        public static final String eta = "eta";
        public static final String passenger_first_name = "passenger_first_name";
        public static final String passenger_last_name = "passenger_last_name";
        public static final String passenger_rating = "rating";
        public static final String passenger_image = "passenger_image";
        public static final String rating = "rating";


        public static final String SourceLat = "SourceLat";
        public static final String Source_Lang = "Source_Lang";
        public static final String Desti_Lat = "Desti_Lat";
        public static final String Desti_Lang = "Desti_Lang";
        public static final String actual_cost = "actual_cost";
        public static final String actual_discount = "actual_discount";
        public static final String base_fare = "base_fare";
        public static final String extra_distance_cost = "extra_distance_cost";
        public static final String pass_msg = "pass_msg";
        public static final String passenger_unique_code = "passenger_unique_code";
        public static final String status = "status";
        public static final String statusRide = "status";
        public static final String acceptRide = "acceptRide";
        public static final String earningID = "id";
        public static final String unique_id = "unique_id";
        public static final String id = "id";
        public static final String document_name = "document_name";
        public static final String front_document_url = "front_document_url";
        public static final String back_document_url = "back_document_url";
        public static final String document_type = "document_type";
        public static final String issue_date = "issue_date";
        public static final String expiry_date = "expiry_date";
        public static final String deleted_at = "deleted_at";
        public static final String created_at = "created_at";
        public static final String updated_at = "updated_at";
        public static final String display_passenger_name = "display_passenger_name";
        public static final String ride_instruction = "ride_instruction";
        public static final String driver_image = "driver_image";
        public static final String passenger_phone_number = "passenger_phone_number";

        public static final String Support_Number = "support_number";
    }

    public static final class PREFERENCE {

        public static final String PREF_FILE = "Rydemate";
        public static final String USER_LOGIN_CHECK = "login";
        public static final String AUTHTOKEN = "authtoken";
        public static final String FIRSTNAME = "firstname";

        public static final String PICK_LATLANG= "pick_latlang";
        public static final String PICK_LATITUDE = "pick_latitude";
        public static final String PICK_LONGITUDE = "pick_longitude";
        public static final String PICK_LOCATION_TXT = "pick_location_txt";
        public static final String DROP_LATITUDE = "drop_latitude";
        public static final String DROP_LONGITUDE = "drop_longitude";
        public static final String DROP_LOCATIOn_TXT = "drop_location_txt";
        public static final String LATER_DATE = "laterdate";
        public static final String LATER_TIME = "latertime";
        public static final String CAR_TYPE_ID = "cartypeid";
        public static final String CLIENTNAME = "clientname";
        public static final String USER_UNIQ_CODE = "useruniqcode";
        public static final String DRIVER_UNIQ_CODE = "driveruniqcode";
        public static final String SELECTREQID = "selectreqid";
        public static final String DRIVERNAME = "drivername";
        public static final String DRIVERIMG = "driverimg";
    }



}
