package com.rydedispatch.driver.utils;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.View;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;

public class DrawRouteMaps {
    private static final String TAG = "DrawRouteMaps";
    static SessionManager mSessionManager;
    private static DrawRouteMaps instance;
    private static Activity mActivity;
    private static View mprogress_view;
    private static boolean mshow_progress;
    private Context context;

    public static DrawRouteMaps getInstance(Context context, Activity activity, SessionManager sessionManager, View view, boolean showprogress) {
        instance = new DrawRouteMaps();
        instance.context = context;
        mSessionManager = sessionManager;
        mActivity = activity;
        mshow_progress = showprogress;
        mprogress_view = view;
        Log.e(TAG, "getInstance: " );
        return instance;
    }

    public static Context getContext() {
        return instance.context;
    }

    public void draw(LatLng origin, LatLng destination, GoogleMap googleMap) {
        String url_route = MapUtils.getDirectionsUrl(origin, destination, context);
        DrawRoute drawRoute = new DrawRoute(googleMap, context, mActivity, mSessionManager, mprogress_view, mshow_progress);
        drawRoute.execute(url_route);
    }
}
