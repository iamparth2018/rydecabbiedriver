package com.rydedispatch.driver.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;

import com.rydedispatch.driver.activity.VoiceActivity;

import java.util.Date;
import java.util.Set;

public class PreferenceHelper {

    private SharedPreferences.Editor editor;
    private SharedPreferences prefs;

    public PreferenceHelper(Context ctx, String FileName) {
        prefs = ctx.getSharedPreferences(FileName, Context.MODE_PRIVATE);
    }

    public PreferenceHelper(Context ctx) {
        prefs = ctx.getSharedPreferences(ctx.getPackageName(), Context.MODE_PRIVATE);

    }



    public void clearAllPrefs() {
        prefs.edit().clear().apply();
    }

    @SuppressLint("CommitPrefEdits")
    public void initPref() {
        editor = prefs.edit();
    }

    public void ApplyPref() {
        editor.apply();
    }

    public void SaveStringPref(String key, String value) {
        editor.putString(key, value);
    }

    public String LoadStringPref(String key, String DefaultValue) {
        return prefs.getString(key, DefaultValue);
    }

    public String LoadStringPref(String key) {
        return prefs.getString(key, "");
    }

    public String loadAuthkey() {
        return prefs != null ? prefs.getString(Prefs.AUTH_KEY, "") : "";
    }

    public void SaveIntPref(String key, int value) {
        editor.putInt(key, value);
    }

    public int LoadIntPref(String key, int DefaultValue) {
        return prefs.getInt(key, DefaultValue);
    }

    public int LoadIntPref(String key) {
        return prefs.getInt(key, 0);
    }

    public void SaveBooleanPref(String key, boolean value) {
        editor.putBoolean(key, value);
    }

    public boolean LoadBooleanPref(String key, boolean DefaultValue) {
        return prefs.getBoolean(key, DefaultValue);
    }

    public void SaveSetPref(String key, Set<String> value) {
        editor.putStringSet(key, value);
    }

    public String getDriverName(){
        return prefs.getString("driverName","");
    }

    public void setDriverName(String id){
        prefs.edit().putString("driverName",id).apply();
    }

    public String getDriverUniqueCode(){
        return prefs.getString("driver_unique_code","");
    }

    public void setDriverUniqueCode(String id){
        prefs.edit().putString("driver_unique_code",id).apply();
    }

    public Set<String> LoadSetPref(String key, Set<String> value) {
        return prefs.getStringSet(key, value);
    }

    public String getTwilioAuth(){
        return prefs.getString("TwilioAuth","");
    }

    public void setTwilioAuth(String user){
        prefs.edit().putString("TwilioAuth",user).apply();
    }

    public String getIdentity() {
        return prefs.getString("setIdentity", "");
    }

    public void setIdentity(String setIdentity) {
        prefs.edit().putString("setIdentity", setIdentity).apply();
    }

    public boolean isLogin(){
        return prefs.getBoolean("isLogin",false);
    }

    public void setLogin(boolean b){
        prefs.edit().putBoolean("isLogin",b).apply();
    }

    public String getUserUniqueCode(){
        return prefs.getString("user_unique_code","");
    }

    public void setUserUniqueCode(String id){
        prefs.edit().putString("user_unique_code",id).apply();
    }

    public String getFirstName(){
        return prefs.getString("FirstName","");
    }

    public void setFirstName(String user){
        prefs.edit().putString("FirstName",user).apply();
    }

    public String getLastName(){
        return prefs.getString("LastName","");
    }

    public void setLastName(String user){
        prefs.edit().putString("LastName",user).apply();
    }

    public String getUserPic(){
        return prefs.getString("ProfilePic","");
    }

    public void setUserPic(String pic){
        prefs.edit().putString("ProfilePic",pic).apply();
    }

    public String getAuth(){
        return prefs.getString("UserAuth","");
    }

    public void setAuth(String user){
        prefs.edit().putString("UserAuth",user).apply();
    }



    public String getDriverPic(){
        return prefs.getString("driverPic","");
    }

    public void setDriverPic(String id){
        prefs.edit().putString("driverPic",id).apply();
    }

    public String getClientName(){
        return prefs.getString("ClientName","");
    }

    public void setClientName(String id){
        prefs.edit().putString("ClientName",id).apply();
    }

    public void SetDate(String format) {
        prefs.edit().putString("date",format).apply();

    }

    public String getDate() {
        return prefs.getString("date","");
    }






   /* public void printVals() {
        for (Map.Entry<String, ?> entry : prefs.getAll().entrySet()) {
            Utils.Log("printVals: ", "key : " + entry.getKey() + " : " + entry.getValue().toString());
        }
    }*/
}
