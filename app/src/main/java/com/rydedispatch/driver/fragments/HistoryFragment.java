package com.rydedispatch.driver.fragments;

import android.animation.ObjectAnimator;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.rydedispatch.driver.R;
import com.rydedispatch.driver.activity.HistoryDetails;
import com.rydedispatch.driver.activity.LoginActivity;
import com.rydedispatch.driver.activity.VoiceActivity;
import com.rydedispatch.driver.adapter.HistoryAdapter;
import com.rydedispatch.driver.network.ApiClient;
import com.rydedispatch.driver.network.ApiInterface;
import com.rydedispatch.driver.response.RespHistory;
import com.rydedispatch.driver.utils.AppConfig;
import com.rydedispatch.driver.utils.GPSTracker;
import com.rydedispatch.driver.utils.PreferenceHelper;
import com.rydedispatch.driver.utils.Prefs;
import com.rydedispatch.driver.utils.Progress;
import com.rydedispatch.driver.utils.Utils;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HistoryFragment extends Fragment /*implements PermissionsListener*/ {

    private static final String TAG = HistoryFragment.class.getSimpleName();

    ArrayList<String> sortByList = new ArrayList<String>();
    @BindView(R.id.circular_progress_bar)
    ProgressBar pbar;
    @BindView(R.id.rv_history)
    RecyclerView rvHistory;
    @BindView(R.id.tv_list)
    AppCompatTextView tv_list;
    @BindView(R.id.tv_upcoming)
    AppCompatTextView tv_upcoming;
    @BindView(R.id.tv_complete)
    AppCompatTextView tv_complete;
    @BindView(R.id.frame_history)
    FrameLayout frame_history;
    private Utils utils;
    private String accept = "", authorization = "", status = "", displaypassengername = "",
            schedule = "", requestId = "", runningRideStatus = "", uniqueCode = "",
            firstname = "", lastname = "", selectedItem = "",rideinstruction = "";
    protected PreferenceHelper helper;
    protected Progress progress;
    CardView media_card_view;
    private LinearLayoutManager mLayoutManager;
    private HistoryAdapter mHistoryAdapter;
    private List<RespHistory.DataEntityX.HistoryEntity.DataEntity> listData = new ArrayList<>();
    final Calendar myCalendar = Calendar.getInstance();
    Date date = null;

    DatePickerDialog datePickerDialog;
    Dialog mBottomSheetDialog;
    int year;
    int month;
    int dayOfMonth;
    private Calendar calendar;

    double destinationLat, destinationLang, sourceLat, sourceLang;
//    private MapView mapView;
//    MapboxMap mapboxMap;
//    NavigationLauncherOptions options;
    GPSTracker gps;
//    private DirectionsRoute currentRoute;
//    private LocationComponent locationComponent;
//    private PermissionsManager permissionsManager;

    private int currentPage = 1;
    private int lastPage = 1;
    private int totalpage;
    private int nextPage = 1;


    public HistoryFragment() {
    }

    public static HistoryFragment newInstance() {

        HistoryFragment fragment = new HistoryFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        Mapbox.getInstance(getActivity(), getString(R.string.map_box_token));
        setHasOptionsMenu(true);

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
//        Mapbox.getInstance(getActivity(), getString(R.string.map_box_token));
        gps = new GPSTracker(getActivity());

        View rootView = inflater.inflate(R.layout.fragment_history, container, false);
        ButterKnife.bind(this, rootView);

        setHasOptionsMenu(true);

        utils = new Utils(getActivity());
        progress = new Progress(getActivity());
        helper = new PreferenceHelper(getActivity(), Prefs.PREF_FILE);

        accept = "application/json";
        authorization = "Bearer " + helper.LoadStringPref(Prefs.token, "");


        /*mapView = rootView.findViewById(R.id.mapView_main_history);
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(new com.mapbox.mapboxsdk.maps.OnMapReadyCallback() {
            @Override
            public void onMapReady(@NonNull MapboxMap map) {
                mapboxMap = map;
                mapboxMap.setStyle(getString(R.string.navigation_guidance_day), new Style.OnStyleLoaded() {
                    @Override
                    public void onStyleLoaded(@NonNull Style style) {
                        enableLocationComponent(style);

                        addDestinationIconSymbolLayer(style);

                    }
                });
            }
        });*/

        mLayoutManager = new LinearLayoutManager(getActivity());
        rvHistory.setLayoutManager(mLayoutManager);
        rvHistory.setItemAnimator(new DefaultItemAnimator());
        mHistoryAdapter = new HistoryAdapter(getActivity(), listData);
        rvHistory.setAdapter(mHistoryAdapter);


        mHistoryAdapter.setListener(new HistoryAdapter.customOnclickListener() {
            @Override
            public void onclick(int adapterPosition, RespHistory.DataEntityX.HistoryEntity.DataEntity listhistories) {

                requestId = String.valueOf(listhistories.getId());
                status = String.valueOf(listhistories.getStatus());
                schedule = String.valueOf(listhistories.getIsSchedule());
                displaypassengername = String.valueOf(listhistories.getDisplay_passenger_name());
                firstname = String.valueOf(listhistories.getFirstName());
                lastname = String.valueOf(listhistories.getLastName());
                uniqueCode = String.valueOf(listhistories.getUser_unique_code());

                startActivity(new Intent(getActivity(), HistoryDetails.class)
                        .putExtra(AppConfig.BUNDLE.request_id, requestId)
                        .putExtra(AppConfig.BUNDLE.statusRide, status)
                        .putExtra(AppConfig.BUNDLE.is_schedule, schedule));
            }
        });

        mHistoryAdapter.setCalllistener(new HistoryAdapter.callListener() {
            @Override
            public void oncall(int adapterPosition, RespHistory.DataEntityX.HistoryEntity.DataEntity listhistories) {
                displaypassengername = String.valueOf(listhistories.getDisplay_passenger_name());
                firstname = String.valueOf(listhistories.getFirstName());
                lastname = String.valueOf(listhistories.getLastName());
                uniqueCode = String.valueOf(listhistories.getUser_unique_code());

                startActivity(new Intent(getActivity(), VoiceActivity.class)
                        .putExtra(AppConfig.BUNDLE.passenger_first_name, firstname)
                        .putExtra(AppConfig.BUNDLE.passenger_last_name, lastname)
                        .putExtra(AppConfig.BUNDLE.PassengerUniqueCode, uniqueCode)
                        .putExtra(AppConfig.BUNDLE.display_passenger_name, displaypassengername)
                        .putExtra(AppConfig.EXTRA.CHECKOUTGOING, 1));
            }
        });

        mHistoryAdapter.setInstructionlistener(new HistoryAdapter.instructionlistener() {
            @Override
            public void instruction(int adapterPosition, RespHistory.DataEntityX.HistoryEntity.DataEntity listhistories) {
                Toast.makeText(getActivity(), "" +listhistories.getRide_instruction(), Toast.LENGTH_SHORT).show();
            }
        });

        mHistoryAdapter.setNavilistener(new HistoryAdapter.naviListener() {
            @Override
            public void navigate(int adapterPosition, RespHistory.DataEntityX.HistoryEntity.DataEntity listhistories) {
                destinationLat = Double.parseDouble(listhistories.getDestination_latitude());
                destinationLang = Double.parseDouble(listhistories.getDestination_longitude());
                sourceLat = Double.parseDouble(listhistories.getSource_latitude());
                sourceLang = Double.parseDouble(listhistories.getSource_longitude());

//                initMap(sourceLat, sourceLang);

//                onMapBox();

                googleMapExternal(sourceLat, sourceLang);

            }
        });

        currentPage = 1;
        getHistoryList(accept, authorization, "", "", "", "0", "all", currentPage);


        return rootView;
    }

    private void openMoreInstructions() {
        final Dialog mBottomSheetDialog = new Dialog(getActivity(), R.style.MaterialDialogSheet);
        mBottomSheetDialog.setContentView(R.layout.dialog_instruction);
        mBottomSheetDialog.setCancelable(true);
        mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        mBottomSheetDialog.getWindow().setGravity(Gravity.CENTER);
        mBottomSheetDialog.setCanceledOnTouchOutside(true);
        AppCompatTextView tv_instruction = mBottomSheetDialog.findViewById(R.id.tv_instruction_details);
        AppCompatTextView btn_ok = mBottomSheetDialog.findViewById(R.id.btn_ok);

        tv_instruction.setText(rideinstruction);


        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBottomSheetDialog.dismiss();

            }
        });
        mBottomSheetDialog.show();
    }

    private void googleMapExternal(double sourceLat, double sourceLang) {
        String url = "google.navigation:q=" + sourceLat + "," + sourceLang + "&mode=d";
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        startActivity(intent);
    }


    private void getHistoryList(String accept, String authorization,
                                String search, String startDate, String endDate, String isschedule,
                                String status, int page) {
        pbar.setVisibility(View.VISIBLE);
        ObjectAnimator anim = ObjectAnimator.ofInt(pbar, "progress", 0, 100);
        anim.setDuration(15000);
        anim.setInterpolator(new DecelerateInterpolator());
        anim.start();

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        RequestBody search1 = RequestBody.create(MediaType.parse("multipart/form-data"), search);
        RequestBody startDate1 = RequestBody.create(MediaType.parse("multipart/form-data"), startDate);
        RequestBody endDate1 = RequestBody.create(MediaType.parse("multipart/form-data"), endDate);
        RequestBody isschedule1 = RequestBody.create(MediaType.parse("multipart/form-data"), isschedule);
        RequestBody status1 = RequestBody.create(MediaType.parse("multipart/form-data"), status);
        RequestBody page1 = RequestBody.create(MediaType.parse("multipart/form-data"), String.valueOf(page));

        final Call<RespHistory> respHistoryCall = apiService.HISTORY_CALL(accept, authorization,
                search1, startDate1, endDate1, isschedule1, status1, page1);
        respHistoryCall.enqueue(new Callback<RespHistory>() {
            @Override
            public void onResponse(Call<RespHistory> call, Response<RespHistory> response) {
                pbar.setVisibility(View.GONE);


                if (response.isSuccessful()) {
                    if (response.body().getMessage().equalsIgnoreCase("Your history has been get successfully.")) {
                        tv_list.setVisibility(View.GONE);
                        listData.clear();

                        listData.addAll(response.body().getData().getHistory().getData());
                        mHistoryAdapter.notifyDataSetChanged();

                        lastPage = response.body().getData().getHistory().getLastPage();
                        currentPage = response.body().getData().getHistory().getCurrentPage();
                    } else if (response.body().getData().getHistory().getData().isEmpty()) {
                        tv_list.setVisibility(View.VISIBLE);
                        tv_list.setText("Empty List");
                    } else if (response.body().getMessage().contains("Unauthenticated")) {
                        openAlertUnauthLogout();
                    } else {
                        Snackbar.make(frame_history, "No data available..!!", Snackbar.LENGTH_SHORT).show();
                    }
                } else if (response.code() == 500) {
                    Snackbar.make(frame_history, "Internal Server Error..!!", Snackbar.LENGTH_SHORT).show();
                } else {
                    Snackbar.make(frame_history, "" + response.message(), Snackbar.LENGTH_SHORT).show();

                }

            }

            @Override
            public void onFailure(Call<RespHistory> call, Throwable t) {
                pbar.setVisibility(View.GONE);

            }
        });
    }


    private void getHistoryList1(String accept, String authorization,
                                 String search, String startDate, String endDate, String status) {

        progress.createDialog(false);
        progress.DialogMessage(getString(R.string.please_wait));
        progress.showDialog();

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        RequestBody search1 = RequestBody.create(MediaType.parse("multipart/form-data"), search);
        RequestBody startDate1 = RequestBody.create(MediaType.parse("multipart/form-data"), startDate);
        RequestBody endDate1 = RequestBody.create(MediaType.parse("multipart/form-data"), endDate);
        RequestBody status1 = RequestBody.create(MediaType.parse("multipart/form-data"), status);

        final Call<RespHistory> respHistoryCall = apiService.HISTORY_CALL_FILTER(accept, authorization,
                search1, startDate1, endDate1, status1);
        respHistoryCall.enqueue(new Callback<RespHistory>() {
            @Override
            public void onResponse(Call<RespHistory> call, Response<RespHistory> response) {
                progress.hideDialog();


                if (response.isSuccessful()) {
                    if (response.body().getMessage().contains("Unauthenticated")) {
                        openAlertUnauthLogout();
                        return;
                    }

                    if (response.body().getData().getHistory().getData().isEmpty()) {
                        Snackbar.make(frame_history, "No data available..!!", Snackbar.LENGTH_SHORT).show();
                        return;
                    }
                    sortByList.clear();
                    listData.clear();
                    listData.addAll(response.body().getData().getHistory().getData());
                    mHistoryAdapter.notifyDataSetChanged();
                } else {
                    Snackbar.make(frame_history, "" + response.body().getMessage(), Snackbar.LENGTH_SHORT).show();

                }

            }

            @Override
            public void onFailure(Call<RespHistory> call, Throwable t) {
                progress.hideDialog();
                Log.d(TAG, "HISTORY_CALL_FILTER on failure :" + t.getLocalizedMessage());
                Log.d(TAG, "HISTORY_CALL_FILTER on failure :" + t.getMessage());
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();

    }


    @OnClick({R.id.tv_upcoming, R.id.tv_complete})
    public void onViewClicked(View view) {
        switch (view.getId()) {

            case R.id.tv_complete:
                setHasOptionsMenu(true);
                getHistoryList1(accept, authorization, "",
                        "", "", "completed");
                break;

            case R.id.tv_upcoming:
                tv_upcoming.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                tv_complete.setBackgroundColor(Color.TRANSPARENT);
                tv_upcoming.setTextColor(getResources().getColor(R.color.black));
                setHasOptionsMenu(false);
                break;
        }
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.filter, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_filter:
                sortByList.clear();
                openFilterpopUp();
                break;
        }
        return true;
    }

    private void openFilterpopUp() {
        mBottomSheetDialog = new Dialog(getActivity(), R.style.MaterialDialogSheet);
        mBottomSheetDialog.setContentView(R.layout.dialog_filter);
        mBottomSheetDialog.setCancelable(false);
        mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        mBottomSheetDialog.getWindow().setGravity(Gravity.CENTER);
        mBottomSheetDialog.setCanceledOnTouchOutside(false);
        AppCompatTextView btn_ok = mBottomSheetDialog.findViewById(R.id.btn_ok);
        ImageButton btn_close = mBottomSheetDialog.findViewById(R.id.btn_close);
        EditText et_start_date = mBottomSheetDialog.findViewById(R.id.et_start_date);
        EditText et_end_date = mBottomSheetDialog.findViewById(R.id.et_end_date);
        EditText et_search_name = mBottomSheetDialog.findViewById(R.id.et_search_name);
        Spinner spinner1 = mBottomSheetDialog.findViewById(R.id.spinner_filter_status);

        sortByList.add("Status");
        sortByList.add(getString(R.string.res_all));
        sortByList.add(getString(R.string.res_cancelled));
        sortByList.add(getString(R.string.res_completed));
        sortByList.add(getString(R.string.res_cancelled_by_driver));
        sortByList.add(getString(R.string.res_no_show));

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity()
                , R.layout.spinner_item, sortByList);

        adapter.setDropDownViewResource(R.layout.row_auto_complete);
        spinner1.setAdapter(adapter);

        spinner1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (!adapterView.getItemAtPosition(i).toString().equalsIgnoreCase("Status")) {
                    selectedItem = spinner1.getItemAtPosition(i).toString();

                    if (selectedItem.equals(getString(R.string.res_all)))
                        selectedItem = "all";
                    else if (selectedItem.equals(getString(R.string.res_cancelled)))
                        selectedItem = "cancelled";
                    else if (selectedItem.equals(getString(R.string.res_completed)))
                        selectedItem = "completed";

                    else if (selectedItem.equals(getString(R.string.res_cancelled_by_driver)))
                        selectedItem = "cancelled_by_driver";
                    else
                        selectedItem = "no_show";

                } else {
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        et_start_date.setFocusable(false);
        et_end_date.setFocusable(false);

        et_start_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calendar = Calendar.getInstance();
                year = calendar.get(Calendar.YEAR);
                month = calendar.get(Calendar.MONTH);
                dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
                calendar.add(Calendar.DATE, 3);
                datePickerDialog = new DatePickerDialog(getActivity(),
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker datePicker, int year, int month, int day) {

                                et_start_date.setText(day + "-" + (month + 1) + "-" + year);
                            }
                        }, year, month, dayOfMonth);
                datePickerDialog.show();
            }
        });

        et_end_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calendar = Calendar.getInstance();
                year = calendar.get(Calendar.YEAR);
                month = calendar.get(Calendar.MONTH);
                dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
                calendar.add(Calendar.DATE, 3);
                datePickerDialog = new DatePickerDialog(getActivity(),
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker datePicker, int year, int month, int day) {

                                et_end_date.setText(day + "-" + (month + 1) + "-" + year);
                            }
                        }, year, month, dayOfMonth);
                datePickerDialog.show();
            }
        });


        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Log.d("HistoryFragment", "filter selectedItem :" + selectedItem);
                getHistoryList1(accept, authorization, et_search_name.getText().toString(),
                        et_start_date.getText().toString(), et_end_date.getText().toString(), selectedItem);
                mBottomSheetDialog.dismiss();

            }
        });


        btn_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBottomSheetDialog.dismiss();
            }
        });


        mBottomSheetDialog.show();
    }


    private void updateLabel() {
        String myFormat = "yyyy-mm-dd"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

//        edittext.setText(sdf.format(myCalendar.getTime()));

        Log.d("HistoryFragment", "date : " + sdf.format(myCalendar.getTime()));
    }

    private void openAlertUnauthLogout() {
        final Dialog mBottomSheetDialog = new Dialog(getActivity(), R.style.MaterialDialogSheet);
        mBottomSheetDialog.setContentView(R.layout.dialog_unauthorised);
        mBottomSheetDialog.setCancelable(false);
        mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        mBottomSheetDialog.getWindow().setGravity(Gravity.CENTER);
        mBottomSheetDialog.setCanceledOnTouchOutside(false);
        AppCompatTextView btn_ok = mBottomSheetDialog.findViewById(R.id.btn_ok);

        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progress.createDialog(false);
                progress.DialogMessage(getString(R.string.msg_logout));
                progress.showDialog();
                AsyncTask.execute(new Runnable() {
                    @Override
                    public void run() {

                        try {
                            FirebaseInstanceId.getInstance().deleteInstanceId();
                            helper.clearAllPrefs();
                            startActivity(new Intent(getActivity(), LoginActivity.class)
                                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                            progress.hideDialog();
                            getActivity().finish();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        });
        mBottomSheetDialog.show();
    }


    /*private void initMap(double sourceLat, double sourceLang) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {


                try {
                    Point destinationPoint = Point.fromLngLat(sourceLang, sourceLat);

                    Point originPoint = Point.fromLngLat(gps.getLongitude(), gps.getLatitude());

                    getRoute(originPoint, destinationPoint);

                    mapboxMap.setStyle(getString(R.string.navigation_guidance_day), new Style.OnStyleLoaded() {
                        @Override
                        public void onStyleLoaded(@NonNull Style style) {

//                            progress.hideDialog();
                            enableLocationComponent(style);

                            addDestinationIconSymbolLayer(style);

                        }
                    });
                } catch (RuntimeException r) {
                    r.printStackTrace();
                }

            }
        }, 1000);
    }


    @SuppressWarnings({"MissingPermission"})
    private void enableLocationComponent(@NonNull Style loadedMapStyle) {
// Check if permissions are enabled and if not request
        if (PermissionsManager.areLocationPermissionsGranted(getActivity())) {
// Activate the MapboxMap LocationComponent to show user location
// Adding in LocationComponentOptions is also an optional parameter
            locationComponent = mapboxMap.getLocationComponent();
            locationComponent.activateLocationComponent(getActivity(), loadedMapStyle);
            locationComponent.setLocationComponentEnabled(true);
// Set the component's camera mode
            locationComponent.setCameraMode(CameraMode.TRACKING);
        } else {
            permissionsManager = new PermissionsManager(this);
            permissionsManager.requestLocationPermissions(getActivity());
        }
    }

    private void addDestinationIconSymbolLayer(@NonNull Style loadedMapStyle) {
        loadedMapStyle.addImage("destination-icon-id",
                BitmapFactory.decodeResource(this.getResources(), R.drawable.mapbox_marker_icon_default));
        GeoJsonSource geoJsonSource = new GeoJsonSource("destination-source-id");
        loadedMapStyle.addSource(geoJsonSource);
        SymbolLayer destinationSymbolLayer = new SymbolLayer("destination-symbol-layer-id", "destination-source-id");
        destinationSymbolLayer.withProperties(
                iconImage("destination-icon-id"),
                iconAllowOverlap(true),
                iconIgnorePlacement(true)
        );
        loadedMapStyle.addLayer(destinationSymbolLayer);
    }


    private void getRoute(Point origin, Point destination) {
        progress.createDialog(true);
        progress.DialogMessage(getString(R.string.please_wait));
        progress.showDialog();

        NavigationRoute.builder(getActivity())
                .accessToken(Mapbox.getAccessToken())
                .origin(origin)
                .destination(destination)
                .build()
                .getRoute(new Callback<DirectionsResponse>() {
                    @Override
                    public void onResponse(Call<DirectionsResponse> call, Response<DirectionsResponse> response) {
// You can get the generic HTTP info about the response

                        progress.hideDialog();
                        Log.d(TAG, "Response code: " + response.code());
                        if (response.body() == null) {
                            Log.e(TAG, "No routes found, make sure you set the right user and access token.");
                        } else if (response.body().routes().size() < 1) {
                            Log.e(TAG, "No routes found");

                            currentRoute = response.body().routes().get(0);

                            boolean simulateRoute = false;
                            options = NavigationLauncherOptions.builder()
                                    .directionsRoute(currentRoute)
                                    .shouldSimulateRoute(simulateRoute)
                                    .build();
                        } else {
                            currentRoute = response.body().routes().get(0);

                            boolean simulateRoute = false;
                            options = NavigationLauncherOptions.builder()
                                    .directionsRoute(currentRoute)
                                    .shouldSimulateRoute(simulateRoute)
                                    .build();
                        }
                    }

                    @Override
                    public void onFailure(Call<DirectionsResponse> call, Throwable throwable) {
                        progress.hideDialog();

                        Log.e(TAG, "Error: " + throwable.getMessage());
                    }
                });
    }*/


    /*private void onMapBox() {


        if (options == null) {
            return;
        }
        app.setVisibility(View.GONE);
        NavigationLauncher.startNavigation(getActivity(), options);
    }*/


    /*@Override
    public void onExplanationNeeded(List<String> permissionsToExplain) {
        Toast.makeText(getActivity(), R.string.user_location_permission_explanation, Toast.LENGTH_LONG).show();

    }

    @Override
    public void onPermissionResult(boolean granted) {
        if (granted) {
//            enableLocationComponent(mapboxMap.getStyle());
        } else {
            Toast.makeText(getActivity(), R.string.user_location_permission_not_granted, Toast.LENGTH_LONG).show();
            getActivity().finish();
        }
    }*/
}
