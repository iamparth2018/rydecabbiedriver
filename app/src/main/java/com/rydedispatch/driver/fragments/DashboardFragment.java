package com.rydedispatch.driver.fragments;

import android.app.Dialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatTextView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.rydedispatch.driver.R;
import com.rydedispatch.driver.activity.AddAddressActivity;
import com.rydedispatch.driver.activity.LoginActivity;
import com.rydedispatch.driver.activity.MainActivity;
import com.rydedispatch.driver.network.ApiClient;
import com.rydedispatch.driver.network.ApiInterface;
import com.rydedispatch.driver.response.RespDashboardDriver;
import com.rydedispatch.driver.utils.AppConfig;
import com.rydedispatch.driver.utils.PreferenceHelper;
import com.rydedispatch.driver.utils.Prefs;
import com.rydedispatch.driver.utils.Progress;
import com.rydedispatch.driver.utils.Utils;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DashboardFragment extends Fragment {

    private static final String TAG = "DashboardFragment";
    private Utils utils;
    private String accept = "", authorization = "", firstname = "", lastname = "",
            email = "", city = "", address = "";
    protected PreferenceHelper helper;
    protected Progress progress;
    @BindView(R.id.tv_status_document)
    AppCompatTextView tv_status;
    @BindView(R.id.rl_layout_dashboard)
    RelativeLayout rl_layout_dashboard;

    public static DashboardFragment newInstance() {
        DashboardFragment fragment = new DashboardFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_dashboard, container, false);
        ButterKnife.bind(this, rootView);

        utils = new Utils(getActivity());
        progress = new Progress(getActivity());
        helper = new PreferenceHelper(getActivity(), Prefs.PREF_FILE);

        accept = "application/json";
        authorization = "Bearer " + helper.LoadStringPref(Prefs.token, "");

        initialDash();
        return rootView;
    }

    private void initialDash() {
        getDashboard(accept, authorization);
    }

    private void getDashboard(String accept, String authorization) {
        progress.createDialog(true);
        progress.DialogMessage(getString(R.string.please_wait));
        progress.hideDialog();

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        final Call<RespDashboardDriver> respDashboardDriverCall = apiService.DRIVER_DASHBOARD(accept, authorization);
        respDashboardDriverCall.enqueue(new Callback<RespDashboardDriver>() {
            @Override
            public void onResponse(Call<RespDashboardDriver> call, Response<RespDashboardDriver> response) {
                progress.hideDialog();

                try {
                    if (response.isSuccessful()) {

                        if (response.body().getMessage().contains("Unauthenticated")) {
                            progress.hideDialog();

                            openAlertUnauthLogout();
                            return;
                        }

                        address = response.body().getData().getUserDetails().getAddress();

                        if (response.body().getData().getUserDetails().getDocumentStatus().equalsIgnoreCase("pending")) {
                            if (response.body().getData().getUserDetails().getDocumentStatus().equalsIgnoreCase("pending")) {
                                tv_status.setVisibility(View.VISIBLE);
                            } else {
                                tv_status.setVisibility(View.GONE);

                            }
                            return;
                        }

                        if (response.body().getData().getUserDetails().getAddress().equalsIgnoreCase("")) {
                            Intent intent = new Intent(getActivity(), AddAddressActivity.class);
                            intent.putExtra(AppConfig.BUNDLE.Token, helper.LoadStringPref(Prefs.token, ""));
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            return;
                        }

                        if (!response.body().getData().getUserDetails().getAddress().isEmpty() || !address.isEmpty()) {
                            if (response.body().getData().getUserDetails().getDocumentStatus().equalsIgnoreCase("pending")) {
                                tv_status.setVisibility(View.VISIBLE);


                            } else {
                                tv_status.setVisibility(View.GONE);

                            }


                        } else {
                            Intent intent = new Intent(getActivity(), AddAddressActivity.class);
                            intent.putExtra(AppConfig.BUNDLE.Token, helper.LoadStringPref(Prefs.token, ""));
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                        }


                        Log.d(TAG, "response dashboard : " + new Gson().toJson(response.body()));
                        Log.d(TAG, "response addresss : " + new Gson().toJson(response.body().getData().getUserDetails().getAddress()));

                    }
                    else if (response.code()==500) {
                        openAlertUnauthLogout();

                    }
                    else {
                        Snackbar.make(rl_layout_dashboard, "Something Went Wrong..!!", Snackbar.LENGTH_SHORT).show();
                        openAlertUnauthLogout();
                    }
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                }


            }

            @Override
            public void onFailure(Call<RespDashboardDriver> call, Throwable t) {
                progress.hideDialog();
            }
        });
    }

    private void openAlertUnauthLogout() {


        final Dialog mBottomSheetDialog = new Dialog(getActivity(), R.style.MaterialDialogSheet);
        mBottomSheetDialog.setContentView(R.layout.dialog_unauthorised);
        mBottomSheetDialog.setCancelable(false);
        mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        mBottomSheetDialog.getWindow().setGravity(Gravity.CENTER);
        mBottomSheetDialog.setCanceledOnTouchOutside(false);
        AppCompatTextView btn_ok = mBottomSheetDialog.findViewById(R.id.btn_ok);

        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                progress.createDialog(false);
                progress.DialogMessage(getString(R.string.msg_logout));
                progress.showDialog();
                AsyncTask.execute(new Runnable() {
                    @Override
                    public void run() {

                        try {
                            FirebaseInstanceId.getInstance().deleteInstanceId();
                            helper.clearAllPrefs();
                            startActivity(new Intent(getActivity(), LoginActivity.class)
                                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                            progress.hideDialog();
                            getActivity().finish();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        });
        mBottomSheetDialog.show();
    }


    @Override
    public void onPause() {
        super.onPause();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.main, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                break;
        }
        return true;
    }
}
