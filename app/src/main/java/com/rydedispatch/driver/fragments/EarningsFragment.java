package com.rydedispatch.driver.fragments;

import android.animation.ObjectAnimator;
import android.app.Dialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.google.firebase.iid.FirebaseInstanceId;
import com.rydedispatch.driver.R;
import com.rydedispatch.driver.activity.EarningDetailsActivity;
import com.rydedispatch.driver.activity.LoginActivity;
import com.rydedispatch.driver.adapter.EarningsYearAdapter;
import com.rydedispatch.driver.network.ApiClient;
import com.rydedispatch.driver.network.ApiInterface;
import com.rydedispatch.driver.response.RespGetStatements;
import com.rydedispatch.driver.utils.AppConfig;
import com.rydedispatch.driver.utils.PreferenceHelper;
import com.rydedispatch.driver.utils.Prefs;
import com.rydedispatch.driver.utils.Progress;
import com.rydedispatch.driver.utils.Utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class EarningsFragment extends Fragment {
    @BindView(R.id.circular_progress_bar)
    ProgressBar pbar;
    @BindView(R.id.rl_earning)
    RelativeLayout rl_earning;
    @BindView(R.id.rv_item_doc)
    RelativeLayout rvItem;
    @BindView(R.id.rv_earning_years)
    RecyclerView rvearningYears;
    @BindView(R.id.tv_earning)
    AppCompatTextView tvearning;
    @BindView(R.id.tv_total_completed)
    AppCompatTextView tv_total_completed;
    private LinearLayoutManager mLayoutManager;
    private EarningsYearAdapter mDocumentAdapter;

    protected PreferenceHelper helper;
    protected Progress progress;

    private Utils utils;
    private String accept = "", authorization = "",status = "", requestId = "", earningID = "";

    private List<RespGetStatements.DataEntityX.StatementsEntity.DataEntity> listData = new ArrayList<>();


    public EarningsFragment() {
        // Required empty public constructor
    }

    public static EarningsFragment newInstance() {
        EarningsFragment fragment = new EarningsFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(false);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_earnings, container, false);
        ButterKnife.bind(this, rootView);
        setHasOptionsMenu(false);

        utils = new Utils(getActivity());
        progress = new Progress(getActivity());
        helper = new PreferenceHelper(getActivity(), Prefs.PREF_FILE);

        accept = "application/json";
        authorization = "Bearer " + helper.LoadStringPref(Prefs.token, "");

        mLayoutManager = new LinearLayoutManager(getActivity());
        rvearningYears.setLayoutManager(mLayoutManager);
        rvearningYears.setItemAnimator(new DefaultItemAnimator());
        mDocumentAdapter = new EarningsYearAdapter(getActivity(), listData);
        rvearningYears.setAdapter(mDocumentAdapter);
        mDocumentAdapter.setListener(new EarningsYearAdapter.customOnclickListener() {
            @Override
            public void onclick(int adapterPosition, RespGetStatements.DataEntityX.StatementsEntity.DataEntity listhistories) {


                earningID = String.valueOf(listhistories.getId());
                status = String.valueOf(listhistories.getStatus());
                Log.d("Earning", "id :" + listhistories.getId());
                Log.d("Earning", "getUserId :" + listhistories.getUserId());
                Log.d("Earning", "getStatus :" + listhistories.getStatus());
                startActivity(new Intent(getActivity(), EarningDetailsActivity.class)
                        .putExtra(AppConfig.BUNDLE.earningID, earningID)
                        .putExtra(AppConfig.BUNDLE.statusRide, status));

            }
        });

        getEarningsGetStatements(accept, authorization);

        return rootView;
    }

    private void getEarningsGetStatements(String accept, String authorization) {
//        progress.createDialog(false);
//        progress.DialogMessage(getString(R.string.please_wait));
//        progress.showDialog();
        pbar.setVisibility(View.VISIBLE);
        ObjectAnimator anim = ObjectAnimator.ofInt(pbar, "progress", 0, 100);
        anim.setDuration(15000);
        anim.setInterpolator(new DecelerateInterpolator());
        anim.start();


        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);


        final Call<RespGetStatements> respHistoryCall = apiService.GET_STATEMENTS_CALL(accept, authorization);
        respHistoryCall.enqueue(new Callback<RespGetStatements>() {
            @Override
            public void onResponse(Call<RespGetStatements> call, Response<RespGetStatements> response) {
//                progress.hideDialog();
                pbar.setVisibility(View.GONE);

                if (response.body().getMessage().contains("Unauthenticated")) {
                    openAlertUnauthLogout();
                    return;
                }
                if (response.body().getData().getStatements().getData().isEmpty()) {
                    Snackbar.make(rl_earning, "No data available..!!", Snackbar.LENGTH_SHORT).show();

                    tvearning.setText("$ " + "0");
                    tv_total_completed.setText("0");

                    return;
                }

                listData.addAll(response.body().getData().getStatements().getData());
                mDocumentAdapter.notifyDataSetChanged();

                tvearning.setText("$ " + response.body().getData().getTotalEarnings());
                tv_total_completed.setText(response.body().getData().getTotalCompeleted());


            }

            @Override
            public void onFailure(Call<RespGetStatements> call, Throwable t) {
//                progress.hideDialog();
                pbar.setVisibility(View.GONE);

//                Toast.makeText(MainActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }


    private void openAlertUnauthLogout() {
        final Dialog mBottomSheetDialog = new Dialog(getActivity(), R.style.MaterialDialogSheet);
        mBottomSheetDialog.setContentView(R.layout.dialog_unauthorised);
        mBottomSheetDialog.setCancelable(true);
        mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        mBottomSheetDialog.getWindow().setGravity(Gravity.CENTER);
        mBottomSheetDialog.setCanceledOnTouchOutside(true);
        AppCompatTextView btn_ok = mBottomSheetDialog.findViewById(R.id.btn_ok);

        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progress.createDialog(false);
                progress.DialogMessage(getString(R.string.msg_logout));
                progress.showDialog();
                AsyncTask.execute(new Runnable() {
                    @Override
                    public void run() {

                        try {
                            FirebaseInstanceId.getInstance().deleteInstanceId();
                            helper.clearAllPrefs();
                            startActivity(new Intent(getActivity(), LoginActivity.class)
                                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                            progress.hideDialog();
                            getActivity().finish();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        });
        mBottomSheetDialog.show();


    }

    @OnClick({R.id.rv_item_doc})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.rv_item_doc:
//                startActivity(new Intent(getActivity(), EarningDetailsActivity.class));
                break;


        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.main, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                break;
        }
        return true;
    }


}