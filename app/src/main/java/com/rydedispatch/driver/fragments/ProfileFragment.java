package com.rydedispatch.driver.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.rydedispatch.driver.R;
import com.rydedispatch.driver.activity.AddVehicleActivity;
import com.rydedispatch.driver.activity.DocumentActivity;
import com.rydedispatch.driver.activity.ProfileActivity;
import com.rydedispatch.driver.activity.SelectManuallyActivity;
import com.rydedispatch.driver.utils.AppConfig;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ProfileFragment extends Fragment {

    @BindView(R.id.btn_update_profile)
    Button btnUpdateProfile;
    @BindView(R.id.btn_update_document)
    Button btnupdatedocument;
    @BindView(R.id.btn_update_vehicle)
    Button btnupdatevehicle;

    public ProfileFragment() {
    }

    public static ProfileFragment newInstance() {
        ProfileFragment fragment = new ProfileFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(false);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_profile, container, false);
        ButterKnife.bind(this, rootView);
        setHasOptionsMenu(false);

        return rootView;
    }

    @OnClick({R.id.btn_update_profile,R.id.btn_update_document, R.id.btn_update_vehicle})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_update_profile:
                startActivity(new Intent(getActivity(), ProfileActivity.class));
                break;
            case R.id.btn_update_document:
                startActivity(new Intent(getActivity(), DocumentActivity.class));
                break;
            case R.id.btn_update_vehicle:
                startActivity(new Intent(getActivity(), AddVehicleActivity.class)
                        .putExtra(AppConfig.BUNDLE.LOGIN, true));
                break;

        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.main, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                break;
        }
        return true;
    }

    @Override
    public void onResume() {
        super.onResume();
    }
}
