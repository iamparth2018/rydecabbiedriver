package com.rydedispatch.driver.fcm;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.AudioAttributes;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.service.notification.StatusBarNotification;
import android.support.annotation.NonNull;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.rydedispatch.driver.R;
import com.rydedispatch.driver.activity.MainActivity;
import com.rydedispatch.driver.activity.RideConfirmDialogActivity;
import com.rydedispatch.driver.activity.VoiceActivity;
import com.rydedispatch.driver.utils.AppConfig;
import com.rydedispatch.driver.utils.Utils;
import com.twilio.voice.CallInvite;
import com.twilio.voice.CancelledCallInvite;
import com.twilio.voice.MessageListener;
import com.twilio.voice.Voice;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;
import java.util.Random;


public class VoiceFirebaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = VoiceFirebaseMessagingService.class.getSimpleName();
    private NotificationManager notificationManager;
    String notificationId;
    private static final String NOTIFICATION_ID_KEY = "NOTIFICATION_ID";
    private static final String CALL_SID_KEY = "CALL_SID";
    private static final String VOICE_CHANNEL = "default";


    @Override
    public void onCreate() {
        super.onCreate();
        notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
//        Log.d(TAG, "Received onMessageReceived()");
        Log.d(TAG, "Received onMessageReceived()" + remoteMessage.getData());
        Log.d(TAG, "From: " + remoteMessage.getFrom());
        Log.d(TAG, "Remote getData : " + remoteMessage.getData());
//        Log.w(TAG, "Remote getData " + remoteMessage.getData());
//        Log.e(TAG, "Remote getData " + remoteMessage.getData());


        if (remoteMessage.getData().containsValue("new_message")
                || remoteMessage.getData().containsValue("new_message")
                || remoteMessage.getData().equals("new_message")
                || remoteMessage.getData().containsValue("new_message")
                || remoteMessage.getData().containsValue("new_message")) {
            sendNotificationNewMessage(remoteMessage);
            Log.d(TAG, "Received new_message" + remoteMessage.getData());

        } else if (remoteMessage.getData().containsValue("received_bonus")
                || remoteMessage.getData().containsValue("received_bonus")
                || remoteMessage.getData().equals("received_bonus")
                || remoteMessage.getData().containsValue("received_bonus")
                || remoteMessage.getData().containsValue("received_bonus")) {
            sendNotificationBonus(remoteMessage);
        } else if (remoteMessage.getData().containsValue("Newriderequesthasbeenreceived")
                || remoteMessage.getData().containsValue("Newridere<string name=\"no_instructions\">No Instructions Available</string>questhasbeenreceived")
                || remoteMessage.getData().equals("Newriderequesthasbeenreceived")
                || remoteMessage.getData().containsValue("Newriderequesthasbeenreceived") ||
                remoteMessage.getData().containsValue("Newriderequesthasbeenreceived")) {
//            sendNotificationNew(remoteMessage);
            sendNotificationNewInsideData(remoteMessage.getData());
        } else if (remoteMessage.getData().containsValue("new_ride_request")
                || remoteMessage.getData().containsValue("new_ride_request")
                || remoteMessage.getData().equals("new_ride_request")
                || remoteMessage.getData().containsValue("new_ride_request") ||
                remoteMessage.getData().containsValue("new_ride_request")) {
//            sendNotificationNew(remoteMessage);
            sendNotificationNewInsideData(remoteMessage.getData());


        } else if (remoteMessage.getData().containsValue("new_accepted_ride_request")
                || remoteMessage.getData().containsValue("new_accepted_ride_request")
                || remoteMessage.getData().equals("new_accepted_ride_request")
                || remoteMessage.getData().containsValue("new_accepted_ride_request") ||
                remoteMessage.getData().containsValue("new_accepted_ride_request")) {
//            sendNotificationNew(remoteMessage);
            sendNotificationNewInsideData(remoteMessage.getData());
//            sendNotificationRide(remoteMessage.getData());
//            sendNotificationriderequest(remoteMessage.getData());

            Log.d(TAG, "remote data :" + remoteMessage.getData());
        } else if (remoteMessage.getData().containsValue("ride_instruction_updated")
                || remoteMessage.getData().containsValue("ride_instruction_updated")
                || remoteMessage.getData().equals("ride_instruction_updated")
                || remoteMessage.getData().containsValue("ride_instruction_updated") ||
                remoteMessage.getData().containsValue("ride_instruction_updated")) {
//            sendNotificationNew(remoteMessage);
            sendNotificationNewInsideData(remoteMessage.getData());


        } else if (remoteMessage.getData().containsValue("schedule_notification")
                || remoteMessage.getData().containsValue("schedule_notification")
                || remoteMessage.getData().equals("schedule_notification")
                || remoteMessage.getData().containsValue("schedule_notification") ||
                remoteMessage.getData().containsValue("schedule_notification")) {
            sendNotificationSchedule(remoteMessage);
        } else if (remoteMessage.getData().containsValue("Your ride has been cancelled.")
                || remoteMessage.getData().containsValue("Your ride has been cancelled.")
                || remoteMessage.getData().equals("Your ride has been cancelled.")
                || remoteMessage.getData().containsValue("Your ride has been cancelled.")
                || remoteMessage.getData().containsValue("Your ride has been cancelled.")) {
            sendNotificationCancel(remoteMessage);
        } else if (remoteMessage.getData().containsValue("reminder_notification")
                || remoteMessage.getData().containsValue("reminder_notification")
                || remoteMessage.getData().equals("reminder_notification")
                || remoteMessage.getData().containsValue("reminder_notification")) {
//            sendNotificationScheduleNew(remoteMessage.getData());
            sendNotificationScheduleRide(remoteMessage.getData());
        } else if (remoteMessage.getData().containsValue("Yourridehasbeencancelled.") ||
                remoteMessage.getData().containsValue("cancelled_ride_request")) {
            sendNotificationCancel(remoteMessage);
        } else if (remoteMessage.getData().containsValue("pass_ride_request")
                || remoteMessage.getData().containsValue("pass_ride_request")
                || remoteMessage.getData().equals("pass_ride_request")
                || remoteMessage.getData().containsValue("pass_ride_request")
                || remoteMessage.getData().containsValue("pass_ride_request")) {
            sendNotificationPassRide(remoteMessage);

        } else if (remoteMessage.getData().size() > 0) {


            Map<String, String> data = remoteMessage.getData();
            final int notificationId = (int) System.currentTimeMillis();
            Log.d(TAG, "twilio data" + remoteMessage.getData());
            Log.d(TAG, "twilio data" + data);
            Log.d(TAG, "twilio notificationId" + notificationId);

            boolean valid = Voice.handleMessage(remoteMessage.getData(), new MessageListener() {
                @Override
                public void onCallInvite(@NonNull CallInvite callInvite) {
                    final int notificationId = (int) System.currentTimeMillis();
                    VoiceFirebaseMessagingService.this.notify(callInvite, notificationId);
                    VoiceFirebaseMessagingService.this.sendCallInviteToActivity(callInvite, notificationId);

                    Log.d(TAG, "getFrom :" + callInvite.getFrom());
                    Log.d(TAG, "getTo :" + callInvite.getTo());
                }

                @Override
                public void onCancelledCallInvite(@NonNull CancelledCallInvite cancelledCallInvite) {
                    VoiceFirebaseMessagingService.this.cancelNotification(cancelledCallInvite);
                    VoiceFirebaseMessagingService.this.sendCancelledCallInviteToActivity(
                            cancelledCallInvite);
                }

            });

        }


//        Log.w(TAG, "From: " + remoteMessage.getFrom());

    }

    private void sendNotificationriderequest(Map<String, String> data) {
        Log.d(TAG, "new_accepted_ride_request : " + data);

        String title = data.get("title");
        String main_title = data.get("main_title");
        String nestedBody = data.get("body");

        Log.e(TAG, "sendNotification body : " + " " + nestedBody);
        Log.e(TAG, "sendNotification title : " + " " + title);
        Log.e(TAG, "sendNotification main_title : " + " " + main_title);

        Intent intent = new Intent(getApplicationContext(), RideConfirmDialogActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

        if(main_title.equalsIgnoreCase("LetsTow_offer")){
            try {
                if(data.get("data")!=null){
                    JSONObject jsonObject = new JSONObject(data.get("data"));
                    String requestID = jsonObject.getString("request_id");
                    String user_id = jsonObject.getString("user_id");
                    String driver_id = jsonObject.getString("driver_id");
                    String passenger_first_name = jsonObject.getString("passenger_first_name");
                    String passenger_last_name = jsonObject.getString("passenger_last_name");
                    String source_location = jsonObject.getString("source_location");
                    String source_latitude = jsonObject.getString("source_latitude");
                    String source_longitude = jsonObject.getString("source_longitude");
                    String destination_location = jsonObject.getString("destination_location");
                    String destination_latitude = jsonObject.getString("destination_latitude");
                    String destination_longitude = jsonObject.getString("destination_longitude");
                    String ride_instruction = jsonObject.getString("ride_instruction");
                    String payment_type = jsonObject.getString("payment_type");
                    String user_image = jsonObject.getString("user_image");
                    String eta = jsonObject.getString("eta");
                    String est_fare = jsonObject.getString("est_fare");
                    String rating = jsonObject.getString("rating");
                    String is_schedule = jsonObject.getString("is_schedule");
                    String schedule_date = jsonObject.getString("schedule_date");
                    String display_passenger_name = jsonObject.getString("display_passenger_name");
                    String passenger_phone_number = jsonObject.getString("passenger_phone_number");
                    String status = jsonObject.getString("status");
                    intent = new Intent(this, RideConfirmDialogActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra(AppConfig.BUNDLE.request_id, requestID);
                    intent.putExtra(AppConfig.BUNDLE.user_id, user_id);
                    intent.putExtra(AppConfig.BUNDLE.driver_id, driver_id);
                    intent.putExtra(AppConfig.BUNDLE.passenger_first_name, passenger_first_name);
                    intent.putExtra(AppConfig.BUNDLE.passenger_last_name, passenger_last_name);
                    intent.putExtra(AppConfig.BUNDLE.source_location, source_location);
                    intent.putExtra(AppConfig.BUNDLE.source_latitude, source_latitude);
                    intent.putExtra(AppConfig.BUNDLE.source_longitude, source_longitude);
                    intent.putExtra(AppConfig.BUNDLE.destination_location, destination_location);
                    intent.putExtra(AppConfig.BUNDLE.destination_latitude, destination_latitude);
                    intent.putExtra(AppConfig.BUNDLE.destination_longitude, destination_longitude);
                    intent.putExtra(AppConfig.BUNDLE.ride_instruction, ride_instruction);
                    intent.putExtra(AppConfig.BUNDLE.payment_type, payment_type);
                    intent.putExtra(AppConfig.BUNDLE.user_image, user_image);
                    intent.putExtra(AppConfig.BUNDLE.eta, eta);
                    intent.putExtra(AppConfig.BUNDLE.est_fare, est_fare);
                    intent.putExtra(AppConfig.BUNDLE.rating, rating);
                    intent.putExtra(AppConfig.BUNDLE.is_schedule, is_schedule);
                    intent.putExtra(AppConfig.BUNDLE.schedule_date, schedule_date);
                    intent.putExtra(AppConfig.BUNDLE.display_passenger_name, display_passenger_name);
                    intent.putExtra(AppConfig.BUNDLE.passenger_phone_number, passenger_phone_number);
                    intent.putExtra(AppConfig.BUNDLE.status, status);
                    startActivity(intent);
                }else{
                    intent = new Intent(this, MainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        createNotification(intent,title,nestedBody);
    }

    private void createNotification(Intent intent, String title, String nestedBody){
        playSound();


        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_ONE_SHOT);

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        String NOTIFICATION_CHANNEL_ID = "101";

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            @SuppressLint("WrongConstant")
            NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, "Notification", NotificationManager.IMPORTANCE_MAX);

            notificationChannel.setDescription("Game Notifications");
            notificationChannel.enableLights(true);
            notificationChannel.setVibrationPattern(new long[]{0, 1000, 500, 1000});
            notificationChannel.setLightColor(getColor(R.color.colorPrimary));
            notificationChannel.enableVibration(true);
            notificationManager.createNotificationChannel(notificationChannel);
        }

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_notification)
                .setContentTitle(title)
                .setAutoCancel(true)
                .setOngoing(false)
                .setColor(getResources().getColor(R.color.colorPrimary))
                .setContentText(nestedBody)
                .setContentIntent(pendingIntent)
                .setWhen(System.currentTimeMillis())
                .setPriority(Notification.PRIORITY_MAX);

        notificationManager.notify(1, notificationBuilder.build());
    }

    private void sendNotificationRide(Map<String, String> data) {
        String title = data.get("title");
        String main_title = data.get("main_title");
        String nestedBody = data.get("body");

        Log.e(TAG, "sendNotification body : " + " " + nestedBody);
        Log.e(TAG, "sendNotification title : " + " " + title);
        Log.e(TAG, "sendNotification main_title : " + " " + main_title);


        String rideReqId = data.get("request_id");
        String user_id = data.get("user_id");
        String driver_id = data.get("driver_id");
        String passenger_first_name = data.get("passenger_first_name");
        String passenger_last_name = data.get("passenger_last_name");
        String source_location = data.get("source_location");
        String source_latitude = data.get("source_latitude");
        String source_longitude = data.get("source_longitude");
        String destination_latitude = data.get("destination_latitude");
        String destination_longitude = data.get("destination_longitude");
        String destination_location = data.get("destination_location");
        String ride_instruction = data.get("ride_instruction");
        String payment_type = data.get("payment_type");
        String user_image = data.get("user_image");
        String eta = data.get("eta");
        String est_fare = data.get("est_fare");
        String rating = data.get("rating");
        String is_schedule = data.get("is_schedule");
        String schedule_date = data.get("schedule_date");
        String display_passenger_name = data.get("display_passenger_name");
        String passenger_phone_number = data.get("passenger_phone_number");
        String status = data.get("status");


        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra(AppConfig.BUNDLE.request_id, rideReqId);
        intent.putExtra(AppConfig.BUNDLE.user_id, user_id);
        intent.putExtra(AppConfig.BUNDLE.driver_id, driver_id);
        intent.putExtra(AppConfig.BUNDLE.passenger_first_name, passenger_first_name);
        intent.putExtra(AppConfig.BUNDLE.passenger_last_name, passenger_last_name);
        intent.putExtra(AppConfig.BUNDLE.source_latitude, source_latitude);
        intent.putExtra(AppConfig.BUNDLE.source_longitude, source_longitude);
        intent.putExtra(AppConfig.BUNDLE.destination_latitude, destination_latitude);
        intent.putExtra(AppConfig.BUNDLE.destination_longitude, destination_longitude);
        intent.putExtra(AppConfig.BUNDLE.destination_longitude, source_location);
        intent.putExtra(AppConfig.BUNDLE.destination_location, destination_location);
        intent.putExtra(AppConfig.BUNDLE.ride_instruction, ride_instruction);
        intent.putExtra(AppConfig.BUNDLE.payment_type, payment_type);
        intent.putExtra(AppConfig.BUNDLE.user_image, user_image);
        intent.putExtra(AppConfig.BUNDLE.eta, eta);
        intent.putExtra(AppConfig.BUNDLE.est_fare, est_fare);
        intent.putExtra(AppConfig.BUNDLE.rating, rating);
        intent.putExtra(AppConfig.BUNDLE.is_schedule, is_schedule);
        intent.putExtra(AppConfig.BUNDLE.schedule_date, schedule_date);
        intent.putExtra(AppConfig.BUNDLE.display_passenger_name, display_passenger_name);
        intent.putExtra(AppConfig.BUNDLE.passenger_phone_number, passenger_phone_number);
        intent.putExtra(AppConfig.BUNDLE.status, status);
        startActivity(intent);

//        Uri defaultSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        playSound();


        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_ONE_SHOT);

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        String NOTIFICATION_CHANNEL_ID = "101";

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            @SuppressLint("WrongConstant")
            NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, "Notification", NotificationManager.IMPORTANCE_MAX);

            notificationChannel.setDescription("Game Notifications");
            notificationChannel.enableLights(true);
            notificationChannel.setVibrationPattern(new long[]{0, 1000, 500, 1000});
            notificationChannel.setLightColor(getColor(R.color.colorPrimary));
            notificationChannel.enableVibration(true);
            notificationManager.createNotificationChannel(notificationChannel);
        }

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_notification)
                .setContentTitle(title)
                .setAutoCancel(true)
                .setOngoing(false)
                .setColor(getResources().getColor(R.color.colorPrimary))
                .setContentText(nestedBody)
                .setContentIntent(pendingIntent)
                .setWhen(System.currentTimeMillis())
                .setPriority(Notification.PRIORITY_MAX);

        notificationManager.notify(1, notificationBuilder.build());

    }

    private void sendNotificationBonus(RemoteMessage data) {
        String notificationbody = "Received Bonus";
        String body;

        String title = data.getData().get("title");
        String main_title = data.getData().get("main_title");

        body = data.getData().get(AppConfig.EXTRA.body);

        String id = getString(R.string.default_notification_channel_id);

        NotificationCompat.Builder builder;

        Log.d(TAG, "sender Broadcasting message");
        Intent intent1 = new Intent("custom-event-name");
        // You can also include some extra data.
        intent1.putExtra("message", "This is my message!");
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent1);
        Intent intent = new Intent(this, MainActivity.class)
                .putExtra(AppConfig.EXTRA.body, body);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);


        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent,
                PendingIntent.FLAG_ONE_SHOT);


        Log.d(TAG, "final new msg body :" + body);
        Log.d(TAG, "final new msg title :" + title);
        Log.d(TAG, "final new msg main_title :" + main_title);

        Utils.Log(TAG, "sendNotification: ");
        String channelId = getString(R.string.default_notification_channel_id);
        NotificationCompat.BigTextStyle bigTextStyle = new NotificationCompat.BigTextStyle();
        bigTextStyle.setBigContentTitle(title);
        bigTextStyle.bigText(body);

        builder = new NotificationCompat.Builder(this, id);
        builder.setContentTitle(title)  // required
                .setSmallIcon(R.mipmap.ic_launcher) // required
                .setContentText(notificationbody)  // required
                .setDefaults(Notification.DEFAULT_ALL)
                .setAutoCancel(true)
                .setPriority(NotificationCompat.PRIORITY_MAX)
                .setContentIntent(pendingIntent)
                .setTicker(body)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        Utils.Log(TAG, "sendNotification: ");
        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            NotificationChannel channel = new NotificationChannel(channelId,
                    "Ryde Driver",
                    NotificationManager.IMPORTANCE_DEFAULT);
            channel.setShowBadge(true);
            channel.enableLights(true);
            channel.enableVibration(true);
            notificationManager.createNotificationChannel(channel);
        }
        Utils.Log(TAG, "sendNotification: ");
        notificationManager.notify(new Random().nextInt(), builder.build());
    }

    private void sendCallInviteToActivity(CallInvite callInvite, int notificationId) {
        Intent intent = new Intent(this, VoiceActivity.class);
        intent.setAction(VoiceActivity.ACTION_INCOMING_CALL);
        intent.putExtra(VoiceActivity.INCOMING_CALL_NOTIFICATION_ID, notificationId);
        intent.putExtra(VoiceActivity.INCOMING_CALL_INVITE, callInvite);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        this.startActivity(intent);
    }


    private void cancelNotification(CancelledCallInvite cancelledCallInvite) {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {

            StatusBarNotification[] activeNotifications =
                    notificationManager.getActiveNotifications();
            for (StatusBarNotification statusBarNotification : activeNotifications) {
                Notification notification = statusBarNotification.getNotification();
                Bundle extras = notification.extras;
                String notificationCallSid = extras.getString(CALL_SID_KEY);

                if (cancelledCallInvite.getCallSid().equals(notificationCallSid)) {
                    notificationManager.cancel(extras.getInt(NOTIFICATION_ID_KEY));
                }
            }
        } else {

            notificationManager.cancelAll();
        }
    }

    private void sendCancelledCallInviteToActivity(CancelledCallInvite cancelledCallInvite) {
        Intent intent = new Intent(VoiceActivity.ACTION_CANCEL_CALL);
        intent.putExtra(VoiceActivity.CANCELLED_CALL_INVITE, cancelledCallInvite);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }


    private void sendNotificationScheduleRide(Map<String, String> data) {
        String title = data.get("title");
        String main_title = data.get("main_title");
        String nestedBody = data.get("body");


        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);


        if (main_title.equalsIgnoreCase("new_ride_request")) {
            intent = new Intent(this, MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }

//        Uri defaultSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        playSound();


        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_ONE_SHOT);

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        String NOTIFICATION_CHANNEL_ID = "101";

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            @SuppressLint("WrongConstant")
            NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, "Notification", NotificationManager.IMPORTANCE_MAX);

            notificationChannel.setDescription("Game Notifications");
            notificationChannel.enableLights(true);
            notificationChannel.setVibrationPattern(new long[]{0, 1000, 500, 1000});
            notificationChannel.setLightColor(getColor(R.color.colorPrimary));
            notificationChannel.enableVibration(true);
            notificationManager.createNotificationChannel(notificationChannel);
        }

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_notification)
                .setContentTitle(title)
                .setAutoCancel(true)
                .setOngoing(false)
                .setColor(getResources().getColor(R.color.colorPrimary))
                .setContentText(nestedBody)
                .setContentIntent(pendingIntent)
                .setWhen(System.currentTimeMillis())
                .setPriority(Notification.PRIORITY_MAX);

        notificationManager.notify(1, notificationBuilder.build());
    }


    private void sendNotificationScheduleNew(Map<String, String> data) {

//        body = String.valueOf(data.get("body"));
//        data1 = String.valueOf(data.get("title"));
//        main_title = String.valueOf(data.get("main_title"));
        String title = data.get("title");
        String main_title = data.get("main_title");
        String body = data.get("body");

        String name = "my_package_channel";
        String id = getString(R.string.default_notification_channel_id); // The user-visible name of the channel.
        String description = "my_package_first_channel"; // The user-visible description of the channel.

        Intent intent;
        PendingIntent pendingIntent;
        NotificationCompat.Builder builder;


        if (notificationManager == null) {
            notificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel mChannel = notificationManager.getNotificationChannel(id);
            if (mChannel == null) {
                mChannel = new NotificationChannel(id, name, importance);
                mChannel.setDescription(description);
                mChannel.enableVibration(true);
                mChannel.setLightColor(Color.GREEN);
                mChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
                notificationManager.createNotificationChannel(mChannel);
            }
            builder = new NotificationCompat.Builder(this, id);

            intent = new Intent(this, MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);

            builder.setContentTitle(title)  // required
                    .setSmallIcon(R.mipmap.ic_launcher) // required
                    .setContentText(getString(R.string.app_name))  // required
                    .setDefaults(Notification.DEFAULT_ALL)
                    .setAutoCancel(true)
                    .setPriority(NotificationCompat.PRIORITY_MAX)
                    .setContentIntent(pendingIntent)
                    .setTicker(body)
                    .setVibrate(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});

//            playSound();

        } else {

            builder = new NotificationCompat.Builder(this);
            intent = new Intent(this, MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);
            builder.setContentTitle(title)                           // required
                    .setSmallIcon(R.mipmap.ic_launcher) // required
                    .setContentIntent(pendingIntent)
                    .setTicker(title)
                    .setVibrate(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400})
                    .setPriority(Notification.PRIORITY_HIGH)
                    .setPriority(NotificationCompat.PRIORITY_MAX)
                    .setDefaults(Notification.DEFAULT_ALL)
                    .setCategory(Notification.CATEGORY_CALL)
                    .setOngoing(false)
                    .setAutoCancel(true)
                    .setWhen(System.currentTimeMillis())
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setPriority(NotificationCompat.PRIORITY_HIGH)
                    .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                    .setContentTitle(title)
                    .setContentText(body)
                    .setFullScreenIntent(pendingIntent, true);
//            playSound();
        } // else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

        Notification notification = builder.build();
        notificationManager.notify(new Random().nextInt(), notification);
    }

    private void sendNotificationNewMessage(RemoteMessage data) {

        String title = data.getData().get("title");
        String main_title = data.getData().get("main_title");
        String body = data.getData().get("body");


        String id = getString(R.string.default_notification_channel_id);

        NotificationCompat.Builder builder;

        Log.d(TAG, "sender Broadcasting message");
        Intent intent1 = new Intent("custom-event-name-chat");
        // You can also include some extra data.
        intent1.putExtra("message", "This is my message!");
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent1);
        Intent intent = new Intent(this, MainActivity.class)
                .putExtra(AppConfig.EXTRA.body, body);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

//        Uri defaultSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        playSound();

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent,
                PendingIntent.FLAG_ONE_SHOT);


        Log.d(TAG, "final new msg body :" + body);
        Log.d(TAG, "final new msg title :" + title);
        Log.d(TAG, "final new msg main_title :" + main_title);

        Utils.Log(TAG, "sendNotification: ");
        String channelId = getString(R.string.default_notification_channel_id);
        NotificationCompat.BigTextStyle bigTextStyle = new NotificationCompat.BigTextStyle();
        bigTextStyle.setBigContentTitle(title);
        bigTextStyle.bigText(body);


        builder = new NotificationCompat.Builder(this, id);
        builder.setContentTitle(title)  // required
                .setSmallIcon(R.mipmap.ic_launcher) // required
                .setContentText(body)  // required
//                .setDefaults(Notification.DEFAULT_ALL)
                .setAutoCancel(true)
//                .setPriority(NotificationCompat.PRIORITY_MAX)
                .setPriority(Notification.PRIORITY_MAX)
                .setContentIntent(pendingIntent)
                .setTicker(body)
//                .setSound(defaultSound)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        Utils.Log(TAG, "sendNotification: ");
        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            NotificationChannel channel = new NotificationChannel(channelId,
                    "Ryde Driver",
                    NotificationManager.IMPORTANCE_DEFAULT);
            channel.setShowBadge(true);
            channel.enableLights(true);
            channel.enableVibration(true);
            notificationManager.createNotificationChannel(channel);
        }
        Utils.Log(TAG, "sendNotification: ");
        notificationManager.notify(new Random().nextInt(), builder.build());
    }

    private void sendNotificationSchedule(RemoteMessage data) {
        Log.w(TAG, "Message schedule : " + data.getData());

        String notificationtitle = data.getData().get("title");
        String notificationbody = data.getData().get("body");
        String destination_latitude;
        String payment_type;
        String user_id;
        String destination_longitude;
        String request_id;
        String is_schedule = data.getData().get("is_schedule");
        String schedule_date = data.getData().get("schedule_date");

        String est_fare = data.getData().get("est_fare");
        String source_longitude = data.getData().get(AppConfig.BUNDLE.source_longitude);
        String source_location = data.getData().get(AppConfig.BUNDLE.source_location);
        String main_title = data.getData().get("main_title");
        String source_latitude = data.getData().get(AppConfig.BUNDLE.source_latitude);
        String destination_location = data.getData().get(AppConfig.BUNDLE.destination_location);
        String user_image = data.getData().get(AppConfig.BUNDLE.user_image);
        String eta = data.getData().get("eta");
        String display_passenger_name = data.getData().get("display_passenger_name");

        String body;


        destination_latitude = data.getData().get(AppConfig.BUNDLE.destination_latitude);
        payment_type = data.getData().get(AppConfig.BUNDLE.payment_type);
        user_id = data.getData().get(AppConfig.BUNDLE.user_id);
        destination_longitude = data.getData().get(AppConfig.BUNDLE.destination_longitude);
        request_id = data.getData().get(AppConfig.BUNDLE.request_id);
        String title = data.getData().get("title");

        body = data.getData().get(AppConfig.EXTRA.body);

        String id = getString(R.string.default_notification_channel_id);

        NotificationCompat.Builder builder;

        //using local broadcast manager
        Log.d(TAG, "sender Broadcasting message");
        Intent intent1 = new Intent("custom-event-name");
        // You can also include some extra data.
        intent1.putExtra("message", "Ride_Request!");
        intent1.putExtra(AppConfig.BUNDLE.destination_latitude, destination_latitude);
        intent1.putExtra(AppConfig.BUNDLE.payment_type, payment_type);
        intent1.putExtra(AppConfig.BUNDLE.user_id, user_id);
        intent1.putExtra(AppConfig.BUNDLE.destination_longitude, destination_longitude);
        intent1.putExtra(AppConfig.BUNDLE.request_id, request_id);
        intent1.putExtra(AppConfig.BUNDLE.source_longitude, source_longitude);
        intent1.putExtra(AppConfig.BUNDLE.source_location, source_location);
        intent1.putExtra(AppConfig.BUNDLE.source_latitude, source_latitude);
        intent1.putExtra(AppConfig.BUNDLE.destination_location, destination_location);
        intent1.putExtra(AppConfig.BUNDLE.user_image, user_image);
        intent1.putExtra(AppConfig.BUNDLE.eta, eta);
        intent1.putExtra(AppConfig.BUNDLE.est_fare, est_fare);
        intent1.putExtra(AppConfig.BUNDLE.is_schedule, is_schedule);
        intent1.putExtra(AppConfig.BUNDLE.schedule_date, schedule_date);
        intent1.putExtra(AppConfig.BUNDLE.display_passenger_name, display_passenger_name);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent1);

        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra(AppConfig.BUNDLE.destination_latitude, destination_latitude);
        intent.putExtra(AppConfig.BUNDLE.payment_type, payment_type);
        intent.putExtra(AppConfig.BUNDLE.user_id, user_id);
        intent.putExtra(AppConfig.BUNDLE.destination_longitude, destination_longitude);
        intent.putExtra(AppConfig.BUNDLE.request_id, request_id);
        intent.putExtra(AppConfig.BUNDLE.source_longitude, source_longitude);
        intent.putExtra(AppConfig.BUNDLE.source_location, source_location);
        intent.putExtra(AppConfig.BUNDLE.source_latitude, source_latitude);
        intent.putExtra(AppConfig.BUNDLE.destination_location, destination_location);
        intent.putExtra(AppConfig.BUNDLE.user_image, user_image);
        intent.putExtra(AppConfig.BUNDLE.eta, eta);
        intent.putExtra(AppConfig.BUNDLE.est_fare, est_fare);
        intent1.putExtra(AppConfig.BUNDLE.is_schedule, is_schedule);
        intent1.putExtra(AppConfig.BUNDLE.schedule_date, schedule_date);
        intent1.putExtra(AppConfig.BUNDLE.display_passenger_name, display_passenger_name);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);


        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent,
                PendingIntent.FLAG_ONE_SHOT);

        playSound();

        Utils.Log(TAG, "sendNotification: ");
        String channelId = getString(R.string.default_notification_channel_id);
        NotificationCompat.BigTextStyle bigTextStyle = new NotificationCompat.BigTextStyle();
        bigTextStyle.setBigContentTitle(title);
        bigTextStyle.bigText(body);


        builder = new NotificationCompat.Builder(this, id);
        builder.setContentTitle(notificationtitle)  // required
                .setSmallIcon(R.mipmap.ic_launcher) // required
                .setContentText(notificationbody)  // required
                .setAutoCancel(true)
                .setPriority(NotificationCompat.PRIORITY_MAX)
                .setContentIntent(pendingIntent)
                .setTicker(body)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        Utils.Log(TAG, "sendNotification: ");
        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            NotificationChannel channel = new NotificationChannel(channelId,
                    "Ryde Driver",
                    NotificationManager.IMPORTANCE_DEFAULT);
            channel.setShowBadge(true);
            channel.enableLights(true);
            channel.enableVibration(true);

            notificationManager.createNotificationChannel(channel);
        }
        Utils.Log(TAG, "sendNotification: ");
        notificationManager.notify(new Random().nextInt(), builder.build());


    }

    private void sendNotificationCancel(RemoteMessage data) {
        String notificationtitle = "Rydemate";
        String notificationbody = "Your ride has been cancelled";
        String request_id;

        String body;

        request_id = data.getData().get(AppConfig.BUNDLE.request_id);
        String title = data.getData().get("title");
        String main_title = data.getData().get("main_title");

        body = data.getData().get(AppConfig.EXTRA.body);

        notificationId = getString(R.string.default_notification_channel_id);

        NotificationCompat.Builder builder;

        Log.d(TAG, "sender Broadcasting message");

        Intent intent1 = new Intent("custom-event-name");
        // You can also include some extra data.
        intent1.putExtra("message", "This is my message!");
        intent1.putExtra(AppConfig.EXTRA.body, body);
        intent1.putExtra(AppConfig.BUNDLE.request_id, request_id);
        intent1.putExtra(AppConfig.EXTRA.main_title, main_title);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent1);

        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra(AppConfig.EXTRA.body, body);
        intent.putExtra(AppConfig.EXTRA.main_title, main_title);
        intent.putExtra(AppConfig.BUNDLE.request_id, request_id);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

//        clearNotification();

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent,
                PendingIntent.FLAG_ONE_SHOT);


        Log.d(TAG, "final cancel request_id :" + request_id);
        Log.d(TAG, "final cancel body :" + body);
        Log.d(TAG, "final cancel main_title :" + main_title);

        Utils.Log(TAG, "sendNotification: ");
        String channelId = getString(R.string.default_notification_channel_id);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.BigTextStyle bigTextStyle = new NotificationCompat.BigTextStyle();
        bigTextStyle.setBigContentTitle(title);
        bigTextStyle.bigText(body);


        builder = new NotificationCompat.Builder(this, notificationId);
        builder.setContentTitle(notificationtitle)  // required
                .setSmallIcon(R.mipmap.ic_launcher) // required
                .setContentText(notificationbody)  // required
                .setDefaults(Notification.DEFAULT_ALL)
                .setAutoCancel(true)
                .setPriority(NotificationCompat.PRIORITY_MAX)
                .setContentIntent(pendingIntent)
                .setTicker(body)
//                .setVibrate(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);
//        playSound();

        NotificationManager notifManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        Utils.Log(TAG, "sendNotification: ");
        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            NotificationChannel channel = new NotificationChannel(channelId,
                    "Ryde Driver",
                    NotificationManager.IMPORTANCE_DEFAULT);
            channel.setShowBadge(true);
            channel.enableLights(true);
            channel.enableVibration(true);
            notifManager.createNotificationChannel(channel);
//            playSound();
        }
        Utils.Log(TAG, "sendNotification: ");
        notifManager.notify(new Random().nextInt(), builder.build());

    }

    private void sendNotificationPassRide(RemoteMessage data) {
        String notificationtitle = "Rydemate";
        String notificationbody = "Your ride has been Passed..!!";

        String body;

        String title = data.getData().get("title");
        String main_title = data.getData().get("main_title");

        body = data.getData().get(AppConfig.EXTRA.body);

        notificationId = getString(R.string.default_notification_channel_id);

        NotificationCompat.Builder builder;

        Log.d(TAG, "sender Broadcasting message");

        Intent intent1 = new Intent("custom-event-name");
        // You can also include some extra data.
        intent1.putExtra("message", "This is my message!");
        intent1.putExtra(AppConfig.EXTRA.body, body);
        intent1.putExtra(AppConfig.EXTRA.main_title, main_title);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent1);

        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra(AppConfig.EXTRA.body, body);
        intent.putExtra(AppConfig.EXTRA.main_title, main_title);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

//        clearNotification();

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent,
                PendingIntent.FLAG_ONE_SHOT);


        Log.d(TAG, "final cancel body :" + body);
        Log.d(TAG, "final cancel main_title :" + main_title);

        Utils.Log(TAG, "sendNotification: ");
        String channelId = getString(R.string.default_notification_channel_id);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.BigTextStyle bigTextStyle = new NotificationCompat.BigTextStyle();
        bigTextStyle.setBigContentTitle(title);
        bigTextStyle.bigText(body);


        builder = new NotificationCompat.Builder(this, notificationId);
        builder.setContentTitle(notificationtitle)  // required
                .setSmallIcon(R.mipmap.ic_launcher) // required
                .setContentText(notificationbody)  // required
                .setDefaults(Notification.DEFAULT_ALL)
                .setAutoCancel(true)
                .setPriority(NotificationCompat.PRIORITY_MAX)
                .setContentIntent(pendingIntent)
                .setTicker(body)
//                .setVibrate(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);
//        playSound();

        NotificationManager notifManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        Utils.Log(TAG, "sendNotification: ");
        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            NotificationChannel channel = new NotificationChannel(channelId,
                    "Ryde Driver",
                    NotificationManager.IMPORTANCE_DEFAULT);
            channel.setShowBadge(true);
            channel.enableLights(true);
            channel.enableVibration(true);
            notifManager.createNotificationChannel(channel);
//            playSound();
        }
        Utils.Log(TAG, "sendNotification: ");
        notifManager.notify(new Random().nextInt(), builder.build());
    }


    private void sendNotificationNewInsideData(Map<String, String> data) {


        String title = data.get("title");
        String main_title = data.get("main_title");
        String nestedBody = data.get("body");
        String rideReqId = data.get("request_id");
        String source_latitude = data.get("source_latitude");
        String source_longitude = data.get("source_longitude");
        String destination_latitude = data.get("destination_latitude");
        String destination_longitude = data.get("destination_longitude");


        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("request_id", rideReqId);
        intent.putExtra("source_latitude", source_latitude);
        intent.putExtra("source_longitude", source_longitude);
        intent.putExtra("destination_latitude", destination_latitude);
        intent.putExtra("destination_longitude", destination_longitude);
        startActivity(intent);

//        Uri defaultSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        playSound();


        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_ONE_SHOT);

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        String NOTIFICATION_CHANNEL_ID = "101";

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            @SuppressLint("WrongConstant")
            NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, "Notification", NotificationManager.IMPORTANCE_MAX);

            notificationChannel.setDescription("Game Notifications");
            notificationChannel.enableLights(true);
            notificationChannel.setVibrationPattern(new long[]{0, 1000, 500, 1000});
            notificationChannel.setLightColor(getColor(R.color.colorPrimary));
            notificationChannel.enableVibration(true);
            notificationManager.createNotificationChannel(notificationChannel);
        }

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_notification)
                .setContentTitle(title)
                .setAutoCancel(true)
                .setOngoing(false)
                .setColor(getResources().getColor(R.color.colorPrimary))
                .setContentText(nestedBody)
                .setContentIntent(pendingIntent)
                .setWhen(System.currentTimeMillis())
                .setPriority(Notification.PRIORITY_MAX);

        notificationManager.notify(1, notificationBuilder.build());

    }


    private void sendNotificationNew(RemoteMessage data) {
        String notificationtitle = "Rydemate";
        String notificationbody = "New ride request has been received";
        String body;

        String request_id = data.getData().get("request_id");
        String user_id = data.getData().get("user_id");
        String driver_id = data.getData().get("driver_id");
        String is_schedule = data.getData().get("is_schedule");
        String schedule_date = data.getData().get("schedule_date");

        String est_fare = data.getData().get("est_fare");
        String ride_instruction = data.getData().get("ride_instruction");
        String passenger_unique_code = data.getData().get("passenger_unique_code");
        String source_longitude = data.getData().get(AppConfig.BUNDLE.source_longitude);
        String source_location = data.getData().get(AppConfig.BUNDLE.source_location);
        String main_title = data.getData().get("main_title");
        String source_latitude = data.getData().get(AppConfig.BUNDLE.source_latitude);
        String destination_location = data.getData().get(AppConfig.BUNDLE.destination_location);
        String user_image = data.getData().get(AppConfig.BUNDLE.user_image);
        String eta = data.getData().get("eta");
        String display_passenger_name = data.getData().get("display_passenger_name");
        String passenger_phone_number = data.getData().get("passenger_phone_number");
        String destination_latitude = data.getData().get("destination_latitude");
        String payment_type = data.getData().get("payment_type");


        String destination_longitude = data.getData().get(AppConfig.BUNDLE.destination_longitude);
        String title = data.getData().get("title");
        String passenger_first_name = data.getData().get("passenger_first_name");
        String rating = data.getData().get("rating");
        String status = data.getData().get("status");

        body = data.getData().get(AppConfig.EXTRA.body);

        String id = getString(R.string.default_notification_channel_id);

        NotificationCompat.Builder builder;

        //using local broadcast manager
        Log.d(TAG, "sender Broadcasting message");
        Intent intent1 = new Intent("custom-event-name");
        // You can also include some extra data.
        intent1.putExtra("message", "Ride_Request!");
        intent1.putExtra(AppConfig.BUNDLE.destination_latitude, destination_latitude);
        intent1.putExtra(AppConfig.BUNDLE.ride_instruction, ride_instruction);
        intent1.putExtra(AppConfig.BUNDLE.payment_type, payment_type);
        intent1.putExtra(AppConfig.BUNDLE.user_id, user_id);
        intent1.putExtra(AppConfig.BUNDLE.destination_longitude, destination_longitude);
        intent1.putExtra(AppConfig.BUNDLE.request_id, request_id);
        intent1.putExtra(AppConfig.BUNDLE.source_longitude, source_longitude);
        intent1.putExtra(AppConfig.BUNDLE.source_location, source_location);
        intent1.putExtra(AppConfig.BUNDLE.source_latitude, source_latitude);
        intent1.putExtra(AppConfig.BUNDLE.destination_location, destination_location);
        intent1.putExtra(AppConfig.BUNDLE.user_image, user_image);
        intent1.putExtra(AppConfig.BUNDLE.eta, eta);
        intent1.putExtra(AppConfig.BUNDLE.est_fare, est_fare);
        intent1.putExtra(AppConfig.BUNDLE.is_schedule, is_schedule);
        intent1.putExtra(AppConfig.BUNDLE.schedule_date, schedule_date);
        intent1.putExtra(AppConfig.BUNDLE.passenger_first_name, passenger_first_name);
        intent1.putExtra(AppConfig.BUNDLE.display_passenger_name, display_passenger_name);
        intent1.putExtra(AppConfig.BUNDLE.passenger_rating, rating);
        intent1.putExtra(AppConfig.BUNDLE.passenger_unique_code, passenger_unique_code);
        intent1.putExtra(AppConfig.BUNDLE.status, status);
        intent1.putExtra(AppConfig.BUNDLE.driver_id, driver_id);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent1);

        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra(AppConfig.BUNDLE.destination_latitude, destination_latitude);
        intent.putExtra(AppConfig.BUNDLE.payment_type, payment_type);
        intent.putExtra(AppConfig.BUNDLE.user_id, user_id);
        intent.putExtra(AppConfig.BUNDLE.destination_longitude, destination_longitude);
        intent.putExtra(AppConfig.BUNDLE.request_id, request_id);
        intent.putExtra(AppConfig.BUNDLE.source_longitude, source_longitude);
        intent.putExtra(AppConfig.BUNDLE.source_location, source_location);
        intent.putExtra(AppConfig.BUNDLE.source_latitude, source_latitude);
        intent.putExtra(AppConfig.BUNDLE.destination_location, destination_location);
        intent.putExtra(AppConfig.BUNDLE.user_image, user_image);
        intent.putExtra(AppConfig.BUNDLE.eta, eta);
        intent1.putExtra(AppConfig.BUNDLE.ride_instruction, ride_instruction);
        intent.putExtra(AppConfig.BUNDLE.est_fare, est_fare);
        intent.putExtra(AppConfig.BUNDLE.is_schedule, is_schedule);
        intent.putExtra(AppConfig.BUNDLE.schedule_date, schedule_date);
        intent1.putExtra(AppConfig.BUNDLE.passenger_first_name, passenger_first_name);
        intent1.putExtra(AppConfig.BUNDLE.display_passenger_name, display_passenger_name);
        intent1.putExtra(AppConfig.BUNDLE.passenger_rating, rating);
        intent1.putExtra(AppConfig.BUNDLE.passenger_unique_code, passenger_unique_code);
        intent1.putExtra(AppConfig.BUNDLE.status, status);
        intent1.putExtra(AppConfig.BUNDLE.driver_id, driver_id);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);


        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Utils.Log(TAG, "sendNotification: ");
        String channelId = getString(R.string.default_notification_channel_id);
//        Uri sound = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + getPackageName() + "/" + R.raw.ride_notificaition);
//        Uri defaultSoundUri = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.ride_notificaition);
        NotificationCompat.BigTextStyle bigTextStyle = new NotificationCompat.BigTextStyle();
        bigTextStyle.setBigContentTitle(title);
        bigTextStyle.bigText(body);


        builder = new NotificationCompat.Builder(this, id);
        builder.setContentTitle(notificationtitle)  // required
                .setSmallIcon(R.mipmap.ic_launcher) // required
                .setContentText(notificationbody)  // required
                .setAutoCancel(true)
//                .setPriority(NotificationCompat.PRIORITY_MAX)
                .setPriority(Notification.PRIORITY_MAX)
                .setContentIntent(pendingIntent)
                .setTicker(body)

//                .setVibrate(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
//                .setSound(Uri.parse("android.resource://" + getApplicationContext().getPackageName() + "/" + R.raw.ride_notificaition))
//                .setSound(sound)
                .setContentIntent(pendingIntent);


//        playSound();

        NotificationManager notifManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        Utils.Log(TAG, "sendNotification: ");
        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            AudioAttributes attributes = new AudioAttributes.Builder()
                    .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                    .setUsage(AudioAttributes.USAGE_ALARM)
                    .build();

            NotificationChannel channel = new NotificationChannel(channelId,
                    "Ryde Driver",
                    NotificationManager.IMPORTANCE_HIGH);


            channel.setShowBadge(true);
            channel.enableLights(true);
            channel.enableVibration(true);
//            channel.setSound(sound, attributes);
            notifManager.createNotificationChannel(channel);

//            playSound();
        }
        Utils.Log(TAG, "sendNotification: ");
        notifManager.notify(new Random().nextInt(), builder.build());


    }


    private void cancelSound() {
        try {
            Uri alarmSound = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE
                    + "://" + getApplicationContext().getPackageName() + "/raw/ride_notificaition");
            Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), alarmSound);
            r.stop();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void playSound() {

        try {
            Uri alarmSound = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE
                    + "://" + getApplicationContext().getPackageName() + "/raw/ride_notificaition");
            Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), alarmSound);
            r.play();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void notify(CallInvite callInvite, int notificationId) {

        Intent intent = new Intent(this, VoiceActivity.class);
        intent.setAction(VoiceActivity.ACTION_INCOMING_CALL);
        intent.putExtra(VoiceActivity.INCOMING_CALL_NOTIFICATION_ID, notificationId);
        intent.putExtra(VoiceActivity.INCOMING_CALL_INVITE, callInvite);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent =
                PendingIntent.getActivity(this, notificationId, intent, PendingIntent.FLAG_CANCEL_CURRENT);
        /*
         * Pass the notification id and call sid to use as an identifier to cancel the
         * notification later
         */
        Bundle extras = new Bundle();
        extras.putInt(NOTIFICATION_ID_KEY, notificationId);
        extras.putString(CALL_SID_KEY, callInvite.getCallSid());

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel callInviteChannel = new NotificationChannel(VOICE_CHANNEL,
                    "Primary Voice Channel", NotificationManager.IMPORTANCE_DEFAULT);
            callInviteChannel.setLightColor(Color.GREEN);
            callInviteChannel.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
            notificationManager.createNotificationChannel(callInviteChannel);

            Notification notification =
                    buildNotification(callInvite.getFrom() + " is calling.",
                            pendingIntent,
                            extras);
            notificationManager.notify(notificationId, notification);
        } else {
            NotificationCompat.Builder notificationBuilder =
                    new NotificationCompat.Builder(this)
                            .setSmallIcon(R.drawable.ic_call_end)
                            .setContentTitle(getString(R.string.app_name))
                            .setContentText(callInvite.getFrom() + " is calling.")
                            .setAutoCancel(true)
                            .setExtras(extras)
                            .setContentIntent(pendingIntent)
                            .setGroup("test_app_notification")
                            .setColor(Color.rgb(214, 10, 37));

            notificationManager.notify(notificationId, notificationBuilder.build());
        }
    }

    @Override
    public void onNewToken(String token) {
        Log.w(TAG, "Refreshed token: " + token);
        Log.d(TAG, "Broadcasting message");
    }

    @TargetApi(Build.VERSION_CODES.O)
    public Notification buildNotification(String text, PendingIntent pendingIntent, Bundle extras) {
        return new Notification.Builder(getApplicationContext(), VOICE_CHANNEL)
                .setSmallIcon(R.drawable.ic_call_end)
                .setContentTitle(getString(R.string.app_name))
                .setContentText(text)
                .setContentIntent(pendingIntent)
                .setExtras(extras)
                .setAutoCancel(true)
                .build();
    }

    private void sendNotification(Map<String, String> data) {

        String request_id = data.get("request_id");
        String destination_latitude = data.get("destination_latitude");
        String payment_type = data.get("payment_type");
        String user_id = data.get("user_id");
        String body = data.get("body");
        String title = data.get("title");

        Intent intent;
        PendingIntent pendingIntent;
        NotificationCompat.Builder builder;

        String data1 = data.get("title");

        String name = "my_package_channel";
        String id = getString(R.string.default_notification_channel_id); // The user-visible name of the channel.
        String description = "my_package_first_channel"; // The user-visible description of the channel.

        Log.d(TAG, "remote data post :" + request_id);

        if (notificationManager == null) {
            notificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel mChannel = notificationManager.getNotificationChannel(id);
            if (mChannel == null) {
                mChannel = new NotificationChannel(id, name, importance);
                mChannel.setDescription(description);
                mChannel.enableVibration(true);
                mChannel.setLightColor(Color.GREEN);
                mChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
                notificationManager.createNotificationChannel(mChannel);
            }
            builder = new NotificationCompat.Builder(this, id);

            intent = new Intent(this, MainActivity.class)
                    .putExtra(AppConfig.BUNDLE.request_id, request_id);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);

            builder.setContentTitle(data1)  // required
                    .setSmallIcon(R.mipmap.ic_launcher) // required
                    .setContentText(this.getString(R.string.app_name))  // required
                    .setDefaults(Notification.DEFAULT_ALL)
                    .setAutoCancel(true)
                    .setPriority(NotificationCompat.PRIORITY_HIGH)
                    .setContentIntent(pendingIntent)
                    .setTicker(data1)
                    .setVibrate(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
        } else {

            builder = new NotificationCompat.Builder(this);

            intent = new Intent(this, MainActivity.class)
                    .putExtra(AppConfig.BUNDLE.request_id, request_id);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);

            builder.setContentTitle(data1)                           // required
                    .setSmallIcon(R.mipmap.ic_launcher) // required
                    .setContentIntent(pendingIntent)
                    .setTicker(data1)
                    .setVibrate(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400})
                    .setPriority(Notification.PRIORITY_HIGH)
                    .setPriority(NotificationCompat.PRIORITY_MAX)
                    .setDefaults(Notification.DEFAULT_ALL)
                    .setCategory(Notification.CATEGORY_CALL)
                    .setOngoing(false)
                    .setAutoCancel(true)
                    .setWhen(System.currentTimeMillis())
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setPriority(NotificationCompat.PRIORITY_HIGH)
                    .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                    .setContentTitle(data1)
//                    .setContentText(body)
                    .setContentText(data1)
                    .setFullScreenIntent(pendingIntent, true);
        } // else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

        Notification notification = builder.build();
        notificationManager.notify(new Random().nextInt(), notification);
    }
}
