package com.rydedispatch.driver.adapter;

import android.app.Dialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.rydedispatch.driver.R;
import com.rydedispatch.driver.activity.HistoryDetails;
import com.rydedispatch.driver.response.RespHistory;
import com.rydedispatch.driver.utils.AppConfig;
import com.rydedispatch.driver.utils.PreferenceHelper;
import com.rydedispatch.driver.utils.Prefs;
import com.rydedispatch.driver.utils.Progress;
import com.rydedispatch.driver.utils.Utils;
import com.squareup.picasso.Picasso;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.MyViewHolder> {
    private static final String TAG = "HistoryAdapter";
    private final int VIEW_TYPE_ITEM = 0;
    private FragmentActivity mContext;
    private List<RespHistory.DataEntityX.HistoryEntity.DataEntity> lists;
    private Utils utils;
    private Progress progress;
    private PreferenceHelper helper;
    public customOnclickListener listener;
    public callListener calllistener;
    public naviListener navilistener;
    public instructionlistener instructionlistener;
    private String rideinstruction = "";

    public interface instructionlistener {
        void instruction(int adapterPosition, RespHistory.DataEntityX.HistoryEntity.DataEntity listhistories);
    }

    public interface naviListener {
        void navigate(int adapterPosition, RespHistory.DataEntityX.HistoryEntity.DataEntity listhistories);
    }

    public interface callListener {
        void oncall(int adapterPosition, RespHistory.DataEntityX.HistoryEntity.DataEntity listhistories);
    }

    public interface customOnclickListener {
        void onclick(int adapterPosition, RespHistory.DataEntityX.HistoryEntity.DataEntity listhistories);
    }

    public void setNavilistener(naviListener navilistener) {
        this.navilistener = navilistener;
    }

    public void setInstructionlistener(instructionlistener instructionlistener) {
        this.instructionlistener = instructionlistener;
    }

    public void setCalllistener(callListener calllistener) {
        this.calllistener = calllistener;
    }

    public void setListener(customOnclickListener listener) {
        this.listener = listener;
    }

    public HistoryAdapter(FragmentActivity activity, List<RespHistory.DataEntityX.HistoryEntity.DataEntity> listData) {
        this.lists = listData;
        this.mContext = activity;

        utils = new Utils(mContext);
        progress = new Progress(mContext);
        helper = new PreferenceHelper(mContext, Prefs.PREF_FILE);

    }


    @NonNull
    @Override
    public HistoryAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.row_history, parent, false);
            return new MyViewHolder(itemView);
        } else {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_loading, parent, false);
            return new MyViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull HistoryAdapter.MyViewHolder holder, int position) {
        final RespHistory.DataEntityX.HistoryEntity.DataEntity entity = lists.get(position);

        holder.tv_name.setText(entity.getFirstName());
        holder.tv_source_location.setText(entity.getSourceLocation());
        holder.tv_destination_location.setText(entity.getDestinationLocation());

        if (entity.getStatus().contains("_")) {
            holder.tv_status.setText(entity.getStatus().replace("_", " ").replace("_", " "));
        }
        else {
            holder.tv_status.setText(entity.getStatus());
        }


        if (entity.getStatus().equalsIgnoreCase("cancelled_by_driver")
                || entity.getStatus().equalsIgnoreCase("cancelled")
                || entity.getStatus().equalsIgnoreCase("no_show")) {
            holder.tv_status.setTextColor(mContext.getResources().getColor(R.color.red_background));
        }
        else {
            holder.tv_status.setTextColor(mContext.getResources().getColor(R.color.green));
        }

        if (entity.getStatus().equalsIgnoreCase("inprogress")
                || entity.getStatus().equalsIgnoreCase("pending")) {
            holder.ll_navigate_schedule.setVisibility(View.VISIBLE);
            holder.ll_call_schedule.setVisibility(View.VISIBLE);
        }
        else {
            holder.ll_navigate_schedule.setVisibility(View.INVISIBLE);
            holder.ll_call_schedule.setVisibility(View.INVISIBLE);
        }

        if (entity.getTotalCost() == null) {
            holder.tv_total_cost.setText("$ " + "0");

        }
        else {
            holder.tv_total_cost.setText("$ " +entity.getTotalCost());

        }


        if (entity.getScheduleDate()==null) {
            holder.tv_completed_time.setText(entity.getCompletedTime());
        }
        else {
            holder.tv_completed_time.setText(entity.getScheduleDate());

        }



        if (entity.getSub_user_id()>0) {

            holder.tv_display_name.setVisibility(View.VISIBLE);
            holder.tv_display_name.setText(entity.getDisplay_passenger_name());

        }
        else {
            holder.tv_display_name.setVisibility(View.INVISIBLE);

        }


        Picasso.with(mContext)
                .load(entity.getProfilePicture())
                .placeholder(R.mipmap.ic_launcher)
                .error(R.mipmap.ic_launcher)
                .into(holder.profile_image);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onclick(holder.getAdapterPosition(), entity);
            }
        });

        holder.ll_call_schedule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calllistener.oncall(holder.getAdapterPosition(), entity);
            }
        });

        Log.d(TAG, "is schedule :" + entity.getIsSchedule());

        if (entity.getIsSchedule()==1) {
            holder.ll_instruction_details.setVisibility(View.VISIBLE);
        }
        else {
            holder.ll_instruction_details.setVisibility(View.GONE);
        }

        holder.ll_navigate_schedule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navilistener.navigate(holder.getAdapterPosition(), entity);
            }
        });

        if (TextUtils.isEmpty(entity.getRide_instruction())
                || entity.getRide_instruction() == null) {

            holder.tv_pre_schedule_instruction.setText(mContext.getResources().getString(R.string.no_instructions));
        }
        else {
            holder.tv_pre_schedule_instruction.setText(entity.getRide_instruction());
            holder.tv_pre_schedule_instruction.setInputType(InputType.TYPE_TEXT_FLAG_MULTI_LINE);

        }

        rideinstruction = entity.getRide_instruction();
        holder.ll_more_history.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (TextUtils.isEmpty(rideinstruction)) {
                    Toast.makeText(mContext, "" + mContext.getString(R.string.no_instructions), Toast.LENGTH_SHORT).show();
                } else {
                    openMoreInstructions();
                }
            }
        });
    }

    private void openMoreInstructions() {
        final Dialog mBottomSheetDialog = new Dialog(mContext, R.style.MaterialDialogSheet);
        mBottomSheetDialog.setContentView(R.layout.dialog_instruction);
        mBottomSheetDialog.setCancelable(true);
        mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        mBottomSheetDialog.getWindow().setGravity(Gravity.CENTER);
        mBottomSheetDialog.setCanceledOnTouchOutside(true);
        AppCompatTextView tv_instruction = mBottomSheetDialog.findViewById(R.id.tv_instruction_details);
        AppCompatTextView btn_ok = mBottomSheetDialog.findViewById(R.id.btn_ok);

        tv_instruction.setText(rideinstruction);


        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBottomSheetDialog.dismiss();

            }
        });
        mBottomSheetDialog.show();
    }

    @Override
    public int getItemCount() {
        return lists.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public AppCompatTextView tv_name, tv_status,
                tv_source_location, tv_destination_location,tv_display_name,
                tv_completed_time, tv_total_cost,tv_pre_schedule_instruction;
        public RelativeLayout rl_item;
        public LinearLayout ll_navigate_schedule,ll_call_schedule, ll_more_history,ll_instruction_details;
        public CircleImageView profile_image;


        public MyViewHolder(View view) {
            super(view);

            tv_name = view.findViewById(R.id.tv_name);
            tv_status = view.findViewById(R.id.tv_status);
            tv_source_location = view.findViewById(R.id.tv_source_location);
            tv_destination_location = view.findViewById(R.id.tv_destination_location);
            tv_completed_time = view.findViewById(R.id.tv_completed_time);
            tv_total_cost = view.findViewById(R.id.tv_total_cost);
            tv_display_name = view.findViewById(R.id.tv_display_name);
            tv_pre_schedule_instruction = view.findViewById(R.id.tv_pre_schedule_instruction);
            profile_image = view.findViewById(R.id.profile_image);

            rl_item = view.findViewById(R.id.rl_item);
            ll_navigate_schedule = view.findViewById(R.id.ll_navigate_schedule);
            ll_call_schedule = view.findViewById(R.id.ll_call_schedule);
            ll_more_history = view.findViewById(R.id.ll_more_history);
            ll_instruction_details = view.findViewById(R.id.ll_instruction_details);
        }
    }
}