package com.rydedispatch.driver.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

@SuppressWarnings("unchecked")
public class GooglePlaceAdapter extends ArrayAdapter implements Filterable {


    private ArrayList resultList;
    private static final String LOG_TAG = "Google Places Autocomplete";
    private static final String PLACES_API_BASE = "https://maps.googleapis.com/maps/api/place";
    private static final String TYPE_AUTOCOMPLETE = "/autocomplete";
    private static final String OUT_JSON = "/json";
//    private static final String GOOGLE_API_KEY = "AIzaSyBLlbs11nZRiYAEIOczQwLUrOsQsEUFV28";
//    private static final String GOOGLE_API_KEY = "AIzaSyCFilVp23_Z0ywSmTVH37Mz6llpQViPNxA";
//    private static final String GOOGLE_API_KEY = "AIzaSyAIZ0dcbVqVatPnrSnMUX6M9gjFi7zAuqU";
    private static final String GOOGLE_API_KEY = "AIzaSyCFilVp23_Z0ywSmTVH37Mz6llpQViPNxA";


    public GooglePlaceAdapter(Context context, int textViewResourceId) {
        super(context, textViewResourceId);
        resultList = new ArrayList();
    }

    @Override
    public int getCount() {
        return resultList.size();
    }

    @Override
    public Object getItem(int index) {
        return resultList.get(index);
    }

    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults filterResults = new FilterResults();
                if (constraint != null) {
                    // Retrieve the autocomplete results.
                    resultList = autocomplete(constraint.toString());

                    // Assign the data to the FilterResults
                    filterResults.values = resultList;
                    filterResults.count = resultList.size();
                }
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, Filter.FilterResults results) {
                if (results != null && results.count > 0) {
                    notifyDataSetChanged();
                } else {
                    notifyDataSetInvalidated();
                }
            }
        };
        return filter;
    }

    @SuppressLint("LongLogTag")
    public static ArrayList autocomplete(String input) {
        ArrayList resultList = null;

        HttpURLConnection conn = null;
        StringBuilder jsonResults = new StringBuilder();
        try {

            //Creating Request URL for Google Places Api
            StringBuilder sb = new StringBuilder(PLACES_API_BASE + TYPE_AUTOCOMPLETE + OUT_JSON);
//            sb.append("?key=AIzaSyAztmjFlR3ly2uDMYwR600l4SNA0rMaUac"); //working autocomplete
//            sb.append("?key=AIzaSyCqe13oee9IaTOraNp-ZcPGDqieq3_o0UQ"); //not working current key for autocomplete
            sb.append("?key=" + GOOGLE_API_KEY);
//            sb.append("&components=country:ca"); // In case you want to restrict results according to country
//            sb.append("&input=" + URLEncoder.encode(input, "utf8"));
            sb.append("&input=" + URLEncoder.encode(input, "utf8"));
//            sb.append("&types=establishment");
            sb.append("&types=geocode");

            URL url = new URL(sb.toString());
            conn = (HttpURLConnection) url.openConnection();
            InputStreamReader in = new InputStreamReader(conn.getInputStream());

            // Load the results into a StringBuilder
            int read;
            char[] buff = new char[1024];
            while ((read = in.read(buff)) != -1) {
                jsonResults.append(buff, 0, read);
            }
        } catch (MalformedURLException e) {
            Log.e(LOG_TAG, "Error processing Places API URL", e);
            return resultList;
        } catch (IOException e) {
            Log.e(LOG_TAG, "Error connecting to Places API", e);
            return resultList;
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }

        try {
            // Create a JSON object hierarchy from the results
            JSONObject jsonObj = new JSONObject(jsonResults.toString());
            JSONArray predsJsonArray = jsonObj.getJSONArray("predictions");

            // Extract the Place descriptions from the results
            resultList = new ArrayList(predsJsonArray.length());
            for (int i = 0; i < predsJsonArray.length(); i++) {

                Log.d("Googleplace","api :" + predsJsonArray.getJSONObject(i).getString("description") +"@"+predsJsonArray.getJSONObject(i).getString("place_id"));
                Log.d("Googleplace","api :" + resultList.toString());
                System.out.println(predsJsonArray.getJSONObject(i).getString("description") +"@"+predsJsonArray.getJSONObject(i).getString("place_id"));
                System.out.println();
                System.out.println("============================================================");
//                resultList.add(predsJsonArray.getJSONObject(i).getString("description") +"@"+predsJsonArray.getJSONObject(i).getString("place_id"));
                resultList.add(predsJsonArray.getJSONObject(i).getString("description"));
            }
        } catch (JSONException e) {
            Log.e("error", "Cannot process JSON results", e);
        }

        return resultList;
    }

}