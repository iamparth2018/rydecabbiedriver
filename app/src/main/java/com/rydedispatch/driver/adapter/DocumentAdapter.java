package com.rydedispatch.driver.adapter;

import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.rydedispatch.driver.R;
import com.rydedispatch.driver.activity.DocumentActivity;
import com.rydedispatch.driver.response.RespGetDocumentList;
import com.rydedispatch.driver.utils.PreferenceHelper;
import com.rydedispatch.driver.utils.Prefs;
import com.rydedispatch.driver.utils.Progress;
import com.rydedispatch.driver.utils.Utils;

import java.util.List;

public class DocumentAdapter extends RecyclerView.Adapter<DocumentAdapter.MyViewHolder> {
    private static final String TAG = "DocumentAdapter";
    private final int VIEW_TYPE_ITEM = 0;
    private FragmentActivity mContext;
    private List<RespGetDocumentList.DataEntity> lists;
    private Utils utils;
    private Progress progress;
    private PreferenceHelper helper;
    private customOnclickListener listener;

    public void setListener(customOnclickListener listener) {
        this.listener = listener;
    }

    public interface customOnclickListener {
        void onclick(int adapterPosition, RespGetDocumentList.DataEntity documentsEntityList);
    }


    public DocumentAdapter(DocumentActivity documentActivity, List<RespGetDocumentList.DataEntity> listData) {
        this.lists = listData;
        this.mContext = documentActivity;

        utils = new Utils(mContext);
        progress = new Progress(mContext);
        helper = new PreferenceHelper(mContext, Prefs.PREF_FILE);
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        if (viewType == VIEW_TYPE_ITEM) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.row_documents, parent, false);
            return new MyViewHolder(itemView);
        } else {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_loading, parent, false);
            return new MyViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull DocumentAdapter.MyViewHolder holder, int position) {
        final RespGetDocumentList.DataEntity entity = lists.get(position);

        holder.tv_document_name.setText(entity.getDocumentName());
        holder.tv_status_doc.setText(entity.getStatus());
        holder.rvItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onclick(holder.getAdapterPosition(), entity);
            }
        });
    }

    @Override
    public int getItemCount() {
        return lists.size();

    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public AppCompatTextView tv_status_doc, tv_document_name;
        public RelativeLayout rvItem;

        public MyViewHolder(View view) {
            super(view);

            tv_document_name = view.findViewById(R.id.tv_document_name);
            tv_status_doc = view.findViewById(R.id.tv_status_doc);
            rvItem = view.findViewById(R.id.rv_item_doc);
        }
    }
}
