package com.rydedispatch.driver.adapter;

import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.rydedispatch.driver.R;
import com.rydedispatch.driver.activity.EarningDetailsActivity;
import com.rydedispatch.driver.activity.RideConfirmDialogActivity;
import com.rydedispatch.driver.response.RespStatementDetails;
import com.rydedispatch.driver.utils.AppConfig;
import com.rydedispatch.driver.utils.PreferenceHelper;
import com.rydedispatch.driver.utils.Prefs;
import com.rydedispatch.driver.utils.Progress;
import com.rydedispatch.driver.utils.Utils;
import com.squareup.picasso.Picasso;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class EarningDetailsAdapter extends RecyclerView.Adapter<EarningDetailsAdapter.MyViewHolder> {
    private static final String TAG = "EarningDetailsAdapter";
    private final int VIEW_TYPE_ITEM = 0;
    private FragmentActivity mContext;
    private List<RespStatementDetails.DataEntityX.StatementsDetailsEntity.DataEntity> lists;

    private Utils utils;
    private Progress progress;
    private PreferenceHelper helper;
    public customOnclickListener listener;
    private String pic;

    public interface customOnclickListener {
        void onclick(int adapterPosition, RespStatementDetails.DataEntityX.StatementsDetailsEntity.DataEntity entity);
    }

    public void setListener(customOnclickListener listener) {
        this.listener = listener;

    }

    public EarningDetailsAdapter(EarningDetailsActivity earningDetailsActivity,
                                 List<RespStatementDetails.DataEntityX.StatementsDetailsEntity.DataEntity> listData) {
        this.lists = listData;
        this.mContext = earningDetailsActivity;

        utils = new Utils(mContext);
        progress = new Progress(mContext);
        helper = new PreferenceHelper(mContext, Prefs.PREF_FILE);
    }

    @NonNull
    @Override
    public EarningDetailsAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.row_earning_statements, parent, false);
            return new MyViewHolder(itemView);
        } else {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_loading, parent, false);
            return new MyViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull EarningDetailsAdapter.MyViewHolder holder, int position) {
        final RespStatementDetails.DataEntityX.StatementsDetailsEntity.DataEntity entity = lists.get(position);
        pic = lists.get(position).getProfilePicture();

        if (entity.getStatus().equalsIgnoreCase("bonus")) {
            holder.tv_first_name.setText("-----");
            holder.tv_payment_type.setText(entity.getStatus());
            holder.tv_status.setVisibility(View.GONE);
        } else {
            holder.tv_status.setVisibility(View.VISIBLE);

            holder.tv_first_name.setText(entity.getFirstName());
            holder.tv_payment_type.setText(entity.getPaymentType());


        }

        holder.tvactualCost.setText("$ " + entity.getActualCostWithoutCommission());


        holder.tv_create_date.setText(entity.getCreatedAt());
//        holder.tv_status.setText(entity.getStatus());

        if (entity.getStatus().contains("cancelled_by_driver")) {
            holder.tv_status.setText(entity.getStatus().replace("_" , " " ).replace("_", " "));
            holder.tv_status.setTextColor(mContext.getResources().getColor(R.color.icons_8_muted_red));

        } else if (entity.getStatus().contains("cancelled")) {
            holder.tv_status.setText(entity.getStatus());
            holder.tv_status.setTextColor(mContext.getResources().getColor(R.color.icons_8_muted_red));
        } else {
            holder.tv_status.setText(entity.getStatus().replace("_", " ").replace("_", " "));
            holder.tv_status.setTextColor(mContext.getResources().getColor(R.color.green));
        }

        if (pic == null) {
            Picasso.with(mContext)
                    .load(R.mipmap.ic_launcher)
                    .placeholder(R.mipmap.ic_launcher)
                    .error(R.drawable.dummy_pic)
                    .into(holder.profile_image);
        } else if (pic.contains("https://")) {
            Picasso.with(mContext)
                    .load(pic)
                    .placeholder(R.mipmap.ic_launcher)
                    .error(R.drawable.dummy_pic)
                    .into(holder.profile_image);
        } else {
            pic = "" + entity;

            Picasso.with(mContext)
                    .load(pic)
                    .placeholder(R.mipmap.ic_launcher)
                    .error(R.drawable.dummy_pic)
                    .into(holder.profile_image);

        }
        holder.rl_earning_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onclick(holder.getAdapterPosition(), entity);
            }
        });
    }

    @Override
    public int getItemCount() {
        return lists.size();

    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public AppCompatTextView tv_first_name, tvactualCost, tv_payment_type, tv_create_date, tv_status;
        public RelativeLayout rl_earning_item;
        public CircleImageView profile_image;

        public MyViewHolder(View view) {
            super(view);

            tv_first_name = view.findViewById(R.id.tv_first_name);
            tvactualCost = view.findViewById(R.id.actual_cost_without_commission);
            tv_create_date = view.findViewById(R.id.tv_create_date);
            tv_status = view.findViewById(R.id.tv_status);
            tv_payment_type = view.findViewById(R.id.tv_payment_type);
            rl_earning_item = view.findViewById(R.id.rl_earning_item);
            profile_image = view.findViewById(R.id.profile_image);
        }
    }
}
