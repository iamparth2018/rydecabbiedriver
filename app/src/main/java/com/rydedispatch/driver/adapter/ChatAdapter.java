package com.rydedispatch.driver.adapter;

import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.rydedispatch.driver.R;
import com.rydedispatch.driver.activity.ChatActivity;
import com.rydedispatch.driver.response.RespGetMessageList;
import com.rydedispatch.driver.utils.PreferenceHelper;
import com.rydedispatch.driver.utils.Prefs;
import com.rydedispatch.driver.utils.Progress;
import com.rydedispatch.driver.utils.Utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.StringTokenizer;
import java.util.TimeZone;

public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.MyViewHolder> {
    private static final String TAG = "ChatAdapter";
    private final int VIEW_TYPE_ITEM = 0;
    private FragmentActivity mContext;
    private List<RespGetMessageList.DataEntityX.ChatsEntity.DataEntity> lists;
    private Utils utils;
    private Progress progress;
    private PreferenceHelper helper;
    private String createdAt , senderTime = "";
    private int checkhistory = 0;
    private SimpleDateFormat dfFormat;
    private TimeZone etTimeZone;
    private Date date = null;
    private Calendar cal;

    public ChatAdapter(ChatActivity chatActivity, List<RespGetMessageList.DataEntityX.ChatsEntity.DataEntity> listData, int checkHistory) {
        this.lists = listData;
        this.mContext = chatActivity;
        this.checkhistory = checkHistory;

        utils = new Utils(mContext);
        progress = new Progress(mContext);
        helper = new PreferenceHelper(mContext, Prefs.PREF_FILE);

    }

    @NonNull
    @Override
    public ChatAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.row_message_list, parent, false);
            return new MyViewHolder(itemView);
        } else {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_loading, parent, false);
            return new MyViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull ChatAdapter.MyViewHolder holder, int position) {
        final RespGetMessageList.DataEntityX.ChatsEntity.DataEntity entity = lists.get(position);

        if (entity.getDirection().contains("0")) {
            holder.tv_from_name.setText("Support Rydemate");
//            holder.ll_msg_bg.setBackgroundResource(R.drawable.support_theme);
            holder.ll_message.setGravity(Gravity.LEFT);
            holder.tv_message.setGravity(Gravity.LEFT);
            holder.tv_message.setBackgroundResource(R.drawable.support_chat_theme);
            holder.tv_message.setText(entity.getMessage());
            holder.rvItem.setGravity(Gravity.LEFT);
            holder.ll_item.setGravity(Gravity.LEFT);

            try {
                senderTime = entity.getCreatedAt();
                dfFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",Locale.getDefault());
                date = dfFormat.parse(senderTime);
                dfFormat.setTimeZone(TimeZone.getDefault());

                Calendar cal = Calendar.getInstance();
                cal.setTime(date);
                int hours = cal.get(Calendar.HOUR_OF_DAY);
                int mins = cal.get(Calendar.MINUTE);


                if (hours >= 12) {
                    holder.tvTime.setText(String.format("%02d", hours) + ":" + String.format("%02d", mins) + " PM");
                }
                else {
                    holder.tvTime.setText(String.format("%02d", hours) + ":" + String.format("%02d", mins) + " AM");
                }
            }
            catch (ParseException e) {
                e.printStackTrace();
            }

            //second try

        } else {
            holder.tv_message.setBackgroundResource(R.drawable.chat_shape);
            holder.ll_message.setGravity(Gravity.RIGHT);

            holder.rvItem.setGravity(Gravity.RIGHT);
            holder.ll_item.setGravity(Gravity.RIGHT);
            holder.tv_from_name.setText(entity.getFirstName() + " " + entity.getLastName());
            holder.tv_message.setGravity(Gravity.RIGHT);
            holder.tv_message.setText(entity.getMessage());


            dfFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

            try {
                date = dfFormat.parse(senderTime);
                dfFormat.setTimeZone(TimeZone.getDefault());

                Calendar cal = Calendar.getInstance();
                cal.setTime(date);
                int hours = cal.get(Calendar.HOUR_OF_DAY);
                int mins = cal.get(Calendar.MINUTE);

                if (hours >= 12) {
                    holder.tvTime.setText(String.format("%02d", hours) + ":" + String.format("%02d", mins) + " PM");
                }
                else {
                    holder.tvTime.setText(String.format("%02d", hours) + ":" + String.format("%02d", mins) + " AM");
                }

            }
            catch (ParseException e) {
                e.printStackTrace();
            }

        }

    }

    @Override
    public int getItemCount() {
        return lists.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public AppCompatTextView tv_from_name, tv_message, tvTime;
        public LinearLayout ll_message, ll_item;
        public RelativeLayout rvItem;

        public MyViewHolder(View view) {
            super(view);

            tv_from_name = view.findViewById(R.id.tv_from_name);
            tv_message = view.findViewById(R.id.tv_message);
            ll_message = view.findViewById(R.id.ll_message);
            tvTime = view.findViewById(R.id.tv_time);
//            ll_msg_bg = view.findViewById(R.id.ll_msg_bg);
            ll_item = view.findViewById(R.id.ll_item);
            rvItem = view.findViewById(R.id.rv_item);
        }
    }
}
