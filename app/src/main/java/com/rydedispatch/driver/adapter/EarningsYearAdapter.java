package com.rydedispatch.driver.adapter;

import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.rydedispatch.driver.R;
import com.rydedispatch.driver.response.RespGetStatements;
import com.rydedispatch.driver.response.RespHistory;
import com.rydedispatch.driver.utils.PreferenceHelper;
import com.rydedispatch.driver.utils.Prefs;
import com.rydedispatch.driver.utils.Progress;
import com.rydedispatch.driver.utils.RUtils;
import com.rydedispatch.driver.utils.Utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class EarningsYearAdapter extends RecyclerView.Adapter<EarningsYearAdapter.MyViewHolder> {
    private static final String TAG = "HistoryAdapter";
    private final int VIEW_TYPE_ITEM = 0;
    private FragmentActivity mContext;
    private List<RespGetStatements.DataEntityX.StatementsEntity.DataEntity> lists;
    private Utils utils;
    private Progress progress;
    private PreferenceHelper helper;
    private String newDateStr, newEndDateStr;
    public customOnclickListener listener;

    public interface customOnclickListener {
        void onclick(int adapterPosition, RespGetStatements.DataEntityX.StatementsEntity.DataEntity listhistories);
    }

    public void setListener(customOnclickListener listener) {
        this.listener = listener;
    }

    public EarningsYearAdapter(FragmentActivity activity, List<RespGetStatements.DataEntityX.StatementsEntity.DataEntity> listData) {
        this.lists = listData;
        this.mContext = activity;

        utils = new Utils(mContext);
        progress = new Progress(mContext);
        helper = new PreferenceHelper(mContext, Prefs.PREF_FILE);

    }

    @NonNull
    @Override
    public EarningsYearAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.row_earning_year, parent, false);
            return new MyViewHolder(itemView);
        } else {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_loading, parent, false);
            return new MyViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull EarningsYearAdapter.MyViewHolder holder, int position) {
        final RespGetStatements.DataEntityX.StatementsEntity.DataEntity entity = lists.get(position);

        try {

            String dateStr = entity.getStartDate();
            String dateEndStr = entity.getEndDate();

            SimpleDateFormat curFormater = new SimpleDateFormat("yyyy-MM-dd");
            Date dateObj = curFormater.parse(dateStr);

            SimpleDateFormat curEndFormater = new SimpleDateFormat("yyyy-MM-dd");
            Date dateEndObj = curEndFormater.parse(dateEndStr);

            newDateStr = curFormater.format(dateObj);
            newEndDateStr = curFormater.format(dateEndObj);

//            Log.d("Earning", "date :" + newDateStr);
//            Log.d("Earning", "EndDate :" + newEndDateStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        holder.tv_start_end.setText(newDateStr + " To " + newEndDateStr);
        holder.tv_actual_cost_without_commission.setText("$ " +entity.getActualCostWithoutCommission());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onclick(holder.getAdapterPosition(), entity);
            }
        });
    }

    @Override
    public int getItemCount() {
        return lists.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {

        public AppCompatTextView tv_start_end, tv_actual_cost_without_commission;
        public RelativeLayout rv_item_doc;

        public MyViewHolder(View view) {
            super(view);

            tv_start_end = view.findViewById(R.id.tv_start_end);
            tv_actual_cost_without_commission = view.findViewById(R.id.tv_actual_cost_without_commission);

            rv_item_doc = view.findViewById(R.id.rv_item_doc);
        }
    }
}
