package com.rydedispatch.driver.network;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatTextView;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.rydedispatch.driver.R;
import com.rydedispatch.driver.activity.AddAddressActivity;
import com.rydedispatch.driver.activity.AddVehicleActivity;
import com.rydedispatch.driver.activity.LoginActivity;
import com.rydedispatch.driver.app.MyApp;
import com.rydedispatch.driver.utils.AppConfig;
import com.rydedispatch.driver.utils.PreferenceHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class PostRequest extends AsyncTask<Void, Void, Void> {
    private String mUrl, auth;
    private RequestBody mRequestBody;
    private Callbacks mCallbacks;
    private Context context;
    private Activity activity;
    private PreferenceHelper helper;
    private ProgressDialog progressDialog;

    public PostRequest(Context c,String auth, String url, Callbacks callbacks){
        this.mUrl = url;
        this.context = c;
        this.mCallbacks = callbacks;
        this.auth = auth;
        activity = (Activity) context;
    }
    public PostRequest(Context c,String auth, String url, RequestBody requestBody , Callbacks callbacks){
        this.mUrl = url;
        this.context = c;
        this.mRequestBody = requestBody;
        this.mCallbacks = callbacks;
        this.auth = auth;
        activity = (Activity) context;
    }
    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progressDialog = new ProgressDialog(context);
        progressDialog.setCancelable(false);
        progressDialog.setMessage(context.getResources().getString(R.string.pls_wait));
        if(!((Activity) context).isFinishing())
            progressDialog.show();

        helper = new PreferenceHelper(context);
    }
    @Override
    protected Void doInBackground(Void... voids) {
        try {
            OkHttpClient client = new OkHttpClient();
            Request request;
            Log.e("PostRequest","Bearer "+auth);
            if(mRequestBody!=null){
                request = new Request.Builder()
                        .url(MyApp.getInstance().BASE_URL + mUrl)
                        .addHeader("Accept","application/json")
                        .addHeader("Authorization","Bearer "+auth)
                        .post(mRequestBody)
                        .build();
            }else{
//                RequestBody reqBody = RequestBody.create( new byte[0], null);

                request = new Request.Builder()
                        .url(MyApp.getInstance().BASE_URL + mUrl)
                        .addHeader("Accept","application/json")
                        .addHeader("Authorization","Bearer "+auth)
                        .build();
            }
            if(AppConfig.isNetworkAvailable(context)){
                client.newCall(request).enqueue(new Callback() {
                    @Override
                    public void onFailure(@NonNull Call call, @NonNull IOException e) {
                        dismissDialog();
                        mCallbacks.onFail(e.getMessage());
                    }
                    @Override
                    public void onResponse(@NonNull Call call, @NonNull Response response) {
                        dismissDialog();
                        if(response.isSuccessful()){
                            try {
                                String str = response.body().string();
                                Log.e("Hello "+mUrl,str);
                                try {
                                    JSONObject jsonObject = new JSONObject(str);
                                    if(!jsonObject.getBoolean("error")){
                                        mCallbacks.onSuccess(jsonObject);
                                    }else{
                                        if(jsonObject.getString("message").equalsIgnoreCase("Unauthenticated")){
                                            FirebaseInstanceId.getInstance().deleteInstanceId();
                                            ((Activity) context).runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    logoutDialog(context);
//                                                    Toast.makeText(context,"Unauthenticated",Toast.LENGTH_LONG).show();
                                                }
                                            });
                                        }else{
                                            mCallbacks.onFail(jsonObject.getString("message"));
                                        }
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }else{
                            mCallbacks.onFail(response.message());
                        }
                    }
                });
            }else{
                dismissDialog();
                mCallbacks.onFail(context.getString(R.string.internet));
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return null;
    }

    private void dismissDialog(){
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(!activity.isFinishing() && !activity.isDestroyed()){
                    if (progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }
                }
            }
        });
    }

    public interface Callbacks {
        void onFail(String error);
        void onSuccess(JSONObject response);
    }
    private void logoutDialog(final Context context){
        /*AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(R.string.auth_title);
        builder.setMessage(R.string.auth_msg);
        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                helper.clearAllPrefs();
                context.startActivity(new Intent(context, LoginActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
            }
        });
        builder.show();*/

        /*final Dialog mBottomSheetDialog = new Dialog(context, R.style.MaterialDialogSheet);
        mBottomSheetDialog.setContentView(R.layout.dialog_unauthorised);
        mBottomSheetDialog.setCancelable(false);
        mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        mBottomSheetDialog.getWindow().setGravity(Gravity.CENTER);
        mBottomSheetDialog.setCanceledOnTouchOutside(false);
        AppCompatTextView btn_ok = mBottomSheetDialog.findViewById(R.id.btn_ok);

        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                progressDialog.show();
                AsyncTask.execute(new Runnable() {
                    @Override
                    public void run() {
                        progressDialog.dismiss();
                        try {
                            FirebaseInstanceId.getInstance().deleteInstanceId();
                            helper.clearAllPrefs();
                            context.startActivity(new Intent(context, LoginActivity.class)
                                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        });
        mBottomSheetDialog.show();*/

    }
}