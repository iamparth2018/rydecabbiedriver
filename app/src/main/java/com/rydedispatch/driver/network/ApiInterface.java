package com.rydedispatch.driver.network;

import com.rydedispatch.driver.response.RespAcceptRide;
import com.rydedispatch.driver.response.RespAddAddress;
import com.rydedispatch.driver.response.RespAddVehicle;
import com.rydedispatch.driver.response.RespArrived;
import com.rydedispatch.driver.response.RespCancelDriver;
import com.rydedispatch.driver.response.RespCompleteRide;
import com.rydedispatch.driver.response.RespCreateDriver;
import com.rydedispatch.driver.response.RespDashboardDriver;
import com.rydedispatch.driver.response.RespForceDashboard;
import com.rydedispatch.driver.response.RespGenerateAudioToken;
import com.rydedispatch.driver.response.RespGetCarType;
import com.rydedispatch.driver.response.RespGetCompany;
import com.rydedispatch.driver.response.RespGetDocumentList;
import com.rydedispatch.driver.response.RespGetMakeOfCar;
import com.rydedispatch.driver.response.RespGetMessageList;
import com.rydedispatch.driver.response.RespGetModelOfCar;
import com.rydedispatch.driver.response.RespGetProfileDetails;
import com.rydedispatch.driver.response.RespGetStatements;
import com.rydedispatch.driver.response.RespGetVehicle;
import com.rydedispatch.driver.response.RespGetVehicleDetails;
import com.rydedispatch.driver.response.RespGetYearCar;
import com.rydedispatch.driver.response.RespHistory;
import com.rydedispatch.driver.response.RespHistoryDetails;
import com.rydedispatch.driver.response.RespInspection;
import com.rydedispatch.driver.response.RespLogin;
import com.rydedispatch.driver.response.RespLogoutDriver;
import com.rydedispatch.driver.response.RespNoShow;
import com.rydedispatch.driver.response.RespPassRequest;
import com.rydedispatch.driver.response.RespSendLoginOTP;
import com.rydedispatch.driver.response.RespSendMessage;
import com.rydedispatch.driver.response.RespSendOTP;
import com.rydedispatch.driver.response.RespStartRide;
import com.rydedispatch.driver.response.RespStatementDetails;
import com.rydedispatch.driver.response.RespSubmitRating;
import com.rydedispatch.driver.response.RespUpdateAvailability;
import com.rydedispatch.driver.response.RespUpdateMyLocation;
import com.rydedispatch.driver.response.RespUpdateProfile;
import com.rydedispatch.driver.response.RespUploadDocument;
import com.rydedispatch.driver.utils.AppConfig;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface ApiInterface {


    @GET(AppConfig.URL.URL_GET_YEAR_OF_CAR)
    Call<RespGetYearCar> GET_YEAR_CAR(@Header(Params.Accept) String accept);

    @Multipart
    @POST(AppConfig.URL.URL_GET_MAKE_OF_CAR)
    Call<RespGetMakeOfCar> GET_MAKE_OF_CAR(@Header(Params.Accept) String accept,
                                           @Part("year") RequestBody makeof);


    @Multipart
    @POST(AppConfig.URL.URL_MODEL_OF_CAR)
    Call<RespGetModelOfCar> MODEL_OF_CAR(@Header(Params.Accept) String accept,
                                         @Part("year") RequestBody yearOf,
                                         @Part("make") RequestBody make);


    @Multipart
    @POST(AppConfig.URL.URL_SEND_OTP)
    Call<RespSendOTP> SEND_OTP_CALL(@Header(Params.Accept) String accept,
                                    @Part("email") RequestBody email,
                                    @Part("phone_number") RequestBody phoneNumber,
                                    @Part("country_code") RequestBody countryCode);

    @Multipart
    @POST(AppConfig.URL.URL_CREATE_DRIVER)
    Call<RespCreateDriver> CREATE_DRIVER(@Header(Params.Accept) String accept,
                                         @Part("first_name") RequestBody firstname,
                                         @Part("last_name") RequestBody lastname,
                                         @Part("email") RequestBody email,
                                         @Part("phone_number") RequestBody phoneNumber,
                                         @Part("country_code") RequestBody countryCode,
                                         @Part("otp") RequestBody et_otp1,
                                         @Part("device_type") RequestBody deviceType1,
                                         @Part("device_token") RequestBody token1,
                                         @Part("driver_type") RequestBody driver_type);

    @Multipart
    @POST(AppConfig.URL.URL_CREATE_DRIVER)
    Call<RespCreateDriver> CREATE_COMPANY_DRIVER(@Header(Params.Accept) String accept,
                                                 @Part("first_name") RequestBody firstname,
                                                 @Part("last_name") RequestBody lastname,
                                                 @Part("email") RequestBody email,
                                                 @Part("phone_number") RequestBody phoneNumber,
                                                 @Part("country_code") RequestBody countryCode,
                                                 @Part("otp") RequestBody et_otp1,
                                                 @Part("device_type") RequestBody deviceType1,
                                                 @Part("device_token") RequestBody token1,
                                                 @Part("driver_type") RequestBody driver_type1,
                                                 @Part("company") RequestBody company1);


    @Multipart
    @POST(AppConfig.URL.URL_SEND_LOGIN_OTP)
    Call<RespSendLoginOTP> LOGIN_OTP_CALL(@Header(Params.Accept) String accept,
                                          @Part("phone_number") RequestBody phoneNumber,
                                          @Part("country_code") RequestBody countryCode);


    @Multipart
    @POST(AppConfig.URL.URL_LOGIN)
    Call<RespLogin> LOGIN_CALL(@Part("phone_number") RequestBody phoneNumber,
                               @Part("country_code") RequestBody countryCode,
                               @Part("otp") RequestBody otp,
                               @Part("device_token") RequestBody deviceToken,
                               @Part("device_type") RequestBody deviceType);

    @POST(AppConfig.URL.URL_PROFILE_DETAILS)
    Call<RespGetProfileDetails> PROFILE_DASHBOARD(@Header(Params.Accept) String accept,
                                                  @Header(Params.Authorization) String token);

    @Multipart
    @POST(AppConfig.URL.URL_UPDATE_PROFILE)
    Call<RespUpdateProfile> UPDATE_PROFILE_CALL(@Header(Params.Accept) String accept,
                                                @Header(Params.Authorization) String token,
                                                @Part("first_name") RequestBody firstname,
                                                @Part("last_name") RequestBody lastname,
                                                @Part("email") RequestBody email,
                                                @Part MultipartBody.Part file);

    @Multipart
    @POST(AppConfig.URL.URL_UPDATE_PROFILE)
    Call<RespUpdateProfile> UPDATE_PROFILE_CALL1(@Header(Params.Accept) String accept,
                                                 @Header(Params.Authorization) String token,
                                                 @Part("first_name") RequestBody firstname,
                                                 @Part("last_name") RequestBody lastname,
                                                 @Part("email") RequestBody email);


    @POST(AppConfig.URL.URL_LOGOUT)
    Call<RespLogoutDriver> LOGOUT_DRIVER(@Header(Params.Authorization) String authorization);

    @POST(AppConfig.URL.URL_DASHBOARD_DRIVER)
    Call<RespDashboardDriver> DRIVER_DASHBOARD(@Header(Params.Accept) String accept,
                                               @Header(Params.Authorization) String token);


    @POST(AppConfig.URL.URL_DASHBOARD_DRIVER)
    Call<RespForceDashboard> DRIVER_DASHBOARD1(@Header(Params.Accept) String accept,
                                               @Header(Params.Authorization) String token);

    @Multipart
    @POST(AppConfig.URL.URL_ADD_VEHICLE)
    Call<RespAddVehicle> ADD_VEHICLE_CALL(@Header(Params.Accept) String accept,
                                          @Header(Params.Authorization) String token,
                                          @Part("vin_code") RequestBody vincode1,
                                          @Part("make") RequestBody make1,
                                          @Part("model") RequestBody model1,
                                          @Part("year") RequestBody yearOf1,
                                          @Part("color") RequestBody color1,
                                          @Part("transmission") RequestBody transmis1,
                                          @Part("driver_train") RequestBody drive1,
                                          @Part("car_type") RequestBody carType1,
                                          @Part("car_number") RequestBody car_number1);


    @Multipart
    @POST(AppConfig.URL.URL_ADD_ADDRESS)
    Call<RespAddAddress> ADD_ADDRESS_CALL(@Header(Params.Accept) String accept,
                                          @Header(Params.Authorization) String token,
                                          @Part("address") RequestBody address,
                                          @Part("country") RequestBody country,
                                          @Part("state") RequestBody state,
                                          @Part("city") RequestBody city,
                                          @Part("latitude") RequestBody latitude,
                                          @Part("longitude") RequestBody longitude);

    @POST(AppConfig.URL.URL_GENERATE_AUDIO_TOKEN)
    Call<RespGenerateAudioToken> GENERATE_AUDIO_TOKEN_CALL(@Header(Params.Accept) String accept,
                                                           @Header(Params.Authorization) String token);

    @Multipart
    @POST(AppConfig.URL.URL_UPDATE_AVAILABILITY)
    Call<RespUpdateAvailability> UPDATE_AVAILABILITY_CALL(@Header(Params.Accept) String accept,
                                                          @Header(Params.Authorization) String token,
                                                          @Part("user_status") RequestBody status);


    @Multipart
    @POST(AppConfig.URL.URL_UPLOAD_DOCUMENT)
    Call<RespUploadDocument> UPLOAD_DOCUMENT(@Header(Params.Accept) String accept,
                                             @Header(Params.Authorization) String token,
                                             @Part("document_id") RequestBody documentid,
                                             @Part("unique_id") RequestBody uniqueid,
                                             @Part("document_type") RequestBody documenttype,
                                             @Part("issue_date") RequestBody issueDate,
                                             @Part("expiry_date") RequestBody expiryDate,
                                             @Part MultipartBody.Part file,
                                             @Part MultipartBody.Part file2);

    @Multipart
    @POST(AppConfig.URL.URL_UPLOAD_DOCUMENT)
    Call<RespUploadDocument> UPLOAD_NEW_DOCUMENT(@Header(Params.Accept) String accept,
                                                 @Header(Params.Authorization) String token,
                                                 @Part("document_id") RequestBody documentid,
                                                 @Part("unique_id") RequestBody uniqueid,
                                                 @Part("expiry_date") RequestBody expiryDate,
                                                 @Part MultipartBody.Part file,
                                                 @Part MultipartBody.Part file2);


    @GET(AppConfig.URL.URL_CAR_TYPE)
    Call<RespGetCarType> CAR_TYPE_CALL(@Header(Params.Accept) String accept,
                                       @Header(Params.Authorization) String token);

    @GET(AppConfig.URL.URL_GET_DOCUMENT)
    Call<RespGetDocumentList> GET_DOCUMENT_LIST_CALL(@Header(Params.Accept) String accept,
                                                     @Header(Params.Authorization) String token);

    @POST(AppConfig.URL.URL_GET_MESSAGE_LIST)
    Call<RespGetMessageList> GET_MESSAGE_LIST(@Header(Params.Accept) String accept,
                                              @Header(Params.Authorization) String token);


    @Multipart
    @POST(AppConfig.URL.URL_ACCEPT_RIDE)
    Call<RespAcceptRide> ACCEPT_RIDE_CALL(@Header(Params.Accept) String accept,
                                          @Header(Params.Authorization) String token,
                                          @Part("request_id") RequestBody request_id);

    @Multipart
    @POST(AppConfig.URL.URL_PASS_RIDE)
    Call<RespPassRequest> PASS_RIDE_CALL(@Header(Params.Accept) String accept,
                                         @Header(Params.Authorization) String token,
                                         @Part("request_id") RequestBody request_id);

    @Multipart
    @POST(AppConfig.URL.URL_NO_SHOW)
    Call<RespNoShow> NO_SHOW_CALL(@Header(Params.Accept) String accept,
                                  @Header(Params.Authorization) String token,
                                  @Part("request_id") RequestBody request_id);

    @Multipart
    @POST(AppConfig.URL.URL_CANCEL_BY_DRIVER)
    Call<RespCancelDriver> CANCEL_BY_DRIVER(@Header(Params.Accept) String accept,
                                            @Header(Params.Authorization) String token,
                                            @Part("request_id") RequestBody request_id);


    @Multipart
    @POST(AppConfig.URL.URL_ARRIVED)
    Call<RespArrived> ARRIVED_CALL(@Header(Params.Accept) String accept,
                                   @Header(Params.Authorization) String token,
                                   @Part("request_id") RequestBody request_id,
                                   @Part("total_distance") RequestBody total_distance);

    @Multipart
    @POST(AppConfig.URL.URL_START_RIDE)
    Call<RespStartRide> START_RIDE_CALL(@Header(Params.Accept) String accept,
                                        @Header(Params.Authorization) String token,
                                        @Part("request_id") RequestBody request_id);

    @Multipart
    @POST(AppConfig.URL.URL_COMPLETE_RIDE)
    Call<RespCompleteRide> COMPLETE_CALL(@Header(Params.Accept) String accept,
                                         @Header(Params.Authorization) String token,
                                         @Part("total_distance") RequestBody totalDistance,
                                         @Part("request_id") RequestBody request_id);


    @Multipart
    @POST(AppConfig.URL.URL_UPDATE_MY_LOCATION)
    Call<RespUpdateMyLocation> UPDATE_MY_LOCATION(@Header(Params.Accept) String accept,
                                                  @Header(Params.Authorization) String token,
                                                  @Part("latitude") RequestBody latitude,
                                                  @Part("longitude") RequestBody longitude);


    @Multipart
    @POST(AppConfig.URL.URL_SUBMIT_RATING)
    Call<RespSubmitRating> SUBMIT_CALL(@Header(Params.Accept) String accept,
                                       @Header(Params.Authorization) String token,
                                       @Part("request_id") RequestBody request_id1,
                                       @Part("rating") RequestBody rate1,
                                       @Part("comment") RequestBody stringComment1);


    @Multipart
    @POST(AppConfig.URL.URL_HISTORY_LIST)
    Call<RespHistory> HISTORY_CALL(@Header(Params.Accept) String accept,
                                   @Header(Params.Authorization) String token,
                                   @Part("search") RequestBody request_id1,
                                   @Part("startDate") RequestBody rate1,
                                   @Part("endDate") RequestBody stringComment1,
                                   @Part("is_schedule") RequestBody is_schedule,
                                   @Part("status") RequestBody status,
                                   @Part("page") RequestBody page);

    @Multipart
    @POST(AppConfig.URL.URL_HISTORY_LIST)
    Call<RespHistory> HISTORY_CALL_FILTER(@Header(Params.Accept) String accept,
                                          @Header(Params.Authorization) String token,
                                          @Part("search") RequestBody request_id1,
                                          @Part("startDate") RequestBody rate1,
                                          @Part("endDate") RequestBody stringComment1,
                                          @Part("status") RequestBody endDate1);


    @Multipart
    @POST(AppConfig.URL.URL_SEND_MESSAGE)
    Call<RespSendMessage> SEND_MESSAGE(@Header(Params.Accept) String accept,
                                       @Header(Params.Authorization) String token,
                                       @Part("message") RequestBody message,
                                       @Part("page") RequestBody page1);

    @FormUrlEncoded
    @POST(AppConfig.URL.URL_SEND_MESSAGE)
    Call<RespGetMessageList> SEND_NEW_MESSAGE(@Header("Accept") String accept,
                                              @Header("Authorization") String authorization,
                                              @Field("message") String message,
                                              @Field("page") String page);

    @Multipart
    @POST(AppConfig.URL.URL_HISTORY_DETAILS)
    Call<RespHistoryDetails> HISTORY_DETAILS(@Header(Params.Accept) String accept,
                                             @Header(Params.Authorization) String token,
                                             @Part("request_id") RequestBody page1);

    @POST(AppConfig.URL.URL_GET_STATEMENTS)
    Call<RespGetStatements> GET_STATEMENTS_CALL(@Header(Params.Accept) String accept,
                                                @Header(Params.Authorization) String token);

    @Multipart
    @POST("/api/uploadSignature")
    Call<ResponseBody> postImage(
            @Header("Accept") String accept,
            @Header("Authorization") String token,
            @Part MultipartBody.Part image,
            @Part("request_id") RequestBody name);


    @Multipart
    @POST(AppConfig.URL.URL_STATEMENT_DETAILS)
    Call<RespStatementDetails> STATEMENT_DETAILS(@Header(Params.Accept) String accept,
                                                 @Header(Params.Authorization) String token,
                                                 @Part("statement_id") RequestBody earningID1);

    @POST(AppConfig.URL.URL_GET_VEHICLE)
    Call<RespGetVehicle> DRIVER_VEHICLE(@Header(Params.Accept) String accept,
                                        @Header(Params.Authorization) String token);

    @Multipart
    @POST(AppConfig.URL.URL_GET_VEHICLE_DETAILS)
    Call<RespGetVehicleDetails> GET_VEHICLE_DETAILS_CALL(@Header(Params.Accept) String accept,
                                                         @Header(Params.Authorization) String token,
                                                         @Part("vin") RequestBody vin_code);


    @GET(AppConfig.URL.URL_GET_COMPANY)
    Call<RespGetCompany> GET_COMPANY(@Header(Params.Accept) String accept);


    @Multipart
    @POST(AppConfig.URL.URL_ADD_INSPECTION)
    Call<RespInspection> ADD_INSPECTION(@Header(Params.Accept) String accept,
                                        @Header(Params.Authorization) String token,
                                        @Part("questions") RequestBody myList1,
                                        @Part("mileage") RequestBody mileage);
}
