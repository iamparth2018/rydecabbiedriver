package com.rydedispatch.driver.response;

import com.google.gson.annotations.SerializedName;

public class RespUpdateProfile {

    /**
     * error : false
     * data : {"id":24,"parent_id":0,"company_id":0,"first_name":"Parth","last_name":"Mistry","company_name":"","company_image":null,"contact_person":"","profile_picture":"/assets/images/5d270e66eea18.jpg","email":"parthmistry.samcom2018@gmail.com","email_verified_at":null,"phone_number":"+917990010933","address":"","country":"","state":"","city":"","device_token":"fOBGpE9Qafc:APA91bFpe9ApEJzkutDE9nX--QQjsBUZHAf5ATWecu7bQcXR0W8vJRPIiZj2jqDCa1HZPNzduqOOC3kmBpQwR_ozd5gg1bbL-rOq5yMrm2CA0c9VJUW06KT-S3nLM9MlrJjaBAcRmYiW","device_type":"android","latitude":"","longitude":"","status":"active","authy_id":"66729740","user_unique_code":"Parth1024","tax_id":null,"commission_type":"ammount","commission":null,"role":"driver","document_status":"pending","deleted_at":null,"created_at":"2019-07-10 12:14:32","updated_at":"2019-07-11 11:08:56"}
     * message : Profile updated successfully.
     */

    @SerializedName("error")
    private boolean error;
    @SerializedName("data")
    private DataEntity data;
    @SerializedName("message")
    private String message;

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public DataEntity getData() {
        return data;
    }

    public void setData(DataEntity data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public static class DataEntity {
        /**
         * id : 24
         * parent_id : 0
         * company_id : 0
         * first_name : Parth
         * last_name : Mistry
         * company_name :
         * company_image : null
         * contact_person :
         * profile_picture : /assets/images/5d270e66eea18.jpg
         * email : parthmistry.samcom2018@gmail.com
         * email_verified_at : null
         * phone_number : +917990010933
         * address :
         * country :
         * state :
         * city :
         * device_token : fOBGpE9Qafc:APA91bFpe9ApEJzkutDE9nX--QQjsBUZHAf5ATWecu7bQcXR0W8vJRPIiZj2jqDCa1HZPNzduqOOC3kmBpQwR_ozd5gg1bbL-rOq5yMrm2CA0c9VJUW06KT-S3nLM9MlrJjaBAcRmYiW
         * device_type : android
         * latitude :
         * longitude :
         * status : active
         * authy_id : 66729740
         * user_unique_code : Parth1024
         * tax_id : null
         * commission_type : ammount
         * commission : null
         * role : driver
         * document_status : pending
         * deleted_at : null
         * created_at : 2019-07-10 12:14:32
         * updated_at : 2019-07-11 11:08:56
         */

        @SerializedName("id")
        private int id;
        @SerializedName("parent_id")
        private int parentId;
        @SerializedName("company_id")
        private int companyId;
        @SerializedName("first_name")
        private String firstName;
        @SerializedName("last_name")
        private String lastName;
        @SerializedName("company_name")
        private String companyName;
        @SerializedName("company_image")
        private Object companyImage;
        @SerializedName("contact_person")
        private String contactPerson;
        @SerializedName("profile_picture")
        private String profilePicture;
        @SerializedName("email")
        private String email;
        @SerializedName("email_verified_at")
        private Object emailVerifiedAt;
        @SerializedName("phone_number")
        private String phoneNumber;
        @SerializedName("address")
        private String address;
        @SerializedName("country")
        private String country;
        @SerializedName("state")
        private String state;
        @SerializedName("city")
        private String city;
        @SerializedName("device_token")
        private String deviceToken;
        @SerializedName("device_type")
        private String deviceType;
        @SerializedName("latitude")
        private String latitude;
        @SerializedName("longitude")
        private String longitude;
        @SerializedName("status")
        private String status;
        @SerializedName("authy_id")
        private String authyId;
        @SerializedName("user_unique_code")
        private String userUniqueCode;
        @SerializedName("tax_id")
        private Object taxId;
        @SerializedName("commission_type")
        private String commissionType;
        @SerializedName("commission")
        private Object commission;
        @SerializedName("role")
        private String role;
        @SerializedName("document_status")
        private String documentStatus;
        @SerializedName("deleted_at")
        private Object deletedAt;
        @SerializedName("created_at")
        private String createdAt;
        @SerializedName("updated_at")
        private String updatedAt;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getParentId() {
            return parentId;
        }

        public void setParentId(int parentId) {
            this.parentId = parentId;
        }

        public int getCompanyId() {
            return companyId;
        }

        public void setCompanyId(int companyId) {
            this.companyId = companyId;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public String getCompanyName() {
            return companyName;
        }

        public void setCompanyName(String companyName) {
            this.companyName = companyName;
        }

        public Object getCompanyImage() {
            return companyImage;
        }

        public void setCompanyImage(Object companyImage) {
            this.companyImage = companyImage;
        }

        public String getContactPerson() {
            return contactPerson;
        }

        public void setContactPerson(String contactPerson) {
            this.contactPerson = contactPerson;
        }

        public String getProfilePicture() {
            return profilePicture;
        }

        public void setProfilePicture(String profilePicture) {
            this.profilePicture = profilePicture;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public Object getEmailVerifiedAt() {
            return emailVerifiedAt;
        }

        public void setEmailVerifiedAt(Object emailVerifiedAt) {
            this.emailVerifiedAt = emailVerifiedAt;
        }

        public String getPhoneNumber() {
            return phoneNumber;
        }

        public void setPhoneNumber(String phoneNumber) {
            this.phoneNumber = phoneNumber;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getDeviceToken() {
            return deviceToken;
        }

        public void setDeviceToken(String deviceToken) {
            this.deviceToken = deviceToken;
        }

        public String getDeviceType() {
            return deviceType;
        }

        public void setDeviceType(String deviceType) {
            this.deviceType = deviceType;
        }

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public String getLongitude() {
            return longitude;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getAuthyId() {
            return authyId;
        }

        public void setAuthyId(String authyId) {
            this.authyId = authyId;
        }

        public String getUserUniqueCode() {
            return userUniqueCode;
        }

        public void setUserUniqueCode(String userUniqueCode) {
            this.userUniqueCode = userUniqueCode;
        }

        public Object getTaxId() {
            return taxId;
        }

        public void setTaxId(Object taxId) {
            this.taxId = taxId;
        }

        public String getCommissionType() {
            return commissionType;
        }

        public void setCommissionType(String commissionType) {
            this.commissionType = commissionType;
        }

        public Object getCommission() {
            return commission;
        }

        public void setCommission(Object commission) {
            this.commission = commission;
        }

        public String getRole() {
            return role;
        }

        public void setRole(String role) {
            this.role = role;
        }

        public String getDocumentStatus() {
            return documentStatus;
        }

        public void setDocumentStatus(String documentStatus) {
            this.documentStatus = documentStatus;
        }

        public Object getDeletedAt() {
            return deletedAt;
        }

        public void setDeletedAt(Object deletedAt) {
            this.deletedAt = deletedAt;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }
    }
}
