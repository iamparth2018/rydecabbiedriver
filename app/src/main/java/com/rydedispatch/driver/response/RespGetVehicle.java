package com.rydedispatch.driver.response;

import com.google.gson.annotations.SerializedName;

public class RespGetVehicle {

    /**
     * error : false
     * data : {"vehicle":{"id":2,"user_id":5,"vin_code":"","car_number":"007","make":"FIAT","model":"500","year":"2018","transmission":"Manual","color":"black","driver_train":"Front wheel drive","car_type":"Cargo Van","car_type_id":1,"deleted_at":null,"created_at":"2019-08-16 12:22:11","updated_at":"2019-09-04 06:03:32"},"vehicle_status":"Yes"}
     * message : Vehicle list get successfully.
     */

    @SerializedName("error")
    private boolean error;
    @SerializedName("data")
    private DataEntity data;
    @SerializedName("message")
    private String message;

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public DataEntity getData() {
        return data;
    }

    public void setData(DataEntity data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public static class DataEntity {
        /**
         * vehicle : {"id":2,"user_id":5,"vin_code":"","car_number":"007","make":"FIAT","model":"500","year":"2018","transmission":"Manual","color":"black","driver_train":"Front wheel drive","car_type":"Cargo Van","car_type_id":1,"deleted_at":null,"created_at":"2019-08-16 12:22:11","updated_at":"2019-09-04 06:03:32"}
         * vehicle_status : Yes
         */

        @SerializedName("vehicle")
        private VehicleEntity vehicle;
        @SerializedName("vehicle_status")
        private String vehicleStatus;

        public VehicleEntity getVehicle() {
            return vehicle;
        }

        public void setVehicle(VehicleEntity vehicle) {
            this.vehicle = vehicle;
        }

        public String getVehicleStatus() {
            return vehicleStatus;
        }

        public void setVehicleStatus(String vehicleStatus) {
            this.vehicleStatus = vehicleStatus;
        }

        public static class VehicleEntity {
            /**
             * id : 2
             * user_id : 5
             * vin_code :
             * car_number : 007
             * make : FIAT
             * model : 500
             * year : 2018
             * transmission : Manual
             * color : black
             * driver_train : Front wheel drive
             * car_type : Cargo Van
             * car_type_id : 1
             * deleted_at : null
             * created_at : 2019-08-16 12:22:11
             * updated_at : 2019-09-04 06:03:32
             */

            @SerializedName("id")
            private int id;
            @SerializedName("user_id")
            private int userId;
            @SerializedName("vin_code")
            private String vinCode;
            @SerializedName("car_number")
            private String carNumber;
            @SerializedName("make")
            private String make;
            @SerializedName("model")
            private String model;
            @SerializedName("year")
            private String year;
            @SerializedName("transmission")
            private String transmission;
            @SerializedName("color")
            private String color;
            @SerializedName("driver_train")
            private String driverTrain;
            @SerializedName("car_type")
            private String carType;
            @SerializedName("car_type_id")
            private int carTypeId;
            @SerializedName("deleted_at")
            private Object deletedAt;
            @SerializedName("created_at")
            private String createdAt;
            @SerializedName("updated_at")
            private String updatedAt;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public int getUserId() {
                return userId;
            }

            public void setUserId(int userId) {
                this.userId = userId;
            }

            public String getVinCode() {
                return vinCode;
            }

            public void setVinCode(String vinCode) {
                this.vinCode = vinCode;
            }

            public String getCarNumber() {
                return carNumber;
            }

            public void setCarNumber(String carNumber) {
                this.carNumber = carNumber;
            }

            public String getMake() {
                return make;
            }

            public void setMake(String make) {
                this.make = make;
            }

            public String getModel() {
                return model;
            }

            public void setModel(String model) {
                this.model = model;
            }

            public String getYear() {
                return year;
            }

            public void setYear(String year) {
                this.year = year;
            }

            public String getTransmission() {
                return transmission;
            }

            public void setTransmission(String transmission) {
                this.transmission = transmission;
            }

            public String getColor() {
                return color;
            }

            public void setColor(String color) {
                this.color = color;
            }

            public String getDriverTrain() {
                return driverTrain;
            }

            public void setDriverTrain(String driverTrain) {
                this.driverTrain = driverTrain;
            }

            public String getCarType() {
                return carType;
            }

            public void setCarType(String carType) {
                this.carType = carType;
            }

            public int getCarTypeId() {
                return carTypeId;
            }

            public void setCarTypeId(int carTypeId) {
                this.carTypeId = carTypeId;
            }

            public Object getDeletedAt() {
                return deletedAt;
            }

            public void setDeletedAt(Object deletedAt) {
                this.deletedAt = deletedAt;
            }

            public String getCreatedAt() {
                return createdAt;
            }

            public void setCreatedAt(String createdAt) {
                this.createdAt = createdAt;
            }

            public String getUpdatedAt() {
                return updatedAt;
            }

            public void setUpdatedAt(String updatedAt) {
                this.updatedAt = updatedAt;
            }
        }
    }
}
