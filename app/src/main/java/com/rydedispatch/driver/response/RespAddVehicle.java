package com.rydedispatch.driver.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RespAddVehicle {


    /**
     * error : false
     * data : {"userDetails":{"id":4,"parent_id":0,"company_id":0,"first_name":"Parth","last_name":"Mistry","company_name":"","company_image":null,"contact_person":"","profile_picture":null,"email":"parth.samcom2018@gmail.com","email_verified_at":null,"phone_number":"+919712146184","address":"Ahmedabad, Gujarat, India","country":"India","state":"Gujarat","city":"Ahmedabad","device_token":"eGp5S8NjIHM:APA91bHXz_JGl8b2YSnkvtUYnmcmEHbLB5POYanc1-HR7t2N5ve4VywIRT-gzWIyCBrHufqOFj676R2U7EyDyIEUVu4Bpe7dqZo8nnW1GXA-mf8jDbG8NfSZ5dlBbY7oJdh52v0fZSoc","device_type":"android","latitude":"23.0224734","longitude":"72.5715931","status":"active","user_status":"online","authy_id":"127634683","user_unique_code":"Parth1004","tax_id":null,"commission_type":"ammount","commission":null,"role":"driver","document_status":"pending","deleted_at":null,"created_at":"2019-07-18 06:52:53","updated_at":"2019-07-18 07:01:01","users_vehicle":{"id":4,"user_id":4,"vin_code":"YV1MS682X62153688","make":"chevrolet","model":"Avalanche","year":"2011","transmission":"Automatic","color":"black","driver_train":"Front wheel drive","car_type":"Cargo van","deleted_at":null,"created_at":"2019-07-18 12:09:45","updated_at":"2019-07-18 12:54:35"},"vehicle_status":"Yes"},"documents":[{"id":10,"user_id":4,"document_name":"Driving License","document_url":"","expiry_date":null,"status":"pending","deleted_at":null,"created_at":"2019-07-18 12:09:45","updated_at":"2019-07-18 12:09:45"},{"id":11,"user_id":4,"document_name":"Comprehensive vehicle insurance","document_url":"","expiry_date":null,"status":"pending","deleted_at":null,"created_at":"2019-07-18 12:09:45","updated_at":"2019-07-18 12:09:45"},{"id":12,"user_id":4,"document_name":"Inspection Certificate","document_url":"","expiry_date":null,"status":"pending","deleted_at":null,"created_at":"2019-07-18 12:09:45","updated_at":"2019-07-18 12:09:45"},{"id":16,"user_id":4,"document_name":"Driving License","document_url":"","expiry_date":null,"status":"pending","deleted_at":null,"created_at":"2019-07-18 12:53:08","updated_at":"2019-07-18 12:53:08"},{"id":17,"user_id":4,"document_name":"Comprehensive vehicle insurance","document_url":"","expiry_date":null,"status":"pending","deleted_at":null,"created_at":"2019-07-18 12:53:08","updated_at":"2019-07-18 12:53:08"},{"id":18,"user_id":4,"document_name":"Inspection Certificate","document_url":"","expiry_date":null,"status":"pending","deleted_at":null,"created_at":"2019-07-18 12:53:08","updated_at":"2019-07-18 12:53:08"},{"id":19,"user_id":4,"document_name":"Driving License","document_url":"","expiry_date":null,"status":"pending","deleted_at":null,"created_at":"2019-07-18 12:54:35","updated_at":"2019-07-18 12:54:35"},{"id":20,"user_id":4,"document_name":"Comprehensive vehicle insurance","document_url":"","expiry_date":null,"status":"pending","deleted_at":null,"created_at":"2019-07-18 12:54:35","updated_at":"2019-07-18 12:54:35"},{"id":21,"user_id":4,"document_name":"Inspection Certificate","document_url":"","expiry_date":null,"status":"pending","deleted_at":null,"created_at":"2019-07-18 12:54:35","updated_at":"2019-07-18 12:54:35"}]}
     * message : Vehicle has been updated successfully.
     */

    @SerializedName("error")
    private boolean error;
    @SerializedName("data")
    private DataEntity data;
    @SerializedName("message")
    private String message;

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public DataEntity getData() {
        return data;
    }

    public void setData(DataEntity data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public static class DataEntity {
        /**
         * userDetails : {"id":4,"parent_id":0,"company_id":0,"first_name":"Parth","last_name":"Mistry","company_name":"","company_image":null,"contact_person":"","profile_picture":null,"email":"parth.samcom2018@gmail.com","email_verified_at":null,"phone_number":"+919712146184","address":"Ahmedabad, Gujarat, India","country":"India","state":"Gujarat","city":"Ahmedabad","device_token":"eGp5S8NjIHM:APA91bHXz_JGl8b2YSnkvtUYnmcmEHbLB5POYanc1-HR7t2N5ve4VywIRT-gzWIyCBrHufqOFj676R2U7EyDyIEUVu4Bpe7dqZo8nnW1GXA-mf8jDbG8NfSZ5dlBbY7oJdh52v0fZSoc","device_type":"android","latitude":"23.0224734","longitude":"72.5715931","status":"active","user_status":"online","authy_id":"127634683","user_unique_code":"Parth1004","tax_id":null,"commission_type":"ammount","commission":null,"role":"driver","document_status":"pending","deleted_at":null,"created_at":"2019-07-18 06:52:53","updated_at":"2019-07-18 07:01:01","users_vehicle":{"id":4,"user_id":4,"vin_code":"YV1MS682X62153688","make":"chevrolet","model":"Avalanche","year":"2011","transmission":"Automatic","color":"black","driver_train":"Front wheel drive","car_type":"Cargo van","deleted_at":null,"created_at":"2019-07-18 12:09:45","updated_at":"2019-07-18 12:54:35"},"vehicle_status":"Yes"}
         * documents : [{"id":10,"user_id":4,"document_name":"Driving License","document_url":"","expiry_date":null,"status":"pending","deleted_at":null,"created_at":"2019-07-18 12:09:45","updated_at":"2019-07-18 12:09:45"},{"id":11,"user_id":4,"document_name":"Comprehensive vehicle insurance","document_url":"","expiry_date":null,"status":"pending","deleted_at":null,"created_at":"2019-07-18 12:09:45","updated_at":"2019-07-18 12:09:45"},{"id":12,"user_id":4,"document_name":"Inspection Certificate","document_url":"","expiry_date":null,"status":"pending","deleted_at":null,"created_at":"2019-07-18 12:09:45","updated_at":"2019-07-18 12:09:45"},{"id":16,"user_id":4,"document_name":"Driving License","document_url":"","expiry_date":null,"status":"pending","deleted_at":null,"created_at":"2019-07-18 12:53:08","updated_at":"2019-07-18 12:53:08"},{"id":17,"user_id":4,"document_name":"Comprehensive vehicle insurance","document_url":"","expiry_date":null,"status":"pending","deleted_at":null,"created_at":"2019-07-18 12:53:08","updated_at":"2019-07-18 12:53:08"},{"id":18,"user_id":4,"document_name":"Inspection Certificate","document_url":"","expiry_date":null,"status":"pending","deleted_at":null,"created_at":"2019-07-18 12:53:08","updated_at":"2019-07-18 12:53:08"},{"id":19,"user_id":4,"document_name":"Driving License","document_url":"","expiry_date":null,"status":"pending","deleted_at":null,"created_at":"2019-07-18 12:54:35","updated_at":"2019-07-18 12:54:35"},{"id":20,"user_id":4,"document_name":"Comprehensive vehicle insurance","document_url":"","expiry_date":null,"status":"pending","deleted_at":null,"created_at":"2019-07-18 12:54:35","updated_at":"2019-07-18 12:54:35"},{"id":21,"user_id":4,"document_name":"Inspection Certificate","document_url":"","expiry_date":null,"status":"pending","deleted_at":null,"created_at":"2019-07-18 12:54:35","updated_at":"2019-07-18 12:54:35"}]
         */

        @SerializedName("userDetails")
        private UserDetailsEntity userDetails;
        @SerializedName("documents")
        private List<DocumentsEntity> documents;

        public UserDetailsEntity getUserDetails() {
            return userDetails;
        }

        public void setUserDetails(UserDetailsEntity userDetails) {
            this.userDetails = userDetails;
        }

        public List<DocumentsEntity> getDocuments() {
            return documents;
        }

        public void setDocuments(List<DocumentsEntity> documents) {
            this.documents = documents;
        }

        public static class UserDetailsEntity {
            /**
             * id : 4
             * parent_id : 0
             * company_id : 0
             * first_name : Parth
             * last_name : Mistry
             * company_name :
             * company_image : null
             * contact_person :
             * profile_picture : null
             * email : parth.samcom2018@gmail.com
             * email_verified_at : null
             * phone_number : +919712146184
             * address : Ahmedabad, Gujarat, India
             * country : India
             * state : Gujarat
             * city : Ahmedabad
             * device_token : eGp5S8NjIHM:APA91bHXz_JGl8b2YSnkvtUYnmcmEHbLB5POYanc1-HR7t2N5ve4VywIRT-gzWIyCBrHufqOFj676R2U7EyDyIEUVu4Bpe7dqZo8nnW1GXA-mf8jDbG8NfSZ5dlBbY7oJdh52v0fZSoc
             * device_type : android
             * latitude : 23.0224734
             * longitude : 72.5715931
             * status : active
             * user_status : online
             * authy_id : 127634683
             * user_unique_code : Parth1004
             * tax_id : null
             * commission_type : ammount
             * commission : null
             * role : driver
             * document_status : pending
             * deleted_at : null
             * created_at : 2019-07-18 06:52:53
             * updated_at : 2019-07-18 07:01:01
             * users_vehicle : {"id":4,"user_id":4,"vin_code":"YV1MS682X62153688","make":"chevrolet","model":"Avalanche","year":"2011","transmission":"Automatic","color":"black","driver_train":"Front wheel drive","car_type":"Cargo van","deleted_at":null,"created_at":"2019-07-18 12:09:45","updated_at":"2019-07-18 12:54:35"}
             * vehicle_status : Yes
             */

            @SerializedName("id")
            private String id;
            @SerializedName("parent_id")
            private String parentId;
            @SerializedName("company_id")
            private String companyId;
            @SerializedName("first_name")
            private String firstName;
            @SerializedName("last_name")
            private String lastName;
            @SerializedName("company_name")
            private String companyName;
            @SerializedName("company_image")
            private String companyImage;
            @SerializedName("contact_person")
            private String contactPerson;
            @SerializedName("profile_picture")
            private String profilePicture;
            @SerializedName("email")
            private String email;
            @SerializedName("email_verified_at")
            private String emailVerifiedAt;
            @SerializedName("phone_number")
            private String phoneNumber;
            @SerializedName("address")
            private String address;
            @SerializedName("country")
            private String country;
            @SerializedName("state")
            private String state;
            @SerializedName("city")
            private String city;
            @SerializedName("device_token")
            private String deviceToken;
            @SerializedName("device_type")
            private String deviceType;
            @SerializedName("latitude")
            private String latitude;
            @SerializedName("longitude")
            private String longitude;
            @SerializedName("status")
            private String status;
            @SerializedName("user_status")
            private String userStatus;
            @SerializedName("authy_id")
            private String authyId;
            @SerializedName("user_unique_code")
            private String userUniqueCode;
            @SerializedName("tax_id")
            private String taxId;
            @SerializedName("commission_type")
            private String commissionType;
            @SerializedName("commission")
            private String commission;
            @SerializedName("role")
            private String role;
            @SerializedName("document_status")
            private String documentStatus;
            @SerializedName("deleted_at")
            private String deletedAt;
            @SerializedName("created_at")
            private String createdAt;
            @SerializedName("updated_at")
            private String updatedAt;
            @SerializedName("users_vehicle")
            private UsersVehicleEntity usersVehicle;
            @SerializedName("vehicle_status")
            private String vehicleStatus;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getParentId() {
                return parentId;
            }

            public void setParentId(String parentId) {
                this.parentId = parentId;
            }

            public String getCompanyId() {
                return companyId;
            }

            public void setCompanyId(String companyId) {
                this.companyId = companyId;
            }

            public String getFirstName() {
                return firstName;
            }

            public void setFirstName(String firstName) {
                this.firstName = firstName;
            }

            public String getLastName() {
                return lastName;
            }

            public void setLastName(String lastName) {
                this.lastName = lastName;
            }

            public String getCompanyName() {
                return companyName;
            }

            public void setCompanyName(String companyName) {
                this.companyName = companyName;
            }

            public String getCompanyImage() {
                return companyImage;
            }

            public void setCompanyImage(String companyImage) {
                this.companyImage = companyImage;
            }

            public String getContactPerson() {
                return contactPerson;
            }

            public void setContactPerson(String contactPerson) {
                this.contactPerson = contactPerson;
            }

            public String getProfilePicture() {
                return profilePicture;
            }

            public void setProfilePicture(String profilePicture) {
                this.profilePicture = profilePicture;
            }

            public String getEmail() {
                return email;
            }

            public void setEmail(String email) {
                this.email = email;
            }

            public String getEmailVerifiedAt() {
                return emailVerifiedAt;
            }

            public void setEmailVerifiedAt(String emailVerifiedAt) {
                this.emailVerifiedAt = emailVerifiedAt;
            }

            public String getPhoneNumber() {
                return phoneNumber;
            }

            public void setPhoneNumber(String phoneNumber) {
                this.phoneNumber = phoneNumber;
            }

            public String getAddress() {
                return address;
            }

            public void setAddress(String address) {
                this.address = address;
            }

            public String getCountry() {
                return country;
            }

            public void setCountry(String country) {
                this.country = country;
            }

            public String getState() {
                return state;
            }

            public void setState(String state) {
                this.state = state;
            }

            public String getCity() {
                return city;
            }

            public void setCity(String city) {
                this.city = city;
            }

            public String getDeviceToken() {
                return deviceToken;
            }

            public void setDeviceToken(String deviceToken) {
                this.deviceToken = deviceToken;
            }

            public String getDeviceType() {
                return deviceType;
            }

            public void setDeviceType(String deviceType) {
                this.deviceType = deviceType;
            }

            public String getLatitude() {
                return latitude;
            }

            public void setLatitude(String latitude) {
                this.latitude = latitude;
            }

            public String getLongitude() {
                return longitude;
            }

            public void setLongitude(String longitude) {
                this.longitude = longitude;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }

            public String getUserStatus() {
                return userStatus;
            }

            public void setUserStatus(String userStatus) {
                this.userStatus = userStatus;
            }

            public String getAuthyId() {
                return authyId;
            }

            public void setAuthyId(String authyId) {
                this.authyId = authyId;
            }

            public String getUserUniqueCode() {
                return userUniqueCode;
            }

            public void setUserUniqueCode(String userUniqueCode) {
                this.userUniqueCode = userUniqueCode;
            }

            public String getTaxId() {
                return taxId;
            }

            public void setTaxId(String taxId) {
                this.taxId = taxId;
            }

            public String getCommissionType() {
                return commissionType;
            }

            public void setCommissionType(String commissionType) {
                this.commissionType = commissionType;
            }

            public String getCommission() {
                return commission;
            }

            public void setCommission(String commission) {
                this.commission = commission;
            }

            public String getRole() {
                return role;
            }

            public void setRole(String role) {
                this.role = role;
            }

            public String getDocumentStatus() {
                return documentStatus;
            }

            public void setDocumentStatus(String documentStatus) {
                this.documentStatus = documentStatus;
            }

            public String getDeletedAt() {
                return deletedAt;
            }

            public void setDeletedAt(String deletedAt) {
                this.deletedAt = deletedAt;
            }

            public String getCreatedAt() {
                return createdAt;
            }

            public void setCreatedAt(String createdAt) {
                this.createdAt = createdAt;
            }

            public String getUpdatedAt() {
                return updatedAt;
            }

            public void setUpdatedAt(String updatedAt) {
                this.updatedAt = updatedAt;
            }

            public UsersVehicleEntity getUsersVehicle() {
                return usersVehicle;
            }

            public void setUsersVehicle(UsersVehicleEntity usersVehicle) {
                this.usersVehicle = usersVehicle;
            }

            public String getVehicleStatus() {
                return vehicleStatus;
            }

            public void setVehicleStatus(String vehicleStatus) {
                this.vehicleStatus = vehicleStatus;
            }

            public static class UsersVehicleEntity {
                /**
                 * id : 4
                 * user_id : 4
                 * vin_code : YV1MS682X62153688
                 * make : chevrolet
                 * model : Avalanche
                 * year : 2011
                 * transmission : Automatic
                 * color : black
                 * driver_train : Front wheel drive
                 * car_type : Cargo van
                 * deleted_at : null
                 * created_at : 2019-07-18 12:09:45
                 * updated_at : 2019-07-18 12:54:35
                 */

                @SerializedName("id")
                private int id;
                @SerializedName("user_id")
                private int userId;
                @SerializedName("vin_code")
                private String vinCode;
                @SerializedName("make")
                private String make;
                @SerializedName("model")
                private String model;
                @SerializedName("year")
                private String year;
                @SerializedName("transmission")
                private String transmission;
                @SerializedName("color")
                private String color;
                @SerializedName("driver_train")
                private String driverTrain;
                @SerializedName("car_type")
                private String carType;
                @SerializedName("deleted_at")
                private Object deletedAt;
                @SerializedName("created_at")
                private String createdAt;
                @SerializedName("updated_at")
                private String updatedAt;

                public int getId() {
                    return id;
                }

                public void setId(int id) {
                    this.id = id;
                }

                public int getUserId() {
                    return userId;
                }

                public void setUserId(int userId) {
                    this.userId = userId;
                }

                public String getVinCode() {
                    return vinCode;
                }

                public void setVinCode(String vinCode) {
                    this.vinCode = vinCode;
                }

                public String getMake() {
                    return make;
                }

                public void setMake(String make) {
                    this.make = make;
                }

                public String getModel() {
                    return model;
                }

                public void setModel(String model) {
                    this.model = model;
                }

                public String getYear() {
                    return year;
                }

                public void setYear(String year) {
                    this.year = year;
                }

                public String getTransmission() {
                    return transmission;
                }

                public void setTransmission(String transmission) {
                    this.transmission = transmission;
                }

                public String getColor() {
                    return color;
                }

                public void setColor(String color) {
                    this.color = color;
                }

                public String getDriverTrain() {
                    return driverTrain;
                }

                public void setDriverTrain(String driverTrain) {
                    this.driverTrain = driverTrain;
                }

                public String getCarType() {
                    return carType;
                }

                public void setCarType(String carType) {
                    this.carType = carType;
                }

                public Object getDeletedAt() {
                    return deletedAt;
                }

                public void setDeletedAt(Object deletedAt) {
                    this.deletedAt = deletedAt;
                }

                public String getCreatedAt() {
                    return createdAt;
                }

                public void setCreatedAt(String createdAt) {
                    this.createdAt = createdAt;
                }

                public String getUpdatedAt() {
                    return updatedAt;
                }

                public void setUpdatedAt(String updatedAt) {
                    this.updatedAt = updatedAt;
                }
            }
        }

        public static class DocumentsEntity {
            /**
             * id : 10
             * user_id : 4
             * document_name : Driving License
             * document_url :
             * expiry_date : null
             * status : pending
             * deleted_at : null
             * created_at : 2019-07-18 12:09:45
             * updated_at : 2019-07-18 12:09:45
             */

            @SerializedName("id")
            private int id;
            @SerializedName("user_id")
            private int userId;
            @SerializedName("document_name")
            private String documentName;
            @SerializedName("document_url")
            private String documentUrl;
            @SerializedName("expiry_date")
            private Object expiryDate;
            @SerializedName("status")
            private String status;
            @SerializedName("deleted_at")
            private Object deletedAt;
            @SerializedName("created_at")
            private String createdAt;
            @SerializedName("updated_at")
            private String updatedAt;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public int getUserId() {
                return userId;
            }

            public void setUserId(int userId) {
                this.userId = userId;
            }

            public String getDocumentName() {
                return documentName;
            }

            public void setDocumentName(String documentName) {
                this.documentName = documentName;
            }

            public String getDocumentUrl() {
                return documentUrl;
            }

            public void setDocumentUrl(String documentUrl) {
                this.documentUrl = documentUrl;
            }

            public Object getExpiryDate() {
                return expiryDate;
            }

            public void setExpiryDate(Object expiryDate) {
                this.expiryDate = expiryDate;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }

            public Object getDeletedAt() {
                return deletedAt;
            }

            public void setDeletedAt(Object deletedAt) {
                this.deletedAt = deletedAt;
            }

            public String getCreatedAt() {
                return createdAt;
            }

            public void setCreatedAt(String createdAt) {
                this.createdAt = createdAt;
            }

            public String getUpdatedAt() {
                return updatedAt;
            }

            public void setUpdatedAt(String updatedAt) {
                this.updatedAt = updatedAt;
            }
        }
    }
}
