package com.rydedispatch.driver.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RespHistory {


    /**
     * error : false
     * data : {"history":{"current_page":1,"data":[{"first_name":"Sandeep","last_name":"Manala","profile_picture":"/assets/images/5d441080d4517.jpg","id":5,"source_location":"Gujarat State Highway 71, Parvati Nagar, Chandkheda, Ahmedabad, Gujarat 380005, India","destination_location":"Gujarat State Highway 41, Ram Nagar, Chandkheda, Ahmedabad, Gujarat 380005, India","status":"completed","completed_time":"2019-08-12 07:08:18","total_cost":"23","schedule_date":null,"is_schedule":0},{"first_name":"samcomredmi6","last_name":"technologies","profile_picture":"/assets/images/5d4e86194722d.jpg","id":4,"source_location":"Vishwakarma Mandir, Harikurpa Society, Chandkheda, Ahmedabad, Gujarat 380005, India","destination_location":"Gujarat State Highway 71, Nigam Nagar, Chandkheda, Ahmedabad, Gujarat 380005, India","status":"completed","completed_time":"2019-08-12 07:05:34","total_cost":"23","schedule_date":null,"is_schedule":0},{"first_name":"Sandeep","last_name":"User","profile_picture":"/assets/images/5d4815e70a1ea.jpg","id":1,"source_location":"Motera, Ahmedabad, Gujarat, India","destination_location":"30, Ram Nagar, Sabarmati, Ahmedabad, Gujarat 380005, India","status":"completed","completed_time":"2019-08-10 13:45:35","total_cost":"23","schedule_date":null,"is_schedule":0}],"first_page_url":"http://159.203.105.17/api/driverHistory?page=1","from":1,"last_page":1,"last_page_url":"http://159.203.105.17/api/driverHistory?page=1","next_page_url":null,"path":"http://159.203.105.17/api/driverHistory","per_page":20,"prev_page_url":null,"to":3,"total":3}}
     * message : Your history has been get successfully.
     */

    @SerializedName("error")
    private boolean error;
    @SerializedName("data")
    private DataEntityX data;
    @SerializedName("message")
    private String message;

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public DataEntityX getData() {
        return data;
    }

    public void setData(DataEntityX data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public static class DataEntityX {
        /**
         * history : {"current_page":1,"data":[{"first_name":"Sandeep","last_name":"Manala","profile_picture":"/assets/images/5d441080d4517.jpg","id":5,"source_location":"Gujarat State Highway 71, Parvati Nagar, Chandkheda, Ahmedabad, Gujarat 380005, India","destination_location":"Gujarat State Highway 41, Ram Nagar, Chandkheda, Ahmedabad, Gujarat 380005, India","status":"completed","completed_time":"2019-08-12 07:08:18","total_cost":"23","schedule_date":null,"is_schedule":0},{"first_name":"samcomredmi6","last_name":"technologies","profile_picture":"/assets/images/5d4e86194722d.jpg","id":4,"source_location":"Vishwakarma Mandir, Harikurpa Society, Chandkheda, Ahmedabad, Gujarat 380005, India","destination_location":"Gujarat State Highway 71, Nigam Nagar, Chandkheda, Ahmedabad, Gujarat 380005, India","status":"completed","completed_time":"2019-08-12 07:05:34","total_cost":"23","schedule_date":null,"is_schedule":0},{"first_name":"Sandeep","last_name":"User","profile_picture":"/assets/images/5d4815e70a1ea.jpg","id":1,"source_location":"Motera, Ahmedabad, Gujarat, India","destination_location":"30, Ram Nagar, Sabarmati, Ahmedabad, Gujarat 380005, India","status":"completed","completed_time":"2019-08-10 13:45:35","total_cost":"23","schedule_date":null,"is_schedule":0}],"first_page_url":"http://159.203.105.17/api/driverHistory?page=1","from":1,"last_page":1,"last_page_url":"http://159.203.105.17/api/driverHistory?page=1","next_page_url":null,"path":"http://159.203.105.17/api/driverHistory","per_page":20,"prev_page_url":null,"to":3,"total":3}
         */

        @SerializedName("history")
        private HistoryEntity history;

        public HistoryEntity getHistory() {
            return history;
        }

        public void setHistory(HistoryEntity history) {
            this.history = history;
        }

        public static class HistoryEntity {
            /**
             * current_page : 1
             * data : [{"first_name":"Sandeep","last_name":"Manala","profile_picture":"/assets/images/5d441080d4517.jpg","id":5,"source_location":"Gujarat State Highway 71, Parvati Nagar, Chandkheda, Ahmedabad, Gujarat 380005, India","destination_location":"Gujarat State Highway 41, Ram Nagar, Chandkheda, Ahmedabad, Gujarat 380005, India","status":"completed","completed_time":"2019-08-12 07:08:18","total_cost":"23","schedule_date":null,"is_schedule":0},{"first_name":"samcomredmi6","last_name":"technologies","profile_picture":"/assets/images/5d4e86194722d.jpg","id":4,"source_location":"Vishwakarma Mandir, Harikurpa Society, Chandkheda, Ahmedabad, Gujarat 380005, India","destination_location":"Gujarat State Highway 71, Nigam Nagar, Chandkheda, Ahmedabad, Gujarat 380005, India","status":"completed","completed_time":"2019-08-12 07:05:34","total_cost":"23","schedule_date":null,"is_schedule":0},{"first_name":"Sandeep","last_name":"User","profile_picture":"/assets/images/5d4815e70a1ea.jpg","id":1,"source_location":"Motera, Ahmedabad, Gujarat, India","destination_location":"30, Ram Nagar, Sabarmati, Ahmedabad, Gujarat 380005, India","status":"completed","completed_time":"2019-08-10 13:45:35","total_cost":"23","schedule_date":null,"is_schedule":0}]
             * first_page_url : http://159.203.105.17/api/driverHistory?page=1
             * from : 1
             * last_page : 1
             * last_page_url : http://159.203.105.17/api/driverHistory?page=1
             * next_page_url : null
             * path : http://159.203.105.17/api/driverHistory
             * per_page : 20
             * prev_page_url : null
             * to : 3
             * total : 3
             */

            @SerializedName("current_page")
            private int currentPage;
            @SerializedName("first_page_url")
            private String firstPageUrl;
            @SerializedName("from")
            private int from;
            @SerializedName("last_page")
            private int lastPage;
            @SerializedName("last_page_url")
            private String lastPageUrl;
            @SerializedName("next_page_url")
            private Object nextPageUrl;
            @SerializedName("path")
            private String path;
            @SerializedName("per_page")
            private int perPage;
            @SerializedName("prev_page_url")
            private Object prevPageUrl;
            @SerializedName("to")
            private int to;
            @SerializedName("total")
            private int total;
            @SerializedName("data")
            private List<DataEntity> data;

            public int getCurrentPage() {
                return currentPage;
            }

            public void setCurrentPage(int currentPage) {
                this.currentPage = currentPage;
            }

            public String getFirstPageUrl() {
                return firstPageUrl;
            }

            public void setFirstPageUrl(String firstPageUrl) {
                this.firstPageUrl = firstPageUrl;
            }

            public int getFrom() {
                return from;
            }

            public void setFrom(int from) {
                this.from = from;
            }

            public int getLastPage() {
                return lastPage;
            }

            public void setLastPage(int lastPage) {
                this.lastPage = lastPage;
            }

            public String getLastPageUrl() {
                return lastPageUrl;
            }

            public void setLastPageUrl(String lastPageUrl) {
                this.lastPageUrl = lastPageUrl;
            }

            public Object getNextPageUrl() {
                return nextPageUrl;
            }

            public void setNextPageUrl(Object nextPageUrl) {
                this.nextPageUrl = nextPageUrl;
            }

            public String getPath() {
                return path;
            }

            public void setPath(String path) {
                this.path = path;
            }

            public int getPerPage() {
                return perPage;
            }

            public void setPerPage(int perPage) {
                this.perPage = perPage;
            }

            public Object getPrevPageUrl() {
                return prevPageUrl;
            }

            public void setPrevPageUrl(Object prevPageUrl) {
                this.prevPageUrl = prevPageUrl;
            }

            public int getTo() {
                return to;
            }

            public void setTo(int to) {
                this.to = to;
            }

            public int getTotal() {
                return total;
            }

            public void setTotal(int total) {
                this.total = total;
            }

            public List<DataEntity> getData() {
                return data;
            }

            public void setData(List<DataEntity> data) {
                this.data = data;
            }

            public static class DataEntity {
                /**
                 * first_name : Sandeep
                 * last_name : Manala
                 * profile_picture : /assets/images/5d441080d4517.jpg
                 * id : 5
                 * source_location : Gujarat State Highway 71, Parvati Nagar, Chandkheda, Ahmedabad, Gujarat 380005, India
                 * destination_location : Gujarat State Highway 41, Ram Nagar, Chandkheda, Ahmedabad, Gujarat 380005, India
                 * status : completed
                 * completed_time : 2019-08-12 07:08:18
                 * total_cost : 23
                 * schedule_date : null
                 * is_schedule : 0
                 */

                @SerializedName("first_name")
                private String firstName;
                @SerializedName("last_name")
                private String lastName;
                @SerializedName("profile_picture")
                private String profilePicture;
                @SerializedName("user_unique_code")
                private String user_unique_code;
                @SerializedName("id")
                private int id;
                @SerializedName("sub_user_id")
                private int sub_user_id;
                @SerializedName("display_passenger_name")
                private String display_passenger_name;
                @SerializedName("passenger_phone_number")
                private String passenger_phone_number;

                @SerializedName("source_location")
                private String sourceLocation;
                @SerializedName("source_latitude")
                private String source_latitude;
                @SerializedName("source_longitude")
                private String source_longitude;

                @SerializedName("destination_location")
                private String destinationLocation;
                @SerializedName("destination_latitude")
                private String destination_latitude;
                @SerializedName("destination_longitude")
                private String destination_longitude;
                @SerializedName("payment_type")
                private String payment_type;
                @SerializedName("status")
                private String status;
                @SerializedName("completed_time")
                private String completedTime;
                @SerializedName("total_cost")
                private String totalCost;
                @SerializedName("schedule_date")
                private String scheduleDate;
                @SerializedName("is_schedule")
                private int isSchedule;
                @SerializedName("ride_instruction")
                private String ride_instruction;

                public String getRide_instruction() {
                    return ride_instruction;
                }

                public void setRide_instruction(String ride_instruction) {
                    this.ride_instruction = ride_instruction;
                }

                public String getPassenger_phone_number() {
                    return passenger_phone_number;
                }

                public void setPassenger_phone_number(String passenger_phone_number) {
                    this.passenger_phone_number = passenger_phone_number;
                }

                public String getPayment_type() {
                    return payment_type;
                }

                public void setPayment_type(String payment_type) {
                    this.payment_type = payment_type;
                }

                public String getFirstName() {
                    return firstName;
                }

                public void setFirstName(String firstName) {
                    this.firstName = firstName;
                }

                public String getLastName() {
                    return lastName;
                }

                public void setLastName(String lastName) {
                    this.lastName = lastName;
                }

                public String getProfilePicture() {
                    return profilePicture;
                }

                public void setProfilePicture(String profilePicture) {
                    this.profilePicture = profilePicture;
                }


                public String getUser_unique_code() {
                    return user_unique_code;
                }

                public void setUser_unique_code(String user_unique_code) {
                    this.user_unique_code = user_unique_code;
                }


                public String getSource_latitude() {
                    return source_latitude;
                }

                public void setSource_latitude(String source_latitude) {
                    this.source_latitude = source_latitude;
                }

                public String getSource_longitude() {
                    return source_longitude;
                }

                public void setSource_longitude(String source_longitude) {
                    this.source_longitude = source_longitude;
                }


                public String getDestination_latitude() {
                    return destination_latitude;
                }

                public void setDestination_latitude(String destination_latitude) {
                    this.destination_latitude = destination_latitude;
                }

                public String getDestination_longitude() {
                    return destination_longitude;
                }

                public void setDestination_longitude(String destination_longitude) {
                    this.destination_longitude = destination_longitude;
                }


                public int getId() {
                    return id;
                }

                public void setId(int id) {
                    this.id = id;
                }


                public int getSub_user_id() {
                    return sub_user_id;
                }

                public void setSub_user_id(int sub_user_id) {
                    this.sub_user_id = sub_user_id;
                }

                public String getDisplay_passenger_name() {
                    return display_passenger_name;
                }

                public void setDisplay_passenger_name(String display_passenger_name) {
                    this.display_passenger_name = display_passenger_name;
                }
                public String getSourceLocation() {
                    return sourceLocation;
                }

                public void setSourceLocation(String sourceLocation) {
                    this.sourceLocation = sourceLocation;
                }

                public String getDestinationLocation() {
                    return destinationLocation;
                }

                public void setDestinationLocation(String destinationLocation) {
                    this.destinationLocation = destinationLocation;
                }

                public String getStatus() {
                    return status;
                }

                public void setStatus(String status) {
                    this.status = status;
                }

                public String getCompletedTime() {
                    return completedTime;
                }

                public void setCompletedTime(String completedTime) {
                    this.completedTime = completedTime;
                }

                public String getTotalCost() {
                    return totalCost;
                }

                public void setTotalCost(String totalCost) {
                    this.totalCost = totalCost;
                }

                public String getScheduleDate() {
                    return scheduleDate;
                }

                public void setScheduleDate(String scheduleDate) {
                    this.scheduleDate = scheduleDate;
                }

                public int getIsSchedule() {
                    return isSchedule;
                }

                public void setIsSchedule(int isSchedule) {
                    this.isSchedule = isSchedule;
                }
            }
        }
    }
}