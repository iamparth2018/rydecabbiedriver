package com.rydedispatch.driver.response;

import com.google.gson.annotations.SerializedName;

public class RespLogin{


    /**
     * error : false
     * data : {"userDetails":{"id":2,"parent_id":0,"company_id":0,"first_name":"Parth","last_name":"Samcom","company_name":"","company_image":null,"contact_person":"","profile_picture":null,"email":"parth.samcom2018@gmail.com","email_verified_at":null,"country_code":"+91","phone_number":"+919712146184","address":"Ranip, Gujarat, India","country":"India","state":"Gujarat","city":"Ahmedabad","city_timezone":"Asia/Calcutta","device_token":"test","device_type":"android","latitude":"0.0","longitude":"0.0","status":"active","user_status":"online","authy_id":"127634683","stripe_customer_id":null,"user_unique_code":"Parth1002","tax_id":null,"commission_type":"ammount","commission":null,"invoice_frequency":"weekly","role":"driver","passenger_rate_card":"","document_status":"completed","previous_pending_amount":null,"deleted_at":null,"created_at":"2019-09-19 06:11:38","updated_at":"2019-09-20 06:10:42","vehicle":{"id":1,"user_id":2,"vin_code":"","car_number":"new","make":"Acura","model":"ILX","year":"2019","transmission":"Automatic","color":"red","driver_train":"4x4","car_type":"Premium","car_type_id":1,"deleted_at":null,"created_at":"2019-09-19 06:16:20","updated_at":"2019-09-19 06:16:52"},"vehicle_status":true,"token":"eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImM1NjliMmZjNGY2NjVhZmVjYjNiYzg5NGY4OWM5NzUzNzhhMTlmYzM4MjRmOGQ5YTJjZWEzYWIwYWYwMDNkODM2M2MyOWFhMGYzMThkZjU0In0.eyJhdWQiOiIzIiwianRpIjoiYzU2OWIyZmM0ZjY2NWFmZWNiM2JjODk0Zjg5Yzk3NTM3OGExOWZjMzgyNGY4ZDlhMmNlYTNhYjBhZjAwM2Q4MzYzYzI5YWEwZjMxOGRmNTQiLCJpYXQiOjE1Njg5NTk4NDIsIm5iZiI6MTU2ODk1OTg0MiwiZXhwIjoxNjAwNTgyMjQyLCJzdWIiOiIyIiwic2NvcGVzIjpbXX0.q-AH6Dj1Z_-dSwTHpF_IeSLquWn465JtwjqvCygIx9u5ScseTv01YdOPZ9Tg4YuZmm66N0U6ERPXPU5qvbEWBKHo1DTEgk-RDO3bgTCj1K_m-3Q0mTqxreJIDyDLdiqUc-WVZ_jXgGkORb8Hnsvju1nmdUqc9DOULYUgSKrheRR6z487An7_pj4XYQ1OFxYkQBY1rtWlm4WGt54qFQgsfEo4Oe06w7Id4WREo7qog7KJrL4X6tWlR1Fzxz30NaDvnJqfDP4bipdS1VdAITPMQMDVdjWbXeE5ZOkyd_OddM1KPK9w9Xt_Xw2ZdcfC_29B48w4d-qCKDdNSNauUfHUzdU9IyNz1NLxNrEaeB2aLO2ljwiqlyg9BvAju6faegpEYX-O85blyuwXza8MaeYqsm4oCWdMwMM_byE5f5vHeP2fKHF1TAwh_FMAvmXEhzis5uzZFda2YJ8ULlP1tqhqbyTCFLH5nRmI-ACDeQxQ-fdXZJ4HlC0ZoFkd-zSc3UEBnkM9-iTj8HuX-bkvw8_nopGBXgIqzuRMlaIR4HAf7mnIKK2HGWGVNPeMCwW-vhmrvhoRPCX-bZg-jXMkxhAaiji3v0LTEWZ_apa5l-G_QJUFOmqw18nFM3Q2EXAgXKCDyzxu078ESo_Scr6HHCl2baUBBCTyCxHehXt0E2OL3t0","support_number":"+16514139117"}}
     * message : Logged in successfully.
     */

    @SerializedName("error")
    private boolean error;
    @SerializedName("data")
    private DataEntity data;
    @SerializedName("message")
    private String message;

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public DataEntity getData() {
        return data;
    }

    public void setData(DataEntity data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public static class DataEntity {
        /**
         * userDetails : {"id":2,"parent_id":0,"company_id":0,"first_name":"Parth","last_name":"Samcom","company_name":"","company_image":null,"contact_person":"","profile_picture":null,"email":"parth.samcom2018@gmail.com","email_verified_at":null,"country_code":"+91","phone_number":"+919712146184","address":"Ranip, Gujarat, India","country":"India","state":"Gujarat","city":"Ahmedabad","city_timezone":"Asia/Calcutta","device_token":"test","device_type":"android","latitude":"0.0","longitude":"0.0","status":"active","user_status":"online","authy_id":"127634683","stripe_customer_id":null,"user_unique_code":"Parth1002","tax_id":null,"commission_type":"ammount","commission":null,"invoice_frequency":"weekly","role":"driver","passenger_rate_card":"","document_status":"completed","previous_pending_amount":null,"deleted_at":null,"created_at":"2019-09-19 06:11:38","updated_at":"2019-09-20 06:10:42","vehicle":{"id":1,"user_id":2,"vin_code":"","car_number":"new","make":"Acura","model":"ILX","year":"2019","transmission":"Automatic","color":"red","driver_train":"4x4","car_type":"Premium","car_type_id":1,"deleted_at":null,"created_at":"2019-09-19 06:16:20","updated_at":"2019-09-19 06:16:52"},"vehicle_status":true,"token":"eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImM1NjliMmZjNGY2NjVhZmVjYjNiYzg5NGY4OWM5NzUzNzhhMTlmYzM4MjRmOGQ5YTJjZWEzYWIwYWYwMDNkODM2M2MyOWFhMGYzMThkZjU0In0.eyJhdWQiOiIzIiwianRpIjoiYzU2OWIyZmM0ZjY2NWFmZWNiM2JjODk0Zjg5Yzk3NTM3OGExOWZjMzgyNGY4ZDlhMmNlYTNhYjBhZjAwM2Q4MzYzYzI5YWEwZjMxOGRmNTQiLCJpYXQiOjE1Njg5NTk4NDIsIm5iZiI6MTU2ODk1OTg0MiwiZXhwIjoxNjAwNTgyMjQyLCJzdWIiOiIyIiwic2NvcGVzIjpbXX0.q-AH6Dj1Z_-dSwTHpF_IeSLquWn465JtwjqvCygIx9u5ScseTv01YdOPZ9Tg4YuZmm66N0U6ERPXPU5qvbEWBKHo1DTEgk-RDO3bgTCj1K_m-3Q0mTqxreJIDyDLdiqUc-WVZ_jXgGkORb8Hnsvju1nmdUqc9DOULYUgSKrheRR6z487An7_pj4XYQ1OFxYkQBY1rtWlm4WGt54qFQgsfEo4Oe06w7Id4WREo7qog7KJrL4X6tWlR1Fzxz30NaDvnJqfDP4bipdS1VdAITPMQMDVdjWbXeE5ZOkyd_OddM1KPK9w9Xt_Xw2ZdcfC_29B48w4d-qCKDdNSNauUfHUzdU9IyNz1NLxNrEaeB2aLO2ljwiqlyg9BvAju6faegpEYX-O85blyuwXza8MaeYqsm4oCWdMwMM_byE5f5vHeP2fKHF1TAwh_FMAvmXEhzis5uzZFda2YJ8ULlP1tqhqbyTCFLH5nRmI-ACDeQxQ-fdXZJ4HlC0ZoFkd-zSc3UEBnkM9-iTj8HuX-bkvw8_nopGBXgIqzuRMlaIR4HAf7mnIKK2HGWGVNPeMCwW-vhmrvhoRPCX-bZg-jXMkxhAaiji3v0LTEWZ_apa5l-G_QJUFOmqw18nFM3Q2EXAgXKCDyzxu078ESo_Scr6HHCl2baUBBCTyCxHehXt0E2OL3t0","support_number":"+16514139117"}
         */

        @SerializedName("userDetails")
        private UserDetailsEntity userDetails;

        public UserDetailsEntity getUserDetails() {
            return userDetails;
        }

        public void setUserDetails(UserDetailsEntity userDetails) {
            this.userDetails = userDetails;
        }

        public static class UserDetailsEntity {
            /**
             * id : 2
             * parent_id : 0
             * company_id : 0
             * first_name : Parth
             * last_name : Samcom
             * company_name :
             * company_image : null
             * contact_person :
             * profile_picture : null
             * email : parth.samcom2018@gmail.com
             * email_verified_at : null
             * country_code : +91
             * phone_number : +919712146184
             * address : Ranip, Gujarat, India
             * country : India
             * state : Gujarat
             * city : Ahmedabad
             * city_timezone : Asia/Calcutta
             * device_token : test
             * device_type : android
             * latitude : 0.0
             * longitude : 0.0
             * status : active
             * user_status : online
             * authy_id : 127634683
             * stripe_customer_id : null
             * user_unique_code : Parth1002
             * tax_id : null
             * commission_type : ammount
             * commission : null
             * invoice_frequency : weekly
             * role : driver
             * passenger_rate_card :
             * document_status : completed
             * previous_pending_amount : null
             * deleted_at : null
             * created_at : 2019-09-19 06:11:38
             * updated_at : 2019-09-20 06:10:42
             * vehicle : {"id":1,"user_id":2,"vin_code":"","car_number":"new","make":"Acura","model":"ILX","year":"2019","transmission":"Automatic","color":"red","driver_train":"4x4","car_type":"Premium","car_type_id":1,"deleted_at":null,"created_at":"2019-09-19 06:16:20","updated_at":"2019-09-19 06:16:52"}
             * vehicle_status : true
             * token : eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImM1NjliMmZjNGY2NjVhZmVjYjNiYzg5NGY4OWM5NzUzNzhhMTlmYzM4MjRmOGQ5YTJjZWEzYWIwYWYwMDNkODM2M2MyOWFhMGYzMThkZjU0In0.eyJhdWQiOiIzIiwianRpIjoiYzU2OWIyZmM0ZjY2NWFmZWNiM2JjODk0Zjg5Yzk3NTM3OGExOWZjMzgyNGY4ZDlhMmNlYTNhYjBhZjAwM2Q4MzYzYzI5YWEwZjMxOGRmNTQiLCJpYXQiOjE1Njg5NTk4NDIsIm5iZiI6MTU2ODk1OTg0MiwiZXhwIjoxNjAwNTgyMjQyLCJzdWIiOiIyIiwic2NvcGVzIjpbXX0.q-AH6Dj1Z_-dSwTHpF_IeSLquWn465JtwjqvCygIx9u5ScseTv01YdOPZ9Tg4YuZmm66N0U6ERPXPU5qvbEWBKHo1DTEgk-RDO3bgTCj1K_m-3Q0mTqxreJIDyDLdiqUc-WVZ_jXgGkORb8Hnsvju1nmdUqc9DOULYUgSKrheRR6z487An7_pj4XYQ1OFxYkQBY1rtWlm4WGt54qFQgsfEo4Oe06w7Id4WREo7qog7KJrL4X6tWlR1Fzxz30NaDvnJqfDP4bipdS1VdAITPMQMDVdjWbXeE5ZOkyd_OddM1KPK9w9Xt_Xw2ZdcfC_29B48w4d-qCKDdNSNauUfHUzdU9IyNz1NLxNrEaeB2aLO2ljwiqlyg9BvAju6faegpEYX-O85blyuwXza8MaeYqsm4oCWdMwMM_byE5f5vHeP2fKHF1TAwh_FMAvmXEhzis5uzZFda2YJ8ULlP1tqhqbyTCFLH5nRmI-ACDeQxQ-fdXZJ4HlC0ZoFkd-zSc3UEBnkM9-iTj8HuX-bkvw8_nopGBXgIqzuRMlaIR4HAf7mnIKK2HGWGVNPeMCwW-vhmrvhoRPCX-bZg-jXMkxhAaiji3v0LTEWZ_apa5l-G_QJUFOmqw18nFM3Q2EXAgXKCDyzxu078ESo_Scr6HHCl2baUBBCTyCxHehXt0E2OL3t0
             * support_number : +16514139117
             */

            @SerializedName("id")
            private String id;
            @SerializedName("parent_id")
            private String parentId;
            @SerializedName("company_id")
            private String companyId;
            @SerializedName("first_name")
            private String firstName;
            @SerializedName("last_name")
            private String lastName;
            @SerializedName("company_name")
            private String companyName;
            @SerializedName("company_image")
            private String companyImage;
            @SerializedName("contact_person")
            private String contactPerson;
            @SerializedName("profile_picture")
            private String profilePicture;
            @SerializedName("email")
            private String email;
            @SerializedName("email_verified_at")
            private String emailVerifiedAt;
            @SerializedName("country_code")
            private String countryCode;
            @SerializedName("phone_number")
            private String phoneNumber;
            @SerializedName("address")
            private String address;
            @SerializedName("country")
            private String country;
            @SerializedName("state")
            private String state;
            @SerializedName("city")
            private String city;
            @SerializedName("city_timezone")
            private String cityTimezone;
            @SerializedName("device_token")
            private String deviceToken;
            @SerializedName("device_type")
            private String deviceType;
            @SerializedName("latitude")
            private String latitude;
            @SerializedName("longitude")
            private String longitude;
            @SerializedName("status")
            private String status;
            @SerializedName("user_status")
            private String userStatus;
            @SerializedName("authy_id")
            private String authyId;
            @SerializedName("stripe_customer_id")
            private String stripeCustomerId;
            @SerializedName("user_unique_code")
            private String userUniqueCode;
            @SerializedName("tax_id")
            private String taxId;
            @SerializedName("commission_type")
            private String commissionType;
            @SerializedName("commission")
            private String commission;
            @SerializedName("invoice_frequency")
            private String invoiceFrequency;
            @SerializedName("role")
            private String role;
            @SerializedName("passenger_rate_card")
            private String passengerRateCard;
            @SerializedName("document_status")
            private String documentStatus;
            @SerializedName("previous_pending_amount")
            private String previousPendingAmount;
            @SerializedName("deleted_at")
            private String deletedAt;
            @SerializedName("created_at")
            private String createdAt;
            @SerializedName("updated_at")
            private String updatedAt;
            @SerializedName("vehicle")
            private VehicleEntity vehicle;
            @SerializedName("vehicle_status")
            private boolean vehicleStatus;
            @SerializedName("token")
            private String token;
            @SerializedName("support_number")
            private String supportNumber;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getParentId() {
                return parentId;
            }

            public void setParentId(String parentId) {
                this.parentId = parentId;
            }

            public String getCompanyId() {
                return companyId;
            }

            public void setCompanyId(String companyId) {
                this.companyId = companyId;
            }

            public String getFirstName() {
                return firstName;
            }

            public void setFirstName(String firstName) {
                this.firstName = firstName;
            }

            public String getLastName() {
                return lastName;
            }

            public void setLastName(String lastName) {
                this.lastName = lastName;
            }

            public String getCompanyName() {
                return companyName;
            }

            public void setCompanyName(String companyName) {
                this.companyName = companyName;
            }

            public String getCompanyImage() {
                return companyImage;
            }

            public void setCompanyImage(String companyImage) {
                this.companyImage = companyImage;
            }

            public String getContactPerson() {
                return contactPerson;
            }

            public void setContactPerson(String contactPerson) {
                this.contactPerson = contactPerson;
            }

            public String getProfilePicture() {
                return profilePicture;
            }

            public void setProfilePicture(String profilePicture) {
                this.profilePicture = profilePicture;
            }

            public String getEmail() {
                return email;
            }

            public void setEmail(String email) {
                this.email = email;
            }

            public String getEmailVerifiedAt() {
                return emailVerifiedAt;
            }

            public void setEmailVerifiedAt(String emailVerifiedAt) {
                this.emailVerifiedAt = emailVerifiedAt;
            }

            public String getCountryCode() {
                return countryCode;
            }

            public void setCountryCode(String countryCode) {
                this.countryCode = countryCode;
            }

            public String getPhoneNumber() {
                return phoneNumber;
            }

            public void setPhoneNumber(String phoneNumber) {
                this.phoneNumber = phoneNumber;
            }

            public String getAddress() {
                return address;
            }

            public void setAddress(String address) {
                this.address = address;
            }

            public String getCountry() {
                return country;
            }

            public void setCountry(String country) {
                this.country = country;
            }

            public String getState() {
                return state;
            }

            public void setState(String state) {
                this.state = state;
            }

            public String getCity() {
                return city;
            }

            public void setCity(String city) {
                this.city = city;
            }

            public String getCityTimezone() {
                return cityTimezone;
            }

            public void setCityTimezone(String cityTimezone) {
                this.cityTimezone = cityTimezone;
            }

            public String getDeviceToken() {
                return deviceToken;
            }

            public void setDeviceToken(String deviceToken) {
                this.deviceToken = deviceToken;
            }

            public String getDeviceType() {
                return deviceType;
            }

            public void setDeviceType(String deviceType) {
                this.deviceType = deviceType;
            }

            public String getLatitude() {
                return latitude;
            }

            public void setLatitude(String latitude) {
                this.latitude = latitude;
            }

            public String getLongitude() {
                return longitude;
            }

            public void setLongitude(String longitude) {
                this.longitude = longitude;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }

            public String getUserStatus() {
                return userStatus;
            }

            public void setUserStatus(String userStatus) {
                this.userStatus = userStatus;
            }

            public String getAuthyId() {
                return authyId;
            }

            public void setAuthyId(String authyId) {
                this.authyId = authyId;
            }

            public String getStripeCustomerId() {
                return stripeCustomerId;
            }

            public void setStripeCustomerId(String stripeCustomerId) {
                this.stripeCustomerId = stripeCustomerId;
            }

            public String getUserUniqueCode() {
                return userUniqueCode;
            }

            public void setUserUniqueCode(String userUniqueCode) {
                this.userUniqueCode = userUniqueCode;
            }

            public String getTaxId() {
                return taxId;
            }

            public void setTaxId(String taxId) {
                this.taxId = taxId;
            }

            public String getCommissionType() {
                return commissionType;
            }

            public void setCommissionType(String commissionType) {
                this.commissionType = commissionType;
            }

            public String getCommission() {
                return commission;
            }

            public void setCommission(String commission) {
                this.commission = commission;
            }

            public String getInvoiceFrequency() {
                return invoiceFrequency;
            }

            public void setInvoiceFrequency(String invoiceFrequency) {
                this.invoiceFrequency = invoiceFrequency;
            }

            public String getRole() {
                return role;
            }

            public void setRole(String role) {
                this.role = role;
            }

            public String getPassengerRateCard() {
                return passengerRateCard;
            }

            public void setPassengerRateCard(String passengerRateCard) {
                this.passengerRateCard = passengerRateCard;
            }

            public String getDocumentStatus() {
                return documentStatus;
            }

            public void setDocumentStatus(String documentStatus) {
                this.documentStatus = documentStatus;
            }

            public String getPreviousPendingAmount() {
                return previousPendingAmount;
            }

            public void setPreviousPendingAmount(String previousPendingAmount) {
                this.previousPendingAmount = previousPendingAmount;
            }

            public String getDeletedAt() {
                return deletedAt;
            }

            public void setDeletedAt(String deletedAt) {
                this.deletedAt = deletedAt;
            }

            public String getCreatedAt() {
                return createdAt;
            }

            public void setCreatedAt(String createdAt) {
                this.createdAt = createdAt;
            }

            public String getUpdatedAt() {
                return updatedAt;
            }

            public void setUpdatedAt(String updatedAt) {
                this.updatedAt = updatedAt;
            }

            public VehicleEntity getVehicle() {
                return vehicle;
            }

            public void setVehicle(VehicleEntity vehicle) {
                this.vehicle = vehicle;
            }

            public boolean isVehicleStatus() {
                return vehicleStatus;
            }

            public void setVehicleStatus(boolean vehicleStatus) {
                this.vehicleStatus = vehicleStatus;
            }

            public String getToken() {
                return token;
            }

            public void setToken(String token) {
                this.token = token;
            }

            public String getSupportNumber() {
                return supportNumber;
            }

            public void setSupportNumber(String supportNumber) {
                this.supportNumber = supportNumber;
            }

            public static class VehicleEntity {
                /**
                 * id : 1
                 * user_id : 2
                 * vin_code :
                 * car_number : new
                 * make : Acura
                 * model : ILX
                 * year : 2019
                 * transmission : Automatic
                 * color : red
                 * driver_train : 4x4
                 * car_type : Premium
                 * car_type_id : 1
                 * deleted_at : null
                 * created_at : 2019-09-19 06:16:20
                 * updated_at : 2019-09-19 06:16:52
                 */

                @SerializedName("id")
                private int id;
                @SerializedName("user_id")
                private int userId;
                @SerializedName("vin_code")
                private String vinCode;
                @SerializedName("car_number")
                private String carNumber;
                @SerializedName("make")
                private String make;
                @SerializedName("model")
                private String model;
                @SerializedName("year")
                private String year;
                @SerializedName("transmission")
                private String transmission;
                @SerializedName("color")
                private String color;
                @SerializedName("driver_train")
                private String driverTrain;
                @SerializedName("car_type")
                private String carType;
                @SerializedName("car_type_id")
                private int carTypeId;
                @SerializedName("deleted_at")
                private String deletedAt;
                @SerializedName("created_at")
                private String createdAt;
                @SerializedName("updated_at")
                private String updatedAt;

                public int getId() {
                    return id;
                }

                public void setId(int id) {
                    this.id = id;
                }

                public int getUserId() {
                    return userId;
                }

                public void setUserId(int userId) {
                    this.userId = userId;
                }

                public String getVinCode() {
                    return vinCode;
                }

                public void setVinCode(String vinCode) {
                    this.vinCode = vinCode;
                }

                public String getCarNumber() {
                    return carNumber;
                }

                public void setCarNumber(String carNumber) {
                    this.carNumber = carNumber;
                }

                public String getMake() {
                    return make;
                }

                public void setMake(String make) {
                    this.make = make;
                }

                public String getModel() {
                    return model;
                }

                public void setModel(String model) {
                    this.model = model;
                }

                public String getYear() {
                    return year;
                }

                public void setYear(String year) {
                    this.year = year;
                }

                public String getTransmission() {
                    return transmission;
                }

                public void setTransmission(String transmission) {
                    this.transmission = transmission;
                }

                public String getColor() {
                    return color;
                }

                public void setColor(String color) {
                    this.color = color;
                }

                public String getDriverTrain() {
                    return driverTrain;
                }

                public void setDriverTrain(String driverTrain) {
                    this.driverTrain = driverTrain;
                }

                public String getCarType() {
                    return carType;
                }

                public void setCarType(String carType) {
                    this.carType = carType;
                }

                public int getCarTypeId() {
                    return carTypeId;
                }

                public void setCarTypeId(int carTypeId) {
                    this.carTypeId = carTypeId;
                }

                public String getDeletedAt() {
                    return deletedAt;
                }

                public void setDeletedAt(String deletedAt) {
                    this.deletedAt = deletedAt;
                }

                public String getCreatedAt() {
                    return createdAt;
                }

                public void setCreatedAt(String createdAt) {
                    this.createdAt = createdAt;
                }

                public String getUpdatedAt() {
                    return updatedAt;
                }

                public void setUpdatedAt(String updatedAt) {
                    this.updatedAt = updatedAt;
                }
            }
        }
    }
}
