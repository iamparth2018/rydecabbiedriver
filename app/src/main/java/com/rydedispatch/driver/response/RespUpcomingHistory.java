package com.rydedispatch.driver.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RespUpcomingHistory {


    /**
     * error : false
     * data : {"history":{"current_page":1,"data":[{"first_name":"Hennepin","last_name":"County","profile_picture":"https://rydedispatch.nyc3.digitaloceanspaces.com/03uPVNnG0VOWszEsbuviFcNgAGPGk32vYBpVWpzf.png","id":19,"source_location":"RTO Cir, Ranip, Ahmedabad, Gujarat, India","destination_location":"Motera, Ahmedabad, Gujarat, India","status":"pending","completed_time":null,"total_cost":null,"actual_cost_without_commission":null,"commission":null,"schedule_date":"2019-09-24 01:30:30","is_schedule":1},{"first_name":"Hennepin","last_name":"County","profile_picture":"https://rydedispatch.nyc3.digitaloceanspaces.com/03uPVNnG0VOWszEsbuviFcNgAGPGk32vYBpVWpzf.png","id":18,"source_location":"Motera, Ahmedabad, Gujarat, India","destination_location":"RTO Cir, Ranip, Ahmedabad, Gujarat, India","status":"pending","completed_time":null,"total_cost":null,"actual_cost_without_commission":null,"commission":null,"schedule_date":"2019-09-24 06:30:00","is_schedule":1},{"first_name":"Hennepin","last_name":"County","profile_picture":"https://rydedispatch.nyc3.digitaloceanspaces.com/03uPVNnG0VOWszEsbuviFcNgAGPGk32vYBpVWpzf.png","id":17,"source_location":"Motera, Ahmedabad, Gujarat, India","destination_location":"RTO Cir, Ranip, Ahmedabad, Gujarat, India","status":"pending","completed_time":null,"total_cost":null,"actual_cost_without_commission":null,"commission":null,"schedule_date":"2019-09-24 05:30:32","is_schedule":1}],"first_page_url":"http://159.203.105.17/api/driverHistory?page=1","from":1,"last_page":1,"last_page_url":"http://159.203.105.17/api/driverHistory?page=1","next_page_url":null,"path":"http://159.203.105.17/api/driverHistory","per_page":20,"prev_page_url":null,"to":3,"total":3}}
     * message : Your history has been get successfully.
     */

    @SerializedName("error")
    private boolean error;
    @SerializedName("data")
    private DataEntityX data;
    @SerializedName("message")
    private String message;

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public DataEntityX getData() {
        return data;
    }

    public void setData(DataEntityX data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public static class DataEntityX {
        /**
         * history : {"current_page":1,"data":[{"first_name":"Hennepin","last_name":"County","profile_picture":"https://rydedispatch.nyc3.digitaloceanspaces.com/03uPVNnG0VOWszEsbuviFcNgAGPGk32vYBpVWpzf.png","id":19,"source_location":"RTO Cir, Ranip, Ahmedabad, Gujarat, India","destination_location":"Motera, Ahmedabad, Gujarat, India","status":"pending","completed_time":null,"total_cost":null,"actual_cost_without_commission":null,"commission":null,"schedule_date":"2019-09-24 01:30:30","is_schedule":1},{"first_name":"Hennepin","last_name":"County","profile_picture":"https://rydedispatch.nyc3.digitaloceanspaces.com/03uPVNnG0VOWszEsbuviFcNgAGPGk32vYBpVWpzf.png","id":18,"source_location":"Motera, Ahmedabad, Gujarat, India","destination_location":"RTO Cir, Ranip, Ahmedabad, Gujarat, India","status":"pending","completed_time":null,"total_cost":null,"actual_cost_without_commission":null,"commission":null,"schedule_date":"2019-09-24 06:30:00","is_schedule":1},{"first_name":"Hennepin","last_name":"County","profile_picture":"https://rydedispatch.nyc3.digitaloceanspaces.com/03uPVNnG0VOWszEsbuviFcNgAGPGk32vYBpVWpzf.png","id":17,"source_location":"Motera, Ahmedabad, Gujarat, India","destination_location":"RTO Cir, Ranip, Ahmedabad, Gujarat, India","status":"pending","completed_time":null,"total_cost":null,"actual_cost_without_commission":null,"commission":null,"schedule_date":"2019-09-24 05:30:32","is_schedule":1}],"first_page_url":"http://159.203.105.17/api/driverHistory?page=1","from":1,"last_page":1,"last_page_url":"http://159.203.105.17/api/driverHistory?page=1","next_page_url":null,"path":"http://159.203.105.17/api/driverHistory","per_page":20,"prev_page_url":null,"to":3,"total":3}
         */

        @SerializedName("history")
        private HistoryEntity history;

        public HistoryEntity getHistory() {
            return history;
        }

        public void setHistory(HistoryEntity history) {
            this.history = history;
        }

        public static class HistoryEntity {
            /**
             * current_page : 1
             * data : [{"first_name":"Hennepin","last_name":"County","profile_picture":"https://rydedispatch.nyc3.digitaloceanspaces.com/03uPVNnG0VOWszEsbuviFcNgAGPGk32vYBpVWpzf.png","id":19,"source_location":"RTO Cir, Ranip, Ahmedabad, Gujarat, India","destination_location":"Motera, Ahmedabad, Gujarat, India","status":"pending","completed_time":null,"total_cost":null,"actual_cost_without_commission":null,"commission":null,"schedule_date":"2019-09-24 01:30:30","is_schedule":1},{"first_name":"Hennepin","last_name":"County","profile_picture":"https://rydedispatch.nyc3.digitaloceanspaces.com/03uPVNnG0VOWszEsbuviFcNgAGPGk32vYBpVWpzf.png","id":18,"source_location":"Motera, Ahmedabad, Gujarat, India","destination_location":"RTO Cir, Ranip, Ahmedabad, Gujarat, India","status":"pending","completed_time":null,"total_cost":null,"actual_cost_without_commission":null,"commission":null,"schedule_date":"2019-09-24 06:30:00","is_schedule":1},{"first_name":"Hennepin","last_name":"County","profile_picture":"https://rydedispatch.nyc3.digitaloceanspaces.com/03uPVNnG0VOWszEsbuviFcNgAGPGk32vYBpVWpzf.png","id":17,"source_location":"Motera, Ahmedabad, Gujarat, India","destination_location":"RTO Cir, Ranip, Ahmedabad, Gujarat, India","status":"pending","completed_time":null,"total_cost":null,"actual_cost_without_commission":null,"commission":null,"schedule_date":"2019-09-24 05:30:32","is_schedule":1}]
             * first_page_url : http://159.203.105.17/api/driverHistory?page=1
             * from : 1
             * last_page : 1
             * last_page_url : http://159.203.105.17/api/driverHistory?page=1
             * next_page_url : null
             * path : http://159.203.105.17/api/driverHistory
             * per_page : 20
             * prev_page_url : null
             * to : 3
             * total : 3
             */

            @SerializedName("current_page")
            private int currentPage;
            @SerializedName("first_page_url")
            private String firstPageUrl;
            @SerializedName("from")
            private int from;
            @SerializedName("last_page")
            private int lastPage;
            @SerializedName("last_page_url")
            private String lastPageUrl;
            @SerializedName("next_page_url")
            private Object nextPageUrl;
            @SerializedName("path")
            private String path;
            @SerializedName("per_page")
            private int perPage;
            @SerializedName("prev_page_url")
            private Object prevPageUrl;
            @SerializedName("to")
            private int to;
            @SerializedName("total")
            private int total;
            @SerializedName("data")
            private List<DataEntity> data;

            public int getCurrentPage() {
                return currentPage;
            }

            public void setCurrentPage(int currentPage) {
                this.currentPage = currentPage;
            }

            public String getFirstPageUrl() {
                return firstPageUrl;
            }

            public void setFirstPageUrl(String firstPageUrl) {
                this.firstPageUrl = firstPageUrl;
            }

            public int getFrom() {
                return from;
            }

            public void setFrom(int from) {
                this.from = from;
            }

            public int getLastPage() {
                return lastPage;
            }

            public void setLastPage(int lastPage) {
                this.lastPage = lastPage;
            }

            public String getLastPageUrl() {
                return lastPageUrl;
            }

            public void setLastPageUrl(String lastPageUrl) {
                this.lastPageUrl = lastPageUrl;
            }

            public Object getNextPageUrl() {
                return nextPageUrl;
            }

            public void setNextPageUrl(Object nextPageUrl) {
                this.nextPageUrl = nextPageUrl;
            }

            public String getPath() {
                return path;
            }

            public void setPath(String path) {
                this.path = path;
            }

            public int getPerPage() {
                return perPage;
            }

            public void setPerPage(int perPage) {
                this.perPage = perPage;
            }

            public Object getPrevPageUrl() {
                return prevPageUrl;
            }

            public void setPrevPageUrl(Object prevPageUrl) {
                this.prevPageUrl = prevPageUrl;
            }

            public int getTo() {
                return to;
            }

            public void setTo(int to) {
                this.to = to;
            }

            public int getTotal() {
                return total;
            }

            public void setTotal(int total) {
                this.total = total;
            }

            public List<DataEntity> getData() {
                return data;
            }

            public void setData(List<DataEntity> data) {
                this.data = data;
            }

            public static class DataEntity {
                /**
                 * first_name : Hennepin
                 * last_name : County
                 * profile_picture : https://rydedispatch.nyc3.digitaloceanspaces.com/03uPVNnG0VOWszEsbuviFcNgAGPGk32vYBpVWpzf.png
                 * id : 19
                 * source_location : RTO Cir, Ranip, Ahmedabad, Gujarat, India
                 * destination_location : Motera, Ahmedabad, Gujarat, India
                 * status : pending
                 * completed_time : null
                 * total_cost : null
                 * actual_cost_without_commission : null
                 * commission : null
                 * schedule_date : 2019-09-24 01:30:30
                 * is_schedule : 1
                 */

                @SerializedName("first_name")
                private String firstName;
                @SerializedName("last_name")
                private String lastName;
                @SerializedName("profile_picture")
                private String profilePicture;
                @SerializedName("id")
                private int id;
                @SerializedName("source_location")
                private String sourceLocation;
                @SerializedName("destination_location")
                private String destinationLocation;
                @SerializedName("status")
                private String status;
                @SerializedName("completed_time")
                private Object completedTime;
                @SerializedName("total_cost")
                private Object totalCost;
                @SerializedName("actual_cost_without_commission")
                private Object actualCostWithoutCommission;
                @SerializedName("commission")
                private Object commission;
                @SerializedName("schedule_date")
                private String scheduleDate;
                @SerializedName("is_schedule")
                private int isSchedule;

                public String getFirstName() {
                    return firstName;
                }

                public void setFirstName(String firstName) {
                    this.firstName = firstName;
                }

                public String getLastName() {
                    return lastName;
                }

                public void setLastName(String lastName) {
                    this.lastName = lastName;
                }

                public String getProfilePicture() {
                    return profilePicture;
                }

                public void setProfilePicture(String profilePicture) {
                    this.profilePicture = profilePicture;
                }

                public int getId() {
                    return id;
                }

                public void setId(int id) {
                    this.id = id;
                }

                public String getSourceLocation() {
                    return sourceLocation;
                }

                public void setSourceLocation(String sourceLocation) {
                    this.sourceLocation = sourceLocation;
                }

                public String getDestinationLocation() {
                    return destinationLocation;
                }

                public void setDestinationLocation(String destinationLocation) {
                    this.destinationLocation = destinationLocation;
                }

                public String getStatus() {
                    return status;
                }

                public void setStatus(String status) {
                    this.status = status;
                }

                public Object getCompletedTime() {
                    return completedTime;
                }

                public void setCompletedTime(Object completedTime) {
                    this.completedTime = completedTime;
                }

                public Object getTotalCost() {
                    return totalCost;
                }

                public void setTotalCost(Object totalCost) {
                    this.totalCost = totalCost;
                }

                public Object getActualCostWithoutCommission() {
                    return actualCostWithoutCommission;
                }

                public void setActualCostWithoutCommission(Object actualCostWithoutCommission) {
                    this.actualCostWithoutCommission = actualCostWithoutCommission;
                }

                public Object getCommission() {
                    return commission;
                }

                public void setCommission(Object commission) {
                    this.commission = commission;
                }

                public String getScheduleDate() {
                    return scheduleDate;
                }

                public void setScheduleDate(String scheduleDate) {
                    this.scheduleDate = scheduleDate;
                }

                public int getIsSchedule() {
                    return isSchedule;
                }

                public void setIsSchedule(int isSchedule) {
                    this.isSchedule = isSchedule;
                }
            }
        }
    }
}
