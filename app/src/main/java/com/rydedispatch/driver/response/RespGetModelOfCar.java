package com.rydedispatch.driver.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RespGetModelOfCar {


    /**
     * error : false
     * data : [{"model_name":"ILX"},{"model_name":"MDX"},{"model_name":"NSX"},{"model_name":"RDX"},{"model_name":"RLX"},{"model_name":"TLX"}]
     * message : Successfully retrieve Models.
     */

    @SerializedName("error")
    private boolean error;
    @SerializedName("message")
    private String message;
    @SerializedName("data")
    private List<DataEntity> data;

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataEntity> getData() {
        return data;
    }

    public void setData(List<DataEntity> data) {
        this.data = data;
    }

    public static class DataEntity {
        /**
         * model_name : ILX
         */

        @SerializedName("model_name")
        private String modelName;

        public String getModelName() {
            return modelName;
        }

        public void setModelName(String modelName) {
            this.modelName = modelName;
        }
    }
}
