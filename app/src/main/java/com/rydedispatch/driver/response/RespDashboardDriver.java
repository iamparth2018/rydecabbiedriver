package com.rydedispatch.driver.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RespDashboardDriver {


    /**
     * error : false
     * data : {"userDetails":{"id":33,"parent_id":0,"company_id":0,"first_name":"Parthh","last_name":"Mistryy","company_name":"","company_image":null,"contact_person":"","profile_picture":"/assets/images/5d2c58bdb4ad3.png","email":"parthmistry.samcom2018@gmail.com","email_verified_at":null,"phone_number":"+919712146184","address":"4 D Square Mall, ONGC Circle, near tune hotel, Chandkheda, Ahmedabad, Gujarat, India","country":"India","state":"Gujarat","city":"Ahmedabad","device_token":"eGp5S8NjIHM:APA91bHXz_JGl8b2YSnkvtUYnmcmEHbLB5POYanc1-HR7t2N5ve4VywIRT-gzWIyCBrHufqOFj676R2U7EyDyIEUVu4Bpe7dqZo8nnW1GXA-mf8jDbG8NfSZ5dlBbY7oJdh52v0fZSoc","device_type":"android","latitude":"23.1029233","longitude":"72.59565090000001","status":"active","user_status":"online","authy_id":"127634683","user_unique_code":"Parth1033","tax_id":null,"commission_type":"ammount","commission":null,"role":"driver","document_status":"pending","deleted_at":null,"created_at":"2019-07-11 06:45:02","updated_at":"2019-07-16 12:11:50","users_vehicle":{"id":2,"user_id":33,"vin_code":"123456","make":"BMW","model":"M3","year":"2019","transmission":"4X4","color":"black","driver_train":"Front","deleted_at":null,"created_at":"2019-07-11 06:46:09","updated_at":"2019-07-16 10:40:00"},"vehicle":{"id":2,"user_id":33,"vin_code":"123456","make":"BMW","model":"M3","year":"2019","transmission":"4X4","color":"black","driver_train":"Front","deleted_at":null,"created_at":"2019-07-11 06:46:09","updated_at":"2019-07-16 10:40:00"},"vehicle_status":"Yes"},"documents":[{"id":1,"user_id":33,"document_name":"Driving License","document_url":"","expiry_date":null,"status":"pending","deleted_at":null,"created_at":"2019-07-16 11:32:48","updated_at":"2019-07-16 11:32:48"},{"id":2,"user_id":33,"document_name":"Insurance","document_url":"","expiry_date":null,"status":"pending","deleted_at":null,"created_at":"2019-07-16 11:32:48","updated_at":"2019-07-16 11:32:48"},{"id":3,"user_id":33,"document_name":"Inspection Certificate","document_url":"","expiry_date":null,"status":"pending","deleted_at":null,"created_at":"2019-07-16 11:32:48","updated_at":"2019-07-16 11:32:48"},{"id":4,"user_id":33,"document_name":"Operators Card (Double disc)","document_url":"","expiry_date":null,"status":"pending","deleted_at":null,"created_at":"2019-07-16 11:32:48","updated_at":"2019-07-16 11:32:48"},{"id":5,"user_id":33,"document_name":"STS Certification ( Minnesota )","document_url":"","expiry_date":null,"status":"pending","deleted_at":null,"created_at":"2019-07-16 11:32:48","updated_at":"2019-07-16 11:32:48"},{"id":6,"user_id":33,"document_name":"Registration Certificate","document_url":"","expiry_date":null,"status":"pending","deleted_at":null,"created_at":"2019-07-16 11:32:48","updated_at":"2019-07-16 11:32:48"},{"id":7,"user_id":33,"document_name":"Police Clearance Certificate","document_url":"","expiry_date":null,"status":"pending","deleted_at":null,"created_at":"2019-07-16 11:32:48","updated_at":"2019-07-16 11:32:48"}]}
     * message : Dashboard has been load successfully
     */

    @SerializedName("error")
    private boolean error;
    @SerializedName("data")
    private DataEntity data;
    @SerializedName("message")
    private String message;

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public DataEntity getData() {
        return data;
    }

    public void setData(DataEntity data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public static class DataEntity {
        /**
         * userDetails : {"id":33,"parent_id":0,"company_id":0,"first_name":"Parthh","last_name":"Mistryy","company_name":"","company_image":null,"contact_person":"","profile_picture":"/assets/images/5d2c58bdb4ad3.png","email":"parthmistry.samcom2018@gmail.com","email_verified_at":null,"phone_number":"+919712146184","address":"4 D Square Mall, ONGC Circle, near tune hotel, Chandkheda, Ahmedabad, Gujarat, India","country":"India","state":"Gujarat","city":"Ahmedabad","device_token":"eGp5S8NjIHM:APA91bHXz_JGl8b2YSnkvtUYnmcmEHbLB5POYanc1-HR7t2N5ve4VywIRT-gzWIyCBrHufqOFj676R2U7EyDyIEUVu4Bpe7dqZo8nnW1GXA-mf8jDbG8NfSZ5dlBbY7oJdh52v0fZSoc","device_type":"android","latitude":"23.1029233","longitude":"72.59565090000001","status":"active","user_status":"online","authy_id":"127634683","user_unique_code":"Parth1033","tax_id":null,"commission_type":"ammount","commission":null,"role":"driver","document_status":"pending","deleted_at":null,"created_at":"2019-07-11 06:45:02","updated_at":"2019-07-16 12:11:50","users_vehicle":{"id":2,"user_id":33,"vin_code":"123456","make":"BMW","model":"M3","year":"2019","transmission":"4X4","color":"black","driver_train":"Front","deleted_at":null,"created_at":"2019-07-11 06:46:09","updated_at":"2019-07-16 10:40:00"},"vehicle":{"id":2,"user_id":33,"vin_code":"123456","make":"BMW","model":"M3","year":"2019","transmission":"4X4","color":"black","driver_train":"Front","deleted_at":null,"created_at":"2019-07-11 06:46:09","updated_at":"2019-07-16 10:40:00"},"vehicle_status":"Yes"}
         * documents : [{"id":1,"user_id":33,"document_name":"Driving License","document_url":"","expiry_date":null,"status":"pending","deleted_at":null,"created_at":"2019-07-16 11:32:48","updated_at":"2019-07-16 11:32:48"},{"id":2,"user_id":33,"document_name":"Insurance","document_url":"","expiry_date":null,"status":"pending","deleted_at":null,"created_at":"2019-07-16 11:32:48","updated_at":"2019-07-16 11:32:48"},{"id":3,"user_id":33,"document_name":"Inspection Certificate","document_url":"","expiry_date":null,"status":"pending","deleted_at":null,"created_at":"2019-07-16 11:32:48","updated_at":"2019-07-16 11:32:48"},{"id":4,"user_id":33,"document_name":"Operators Card (Double disc)","document_url":"","expiry_date":null,"status":"pending","deleted_at":null,"created_at":"2019-07-16 11:32:48","updated_at":"2019-07-16 11:32:48"},{"id":5,"user_id":33,"document_name":"STS Certification ( Minnesota )","document_url":"","expiry_date":null,"status":"pending","deleted_at":null,"created_at":"2019-07-16 11:32:48","updated_at":"2019-07-16 11:32:48"},{"id":6,"user_id":33,"document_name":"Registration Certificate","document_url":"","expiry_date":null,"status":"pending","deleted_at":null,"created_at":"2019-07-16 11:32:48","updated_at":"2019-07-16 11:32:48"},{"id":7,"user_id":33,"document_name":"Police Clearance Certificate","document_url":"","expiry_date":null,"status":"pending","deleted_at":null,"created_at":"2019-07-16 11:32:48","updated_at":"2019-07-16 11:32:48"}]
         */

        @SerializedName("userDetails")
        private UserDetailsEntity userDetails;
        @SerializedName("documents")
        private List<DocumentsEntity> documents;
        @SerializedName("runningRide")
        private RunningRideEntity runningRide;
        @SerializedName("vehicleInspection")
        private String vehicleInspection;

        public void setRunningRide(RunningRideEntity runningRide) {
            this.runningRide = runningRide;
        }

        public String getVehicleInspection() {
            return vehicleInspection;
        }

        public void setVehicleInspection(String vehicleInspection) {
            this.vehicleInspection = vehicleInspection;
        }

        public UserDetailsEntity getUserDetails() {
            return userDetails;
        }

        public void setUserDetails(UserDetailsEntity userDetails) {
            this.userDetails = userDetails;
        }

        public List<DocumentsEntity> getDocuments() {
            return documents;
        }

        public void setDocuments(List<DocumentsEntity> documents) {
            this.documents = documents;
        }

        public RunningRideEntity getRunningRide() {
            return runningRide;
        }

        public void setUserDetails(RunningRideEntity runningRide) {
            this.runningRide = runningRide;
        }

        public static class UserDetailsEntity {
            /**
             * id : 33
             * parent_id : 0
             * company_id : 0
             * first_name : Parthh
             * last_name : Mistryy
             * company_name :
             * company_image : null
             * contact_person :
             * profile_picture : /assets/images/5d2c58bdb4ad3.png
             * email : parthmistry.samcom2018@gmail.com
             * email_verified_at : null
             * phone_number : +919712146184
             * address : 4 D Square Mall, ONGC Circle, near tune hotel, Chandkheda, Ahmedabad, Gujarat, India
             * country : India
             * state : Gujarat
             * city : Ahmedabad
             * device_token : eGp5S8NjIHM:APA91bHXz_JGl8b2YSnkvtUYnmcmEHbLB5POYanc1-HR7t2N5ve4VywIRT-gzWIyCBrHufqOFj676R2U7EyDyIEUVu4Bpe7dqZo8nnW1GXA-mf8jDbG8NfSZ5dlBbY7oJdh52v0fZSoc
             * device_type : android
             * latitude : 23.1029233
             * longitude : 72.59565090000001
             * status : active
             * user_status : online
             * authy_id : 127634683
             * user_unique_code : Parth1033
             * tax_id : null
             * commission_type : ammount
             * commission : null
             * role : driver
             * document_status : pending
             * deleted_at : null
             * created_at : 2019-07-11 06:45:02
             * updated_at : 2019-07-16 12:11:50
             * users_vehicle : {"id":2,"user_id":33,"vin_code":"123456","make":"BMW","model":"M3","year":"2019","transmission":"4X4","color":"black","driver_train":"Front","deleted_at":null,"created_at":"2019-07-11 06:46:09","updated_at":"2019-07-16 10:40:00"}
             * vehicle : {"id":2,"user_id":33,"vin_code":"123456","make":"BMW","model":"M3","year":"2019","transmission":"4X4","color":"black","driver_train":"Front","deleted_at":null,"created_at":"2019-07-11 06:46:09","updated_at":"2019-07-16 10:40:00"}
             * vehicle_status : Yes
             */

            @SerializedName("id")
            private int id;
            @SerializedName("parent_id")
            private int parentId;
            @SerializedName("company_id")
            private int companyId;
            @SerializedName("first_name")
            private String firstName;
            @SerializedName("last_name")
            private String lastName;
            @SerializedName("company_name")
            private String companyName;
            @SerializedName("company_image")
            private String companyImage;
            @SerializedName("contact_person")
            private String contactPerson;
            @SerializedName("profile_picture")
            private String profilePicture;
            @SerializedName("email")
            private String email;
            @SerializedName("email_verified_at")
            private String emailVerifiedAt;
            @SerializedName("phone_number")
            private String phoneNumber;
            @SerializedName("address")
            private String address;
            @SerializedName("country")
            private String country;
            @SerializedName("state")
            private String state;
            @SerializedName("city")
            private String city;
            @SerializedName("device_token")
            private String deviceToken;
            @SerializedName("device_type")
            private String deviceType;
            @SerializedName("latitude")
            private String latitude;
            @SerializedName("longitude")
            private String longitude;
            @SerializedName("status")
            private String status;
            @SerializedName("user_status")
            private String user_status;
            @SerializedName("authy_id")
            private String authyId;
            @SerializedName("user_unique_code")
            private String userUniqueCode;
            @SerializedName("tax_id")
            private String taxId;
            @SerializedName("commission_type")
            private String commissionType;
            @SerializedName("commission")
            private String commission;
            @SerializedName("role")
            private String role;
            @SerializedName("document_status")
            private String documentStatus;
            @SerializedName("deleted_at")
            private String deletedAt;
            @SerializedName("created_at")
            private String createdAt;
            @SerializedName("updated_at")
            private String updatedAt;
            @SerializedName("users_vehicle")
            private UsersVehicleEntity usersVehicle;
            @SerializedName("vehicle")
            private VehicleEntity vehicle;
            @SerializedName("vehicle_status")
            private String vehicleStatus;
            @SerializedName("support_number")
            private String support_number;

            public String getSupport_number() {
                return support_number;
            }

            public void setSupport_number(String support_number) {
                this.support_number = support_number;
            }

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public int getParentId() {
                return parentId;
            }

            public void setParentId(int parentId) {
                this.parentId = parentId;
            }

            public int getCompanyId() {
                return companyId;
            }

            public void setCompanyId(int companyId) {
                this.companyId = companyId;
            }

            public String getFirstName() {
                return firstName;
            }

            public void setFirstName(String firstName) {
                this.firstName = firstName;
            }

            public String getLastName() {
                return lastName;
            }

            public void setLastName(String lastName) {
                this.lastName = lastName;
            }

            public String getCompanyName() {
                return companyName;
            }

            public void setCompanyName(String companyName) {
                this.companyName = companyName;
            }

            public String getCompanyImage() {
                return companyImage;
            }

            public void setCompanyImage(String companyImage) {
                this.companyImage = companyImage;
            }

            public String getContactPerson() {
                return contactPerson;
            }

            public void setContactPerson(String contactPerson) {
                this.contactPerson = contactPerson;
            }

            public String getProfilePicture() {
                return profilePicture;
            }

            public void setProfilePicture(String profilePicture) {
                this.profilePicture = profilePicture;
            }

            public String getEmail() {
                return email;
            }

            public void setEmail(String email) {
                this.email = email;
            }

            public String getEmailVerifiedAt() {
                return emailVerifiedAt;
            }

            public void setEmailVerifiedAt(String emailVerifiedAt) {
                this.emailVerifiedAt = emailVerifiedAt;
            }

            public String getPhoneNumber() {
                return phoneNumber;
            }

            public void setPhoneNumber(String phoneNumber) {
                this.phoneNumber = phoneNumber;
            }

            public String getAddress() {
                return address;
            }

            public void setAddress(String address) {
                this.address = address;
            }

            public String getCountry() {
                return country;
            }

            public void setCountry(String country) {
                this.country = country;
            }

            public String getState() {
                return state;
            }

            public void setState(String state) {
                this.state = state;
            }

            public String getCity() {
                return city;
            }

            public void setCity(String city) {
                this.city = city;
            }

            public String getDeviceToken() {
                return deviceToken;
            }

            public void setDeviceToken(String deviceToken) {
                this.deviceToken = deviceToken;
            }

            public String getDeviceType() {
                return deviceType;
            }

            public void setDeviceType(String deviceType) {
                this.deviceType = deviceType;
            }

            public String getLatitude() {
                return latitude;
            }

            public void setLatitude(String latitude) {
                this.latitude = latitude;
            }

            public String getLongitude() {
                return longitude;
            }

            public void setLongitude(String longitude) {
                this.longitude = longitude;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }


            public String getUser_status() {
                return user_status;
            }

            public void setUser_status(String user_status) {
                this.user_status = user_status;
            }

            public String getAuthyId() {
                return authyId;
            }

            public void setAuthyId(String authyId) {
                this.authyId = authyId;
            }

            public String getUserUniqueCode() {
                return userUniqueCode;
            }

            public void setUserUniqueCode(String userUniqueCode) {
                this.userUniqueCode = userUniqueCode;
            }

            public String getTaxId() {
                return taxId;
            }

            public void setTaxId(String taxId) {
                this.taxId = taxId;
            }

            public String getCommissionType() {
                return commissionType;
            }

            public void setCommissionType(String commissionType) {
                this.commissionType = commissionType;
            }

            public String getCommission() {
                return commission;
            }

            public void setCommission(String commission) {
                this.commission = commission;
            }

            public String getRole() {
                return role;
            }

            public void setRole(String role) {
                this.role = role;
            }

            public String getDocumentStatus() {
                return documentStatus;
            }

            public void setDocumentStatus(String documentStatus) {
                this.documentStatus = documentStatus;
            }

            public String getDeletedAt() {
                return deletedAt;
            }

            public void setDeletedAt(String deletedAt) {
                this.deletedAt = deletedAt;
            }

            public String getCreatedAt() {
                return createdAt;
            }

            public void setCreatedAt(String createdAt) {
                this.createdAt = createdAt;
            }

            public String getUpdatedAt() {
                return updatedAt;
            }

            public void setUpdatedAt(String updatedAt) {
                this.updatedAt = updatedAt;
            }

            public UsersVehicleEntity getUsersVehicle() {
                return usersVehicle;
            }

            public void setUsersVehicle(UsersVehicleEntity usersVehicle) {
                this.usersVehicle = usersVehicle;
            }

            public VehicleEntity getVehicle() {
                return vehicle;
            }

            public void setVehicle(VehicleEntity vehicle) {
                this.vehicle = vehicle;
            }

            public String getVehicleStatus() {
                return vehicleStatus;
            }

            public void setVehicleStatus(String vehicleStatus) {
                this.vehicleStatus = vehicleStatus;
            }

            public static class UsersVehicleEntity {
                /**
                 * id : 2
                 * user_id : 33
                 * vin_code : 123456
                 * make : BMW
                 * model : M3
                 * year : 2019
                 * transmission : 4X4
                 * color : black
                 * driver_train : Front
                 * deleted_at : null
                 * created_at : 2019-07-11 06:46:09
                 * updated_at : 2019-07-16 10:40:00
                 */

                @SerializedName("id")
                private int id;
                @SerializedName("user_id")
                private int userId;
                @SerializedName("vin_code")
                private String vinCode;
                @SerializedName("make")
                private String make;
                @SerializedName("model")
                private String model;
                @SerializedName("year")
                private String year;
                @SerializedName("transmission")
                private String transmission;
                @SerializedName("color")
                private String color;
                @SerializedName("driver_train")
                private String driverTrain;
                @SerializedName("deleted_at")
                private String deletedAt;
                @SerializedName("created_at")
                private String createdAt;
                @SerializedName("updated_at")
                private String updatedAt;

                public int getId() {
                    return id;
                }

                public void setId(int id) {
                    this.id = id;
                }

                public int getUserId() {
                    return userId;
                }

                public void setUserId(int userId) {
                    this.userId = userId;
                }

                public String getVinCode() {
                    return vinCode;
                }

                public void setVinCode(String vinCode) {
                    this.vinCode = vinCode;
                }

                public String getMake() {
                    return make;
                }

                public void setMake(String make) {
                    this.make = make;
                }

                public String getModel() {
                    return model;
                }

                public void setModel(String model) {
                    this.model = model;
                }

                public String getYear() {
                    return year;
                }

                public void setYear(String year) {
                    this.year = year;
                }

                public String getTransmission() {
                    return transmission;
                }

                public void setTransmission(String transmission) {
                    this.transmission = transmission;
                }

                public String getColor() {
                    return color;
                }

                public void setColor(String color) {
                    this.color = color;
                }

                public String getDriverTrain() {
                    return driverTrain;
                }

                public void setDriverTrain(String driverTrain) {
                    this.driverTrain = driverTrain;
                }

                public String getDeletedAt() {
                    return deletedAt;
                }

                public void setDeletedAt(String deletedAt) {
                    this.deletedAt = deletedAt;
                }

                public String getCreatedAt() {
                    return createdAt;
                }

                public void setCreatedAt(String createdAt) {
                    this.createdAt = createdAt;
                }

                public String getUpdatedAt() {
                    return updatedAt;
                }

                public void setUpdatedAt(String updatedAt) {
                    this.updatedAt = updatedAt;
                }
            }

            public static class VehicleEntity {
                /**
                 * id : 2
                 * user_id : 33
                 * vin_code : 123456
                 * make : BMW
                 * model : M3
                 * year : 2019
                 * transmission : 4X4
                 * color : black
                 * driver_train : Front
                 * deleted_at : null
                 * created_at : 2019-07-11 06:46:09
                 * updated_at : 2019-07-16 10:40:00
                 */

                @SerializedName("id")
                private int id;
                @SerializedName("user_id")
                private int userId;
                @SerializedName("vin_code")
                private String vinCode;
                @SerializedName("make")
                private String make;
                @SerializedName("model")
                private String model;
                @SerializedName("year")
                private String year;
                @SerializedName("transmission")
                private String transmission;
                @SerializedName("color")
                private String color;
                @SerializedName("driver_train")
                private String driverTrain;
                @SerializedName("car_type")
                private String carType;

                @SerializedName("deleted_at")
                private String deletedAt;
                @SerializedName("created_at")
                private String createdAt;
                @SerializedName("updated_at")
                private String updatedAt;

                public int getId() {
                    return id;
                }

                public void setId(int id) {
                    this.id = id;
                }

                public int getUserId() {
                    return userId;
                }

                public void setUserId(int userId) {
                    this.userId = userId;
                }

                public String getVinCode() {
                    return vinCode;
                }

                public void setVinCode(String vinCode) {
                    this.vinCode = vinCode;
                }

                public String getMake() {
                    return make;
                }

                public void setMake(String make) {
                    this.make = make;
                }

                public String getModel() {
                    return model;
                }

                public void setModel(String model) {
                    this.model = model;
                }

                public String getYear() {
                    return year;
                }

                public void setYear(String year) {
                    this.year = year;
                }

                public String getTransmission() {
                    return transmission;
                }

                public void setTransmission(String transmission) {
                    this.transmission = transmission;
                }

                public String getColor() {
                    return color;
                }

                public void setColor(String color) {
                    this.color = color;
                }

                public String getDriverTrain() {
                    return driverTrain;
                }

                public void setDriverTrain(String driverTrain) {
                    this.driverTrain = driverTrain;
                }


                public String getCarType() {
                    return carType;
                }

                public void setCarType(String carType) {
                    this.carType = carType;
                }


                public String getDeletedAt() {
                    return deletedAt;
                }

                public void setDeletedAt(String deletedAt) {
                    this.deletedAt = deletedAt;
                }

                public String getCreatedAt() {
                    return createdAt;
                }

                public void setCreatedAt(String createdAt) {
                    this.createdAt = createdAt;
                }

                public String getUpdatedAt() {
                    return updatedAt;
                }

                public void setUpdatedAt(String updatedAt) {
                    this.updatedAt = updatedAt;
                }
            }
        }

        public static class DocumentsEntity {
            /**
             * id : 1
             * user_id : 33
             * document_name : Driving License
             * document_url :
             * expiry_date : null
             * status : pending
             * deleted_at : null
             * created_at : 2019-07-16 11:32:48
             * updated_at : 2019-07-16 11:32:48
             */

            @SerializedName("id")
            private int id;
            @SerializedName("user_id")
            private int userId;
            @SerializedName("document_name")
            private String documentName;
            @SerializedName("front_document_url")
            private String front_document_url;
            @SerializedName("back_document_url")
            private String back_document_url;
            @SerializedName("document_type")
            private String document_type;
            @SerializedName("issue_date")
            private String issue_date;
            @SerializedName("expiry_date")
            private String expiryDate;
            @SerializedName("status")
            private String status;
            @SerializedName("deleted_at")
            private String deletedAt;
            @SerializedName("created_at")
            private String createdAt;
            @SerializedName("updated_at")
            private String updatedAt;
            @SerializedName("document_url")
            private String documentUrl;

            public String getFront_document_url() {
                return front_document_url;
            }

            public void setFront_document_url(String front_document_url) {
                this.front_document_url = front_document_url;
            }

            public String getBack_document_url() {
                return back_document_url;
            }

            public void setBack_document_url(String back_document_url) {
                this.back_document_url = back_document_url;
            }

            public String getDocument_type() {
                return document_type;
            }

            public void setDocument_type(String document_type) {
                this.document_type = document_type;
            }

            public String getIssue_date() {
                return issue_date;
            }

            public void setIssue_date(String issue_date) {
                this.issue_date = issue_date;
            }

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public int getUserId() {
                return userId;
            }

            public void setUserId(int userId) {
                this.userId = userId;
            }

            public String getDocumentName() {
                return documentName;
            }

            public void setDocumentName(String documentName) {
                this.documentName = documentName;
            }

            public String getDocumentUrl() {
                return documentUrl;
            }

            public void setDocumentUrl(String documentUrl) {
                this.documentUrl = documentUrl;
            }

            public String getExpiryDate() {
                return expiryDate;
            }

            public void setExpiryDate(String expiryDate) {
                this.expiryDate = expiryDate;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }

            public String getDeletedAt() {
                return deletedAt;
            }

            public void setDeletedAt(String deletedAt) {
                this.deletedAt = deletedAt;
            }

            public String getCreatedAt() {
                return createdAt;
            }

            public void setCreatedAt(String createdAt) {
                this.createdAt = createdAt;
            }

            public String getUpdatedAt() {
                return updatedAt;
            }

            public void setUpdatedAt(String updatedAt) {
                this.updatedAt = updatedAt;
            }
        }

        public static class RunningRideEntity {
            @SerializedName("id")
            private int id;
            @SerializedName("source_location")
            private String source_location;
            @SerializedName("source_latitude")
            private String source_latitude;
            @SerializedName("source_longitude")
            private String source_longitude;
            @SerializedName("destination_location")
            private String destination_location;
            @SerializedName("destination_latitude")
            private String destination_latitude;
            @SerializedName("destination_longitude")
            private String destination_longitude;
            @SerializedName("payment_type")
            private String payment_type;
            @SerializedName("is_schedule")
            private String is_schedule;
            @SerializedName("schedule_date")
            private String schedule_date;
            @SerializedName("ride_instruction")
            private String ride_instruction;

            @SerializedName("display_passenger_name")
            private String display_passenger_name;
            @SerializedName("passenger_phone_number")
            private String passenger_phone_number;
            @SerializedName("eta")
            private String eta;
            @SerializedName("est_fare")
            private String est_fare;
            @SerializedName("request_id")
            private String request_id;
            @SerializedName("driver_location")
            private String driver_location;
            @SerializedName("driver_latitude")
            private String driver_latitude;
            @SerializedName("driver_longitude")
            private String driver_longitude;
            @SerializedName("status")
            private String status;
            @SerializedName("car_type_id")
            private String car_type_id;
            @SerializedName("actual_discount")
            private String actual_discount;
            @SerializedName("car_image")
            private String car_image;
            @SerializedName("description")
            private String description;
            @SerializedName("car_type_name")
            private String car_type_name;
            @SerializedName("make")
            private String make;
            @SerializedName("model")
            private String model;
            @SerializedName("car_number")
            private String car_number;
            @SerializedName("passenger_id")
            private String passenger_id;
            @SerializedName("passenger_first_name")
            private String passenger_first_name;
            @SerializedName("passenger_last_name")
            private String passenger_last_name;
            @SerializedName("passenger_unique_code")
            private String passenger_unique_code;
            @SerializedName("device_token")
            private String device_token;
            @SerializedName("profile_picture")
            private String profile_picture;
            @SerializedName("driver_id")
            private String driver_id;
            @SerializedName("first_name")
            private String first_name;
            @SerializedName("last_name")
            private String last_name;
            @SerializedName("driver_unique_code")
            private String driver_unique_code;
            @SerializedName("driver_picture")
            private String driver_picture;
            @SerializedName("driver_device_token")
            private String driver_device_token;
            @SerializedName("total_time")
            private String total_time;
            @SerializedName("total_cost")
            private String total_cost;
            @SerializedName("rating")
            private String rating;


            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getSource_location() {
                return source_location;
            }

            public void setSource_location(String source_location) {
                this.source_location = source_location;
            }

            public String getSource_latitude() {
                return source_latitude;
            }

            public void setSource_latitude(String source_latitude) {
                this.source_latitude = source_latitude;
            }

            public String getSource_longitude() {
                return source_longitude;
            }

            public void setSource_longitude(String source_longitude) {
                this.source_longitude = source_longitude;
            }

            public String getDestination_location() {
                return destination_location;
            }

            public void setDestination_location(String destination_location) {
                this.destination_location = destination_location;
            }

            public String getDestination_latitude() {
                return destination_latitude;
            }

            public void setDestination_latitude(String destination_latitude) {
                this.destination_latitude = destination_latitude;
            }

            public String getDestination_longitude() {
                return destination_longitude;
            }

            public void setDestination_longitude(String destination_longitude) {
                this.destination_longitude = destination_longitude;
            }

            public String getPayment_type() {
                return payment_type;
            }

            public void setPayment_type(String payment_type) {
                this.payment_type = payment_type;
            }

            public String getIs_schedule() {
                return is_schedule;
            }

            public void setIs_schedule(String is_schedule) {
                this.is_schedule = is_schedule;
            }

            public String getSchedule_date() {
                return schedule_date;
            }

            public void setSchedule_date(String schedule_date) {
                this.schedule_date = schedule_date;
            }


            public String getRide_instruction() {
                return ride_instruction;
            }

            public void setRide_instruction(String ride_instruction) {
                this.ride_instruction = ride_instruction;
            }


            public String getDisplay_passenger_name() {
                return display_passenger_name;
            }

            public void setDisplay_passenger_name(String display_passenger_name) {
                this.display_passenger_name = display_passenger_name;
            }


            public String getPassenger_phone_number() {
                return passenger_phone_number;
            }

            public void setPassenger_phone_number(String passenger_phone_number) {
                this.passenger_phone_number = passenger_phone_number;
            }



            public String getEta() {
                return eta;
            }

            public void setEta(String eta) {
                this.eta = eta;
            }

            public String getEst_fare() {
                return est_fare;
            }

            public void setEst_fare(String est_fare) {
                this.est_fare = est_fare;
            }

            public String getRequest_id() {
                return request_id;
            }

            public void setRequest_id(String request_id) {
                this.request_id = request_id;
            }

            public String getDriver_location() {
                return driver_location;
            }

            public void setDriver_location(String driver_location) {
                this.driver_location = driver_location;
            }

            public String getDriver_latitude() {
                return driver_latitude;
            }

            public void setDriver_latitude(String driver_latitude) {
                this.driver_latitude = driver_latitude;
            }

            public String getDriver_longitude() {
                return driver_longitude;
            }

            public void setDriver_longitude(String driver_longitude) {
                this.driver_longitude = driver_longitude;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }

            public String getCar_type_id() {
                return car_type_id;
            }

            public void setCar_type_id(String car_type_id) {
                this.car_type_id = car_type_id;
            }

            public String getActual_discount() {
                return actual_discount;
            }

            public void setActual_discount(String actual_discount) {
                this.actual_discount = actual_discount;
            }


            public String getCar_image() {
                return car_image;
            }

            public void setCar_image(String car_image) {
                this.car_image = car_image;
            }

            public String getDescription() {
                return description;
            }

            public void setDescription(String description) {
                this.description = description;
            }

            public String getCar_type_name() {
                return car_type_name;
            }

            public void setCar_type_name(String car_type_name) {
                this.car_type_name = car_type_name;
            }

            public String getMake() {
                return make;
            }

            public void setMake(String make) {
                this.make = make;
            }

            public String getModel() {
                return model;
            }

            public void setModel(String model) {
                this.model = model;
            }

            public String getCar_number() {
                return car_number;
            }

            public void setCar_number(String car_number) {
                this.car_number = car_number;
            }

            public String getPassenger_id() {
                return passenger_id;
            }

            public void setPassenger_id(String passenger_id) {
                this.passenger_id = passenger_id;
            }

            public String getPassenger_first_name() {
                return passenger_first_name;
            }

            public void setPassenger_first_name(String passenger_first_name) {
                this.passenger_first_name = passenger_first_name;
            }

            public String getPassenger_last_name() {
                return passenger_last_name;
            }

            public void setPassenger_last_name(String passenger_last_name) {
                this.passenger_last_name = passenger_last_name;
            }

            public String getPassenger_unique_code() {
                return passenger_unique_code;
            }

            public void setPassenger_unique_code(String passenger_unique_code) {
                this.passenger_unique_code = passenger_unique_code;
            }

            public String getDevice_token() {
                return device_token;
            }

            public void setDevice_token(String device_token) {
                this.device_token = device_token;
            }

            public String getProfile_picture() {
                return profile_picture;
            }

            public void setProfile_picture(String profile_picture) {
                this.profile_picture = profile_picture;
            }

            public String getDriver_id() {
                return driver_id;
            }

            public void setDriver_id(String driver_id) {
                this.driver_id = driver_id;
            }

            public String getFirst_name() {
                return first_name;
            }

            public void setFirst_name(String first_name) {
                this.first_name = first_name;
            }

            public String getLast_name() {
                return last_name;
            }

            public void setLast_name(String last_name) {
                this.last_name = last_name;
            }

            public String getDriver_unique_code() {
                return driver_unique_code;
            }

            public void setDriver_unique_code(String driver_unique_code) {
                this.driver_unique_code = driver_unique_code;
            }

            public String getDriver_picture() {
                return driver_picture;
            }

            public void setDriver_picture(String driver_picture) {
                this.driver_picture = driver_picture;
            }

            public String getDriver_device_token() {
                return driver_device_token;
            }

            public void setDriver_device_token(String driver_device_token) {
                this.driver_device_token = driver_device_token;
            }

            public String getTotal_time() {
                return total_time;
            }

            public void setTotal_time(String total_time) {
                this.total_time = total_time;
            }

            public String getTotal_cost() {
                return total_cost;
            }

            public void setTotal_cost(String total_cost) {
                this.total_cost = total_cost;
            }

            public String getRating() {
                return rating;
            }

            public void setRating(String rating) {
                this.rating = rating;
            }
        }
    }
}
