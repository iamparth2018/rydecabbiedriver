package com.rydedispatch.driver.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RespGetCarType {

    /**
     * error : false
     * data : [{"id":1,"car_type_name":"Cargo Van","car_image":"/assets/images/1563444875.jpeg","description":"This is Cargo Van","deleted_at":null,"created_at":"2019-07-18 10:14:35","updated_at":"2019-07-18 10:14:35"}]
     * message : Successfully retrieve car.
     */

    @SerializedName("error")
    private boolean error;
    @SerializedName("message")
    private String message;
    @SerializedName("data")
    private List<DataEntity> data;

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataEntity> getData() {
        return data;
    }

    public void setData(List<DataEntity> data) {
        this.data = data;
    }

    public static class DataEntity {
        /**
         * id : 1
         * car_type_name : Cargo Van
         * car_image : /assets/images/1563444875.jpeg
         * description : This is Cargo Van
         * deleted_at : null
         * created_at : 2019-07-18 10:14:35
         * updated_at : 2019-07-18 10:14:35
         */

        @SerializedName("id")
        private int id;
        @SerializedName("car_type_name")
        private String carTypeName;
        @SerializedName("car_image")
        private String carImage;
        @SerializedName("description")
        private String description;
        @SerializedName("deleted_at")
        private Object deletedAt;
        @SerializedName("created_at")
        private String createdAt;
        @SerializedName("updated_at")
        private String updatedAt;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getCarTypeName() {
            return carTypeName;
        }

        public void setCarTypeName(String carTypeName) {
            this.carTypeName = carTypeName;
        }

        public String getCarImage() {
            return carImage;
        }

        public void setCarImage(String carImage) {
            this.carImage = carImage;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public Object getDeletedAt() {
            return deletedAt;
        }

        public void setDeletedAt(Object deletedAt) {
            this.deletedAt = deletedAt;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }
    }
}
