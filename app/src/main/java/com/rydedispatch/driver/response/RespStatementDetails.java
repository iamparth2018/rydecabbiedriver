package com.rydedispatch.driver.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RespStatementDetails {

    /**
     * error : false
     * data : {"statementsDetails":{"current_page":1,"data":[{"id":15,"statement_id":2,"request_id":16,"ride_allocation_id":22,"user_id":8,"driver_id":11,"company_id":0,"actual_cost_without_commission":"0","actual_cost":"0","actual_discount":"0","total_cost":"0","total_time":"0","total_distance":"0","commission":"0","payment_type":"cash","status":"cancelled","payment_status":"unpaid","passenger_payment_status":"pending","deleted_at":null,"created_at":"2019-08-21 11:39:29","updated_at":"2019-08-28 05:52:51","first_name":"Sandeep","last_name":"User","profile_picture":"/assets/images/5d5a2db3d0739.jpg"},{"id":14,"statement_id":2,"request_id":15,"ride_allocation_id":20,"user_id":8,"driver_id":11,"company_id":0,"actual_cost_without_commission":"11.7","actual_cost":"13","actual_discount":"0","total_cost":"13","total_time":"0","total_distance":"0","commission":"1.3","payment_type":"cash","status":"completed","payment_status":"unpaid","passenger_payment_status":"pending","deleted_at":null,"created_at":"2019-08-21 11:36:53","updated_at":"2019-08-28 05:52:51","first_name":"Sandeep","last_name":"User","profile_picture":"/assets/images/5d5a2db3d0739.jpg"},{"id":11,"statement_id":2,"request_id":12,"ride_allocation_id":16,"user_id":3,"driver_id":11,"company_id":0,"actual_cost_without_commission":"0","actual_cost":"0","actual_discount":"0","total_cost":"0","total_time":"0","total_distance":"0","commission":"0","payment_type":"cash","status":"cancelled","payment_status":"unpaid","passenger_payment_status":"pending","deleted_at":null,"created_at":"2019-08-21 11:19:41","updated_at":"2019-08-28 05:52:51","first_name":"Mannan","last_name":"Kagdi","profile_picture":null},{"id":9,"statement_id":2,"request_id":10,"ride_allocation_id":12,"user_id":3,"driver_id":11,"company_id":0,"actual_cost_without_commission":"0","actual_cost":"0","actual_discount":"0","total_cost":"0","total_time":"0","total_distance":"0","commission":"0","payment_type":"cash","status":"cancelled","payment_status":"unpaid","passenger_payment_status":"pending","deleted_at":null,"created_at":"2019-08-21 11:15:14","updated_at":"2019-08-28 05:52:51","first_name":"Mannan","last_name":"Kagdi","profile_picture":null},{"id":8,"statement_id":2,"request_id":9,"ride_allocation_id":10,"user_id":3,"driver_id":11,"company_id":0,"actual_cost_without_commission":"11.7","actual_cost":"13","actual_discount":"0","total_cost":"13","total_time":"0","total_distance":"0","commission":"1.3","payment_type":"cash","status":"completed","payment_status":"unpaid","passenger_payment_status":"pending","deleted_at":null,"created_at":"2019-08-21 10:53:05","updated_at":"2019-08-28 05:52:51","first_name":"Mannan","last_name":"Kagdi","profile_picture":null}],"first_page_url":"http://159.203.105.17/api/statementDetails?page=1","from":1,"last_page":1,"last_page_url":"http://159.203.105.17/api/statementDetails?page=1","next_page_url":null,"path":"http://159.203.105.17/api/statementDetails","per_page":20,"prev_page_url":null,"to":5,"total":5},"totalCompeleted":2,"totalEarnings":"23.4","totalSpendTime":"0"}
     * message : statements details!
     */

    @SerializedName("error")
    private boolean error;
    @SerializedName("data")
    private DataEntityX data;
    @SerializedName("message")
    private String message;

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public DataEntityX getData() {
        return data;
    }

    public void setData(DataEntityX data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public static class DataEntityX {
        /**
         * statementsDetails : {"current_page":1,"data":[{"id":15,"statement_id":2,"request_id":16,"ride_allocation_id":22,"user_id":8,"driver_id":11,"company_id":0,"actual_cost_without_commission":"0","actual_cost":"0","actual_discount":"0","total_cost":"0","total_time":"0","total_distance":"0","commission":"0","payment_type":"cash","status":"cancelled","payment_status":"unpaid","passenger_payment_status":"pending","deleted_at":null,"created_at":"2019-08-21 11:39:29","updated_at":"2019-08-28 05:52:51","first_name":"Sandeep","last_name":"User","profile_picture":"/assets/images/5d5a2db3d0739.jpg"},{"id":14,"statement_id":2,"request_id":15,"ride_allocation_id":20,"user_id":8,"driver_id":11,"company_id":0,"actual_cost_without_commission":"11.7","actual_cost":"13","actual_discount":"0","total_cost":"13","total_time":"0","total_distance":"0","commission":"1.3","payment_type":"cash","status":"completed","payment_status":"unpaid","passenger_payment_status":"pending","deleted_at":null,"created_at":"2019-08-21 11:36:53","updated_at":"2019-08-28 05:52:51","first_name":"Sandeep","last_name":"User","profile_picture":"/assets/images/5d5a2db3d0739.jpg"},{"id":11,"statement_id":2,"request_id":12,"ride_allocation_id":16,"user_id":3,"driver_id":11,"company_id":0,"actual_cost_without_commission":"0","actual_cost":"0","actual_discount":"0","total_cost":"0","total_time":"0","total_distance":"0","commission":"0","payment_type":"cash","status":"cancelled","payment_status":"unpaid","passenger_payment_status":"pending","deleted_at":null,"created_at":"2019-08-21 11:19:41","updated_at":"2019-08-28 05:52:51","first_name":"Mannan","last_name":"Kagdi","profile_picture":null},{"id":9,"statement_id":2,"request_id":10,"ride_allocation_id":12,"user_id":3,"driver_id":11,"company_id":0,"actual_cost_without_commission":"0","actual_cost":"0","actual_discount":"0","total_cost":"0","total_time":"0","total_distance":"0","commission":"0","payment_type":"cash","status":"cancelled","payment_status":"unpaid","passenger_payment_status":"pending","deleted_at":null,"created_at":"2019-08-21 11:15:14","updated_at":"2019-08-28 05:52:51","first_name":"Mannan","last_name":"Kagdi","profile_picture":null},{"id":8,"statement_id":2,"request_id":9,"ride_allocation_id":10,"user_id":3,"driver_id":11,"company_id":0,"actual_cost_without_commission":"11.7","actual_cost":"13","actual_discount":"0","total_cost":"13","total_time":"0","total_distance":"0","commission":"1.3","payment_type":"cash","status":"completed","payment_status":"unpaid","passenger_payment_status":"pending","deleted_at":null,"created_at":"2019-08-21 10:53:05","updated_at":"2019-08-28 05:52:51","first_name":"Mannan","last_name":"Kagdi","profile_picture":null}],"first_page_url":"http://159.203.105.17/api/statementDetails?page=1","from":1,"last_page":1,"last_page_url":"http://159.203.105.17/api/statementDetails?page=1","next_page_url":null,"path":"http://159.203.105.17/api/statementDetails","per_page":20,"prev_page_url":null,"to":5,"total":5}
         * totalCompeleted : 2
         * totalEarnings : 23.4
         * totalSpendTime : 0
         */

        @SerializedName("statementsDetails")
        private StatementsDetailsEntity statementsDetails;
        @SerializedName("totalCompeleted")
        private String totalCompeleted;
        @SerializedName("totalEarnings")
        private String totalEarnings;
        @SerializedName("totalSpendTime")
        private String totalSpendTime;

        public StatementsDetailsEntity getStatementsDetails() {
            return statementsDetails;
        }

        public void setStatementsDetails(StatementsDetailsEntity statementsDetails) {
            this.statementsDetails = statementsDetails;
        }

        public String getTotalCompeleted() {
            return totalCompeleted;
        }

        public void setTotalCompeleted(String totalCompeleted) {
            this.totalCompeleted = totalCompeleted;
        }

        public String getTotalEarnings() {
            return totalEarnings;
        }

        public void setTotalEarnings(String totalEarnings) {
            this.totalEarnings = totalEarnings;
        }

        public String getTotalSpendTime() {
            return totalSpendTime;
        }

        public void setTotalSpendTime(String totalSpendTime) {
            this.totalSpendTime = totalSpendTime;
        }

        public static class StatementsDetailsEntity {
            /**
             * current_page : 1
             * data : [{"id":15,"statement_id":2,"request_id":16,"ride_allocation_id":22,"user_id":8,"driver_id":11,"company_id":0,"actual_cost_without_commission":"0","actual_cost":"0","actual_discount":"0","total_cost":"0","total_time":"0","total_distance":"0","commission":"0","payment_type":"cash","status":"cancelled","payment_status":"unpaid","passenger_payment_status":"pending","deleted_at":null,"created_at":"2019-08-21 11:39:29","updated_at":"2019-08-28 05:52:51","first_name":"Sandeep","last_name":"User","profile_picture":"/assets/images/5d5a2db3d0739.jpg"},{"id":14,"statement_id":2,"request_id":15,"ride_allocation_id":20,"user_id":8,"driver_id":11,"company_id":0,"actual_cost_without_commission":"11.7","actual_cost":"13","actual_discount":"0","total_cost":"13","total_time":"0","total_distance":"0","commission":"1.3","payment_type":"cash","status":"completed","payment_status":"unpaid","passenger_payment_status":"pending","deleted_at":null,"created_at":"2019-08-21 11:36:53","updated_at":"2019-08-28 05:52:51","first_name":"Sandeep","last_name":"User","profile_picture":"/assets/images/5d5a2db3d0739.jpg"},{"id":11,"statement_id":2,"request_id":12,"ride_allocation_id":16,"user_id":3,"driver_id":11,"company_id":0,"actual_cost_without_commission":"0","actual_cost":"0","actual_discount":"0","total_cost":"0","total_time":"0","total_distance":"0","commission":"0","payment_type":"cash","status":"cancelled","payment_status":"unpaid","passenger_payment_status":"pending","deleted_at":null,"created_at":"2019-08-21 11:19:41","updated_at":"2019-08-28 05:52:51","first_name":"Mannan","last_name":"Kagdi","profile_picture":null},{"id":9,"statement_id":2,"request_id":10,"ride_allocation_id":12,"user_id":3,"driver_id":11,"company_id":0,"actual_cost_without_commission":"0","actual_cost":"0","actual_discount":"0","total_cost":"0","total_time":"0","total_distance":"0","commission":"0","payment_type":"cash","status":"cancelled","payment_status":"unpaid","passenger_payment_status":"pending","deleted_at":null,"created_at":"2019-08-21 11:15:14","updated_at":"2019-08-28 05:52:51","first_name":"Mannan","last_name":"Kagdi","profile_picture":null},{"id":8,"statement_id":2,"request_id":9,"ride_allocation_id":10,"user_id":3,"driver_id":11,"company_id":0,"actual_cost_without_commission":"11.7","actual_cost":"13","actual_discount":"0","total_cost":"13","total_time":"0","total_distance":"0","commission":"1.3","payment_type":"cash","status":"completed","payment_status":"unpaid","passenger_payment_status":"pending","deleted_at":null,"created_at":"2019-08-21 10:53:05","updated_at":"2019-08-28 05:52:51","first_name":"Mannan","last_name":"Kagdi","profile_picture":null}]
             * first_page_url : http://159.203.105.17/api/statementDetails?page=1
             * from : 1
             * last_page : 1
             * last_page_url : http://159.203.105.17/api/statementDetails?page=1
             * next_page_url : null
             * path : http://159.203.105.17/api/statementDetails
             * per_page : 20
             * prev_page_url : null
             * to : 5
             * total : 5
             */

            @SerializedName("current_page")
            private int currentPage;
            @SerializedName("first_page_url")
            private String firstPageUrl;
            @SerializedName("from")
            private int from;
            @SerializedName("last_page")
            private int lastPage;
            @SerializedName("last_page_url")
            private String lastPageUrl;
            @SerializedName("next_page_url")
            private Object nextPageUrl;
            @SerializedName("path")
            private String path;
            @SerializedName("per_page")
            private int perPage;
            @SerializedName("prev_page_url")
            private Object prevPageUrl;
            @SerializedName("to")
            private int to;
            @SerializedName("total")
            private int total;
            @SerializedName("data")
            private List<DataEntity> data;

            public int getCurrentPage() {
                return currentPage;
            }

            public void setCurrentPage(int currentPage) {
                this.currentPage = currentPage;
            }

            public String getFirstPageUrl() {
                return firstPageUrl;
            }

            public void setFirstPageUrl(String firstPageUrl) {
                this.firstPageUrl = firstPageUrl;
            }

            public int getFrom() {
                return from;
            }

            public void setFrom(int from) {
                this.from = from;
            }

            public int getLastPage() {
                return lastPage;
            }

            public void setLastPage(int lastPage) {
                this.lastPage = lastPage;
            }

            public String getLastPageUrl() {
                return lastPageUrl;
            }

            public void setLastPageUrl(String lastPageUrl) {
                this.lastPageUrl = lastPageUrl;
            }

            public Object getNextPageUrl() {
                return nextPageUrl;
            }

            public void setNextPageUrl(Object nextPageUrl) {
                this.nextPageUrl = nextPageUrl;
            }

            public String getPath() {
                return path;
            }

            public void setPath(String path) {
                this.path = path;
            }

            public int getPerPage() {
                return perPage;
            }

            public void setPerPage(int perPage) {
                this.perPage = perPage;
            }

            public Object getPrevPageUrl() {
                return prevPageUrl;
            }

            public void setPrevPageUrl(Object prevPageUrl) {
                this.prevPageUrl = prevPageUrl;
            }

            public int getTo() {
                return to;
            }

            public void setTo(int to) {
                this.to = to;
            }

            public int getTotal() {
                return total;
            }

            public void setTotal(int total) {
                this.total = total;
            }

            public List<DataEntity> getData() {
                return data;
            }

            public void setData(List<DataEntity> data) {
                this.data = data;
            }

            public static class DataEntity {
                /**
                 * id : 15
                 * statement_id : 2
                 * request_id : 16
                 * ride_allocation_id : 22
                 * user_id : 8
                 * driver_id : 11
                 * company_id : 0
                 * actual_cost_without_commission : 0
                 * actual_cost : 0
                 * actual_discount : 0
                 * total_cost : 0
                 * total_time : 0
                 * total_distance : 0
                 * commission : 0
                 * payment_type : cash
                 * status : cancelled
                 * payment_status : unpaid
                 * passenger_payment_status : pending
                 * deleted_at : null
                 * created_at : 2019-08-21 11:39:29
                 * updated_at : 2019-08-28 05:52:51
                 * first_name : Sandeep
                 * last_name : User
                 * profile_picture : /assets/images/5d5a2db3d0739.jpg
                 */

                @SerializedName("id")
                private int id;
                @SerializedName("statement_id")
                private int statementId;
                @SerializedName("request_id")
                private int requestId;
                @SerializedName("ride_allocation_id")
                private int rideAllocationId;
                @SerializedName("user_id")
                private int userId;
                @SerializedName("driver_id")
                private int driverId;
                @SerializedName("company_id")
                private int companyId;
                @SerializedName("actual_cost_without_commission")
                private String actualCostWithoutCommission;
                @SerializedName("actual_cost")
                private String actualCost;
                @SerializedName("actual_discount")
                private String actualDiscount;
                @SerializedName("total_cost")
                private String totalCost;
                @SerializedName("total_time")
                private String totalTime;
                @SerializedName("total_distance")
                private String totalDistance;
                @SerializedName("commission")
                private String commission;
                @SerializedName("payment_type")
                private String paymentType;
                @SerializedName("status")
                private String status;
                @SerializedName("payment_status")
                private String paymentStatus;
                @SerializedName("passenger_payment_status")
                private String passengerPaymentStatus;
                @SerializedName("deleted_at")
                private Object deletedAt;
                @SerializedName("created_at")
                private String createdAt;
                @SerializedName("updated_at")
                private String updatedAt;
                @SerializedName("first_name")
                private String firstName;
                @SerializedName("last_name")
                private String lastName;
                @SerializedName("profile_picture")
                private String profilePicture;

                public int getId() {
                    return id;
                }

                public void setId(int id) {
                    this.id = id;
                }

                public int getStatementId() {
                    return statementId;
                }

                public void setStatementId(int statementId) {
                    this.statementId = statementId;
                }

                public int getRequestId() {
                    return requestId;
                }

                public void setRequestId(int requestId) {
                    this.requestId = requestId;
                }

                public int getRideAllocationId() {
                    return rideAllocationId;
                }

                public void setRideAllocationId(int rideAllocationId) {
                    this.rideAllocationId = rideAllocationId;
                }

                public int getUserId() {
                    return userId;
                }

                public void setUserId(int userId) {
                    this.userId = userId;
                }

                public int getDriverId() {
                    return driverId;
                }

                public void setDriverId(int driverId) {
                    this.driverId = driverId;
                }

                public int getCompanyId() {
                    return companyId;
                }

                public void setCompanyId(int companyId) {
                    this.companyId = companyId;
                }

                public String getActualCostWithoutCommission() {
                    return actualCostWithoutCommission;
                }

                public void setActualCostWithoutCommission(String actualCostWithoutCommission) {
                    this.actualCostWithoutCommission = actualCostWithoutCommission;
                }

                public String getActualCost() {
                    return actualCost;
                }

                public void setActualCost(String actualCost) {
                    this.actualCost = actualCost;
                }

                public String getActualDiscount() {
                    return actualDiscount;
                }

                public void setActualDiscount(String actualDiscount) {
                    this.actualDiscount = actualDiscount;
                }

                public String getTotalCost() {
                    return totalCost;
                }

                public void setTotalCost(String totalCost) {
                    this.totalCost = totalCost;
                }

                public String getTotalTime() {
                    return totalTime;
                }

                public void setTotalTime(String totalTime) {
                    this.totalTime = totalTime;
                }

                public String getTotalDistance() {
                    return totalDistance;
                }

                public void setTotalDistance(String totalDistance) {
                    this.totalDistance = totalDistance;
                }

                public String getCommission() {
                    return commission;
                }

                public void setCommission(String commission) {
                    this.commission = commission;
                }

                public String getPaymentType() {
                    return paymentType;
                }

                public void setPaymentType(String paymentType) {
                    this.paymentType = paymentType;
                }

                public String getStatus() {
                    return status;
                }

                public void setStatus(String status) {
                    this.status = status;
                }

                public String getPaymentStatus() {
                    return paymentStatus;
                }

                public void setPaymentStatus(String paymentStatus) {
                    this.paymentStatus = paymentStatus;
                }

                public String getPassengerPaymentStatus() {
                    return passengerPaymentStatus;
                }

                public void setPassengerPaymentStatus(String passengerPaymentStatus) {
                    this.passengerPaymentStatus = passengerPaymentStatus;
                }

                public Object getDeletedAt() {
                    return deletedAt;
                }

                public void setDeletedAt(Object deletedAt) {
                    this.deletedAt = deletedAt;
                }

                public String getCreatedAt() {
                    return createdAt;
                }

                public void setCreatedAt(String createdAt) {
                    this.createdAt = createdAt;
                }

                public String getUpdatedAt() {
                    return updatedAt;
                }

                public void setUpdatedAt(String updatedAt) {
                    this.updatedAt = updatedAt;
                }

                public String getFirstName() {
                    return firstName;
                }

                public void setFirstName(String firstName) {
                    this.firstName = firstName;
                }

                public String getLastName() {
                    return lastName;
                }

                public void setLastName(String lastName) {
                    this.lastName = lastName;
                }

                public String getProfilePicture() {
                    return profilePicture;
                }

                public void setProfilePicture(String profilePicture) {
                    this.profilePicture = profilePicture;
                }
            }
        }
    }
}
