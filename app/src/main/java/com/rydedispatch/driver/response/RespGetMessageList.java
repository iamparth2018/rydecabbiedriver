package com.rydedispatch.driver.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RespGetMessageList {

    /**
     * error : false
     * data : {"chats":{"current_page":1,"data":[{"id":33,"admin_id":1,"passenger_id":6,"message":"test","direction":"0","is_read":"1","created_at":"2019-08-09 11:09:30","updated_at":"2019-08-09 17:18:59","first_name":"Parth","last_name":"Mistry","profile_picture":"/assets/images/5d44450ad22d4.png"},{"id":34,"admin_id":1,"passenger_id":6,"message":"test","direction":"0","is_read":"1","created_at":"2019-08-09 11:10:53","updated_at":"2019-08-09 17:18:59","first_name":"Parth","last_name":"Mistry","profile_picture":"/assets/images/5d44450ad22d4.png"},{"id":35,"admin_id":0,"passenger_id":6,"message":"HIii","direction":"1","is_read":"1","created_at":"2019-08-09 11:13:30","updated_at":"2019-08-09 17:18:59","first_name":"Parth","last_name":"Mistry","profile_picture":"/assets/images/5d44450ad22d4.png"},{"id":36,"admin_id":0,"passenger_id":6,"message":"HIii","direction":"1","is_read":"1","created_at":"2019-08-09 11:23:01","updated_at":"2019-08-09 17:18:59","first_name":"Parth","last_name":"Mistry","profile_picture":"/assets/images/5d44450ad22d4.png"},{"id":37,"admin_id":0,"passenger_id":6,"message":"HIii","direction":"1","is_read":"1","created_at":"2019-08-09 11:23:03","updated_at":"2019-08-09 17:18:59","first_name":"Parth","last_name":"Mistry","profile_picture":"/assets/images/5d44450ad22d4.png"},{"id":38,"admin_id":0,"passenger_id":6,"message":"HIii","direction":"1","is_read":"1","created_at":"2019-08-09 11:23:04","updated_at":"2019-08-09 17:18:59","first_name":"Parth","last_name":"Mistry","profile_picture":"/assets/images/5d44450ad22d4.png"},{"id":39,"admin_id":0,"passenger_id":6,"message":"HIii","direction":"1","is_read":"1","created_at":"2019-08-09 11:23:06","updated_at":"2019-08-09 17:18:59","first_name":"Parth","last_name":"Mistry","profile_picture":"/assets/images/5d44450ad22d4.png"},{"id":40,"admin_id":0,"passenger_id":6,"message":"HIii","direction":"1","is_read":"1","created_at":"2019-08-09 11:23:07","updated_at":"2019-08-09 17:18:59","first_name":"Parth","last_name":"Mistry","profile_picture":"/assets/images/5d44450ad22d4.png"},{"id":41,"admin_id":0,"passenger_id":6,"message":"HIii","direction":"1","is_read":"1","created_at":"2019-08-09 11:23:08","updated_at":"2019-08-09 17:18:59","first_name":"Parth","last_name":"Mistry","profile_picture":"/assets/images/5d44450ad22d4.png"},{"id":42,"admin_id":0,"passenger_id":6,"message":"HIii","direction":"1","is_read":"1","created_at":"2019-08-09 11:23:09","updated_at":"2019-08-09 17:18:59","first_name":"Parth","last_name":"Mistry","profile_picture":"/assets/images/5d44450ad22d4.png"},{"id":43,"admin_id":0,"passenger_id":6,"message":"HIii","direction":"1","is_read":"1","created_at":"2019-08-09 11:23:10","updated_at":"2019-08-09 17:18:59","first_name":"Parth","last_name":"Mistry","profile_picture":"/assets/images/5d44450ad22d4.png"},{"id":44,"admin_id":0,"passenger_id":6,"message":"HIii","direction":"1","is_read":"1","created_at":"2019-08-09 11:23:12","updated_at":"2019-08-09 17:18:59","first_name":"Parth","last_name":"Mistry","profile_picture":"/assets/images/5d44450ad22d4.png"},{"id":45,"admin_id":0,"passenger_id":6,"message":"HIii","direction":"1","is_read":"1","created_at":"2019-08-09 11:24:03","updated_at":"2019-08-09 17:18:59","first_name":"Parth","last_name":"Mistry","profile_picture":"/assets/images/5d44450ad22d4.png"},{"id":46,"admin_id":0,"passenger_id":6,"message":"HIii","direction":"1","is_read":"1","created_at":"2019-08-09 11:25:45","updated_at":"2019-08-09 17:18:59","first_name":"Parth","last_name":"Mistry","profile_picture":"/assets/images/5d44450ad22d4.png"},{"id":47,"admin_id":0,"passenger_id":6,"message":"HIii","direction":"1","is_read":"1","created_at":"2019-08-09 11:26:44","updated_at":"2019-08-09 17:18:59","first_name":"Parth","last_name":"Mistry","profile_picture":"/assets/images/5d44450ad22d4.png"},{"id":48,"admin_id":0,"passenger_id":6,"message":"HIii","direction":"1","is_read":"1","created_at":"2019-08-09 11:26:47","updated_at":"2019-08-09 17:18:59","first_name":"Parth","last_name":"Mistry","profile_picture":"/assets/images/5d44450ad22d4.png"},{"id":49,"admin_id":0,"passenger_id":6,"message":"HIii","direction":"1","is_read":"1","created_at":"2019-08-09 11:29:53","updated_at":"2019-08-09 17:18:59","first_name":"Parth","last_name":"Mistry","profile_picture":"/assets/images/5d44450ad22d4.png"},{"id":50,"admin_id":0,"passenger_id":6,"message":"HIii","direction":"1","is_read":"1","created_at":"2019-08-09 11:32:32","updated_at":"2019-08-09 17:18:59","first_name":"Parth","last_name":"Mistry","profile_picture":"/assets/images/5d44450ad22d4.png"},{"id":51,"admin_id":0,"passenger_id":6,"message":"HIii","direction":"1","is_read":"1","created_at":"2019-08-09 11:33:40","updated_at":"2019-08-09 17:18:59","first_name":"Parth","last_name":"Mistry","profile_picture":"/assets/images/5d44450ad22d4.png"},{"id":52,"admin_id":0,"passenger_id":6,"message":"HIii","direction":"1","is_read":"1","created_at":"2019-08-09 11:33:42","updated_at":"2019-08-09 17:18:59","first_name":"Parth","last_name":"Mistry","profile_picture":"/assets/images/5d44450ad22d4.png"},{"id":53,"admin_id":0,"passenger_id":6,"message":"HIii","direction":"1","is_read":"1","created_at":"2019-08-09 11:33:44","updated_at":"2019-08-09 17:18:59","first_name":"Parth","last_name":"Mistry","profile_picture":"/assets/images/5d44450ad22d4.png"},{"id":54,"admin_id":0,"passenger_id":6,"message":"HIii","direction":"1","is_read":"1","created_at":"2019-08-09 11:33:45","updated_at":"2019-08-09 17:18:59","first_name":"Parth","last_name":"Mistry","profile_picture":"/assets/images/5d44450ad22d4.png"},{"id":55,"admin_id":0,"passenger_id":6,"message":"HIii","direction":"1","is_read":"1","created_at":"2019-08-09 11:33:47","updated_at":"2019-08-09 17:18:59","first_name":"Parth","last_name":"Mistry","profile_picture":"/assets/images/5d44450ad22d4.png"},{"id":56,"admin_id":0,"passenger_id":6,"message":"HIii","direction":"1","is_read":"1","created_at":"2019-08-09 11:33:48","updated_at":"2019-08-09 17:18:59","first_name":"Parth","last_name":"Mistry","profile_picture":"/assets/images/5d44450ad22d4.png"},{"id":57,"admin_id":0,"passenger_id":6,"message":"HIii","direction":"1","is_read":"1","created_at":"2019-08-09 11:37:50","updated_at":"2019-08-09 17:18:59","first_name":"Parth","last_name":"Mistry","profile_picture":"/assets/images/5d44450ad22d4.png"},{"id":58,"admin_id":0,"passenger_id":6,"message":"HIii","direction":"1","is_read":"1","created_at":"2019-08-09 11:40:04","updated_at":"2019-08-09 17:18:59","first_name":"Parth","last_name":"Mistry","profile_picture":"/assets/images/5d44450ad22d4.png"},{"id":59,"admin_id":0,"passenger_id":6,"message":"HIii","direction":"1","is_read":"1","created_at":"2019-08-09 11:54:33","updated_at":"2019-08-09 17:18:59","first_name":"Parth","last_name":"Mistry","profile_picture":"/assets/images/5d44450ad22d4.png"},{"id":76,"admin_id":1,"passenger_id":6,"message":"hgkhkhpk","direction":"0","is_read":"1","created_at":"2019-08-09 17:18:59","updated_at":"2019-08-09 17:18:59","first_name":"Parth","last_name":"Mistry","profile_picture":"/assets/images/5d44450ad22d4.png"}],"first_page_url":"http://159.203.105.17/api/getMessages?page=1","from":1,"last_page":1,"last_page_url":"http://159.203.105.17/api/getMessages?page=1","next_page_url":null,"path":"http://159.203.105.17/api/getMessages","per_page":"50","prev_page_url":null,"to":28,"total":28}}
     * message : chat history!
     */

    @SerializedName("error")
    private boolean error;
    @SerializedName("data")
    private DataEntityX data;
    @SerializedName("message")
    private String message;

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public DataEntityX getData() {
        return data;
    }

    public void setData(DataEntityX data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public static class DataEntityX {
        /**
         * chats : {"current_page":1,"data":[{"id":33,"admin_id":1,"passenger_id":6,"message":"test","direction":"0","is_read":"1","created_at":"2019-08-09 11:09:30","updated_at":"2019-08-09 17:18:59","first_name":"Parth","last_name":"Mistry","profile_picture":"/assets/images/5d44450ad22d4.png"},{"id":34,"admin_id":1,"passenger_id":6,"message":"test","direction":"0","is_read":"1","created_at":"2019-08-09 11:10:53","updated_at":"2019-08-09 17:18:59","first_name":"Parth","last_name":"Mistry","profile_picture":"/assets/images/5d44450ad22d4.png"},{"id":35,"admin_id":0,"passenger_id":6,"message":"HIii","direction":"1","is_read":"1","created_at":"2019-08-09 11:13:30","updated_at":"2019-08-09 17:18:59","first_name":"Parth","last_name":"Mistry","profile_picture":"/assets/images/5d44450ad22d4.png"},{"id":36,"admin_id":0,"passenger_id":6,"message":"HIii","direction":"1","is_read":"1","created_at":"2019-08-09 11:23:01","updated_at":"2019-08-09 17:18:59","first_name":"Parth","last_name":"Mistry","profile_picture":"/assets/images/5d44450ad22d4.png"},{"id":37,"admin_id":0,"passenger_id":6,"message":"HIii","direction":"1","is_read":"1","created_at":"2019-08-09 11:23:03","updated_at":"2019-08-09 17:18:59","first_name":"Parth","last_name":"Mistry","profile_picture":"/assets/images/5d44450ad22d4.png"},{"id":38,"admin_id":0,"passenger_id":6,"message":"HIii","direction":"1","is_read":"1","created_at":"2019-08-09 11:23:04","updated_at":"2019-08-09 17:18:59","first_name":"Parth","last_name":"Mistry","profile_picture":"/assets/images/5d44450ad22d4.png"},{"id":39,"admin_id":0,"passenger_id":6,"message":"HIii","direction":"1","is_read":"1","created_at":"2019-08-09 11:23:06","updated_at":"2019-08-09 17:18:59","first_name":"Parth","last_name":"Mistry","profile_picture":"/assets/images/5d44450ad22d4.png"},{"id":40,"admin_id":0,"passenger_id":6,"message":"HIii","direction":"1","is_read":"1","created_at":"2019-08-09 11:23:07","updated_at":"2019-08-09 17:18:59","first_name":"Parth","last_name":"Mistry","profile_picture":"/assets/images/5d44450ad22d4.png"},{"id":41,"admin_id":0,"passenger_id":6,"message":"HIii","direction":"1","is_read":"1","created_at":"2019-08-09 11:23:08","updated_at":"2019-08-09 17:18:59","first_name":"Parth","last_name":"Mistry","profile_picture":"/assets/images/5d44450ad22d4.png"},{"id":42,"admin_id":0,"passenger_id":6,"message":"HIii","direction":"1","is_read":"1","created_at":"2019-08-09 11:23:09","updated_at":"2019-08-09 17:18:59","first_name":"Parth","last_name":"Mistry","profile_picture":"/assets/images/5d44450ad22d4.png"},{"id":43,"admin_id":0,"passenger_id":6,"message":"HIii","direction":"1","is_read":"1","created_at":"2019-08-09 11:23:10","updated_at":"2019-08-09 17:18:59","first_name":"Parth","last_name":"Mistry","profile_picture":"/assets/images/5d44450ad22d4.png"},{"id":44,"admin_id":0,"passenger_id":6,"message":"HIii","direction":"1","is_read":"1","created_at":"2019-08-09 11:23:12","updated_at":"2019-08-09 17:18:59","first_name":"Parth","last_name":"Mistry","profile_picture":"/assets/images/5d44450ad22d4.png"},{"id":45,"admin_id":0,"passenger_id":6,"message":"HIii","direction":"1","is_read":"1","created_at":"2019-08-09 11:24:03","updated_at":"2019-08-09 17:18:59","first_name":"Parth","last_name":"Mistry","profile_picture":"/assets/images/5d44450ad22d4.png"},{"id":46,"admin_id":0,"passenger_id":6,"message":"HIii","direction":"1","is_read":"1","created_at":"2019-08-09 11:25:45","updated_at":"2019-08-09 17:18:59","first_name":"Parth","last_name":"Mistry","profile_picture":"/assets/images/5d44450ad22d4.png"},{"id":47,"admin_id":0,"passenger_id":6,"message":"HIii","direction":"1","is_read":"1","created_at":"2019-08-09 11:26:44","updated_at":"2019-08-09 17:18:59","first_name":"Parth","last_name":"Mistry","profile_picture":"/assets/images/5d44450ad22d4.png"},{"id":48,"admin_id":0,"passenger_id":6,"message":"HIii","direction":"1","is_read":"1","created_at":"2019-08-09 11:26:47","updated_at":"2019-08-09 17:18:59","first_name":"Parth","last_name":"Mistry","profile_picture":"/assets/images/5d44450ad22d4.png"},{"id":49,"admin_id":0,"passenger_id":6,"message":"HIii","direction":"1","is_read":"1","created_at":"2019-08-09 11:29:53","updated_at":"2019-08-09 17:18:59","first_name":"Parth","last_name":"Mistry","profile_picture":"/assets/images/5d44450ad22d4.png"},{"id":50,"admin_id":0,"passenger_id":6,"message":"HIii","direction":"1","is_read":"1","created_at":"2019-08-09 11:32:32","updated_at":"2019-08-09 17:18:59","first_name":"Parth","last_name":"Mistry","profile_picture":"/assets/images/5d44450ad22d4.png"},{"id":51,"admin_id":0,"passenger_id":6,"message":"HIii","direction":"1","is_read":"1","created_at":"2019-08-09 11:33:40","updated_at":"2019-08-09 17:18:59","first_name":"Parth","last_name":"Mistry","profile_picture":"/assets/images/5d44450ad22d4.png"},{"id":52,"admin_id":0,"passenger_id":6,"message":"HIii","direction":"1","is_read":"1","created_at":"2019-08-09 11:33:42","updated_at":"2019-08-09 17:18:59","first_name":"Parth","last_name":"Mistry","profile_picture":"/assets/images/5d44450ad22d4.png"},{"id":53,"admin_id":0,"passenger_id":6,"message":"HIii","direction":"1","is_read":"1","created_at":"2019-08-09 11:33:44","updated_at":"2019-08-09 17:18:59","first_name":"Parth","last_name":"Mistry","profile_picture":"/assets/images/5d44450ad22d4.png"},{"id":54,"admin_id":0,"passenger_id":6,"message":"HIii","direction":"1","is_read":"1","created_at":"2019-08-09 11:33:45","updated_at":"2019-08-09 17:18:59","first_name":"Parth","last_name":"Mistry","profile_picture":"/assets/images/5d44450ad22d4.png"},{"id":55,"admin_id":0,"passenger_id":6,"message":"HIii","direction":"1","is_read":"1","created_at":"2019-08-09 11:33:47","updated_at":"2019-08-09 17:18:59","first_name":"Parth","last_name":"Mistry","profile_picture":"/assets/images/5d44450ad22d4.png"},{"id":56,"admin_id":0,"passenger_id":6,"message":"HIii","direction":"1","is_read":"1","created_at":"2019-08-09 11:33:48","updated_at":"2019-08-09 17:18:59","first_name":"Parth","last_name":"Mistry","profile_picture":"/assets/images/5d44450ad22d4.png"},{"id":57,"admin_id":0,"passenger_id":6,"message":"HIii","direction":"1","is_read":"1","created_at":"2019-08-09 11:37:50","updated_at":"2019-08-09 17:18:59","first_name":"Parth","last_name":"Mistry","profile_picture":"/assets/images/5d44450ad22d4.png"},{"id":58,"admin_id":0,"passenger_id":6,"message":"HIii","direction":"1","is_read":"1","created_at":"2019-08-09 11:40:04","updated_at":"2019-08-09 17:18:59","first_name":"Parth","last_name":"Mistry","profile_picture":"/assets/images/5d44450ad22d4.png"},{"id":59,"admin_id":0,"passenger_id":6,"message":"HIii","direction":"1","is_read":"1","created_at":"2019-08-09 11:54:33","updated_at":"2019-08-09 17:18:59","first_name":"Parth","last_name":"Mistry","profile_picture":"/assets/images/5d44450ad22d4.png"},{"id":76,"admin_id":1,"passenger_id":6,"message":"hgkhkhpk","direction":"0","is_read":"1","created_at":"2019-08-09 17:18:59","updated_at":"2019-08-09 17:18:59","first_name":"Parth","last_name":"Mistry","profile_picture":"/assets/images/5d44450ad22d4.png"}],"first_page_url":"http://159.203.105.17/api/getMessages?page=1","from":1,"last_page":1,"last_page_url":"http://159.203.105.17/api/getMessages?page=1","next_page_url":null,"path":"http://159.203.105.17/api/getMessages","per_page":"50","prev_page_url":null,"to":28,"total":28}
         */

        @SerializedName("chats")
        private ChatsEntity chats;

        public ChatsEntity getChats() {
            return chats;
        }

        public void setChats(ChatsEntity chats) {
            this.chats = chats;
        }

        public static class ChatsEntity {
            /**
             * current_page : 1
             * data : [{"id":33,"admin_id":1,"passenger_id":6,"message":"test","direction":"0","is_read":"1","created_at":"2019-08-09 11:09:30","updated_at":"2019-08-09 17:18:59","first_name":"Parth","last_name":"Mistry","profile_picture":"/assets/images/5d44450ad22d4.png"},{"id":34,"admin_id":1,"passenger_id":6,"message":"test","direction":"0","is_read":"1","created_at":"2019-08-09 11:10:53","updated_at":"2019-08-09 17:18:59","first_name":"Parth","last_name":"Mistry","profile_picture":"/assets/images/5d44450ad22d4.png"},{"id":35,"admin_id":0,"passenger_id":6,"message":"HIii","direction":"1","is_read":"1","created_at":"2019-08-09 11:13:30","updated_at":"2019-08-09 17:18:59","first_name":"Parth","last_name":"Mistry","profile_picture":"/assets/images/5d44450ad22d4.png"},{"id":36,"admin_id":0,"passenger_id":6,"message":"HIii","direction":"1","is_read":"1","created_at":"2019-08-09 11:23:01","updated_at":"2019-08-09 17:18:59","first_name":"Parth","last_name":"Mistry","profile_picture":"/assets/images/5d44450ad22d4.png"},{"id":37,"admin_id":0,"passenger_id":6,"message":"HIii","direction":"1","is_read":"1","created_at":"2019-08-09 11:23:03","updated_at":"2019-08-09 17:18:59","first_name":"Parth","last_name":"Mistry","profile_picture":"/assets/images/5d44450ad22d4.png"},{"id":38,"admin_id":0,"passenger_id":6,"message":"HIii","direction":"1","is_read":"1","created_at":"2019-08-09 11:23:04","updated_at":"2019-08-09 17:18:59","first_name":"Parth","last_name":"Mistry","profile_picture":"/assets/images/5d44450ad22d4.png"},{"id":39,"admin_id":0,"passenger_id":6,"message":"HIii","direction":"1","is_read":"1","created_at":"2019-08-09 11:23:06","updated_at":"2019-08-09 17:18:59","first_name":"Parth","last_name":"Mistry","profile_picture":"/assets/images/5d44450ad22d4.png"},{"id":40,"admin_id":0,"passenger_id":6,"message":"HIii","direction":"1","is_read":"1","created_at":"2019-08-09 11:23:07","updated_at":"2019-08-09 17:18:59","first_name":"Parth","last_name":"Mistry","profile_picture":"/assets/images/5d44450ad22d4.png"},{"id":41,"admin_id":0,"passenger_id":6,"message":"HIii","direction":"1","is_read":"1","created_at":"2019-08-09 11:23:08","updated_at":"2019-08-09 17:18:59","first_name":"Parth","last_name":"Mistry","profile_picture":"/assets/images/5d44450ad22d4.png"},{"id":42,"admin_id":0,"passenger_id":6,"message":"HIii","direction":"1","is_read":"1","created_at":"2019-08-09 11:23:09","updated_at":"2019-08-09 17:18:59","first_name":"Parth","last_name":"Mistry","profile_picture":"/assets/images/5d44450ad22d4.png"},{"id":43,"admin_id":0,"passenger_id":6,"message":"HIii","direction":"1","is_read":"1","created_at":"2019-08-09 11:23:10","updated_at":"2019-08-09 17:18:59","first_name":"Parth","last_name":"Mistry","profile_picture":"/assets/images/5d44450ad22d4.png"},{"id":44,"admin_id":0,"passenger_id":6,"message":"HIii","direction":"1","is_read":"1","created_at":"2019-08-09 11:23:12","updated_at":"2019-08-09 17:18:59","first_name":"Parth","last_name":"Mistry","profile_picture":"/assets/images/5d44450ad22d4.png"},{"id":45,"admin_id":0,"passenger_id":6,"message":"HIii","direction":"1","is_read":"1","created_at":"2019-08-09 11:24:03","updated_at":"2019-08-09 17:18:59","first_name":"Parth","last_name":"Mistry","profile_picture":"/assets/images/5d44450ad22d4.png"},{"id":46,"admin_id":0,"passenger_id":6,"message":"HIii","direction":"1","is_read":"1","created_at":"2019-08-09 11:25:45","updated_at":"2019-08-09 17:18:59","first_name":"Parth","last_name":"Mistry","profile_picture":"/assets/images/5d44450ad22d4.png"},{"id":47,"admin_id":0,"passenger_id":6,"message":"HIii","direction":"1","is_read":"1","created_at":"2019-08-09 11:26:44","updated_at":"2019-08-09 17:18:59","first_name":"Parth","last_name":"Mistry","profile_picture":"/assets/images/5d44450ad22d4.png"},{"id":48,"admin_id":0,"passenger_id":6,"message":"HIii","direction":"1","is_read":"1","created_at":"2019-08-09 11:26:47","updated_at":"2019-08-09 17:18:59","first_name":"Parth","last_name":"Mistry","profile_picture":"/assets/images/5d44450ad22d4.png"},{"id":49,"admin_id":0,"passenger_id":6,"message":"HIii","direction":"1","is_read":"1","created_at":"2019-08-09 11:29:53","updated_at":"2019-08-09 17:18:59","first_name":"Parth","last_name":"Mistry","profile_picture":"/assets/images/5d44450ad22d4.png"},{"id":50,"admin_id":0,"passenger_id":6,"message":"HIii","direction":"1","is_read":"1","created_at":"2019-08-09 11:32:32","updated_at":"2019-08-09 17:18:59","first_name":"Parth","last_name":"Mistry","profile_picture":"/assets/images/5d44450ad22d4.png"},{"id":51,"admin_id":0,"passenger_id":6,"message":"HIii","direction":"1","is_read":"1","created_at":"2019-08-09 11:33:40","updated_at":"2019-08-09 17:18:59","first_name":"Parth","last_name":"Mistry","profile_picture":"/assets/images/5d44450ad22d4.png"},{"id":52,"admin_id":0,"passenger_id":6,"message":"HIii","direction":"1","is_read":"1","created_at":"2019-08-09 11:33:42","updated_at":"2019-08-09 17:18:59","first_name":"Parth","last_name":"Mistry","profile_picture":"/assets/images/5d44450ad22d4.png"},{"id":53,"admin_id":0,"passenger_id":6,"message":"HIii","direction":"1","is_read":"1","created_at":"2019-08-09 11:33:44","updated_at":"2019-08-09 17:18:59","first_name":"Parth","last_name":"Mistry","profile_picture":"/assets/images/5d44450ad22d4.png"},{"id":54,"admin_id":0,"passenger_id":6,"message":"HIii","direction":"1","is_read":"1","created_at":"2019-08-09 11:33:45","updated_at":"2019-08-09 17:18:59","first_name":"Parth","last_name":"Mistry","profile_picture":"/assets/images/5d44450ad22d4.png"},{"id":55,"admin_id":0,"passenger_id":6,"message":"HIii","direction":"1","is_read":"1","created_at":"2019-08-09 11:33:47","updated_at":"2019-08-09 17:18:59","first_name":"Parth","last_name":"Mistry","profile_picture":"/assets/images/5d44450ad22d4.png"},{"id":56,"admin_id":0,"passenger_id":6,"message":"HIii","direction":"1","is_read":"1","created_at":"2019-08-09 11:33:48","updated_at":"2019-08-09 17:18:59","first_name":"Parth","last_name":"Mistry","profile_picture":"/assets/images/5d44450ad22d4.png"},{"id":57,"admin_id":0,"passenger_id":6,"message":"HIii","direction":"1","is_read":"1","created_at":"2019-08-09 11:37:50","updated_at":"2019-08-09 17:18:59","first_name":"Parth","last_name":"Mistry","profile_picture":"/assets/images/5d44450ad22d4.png"},{"id":58,"admin_id":0,"passenger_id":6,"message":"HIii","direction":"1","is_read":"1","created_at":"2019-08-09 11:40:04","updated_at":"2019-08-09 17:18:59","first_name":"Parth","last_name":"Mistry","profile_picture":"/assets/images/5d44450ad22d4.png"},{"id":59,"admin_id":0,"passenger_id":6,"message":"HIii","direction":"1","is_read":"1","created_at":"2019-08-09 11:54:33","updated_at":"2019-08-09 17:18:59","first_name":"Parth","last_name":"Mistry","profile_picture":"/assets/images/5d44450ad22d4.png"},{"id":76,"admin_id":1,"passenger_id":6,"message":"hgkhkhpk","direction":"0","is_read":"1","created_at":"2019-08-09 17:18:59","updated_at":"2019-08-09 17:18:59","first_name":"Parth","last_name":"Mistry","profile_picture":"/assets/images/5d44450ad22d4.png"}]
             * first_page_url : http://159.203.105.17/api/getMessages?page=1
             * from : 1
             * last_page : 1
             * last_page_url : http://159.203.105.17/api/getMessages?page=1
             * next_page_url : null
             * path : http://159.203.105.17/api/getMessages
             * per_page : 50
             * prev_page_url : null
             * to : 28
             * total : 28
             */

            @SerializedName("current_page")
            private int currentPage;
            @SerializedName("first_page_url")
            private String firstPageUrl;
            @SerializedName("from")
            private int from;
            @SerializedName("last_page")
            private int lastPage;
            @SerializedName("last_page_url")
            private String lastPageUrl;
            @SerializedName("next_page_url")
            private Object nextPageUrl;
            @SerializedName("path")
            private String path;
            @SerializedName("per_page")
            private String perPage;
            @SerializedName("prev_page_url")
            private Object prevPageUrl;
            @SerializedName("to")
            private int to;
            @SerializedName("total")
            private int total;
            @SerializedName("data")
            private List<DataEntity> data;

            public int getCurrentPage() {
                return currentPage;
            }

            public void setCurrentPage(int currentPage) {
                this.currentPage = currentPage;
            }

            public String getFirstPageUrl() {
                return firstPageUrl;
            }

            public void setFirstPageUrl(String firstPageUrl) {
                this.firstPageUrl = firstPageUrl;
            }

            public int getFrom() {
                return from;
            }

            public void setFrom(int from) {
                this.from = from;
            }

            public int getLastPage() {
                return lastPage;
            }

            public void setLastPage(int lastPage) {
                this.lastPage = lastPage;
            }

            public String getLastPageUrl() {
                return lastPageUrl;
            }

            public void setLastPageUrl(String lastPageUrl) {
                this.lastPageUrl = lastPageUrl;
            }

            public Object getNextPageUrl() {
                return nextPageUrl;
            }

            public void setNextPageUrl(Object nextPageUrl) {
                this.nextPageUrl = nextPageUrl;
            }

            public String getPath() {
                return path;
            }

            public void setPath(String path) {
                this.path = path;
            }

            public String getPerPage() {
                return perPage;
            }

            public void setPerPage(String perPage) {
                this.perPage = perPage;
            }

            public Object getPrevPageUrl() {
                return prevPageUrl;
            }

            public void setPrevPageUrl(Object prevPageUrl) {
                this.prevPageUrl = prevPageUrl;
            }

            public int getTo() {
                return to;
            }

            public void setTo(int to) {
                this.to = to;
            }

            public int getTotal() {
                return total;
            }

            public void setTotal(int total) {
                this.total = total;
            }

            public List<DataEntity> getData() {
                return data;
            }

            public void setData(List<DataEntity> data) {
                this.data = data;
            }

            public static class DataEntity {
                /**
                 * id : 33
                 * admin_id : 1
                 * passenger_id : 6
                 * message : test
                 * direction : 0
                 * is_read : 1
                 * created_at : 2019-08-09 11:09:30
                 * updated_at : 2019-08-09 17:18:59
                 * first_name : Parth
                 * last_name : Mistry
                 * profile_picture : /assets/images/5d44450ad22d4.png
                 */

                @SerializedName("id")
                private int id;
                @SerializedName("admin_id")
                private int adminId;
                @SerializedName("passenger_id")
                private int passengerId;
                @SerializedName("message")
                private String message;
                @SerializedName("direction")
                private String direction;
                @SerializedName("is_read")
                private String isRead;
                @SerializedName("created_at")
                private String createdAt;
                @SerializedName("updated_at")
                private String updatedAt;
                @SerializedName("first_name")
                private String firstName;
                @SerializedName("last_name")
                private String lastName;
                @SerializedName("profile_picture")
                private String profilePicture;

                public int getId() {
                    return id;
                }

                public void setId(int id) {
                    this.id = id;
                }

                public int getAdminId() {
                    return adminId;
                }

                public void setAdminId(int adminId) {
                    this.adminId = adminId;
                }

                public int getPassengerId() {
                    return passengerId;
                }

                public void setPassengerId(int passengerId) {
                    this.passengerId = passengerId;
                }

                public String getMessage() {
                    return message;
                }

                public void setMessage(String message) {
                    this.message = message;
                }

                public String getDirection() {
                    return direction;
                }

                public void setDirection(String direction) {
                    this.direction = direction;
                }

                public String getIsRead() {
                    return isRead;
                }

                public void setIsRead(String isRead) {
                    this.isRead = isRead;
                }

                public String getCreatedAt() {
                    return createdAt;
                }

                public void setCreatedAt(String createdAt) {
                    this.createdAt = createdAt;
                }

                public String getUpdatedAt() {
                    return updatedAt;
                }

                public void setUpdatedAt(String updatedAt) {
                    this.updatedAt = updatedAt;
                }

                public String getFirstName() {
                    return firstName;
                }

                public void setFirstName(String firstName) {
                    this.firstName = firstName;
                }

                public String getLastName() {
                    return lastName;
                }

                public void setLastName(String lastName) {
                    this.lastName = lastName;
                }

                public String getProfilePicture() {
                    return profilePicture;
                }

                public void setProfilePicture(String profilePicture) {
                    this.profilePicture = profilePicture;
                }
            }
        }
    }
}
