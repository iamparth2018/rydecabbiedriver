package com.rydedispatch.driver.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RespAcceptRide {


    /**
     * error : false
     * data : {"device_token":"f1cyVgqIXJY:APA91bEin14SFR_raoGZ9YMMwx8N3v8rQ5e7BrOYxwVwF8e6GZM896IWXC85iPYR66XUQXbarvwenXpHB626geHPqfayHRzsJ9HYEXiINQRm-X2MsXPORX4-hdr4hGGRt6wxuGgdKDU_","fcmResult":{"multicast_id":8551852394714572978,"success":1,"failure":0,"canonical_ids":0,"results":[{"message_id":"1565098284280014"}]}}
     * message : Your ride has been accepted Successfully.
     */

    @SerializedName("error")
    private boolean error;
    @SerializedName("data")
    private DataEntity data;
    @SerializedName("message")
    private String message;

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public DataEntity getData() {
        return data;
    }

    public void setData(DataEntity data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public static class DataEntity {
        /**
         * device_token : f1cyVgqIXJY:APA91bEin14SFR_raoGZ9YMMwx8N3v8rQ5e7BrOYxwVwF8e6GZM896IWXC85iPYR66XUQXbarvwenXpHB626geHPqfayHRzsJ9HYEXiINQRm-X2MsXPORX4-hdr4hGGRt6wxuGgdKDU_
         * fcmResult : {"multicast_id":8551852394714572978,"success":1,"failure":0,"canonical_ids":0,"results":[{"message_id":"1565098284280014"}]}
         */

        @SerializedName("device_token")
        private String deviceToken;
        @SerializedName("fcmResult")
        private FcmResultEntity fcmResult;

        public String getDeviceToken() {
            return deviceToken;
        }

        public void setDeviceToken(String deviceToken) {
            this.deviceToken = deviceToken;
        }

        public FcmResultEntity getFcmResult() {
            return fcmResult;
        }

        public void setFcmResult(FcmResultEntity fcmResult) {
            this.fcmResult = fcmResult;
        }

        public static class FcmResultEntity {
            /**
             * multicast_id : 8551852394714572978
             * success : 1
             * failure : 0
             * canonical_ids : 0
             * results : [{"message_id":"1565098284280014"}]
             */

            @SerializedName("multicast_id")
            private long multicastId;
            @SerializedName("success")
            private int success;
            @SerializedName("failure")
            private int failure;
            @SerializedName("canonical_ids")
            private int canonicalIds;
            @SerializedName("results")
            private List<ResultsEntity> results;

            public long getMulticastId() {
                return multicastId;
            }

            public void setMulticastId(long multicastId) {
                this.multicastId = multicastId;
            }

            public int getSuccess() {
                return success;
            }

            public void setSuccess(int success) {
                this.success = success;
            }

            public int getFailure() {
                return failure;
            }

            public void setFailure(int failure) {
                this.failure = failure;
            }

            public int getCanonicalIds() {
                return canonicalIds;
            }

            public void setCanonicalIds(int canonicalIds) {
                this.canonicalIds = canonicalIds;
            }

            public List<ResultsEntity> getResults() {
                return results;
            }

            public void setResults(List<ResultsEntity> results) {
                this.results = results;
            }

            public static class ResultsEntity {
                /**
                 * message_id : 1565098284280014
                 */

                @SerializedName("message_id")
                private String messageId;

                public String getMessageId() {
                    return messageId;
                }

                public void setMessageId(String messageId) {
                    this.messageId = messageId;
                }
            }
        }
    }
}
