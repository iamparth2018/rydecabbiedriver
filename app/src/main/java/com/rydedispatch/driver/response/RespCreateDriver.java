package com.rydedispatch.driver.response;

import com.google.gson.annotations.SerializedName;

public class RespCreateDriver {


    /**
     * error : false
     * data : {"userDetails":{"id":3,"parent_id":0,"company_id":0,"first_name":"Parth","last_name":"Mistry","company_name":"","company_image":null,"contact_person":"","profile_picture":null,"email":"parth.samcom2018@gmail.com","email_verified_at":null,"phone_number":"+919712146184","address":"","country":"","state":"","city":"","device_token":"12334tffsdffsd","device_type":"android","latitude":"","longitude":"","status":"active","user_status":"offline","authy_id":"127634683","user_unique_code":"Parth1003","tax_id":null,"commission_type":"ammount","commission":null,"role":"driver","document_status":"pending","deleted_at":null,"created_at":"2019-07-24 12:22:17","updated_at":"2019-07-24 12:22:17","vehicle":"","token":"eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImQyMTdjYTc0NDMwMTVhODIxMjM3YWI4YzIwOWNmMjFjYjVmMmQ0M2EwOWY1NmEzNGM3NjUyOTM3Y2ZlMmY5ZmZiZTM1YmZmNzI1OTkyZTA2In0.eyJhdWQiOiIxIiwianRpIjoiZDIxN2NhNzQ0MzAxNWE4MjEyMzdhYjhjMjA5Y2YyMWNiNWYyZDQzYTA5ZjU2YTM0Yzc2NTI5MzdjZmUyZjlmZmJlMzViZmY3MjU5OTJlMDYiLCJpYXQiOjE1NjM5NzA5MzcsIm5iZiI6MTU2Mzk3MDkzNywiZXhwIjoxNTk1NTkzMzM3LCJzdWIiOiIzIiwic2NvcGVzIjpbXX0.eMz3UkQIyya5ZlaqXcyI8tvzC0PuHKNYBCiUf3JkU-kt18xr3hCN3EJ2TCU-EWTdNijWgY8KWy6j1_p2xK0GF9RaQtd2bbvoNPsnsGRtsbiDvVx8J_iwHXd4Kk-OQWvx2yaq8geLOqvAj5KZTqpTRpB6Mp5TwAe82VXpLuFGSsr2imiaPRu1nQtDZzI-9Du5EpIje0GazrGgqt_4EPhIbaFP6NmCLVPJPLY-4iFuCG411GuqoaTGeE9nVirDsgEFkDUukoNoNv-FNvRQ0n-FJx9dUzKfzSNG48bt9IAcOyac5goA5pwYxyK0cQMT8MuOfkML6g3gTUEzP-ukm0tWquzBK_DDanvcX3l3fPYodkmH8Y1q1OQEEvtPh5m27-D5scXxPN9B9EDnm_ZhAev2gY3oGd8BI3icu77Xfr-8Is4jfTelOMqEVzgp_I1VO9IiSqraVpUCdpV3c8ToErX6X7pm_dzxdc8-9CxCoOwMRpK27Vv_1gKl48sYk9McluY6yY_ieS_JXJHwvEdo1mkoQeGUazaixHv4wNvKA7S98neVIBbt1KNEFBZmA8u4jSWHEm2Rjw1iViSvmImbqetKIVEMz16_KqPsIj04MmHmbb45dH8syh40NRymXcYFGNodRr7muCGzW47v_udfldZFhyQbUa1wxK3QngSgTudDTvc"}}
     * message : Driver has been Registered successfully.
     */

    @SerializedName("error")
    private boolean error;
    @SerializedName("data")
    private DataEntity data;
    @SerializedName("message")
    private String message;

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public DataEntity getData() {
        return data;
    }

    public void setData(DataEntity data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public static class DataEntity {
        /**
         * userDetails : {"id":3,"parent_id":0,"company_id":0,"first_name":"Parth","last_name":"Mistry","company_name":"","company_image":null,"contact_person":"","profile_picture":null,"email":"parth.samcom2018@gmail.com","email_verified_at":null,"phone_number":"+919712146184","address":"","country":"","state":"","city":"","device_token":"12334tffsdffsd","device_type":"android","latitude":"","longitude":"","status":"active","user_status":"offline","authy_id":"127634683","user_unique_code":"Parth1003","tax_id":null,"commission_type":"ammount","commission":null,"role":"driver","document_status":"pending","deleted_at":null,"created_at":"2019-07-24 12:22:17","updated_at":"2019-07-24 12:22:17","vehicle":"","token":"eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImQyMTdjYTc0NDMwMTVhODIxMjM3YWI4YzIwOWNmMjFjYjVmMmQ0M2EwOWY1NmEzNGM3NjUyOTM3Y2ZlMmY5ZmZiZTM1YmZmNzI1OTkyZTA2In0.eyJhdWQiOiIxIiwianRpIjoiZDIxN2NhNzQ0MzAxNWE4MjEyMzdhYjhjMjA5Y2YyMWNiNWYyZDQzYTA5ZjU2YTM0Yzc2NTI5MzdjZmUyZjlmZmJlMzViZmY3MjU5OTJlMDYiLCJpYXQiOjE1NjM5NzA5MzcsIm5iZiI6MTU2Mzk3MDkzNywiZXhwIjoxNTk1NTkzMzM3LCJzdWIiOiIzIiwic2NvcGVzIjpbXX0.eMz3UkQIyya5ZlaqXcyI8tvzC0PuHKNYBCiUf3JkU-kt18xr3hCN3EJ2TCU-EWTdNijWgY8KWy6j1_p2xK0GF9RaQtd2bbvoNPsnsGRtsbiDvVx8J_iwHXd4Kk-OQWvx2yaq8geLOqvAj5KZTqpTRpB6Mp5TwAe82VXpLuFGSsr2imiaPRu1nQtDZzI-9Du5EpIje0GazrGgqt_4EPhIbaFP6NmCLVPJPLY-4iFuCG411GuqoaTGeE9nVirDsgEFkDUukoNoNv-FNvRQ0n-FJx9dUzKfzSNG48bt9IAcOyac5goA5pwYxyK0cQMT8MuOfkML6g3gTUEzP-ukm0tWquzBK_DDanvcX3l3fPYodkmH8Y1q1OQEEvtPh5m27-D5scXxPN9B9EDnm_ZhAev2gY3oGd8BI3icu77Xfr-8Is4jfTelOMqEVzgp_I1VO9IiSqraVpUCdpV3c8ToErX6X7pm_dzxdc8-9CxCoOwMRpK27Vv_1gKl48sYk9McluY6yY_ieS_JXJHwvEdo1mkoQeGUazaixHv4wNvKA7S98neVIBbt1KNEFBZmA8u4jSWHEm2Rjw1iViSvmImbqetKIVEMz16_KqPsIj04MmHmbb45dH8syh40NRymXcYFGNodRr7muCGzW47v_udfldZFhyQbUa1wxK3QngSgTudDTvc"}
         */

        @SerializedName("userDetails")
        private UserDetailsEntity userDetails;

        public UserDetailsEntity getUserDetails() {
            return userDetails;
        }

        public void setUserDetails(UserDetailsEntity userDetails) {
            this.userDetails = userDetails;
        }

        public static class UserDetailsEntity {
            /**
             * id : 3
             * parent_id : 0
             * company_id : 0
             * first_name : Parth
             * last_name : Mistry
             * company_name :
             * company_image : null
             * contact_person :
             * profile_picture : null
             * email : parth.samcom2018@gmail.com
             * email_verified_at : null
             * phone_number : +919712146184
             * address :
             * country :
             * state :
             * city :
             * device_token : 12334tffsdffsd
             * device_type : android
             * latitude :
             * longitude :
             * status : active
             * user_status : offline
             * authy_id : 127634683
             * user_unique_code : Parth1003
             * tax_id : null
             * commission_type : ammount
             * commission : null
             * role : driver
             * document_status : pending
             * deleted_at : null
             * created_at : 2019-07-24 12:22:17
             * updated_at : 2019-07-24 12:22:17
             * vehicle :
             * token : eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImQyMTdjYTc0NDMwMTVhODIxMjM3YWI4YzIwOWNmMjFjYjVmMmQ0M2EwOWY1NmEzNGM3NjUyOTM3Y2ZlMmY5ZmZiZTM1YmZmNzI1OTkyZTA2In0.eyJhdWQiOiIxIiwianRpIjoiZDIxN2NhNzQ0MzAxNWE4MjEyMzdhYjhjMjA5Y2YyMWNiNWYyZDQzYTA5ZjU2YTM0Yzc2NTI5MzdjZmUyZjlmZmJlMzViZmY3MjU5OTJlMDYiLCJpYXQiOjE1NjM5NzA5MzcsIm5iZiI6MTU2Mzk3MDkzNywiZXhwIjoxNTk1NTkzMzM3LCJzdWIiOiIzIiwic2NvcGVzIjpbXX0.eMz3UkQIyya5ZlaqXcyI8tvzC0PuHKNYBCiUf3JkU-kt18xr3hCN3EJ2TCU-EWTdNijWgY8KWy6j1_p2xK0GF9RaQtd2bbvoNPsnsGRtsbiDvVx8J_iwHXd4Kk-OQWvx2yaq8geLOqvAj5KZTqpTRpB6Mp5TwAe82VXpLuFGSsr2imiaPRu1nQtDZzI-9Du5EpIje0GazrGgqt_4EPhIbaFP6NmCLVPJPLY-4iFuCG411GuqoaTGeE9nVirDsgEFkDUukoNoNv-FNvRQ0n-FJx9dUzKfzSNG48bt9IAcOyac5goA5pwYxyK0cQMT8MuOfkML6g3gTUEzP-ukm0tWquzBK_DDanvcX3l3fPYodkmH8Y1q1OQEEvtPh5m27-D5scXxPN9B9EDnm_ZhAev2gY3oGd8BI3icu77Xfr-8Is4jfTelOMqEVzgp_I1VO9IiSqraVpUCdpV3c8ToErX6X7pm_dzxdc8-9CxCoOwMRpK27Vv_1gKl48sYk9McluY6yY_ieS_JXJHwvEdo1mkoQeGUazaixHv4wNvKA7S98neVIBbt1KNEFBZmA8u4jSWHEm2Rjw1iViSvmImbqetKIVEMz16_KqPsIj04MmHmbb45dH8syh40NRymXcYFGNodRr7muCGzW47v_udfldZFhyQbUa1wxK3QngSgTudDTvc
             */

            @SerializedName("id")
            private int id;
            @SerializedName("parent_id")
            private int parentId;
            @SerializedName("company_id")
            private int companyId;
            @SerializedName("first_name")
            private String firstName;
            @SerializedName("last_name")
            private String lastName;
            @SerializedName("company_name")
            private String companyName;
            @SerializedName("company_image")
            private String companyImage;
            @SerializedName("contact_person")
            private String contactPerson;
            @SerializedName("profile_picture")
            private String profilePicture;
            @SerializedName("email")
            private String email;
            @SerializedName("email_verified_at")
            private String emailVerifiedAt;
            @SerializedName("phone_number")
            private String phoneNumber;
            @SerializedName("address")
            private String address;
            @SerializedName("country")
            private String country;
            @SerializedName("state")
            private String state;
            @SerializedName("city")
            private String city;
            @SerializedName("device_token")
            private String deviceToken;
            @SerializedName("device_type")
            private String deviceType;
            @SerializedName("latitude")
            private String latitude;
            @SerializedName("longitude")
            private String longitude;
            @SerializedName("status")
            private String status;
            @SerializedName("user_status")
            private String userStatus;
            @SerializedName("authy_id")
            private String authyId;
            @SerializedName("user_unique_code")
            private String userUniqueCode;
            @SerializedName("tax_id")
            private String taxId;
            @SerializedName("commission_type")
            private String commissionType;
            @SerializedName("commission")
            private String commission;
            @SerializedName("role")
            private String role;
            @SerializedName("document_status")
            private String documentStatus;
            @SerializedName("deleted_at")
            private String deletedAt;
            @SerializedName("created_at")
            private String createdAt;
            @SerializedName("updated_at")
            private String updatedAt;
//            @SerializedName("vehicle")
//            private String vehicle;
            @SerializedName("vehicle")
            private VehicleEntity vehicle;
            @SerializedName("token")
            private String token;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public int getParentId() {
                return parentId;
            }

            public void setParentId(int parentId) {
                this.parentId = parentId;
            }

            public int getCompanyId() {
                return companyId;
            }

            public void setCompanyId(int companyId) {
                this.companyId = companyId;
            }

            public String getFirstName() {
                return firstName;
            }

            public void setFirstName(String firstName) {
                this.firstName = firstName;
            }

            public String getLastName() {
                return lastName;
            }

            public void setLastName(String lastName) {
                this.lastName = lastName;
            }

            public String getCompanyName() {
                return companyName;
            }

            public void setCompanyName(String companyName) {
                this.companyName = companyName;
            }

            public String getCompanyImage() {
                return companyImage;
            }

            public void setCompanyImage(String companyImage) {
                this.companyImage = companyImage;
            }

            public String getContactPerson() {
                return contactPerson;
            }

            public void setContactPerson(String contactPerson) {
                this.contactPerson = contactPerson;
            }

            public String getProfilePicture() {
                return profilePicture;
            }

            public void setProfilePicture(String profilePicture) {
                this.profilePicture = profilePicture;
            }

            public String getEmail() {
                return email;
            }

            public void setEmail(String email) {
                this.email = email;
            }

            public String getEmailVerifiedAt() {
                return emailVerifiedAt;
            }

            public void setEmailVerifiedAt(String emailVerifiedAt) {
                this.emailVerifiedAt = emailVerifiedAt;
            }

            public String getPhoneNumber() {
                return phoneNumber;
            }

            public void setPhoneNumber(String phoneNumber) {
                this.phoneNumber = phoneNumber;
            }

            public String getAddress() {
                return address;
            }

            public void setAddress(String address) {
                this.address = address;
            }

            public String getCountry() {
                return country;
            }

            public void setCountry(String country) {
                this.country = country;
            }

            public String getState() {
                return state;
            }

            public void setState(String state) {
                this.state = state;
            }

            public String getCity() {
                return city;
            }

            public void setCity(String city) {
                this.city = city;
            }

            public String getDeviceToken() {
                return deviceToken;
            }

            public void setDeviceToken(String deviceToken) {
                this.deviceToken = deviceToken;
            }

            public String getDeviceType() {
                return deviceType;
            }

            public void setDeviceType(String deviceType) {
                this.deviceType = deviceType;
            }

            public String getLatitude() {
                return latitude;
            }

            public void setLatitude(String latitude) {
                this.latitude = latitude;
            }

            public String getLongitude() {
                return longitude;
            }

            public void setLongitude(String longitude) {
                this.longitude = longitude;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }

            public String getUserStatus() {
                return userStatus;
            }

            public void setUserStatus(String userStatus) {
                this.userStatus = userStatus;
            }

            public String getAuthyId() {
                return authyId;
            }

            public void setAuthyId(String authyId) {
                this.authyId = authyId;
            }

            public String getUserUniqueCode() {
                return userUniqueCode;
            }

            public void setUserUniqueCode(String userUniqueCode) {
                this.userUniqueCode = userUniqueCode;
            }

            public String getTaxId() {
                return taxId;
            }

            public void setTaxId(String taxId) {
                this.taxId = taxId;
            }

            public String getCommissionType() {
                return commissionType;
            }

            public void setCommissionType(String commissionType) {
                this.commissionType = commissionType;
            }

            public String getCommission() {
                return commission;
            }

            public void setCommission(String commission) {
                this.commission = commission;
            }

            public String getRole() {
                return role;
            }

            public void setRole(String role) {
                this.role = role;
            }

            public String getDocumentStatus() {
                return documentStatus;
            }

            public void setDocumentStatus(String documentStatus) {
                this.documentStatus = documentStatus;
            }

            public String getDeletedAt() {
                return deletedAt;
            }

            public void setDeletedAt(String deletedAt) {
                this.deletedAt = deletedAt;
            }

            public String getCreatedAt() {
                return createdAt;
            }

            public void setCreatedAt(String createdAt) {
                this.createdAt = createdAt;
            }

            public String getUpdatedAt() {
                return updatedAt;
            }

            public void setUpdatedAt(String updatedAt) {
                this.updatedAt = updatedAt;
            }

//            public String getVehicle() {
//                return vehicle;
//            }

//            public void setVehicle(String vehicle) {
//                this.vehicle = vehicle;
//            }

            public static class VehicleEntity {

                @SerializedName("id")
                private int id;
                @SerializedName("user_id")
                private int userId;
                @SerializedName("vin_code")
                private String vinCode;
                @SerializedName("make")
                private String make;
                @SerializedName("model")
                private String model;
                @SerializedName("year")
                private String year;
                @SerializedName("transmission")
                private String transmission;
                @SerializedName("color")
                private String color;
                @SerializedName("driver_train")
                private String driverTrain;
                @SerializedName("car_type")
                private String carType;
                @SerializedName("car_type_id")
                private String car_type_id;



                @SerializedName("deleted_at")
                private String deletedAt;
                @SerializedName("created_at")
                private String createdAt;
                @SerializedName("updated_at")
                private String updatedAt;

                public int getId() {
                    return id;
                }

                public void setId(int id) {
                    this.id = id;
                }

                public int getUserId() {
                    return userId;
                }

                public void setUserId(int userId) {
                    this.userId = userId;
                }

                public String getVinCode() {
                    return vinCode;
                }

                public void setVinCode(String vinCode) {
                    this.vinCode = vinCode;
                }

                public String getMake() {
                    return make;
                }

                public void setMake(String make) {
                    this.make = make;
                }

                public String getModel() {
                    return model;
                }

                public void setModel(String model) {
                    this.model = model;
                }

                public String getYear() {
                    return year;
                }

                public void setYear(String year) {
                    this.year = year;
                }

                public String getTransmission() {
                    return transmission;
                }

                public void setTransmission(String transmission) {
                    this.transmission = transmission;
                }

                public String getColor() {
                    return color;
                }

                public void setColor(String color) {
                    this.color = color;
                }

                public String getDriverTrain() {
                    return driverTrain;
                }

                public void setDriverTrain(String driverTrain) {
                    this.driverTrain = driverTrain;
                }


                public String getCarType() {
                    return carType;
                }

                public void setCarType(String carType) {
                    this.carType = carType;
                }

                public String getCar_type_id() {
                    return car_type_id;
                }

                public void setCar_type_id(String car_type_id) {
                    this.car_type_id = car_type_id;
                }

                public String getDeletedAt() {
                    return deletedAt;
                }

                public void setDeletedAt(String deletedAt) {
                    this.deletedAt = deletedAt;
                }

                public String getCreatedAt() {
                    return createdAt;
                }

                public void setCreatedAt(String createdAt) {
                    this.createdAt = createdAt;
                }

                public String getUpdatedAt() {
                    return updatedAt;
                }

                public void setUpdatedAt(String updatedAt) {
                    this.updatedAt = updatedAt;
                }
            }


            public String getToken() {
                return token;
            }

            public void setToken(String token) {
                this.token = token;
            }
        }
    }
}
