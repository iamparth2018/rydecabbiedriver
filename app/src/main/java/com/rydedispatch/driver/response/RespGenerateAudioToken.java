package com.rydedispatch.driver.response;

import com.google.gson.annotations.SerializedName;

public class RespGenerateAudioToken {

    /**
     * error : false
     * data : {"identity":"Parth1024","token":"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiIsImN0eSI6InR3aWxpby1mcGE7dj0xIn0.eyJqdGkiOiJTS2UzMjA0YmEwOGViYjUwNjExYTFjZGQ0ZmJlNWQyOWJhLTE1NjI5MzIyNDEiLCJpc3MiOiJTS2UzMjA0YmEwOGViYjUwNjExYTFjZGQ0ZmJlNWQyOWJhIiwic3ViIjoiQUNlODczY2U0OWJkMzc3NzRhYTczMTZjMTZmNzM2OTFjZSIsImV4cCI6MTU2MjkzNTg0MSwiZ3JhbnRzIjp7ImlkZW50aXR5IjoiUGFydGgxMDI0Iiwidm9pY2UiOnsib3V0Z29pbmciOnsiYXBwbGljYXRpb25fc2lkIjoiQVA1YTc2NDg3YTBmODY0MDljYWVhMTYxM2ZhNWM5NzM5ZSJ9LCJwdXNoX2NyZWRlbnRpYWxfc2lkIjoiQ1I2ZDdiYzZiZGVhZWRiMWUwN2Q3NmNhMzdjZDRiMDc4YiJ9fX0.OeQtQLVdHewH3_GdQOnSglbfwGycokYgxvt2raSNFvM"}
     * message : Generate Token successfully.
     */

    @SerializedName("error")
    private boolean error;
    @SerializedName("data")
    private DataEntity data;
    @SerializedName("message")
    private String message;

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public DataEntity getData() {
        return data;
    }

    public void setData(DataEntity data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public static class DataEntity {
        /**
         * identity : Parth1024
         * token : eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiIsImN0eSI6InR3aWxpby1mcGE7dj0xIn0.eyJqdGkiOiJTS2UzMjA0YmEwOGViYjUwNjExYTFjZGQ0ZmJlNWQyOWJhLTE1NjI5MzIyNDEiLCJpc3MiOiJTS2UzMjA0YmEwOGViYjUwNjExYTFjZGQ0ZmJlNWQyOWJhIiwic3ViIjoiQUNlODczY2U0OWJkMzc3NzRhYTczMTZjMTZmNzM2OTFjZSIsImV4cCI6MTU2MjkzNTg0MSwiZ3JhbnRzIjp7ImlkZW50aXR5IjoiUGFydGgxMDI0Iiwidm9pY2UiOnsib3V0Z29pbmciOnsiYXBwbGljYXRpb25fc2lkIjoiQVA1YTc2NDg3YTBmODY0MDljYWVhMTYxM2ZhNWM5NzM5ZSJ9LCJwdXNoX2NyZWRlbnRpYWxfc2lkIjoiQ1I2ZDdiYzZiZGVhZWRiMWUwN2Q3NmNhMzdjZDRiMDc4YiJ9fX0.OeQtQLVdHewH3_GdQOnSglbfwGycokYgxvt2raSNFvM
         */

        @SerializedName("identity")
        private String identity;
        @SerializedName("token")
        private String token;

        public String getIdentity() {
            return identity;
        }

        public void setIdentity(String identity) {
            this.identity = identity;
        }

        public String getToken() {
            return token;
        }

        public void setToken(String token) {
            this.token = token;
        }
    }
}
