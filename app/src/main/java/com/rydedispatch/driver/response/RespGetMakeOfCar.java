package com.rydedispatch.driver.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RespGetMakeOfCar {


    /**
     * error : false
     * data : [{"model_make_id":"Acura"},{"model_make_id":"Alfa Romeo"},{"model_make_id":"Aston Martin"},{"model_make_id":"Audi"},{"model_make_id":"Bentley"},{"model_make_id":"BMW"},{"model_make_id":"Buick"},{"model_make_id":"Cadillac"},{"model_make_id":"Chevrolet"},{"model_make_id":"Chrysler"},{"model_make_id":"Dodge"},{"model_make_id":"FIAT"},{"model_make_id":"Ford"},{"model_make_id":"GMC"},{"model_make_id":"Honda"},{"model_make_id":"Hyundai"},{"model_make_id":"Infiniti"},{"model_make_id":"Jaguar"},{"model_make_id":"Jeep"},{"model_make_id":"Kia"},{"model_make_id":"Lamborghini"},{"model_make_id":"Land Rover"},{"model_make_id":"Lexus"},{"model_make_id":"Lincoln"},{"model_make_id":"Maserati"},{"model_make_id":"Mazda"},{"model_make_id":"McLaren"},{"model_make_id":"Mercedes-Benz"},{"model_make_id":"MINI"},{"model_make_id":"Mitsubishi"},{"model_make_id":"Nissan"},{"model_make_id":"Porsche"},{"model_make_id":"Ram"},{"model_make_id":"Rolls-Royce"},{"model_make_id":"Scion"},{"model_make_id":"smart"},{"model_make_id":"Subaru"},{"model_make_id":"Toyota"},{"model_make_id":"Volkswagen"},{"model_make_id":"Volvo"}]
     * message : Successfully retrieve Make.
     */

    @SerializedName("error")
    private boolean error;
    @SerializedName("message")
    private String message;
    @SerializedName("data")
    private List<DataEntity> data;

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataEntity> getData() {
        return data;
    }

    public void setData(List<DataEntity> data) {
        this.data = data;
    }

    public static class DataEntity {
        /**
         * model_make_id : Acura
         */

        @SerializedName("model_make_id")
        private String modelMakeId;

        public String getModelMakeId() {
            return modelMakeId;
        }

        public void setModelMakeId(String modelMakeId) {
            this.modelMakeId = modelMakeId;
        }
    }
}
