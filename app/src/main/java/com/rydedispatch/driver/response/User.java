package com.rydedispatch.driver.response;

public class User {
    public String lattitude;

    public String longitude;

    public String name;
    public String email;

    public User(String lattitude, String longitude) {
        this.lattitude = lattitude;
        this.longitude = longitude;
    }

}
