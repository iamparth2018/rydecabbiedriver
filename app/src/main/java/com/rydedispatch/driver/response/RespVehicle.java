package com.rydedispatch.driver.response;

import com.google.gson.annotations.SerializedName;

public class RespVehicle {


    /**
     * error : false
     * data : true
     * message : Vehicle has been updated successfully.
     */

    @SerializedName("error")
    private boolean error;
    @SerializedName("data")
    private boolean data;
    @SerializedName("message")
    private String message;

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public boolean isData() {
        return data;
    }

    public void setData(boolean data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
