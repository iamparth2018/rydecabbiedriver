package com.rydedispatch.driver.response;

import com.google.gson.annotations.SerializedName;

public class RespUploadDocument {


    /**
     * error : false
     * data : {"id":6,"user_id":4,"unique_id":"12345","document_name":"Police Clearance Certificate","front_document_url":null,"back_document_url":null,"document_type":"HMV","issue_date":"2019-07-19","expiry_date":"2020-07-19","status":"in approval","deleted_at":null,"created_at":"2019-07-19 12:17:43","updated_at":"2019-07-22 06:30:05"}
     * message : Document has been uploaded successfully
     */

    @SerializedName("error")
    private boolean error;
    @SerializedName("data")
    private DataEntity data;
    @SerializedName("message")
    private String message;

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public DataEntity getData() {
        return data;
    }

    public void setData(DataEntity data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public static class DataEntity {
        /**
         * id : 6
         * user_id : 4
         * unique_id : 12345
         * document_name : Police Clearance Certificate
         * front_document_url : null
         * back_document_url : null
         * document_type : HMV
         * issue_date : 2019-07-19
         * expiry_date : 2020-07-19
         * status : in approval
         * deleted_at : null
         * created_at : 2019-07-19 12:17:43
         * updated_at : 2019-07-22 06:30:05
         */

        @SerializedName("id")
        private int id;
        @SerializedName("user_id")
        private int userId;
        @SerializedName("unique_id")
        private String uniqueId;
        @SerializedName("document_name")
        private String documentName;
        @SerializedName("front_document_url")
        private Object frontDocumentUrl;
        @SerializedName("back_document_url")
        private Object backDocumentUrl;
        @SerializedName("document_type")
        private String documentType;
        @SerializedName("issue_date")
        private String issueDate;
        @SerializedName("expiry_date")
        private String expiryDate;
        @SerializedName("status")
        private String status;
        @SerializedName("deleted_at")
        private Object deletedAt;
        @SerializedName("created_at")
        private String createdAt;
        @SerializedName("updated_at")
        private String updatedAt;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getUserId() {
            return userId;
        }

        public void setUserId(int userId) {
            this.userId = userId;
        }

        public String getUniqueId() {
            return uniqueId;
        }

        public void setUniqueId(String uniqueId) {
            this.uniqueId = uniqueId;
        }

        public String getDocumentName() {
            return documentName;
        }

        public void setDocumentName(String documentName) {
            this.documentName = documentName;
        }

        public Object getFrontDocumentUrl() {
            return frontDocumentUrl;
        }

        public void setFrontDocumentUrl(Object frontDocumentUrl) {
            this.frontDocumentUrl = frontDocumentUrl;
        }

        public Object getBackDocumentUrl() {
            return backDocumentUrl;
        }

        public void setBackDocumentUrl(Object backDocumentUrl) {
            this.backDocumentUrl = backDocumentUrl;
        }

        public String getDocumentType() {
            return documentType;
        }

        public void setDocumentType(String documentType) {
            this.documentType = documentType;
        }

        public String getIssueDate() {
            return issueDate;
        }

        public void setIssueDate(String issueDate) {
            this.issueDate = issueDate;
        }

        public String getExpiryDate() {
            return expiryDate;
        }

        public void setExpiryDate(String expiryDate) {
            this.expiryDate = expiryDate;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public Object getDeletedAt() {
            return deletedAt;
        }

        public void setDeletedAt(Object deletedAt) {
            this.deletedAt = deletedAt;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }
    }
}
