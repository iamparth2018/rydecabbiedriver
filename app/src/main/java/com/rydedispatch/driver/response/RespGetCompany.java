package com.rydedispatch.driver.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RespGetCompany {

    /**
     * error : false
     * data : {"company":[{"id":174,"parent_id":0,"company_id":0,"first_name":"","last_name":"","company_name":"samcom","company_image":null,"contact_person":"ketan","profile_picture":null,"email":"info@samcomtechnologies.com","email_verified_at":null,"country_code":"+91","phone_number":"+917940054779","address":"India Colony, Ahmedabad, Gujarat, India","country":"India","state":"Gujarat","city":"Ahmedabad","city_timezone":"Asia/Calcutta","device_token":"","device_type":"web","latitude":"23.0422422","longitude":"72.63433520000001","status":"active","user_status":"offline","authy_id":"","stripe_customer_id":null,"user_unique_code":"1174","tax_id":"321","commission_type":"ammount","commission":"10","invoice_frequency":"weekly","role":"company","passenger_rate_card":"","document_status":"pending","wallet_balance":null,"deleted_at":null,"created_at":"2019-10-17 06:41:00","updated_at":"2019-10-17 06:43:17"},{"id":182,"parent_id":0,"company_id":0,"first_name":"","last_name":"","company_name":"TRANSCARE LLC","company_image":null,"contact_person":"Minella Alexander","profile_picture":null,"email":"info@mtsryde.com","email_verified_at":null,"country_code":"+1","phone_number":"+16127881895","address":"9203 S State Hwy 6, Houston, TX, USA","country":"United States","state":"Texas","city":"Houston","city_timezone":"America/Chicago","device_token":"","device_type":"web","latitude":"29.6783591","longitude":"-95.64100910000002","status":"active","user_status":"online","authy_id":"","stripe_customer_id":null,"user_unique_code":"1182","tax_id":"23-49495533","commission_type":"percentage","commission":"70","invoice_frequency":"weekly","role":"company","passenger_rate_card":"","document_status":"pending","wallet_balance":null,"deleted_at":null,"created_at":"2019-10-20 18:45:21","updated_at":"2019-10-20 20:20:11"}]}
     * message : Success to get company.
     */

    @SerializedName("error")
    private boolean error;
    @SerializedName("data")
    private DataEntity data;
    @SerializedName("message")
    private String message;

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public DataEntity getData() {
        return data;
    }

    public void setData(DataEntity data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public static class DataEntity {
        @SerializedName("company")
        private List<CompanyEntity> company;

        public List<CompanyEntity> getCompany() {
            return company;
        }

        public void setCompany(List<CompanyEntity> company) {
            this.company = company;
        }

        public static class CompanyEntity {
            /**
             * id : 174
             * parent_id : 0
             * company_id : 0
             * first_name :
             * last_name :
             * company_name : samcom
             * company_image : null
             * contact_person : ketan
             * profile_picture : null
             * email : info@samcomtechnologies.com
             * email_verified_at : null
             * country_code : +91
             * phone_number : +917940054779
             * address : India Colony, Ahmedabad, Gujarat, India
             * country : India
             * state : Gujarat
             * city : Ahmedabad
             * city_timezone : Asia/Calcutta
             * device_token :
             * device_type : web
             * latitude : 23.0422422
             * longitude : 72.63433520000001
             * status : active
             * user_status : offline
             * authy_id :
             * stripe_customer_id : null
             * user_unique_code : 1174
             * tax_id : 321
             * commission_type : ammount
             * commission : 10
             * invoice_frequency : weekly
             * role : company
             * passenger_rate_card :
             * document_status : pending
             * wallet_balance : null
             * deleted_at : null
             * created_at : 2019-10-17 06:41:00
             * updated_at : 2019-10-17 06:43:17
             */

            @SerializedName("id")
            private int id;
            @SerializedName("parent_id")
            private int parentId;
            @SerializedName("company_id")
            private int companyId;
            @SerializedName("first_name")
            private String firstName;
            @SerializedName("last_name")
            private String lastName;
            @SerializedName("company_name")
            private String companyName;
            @SerializedName("company_image")
            private Object companyImage;
            @SerializedName("contact_person")
            private String contactPerson;
            @SerializedName("profile_picture")
            private Object profilePicture;
            @SerializedName("email")
            private String email;
            @SerializedName("email_verified_at")
            private Object emailVerifiedAt;
            @SerializedName("country_code")
            private String countryCode;
            @SerializedName("phone_number")
            private String phoneNumber;
            @SerializedName("address")
            private String address;
            @SerializedName("country")
            private String country;
            @SerializedName("state")
            private String state;
            @SerializedName("city")
            private String city;
            @SerializedName("city_timezone")
            private String cityTimezone;
            @SerializedName("device_token")
            private String deviceToken;
            @SerializedName("device_type")
            private String deviceType;
            @SerializedName("latitude")
            private String latitude;
            @SerializedName("longitude")
            private String longitude;
            @SerializedName("status")
            private String status;
            @SerializedName("user_status")
            private String userStatus;
            @SerializedName("authy_id")
            private String authyId;
            @SerializedName("stripe_customer_id")
            private Object stripeCustomerId;
            @SerializedName("user_unique_code")
            private String userUniqueCode;
            @SerializedName("tax_id")
            private String taxId;
            @SerializedName("commission_type")
            private String commissionType;
            @SerializedName("commission")
            private String commission;
            @SerializedName("invoice_frequency")
            private String invoiceFrequency;
            @SerializedName("role")
            private String role;
            @SerializedName("passenger_rate_card")
            private String passengerRateCard;
            @SerializedName("document_status")
            private String documentStatus;
            @SerializedName("wallet_balance")
            private Object walletBalance;
            @SerializedName("deleted_at")
            private Object deletedAt;
            @SerializedName("created_at")
            private String createdAt;
            @SerializedName("updated_at")
            private String updatedAt;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public int getParentId() {
                return parentId;
            }

            public void setParentId(int parentId) {
                this.parentId = parentId;
            }

            public int getCompanyId() {
                return companyId;
            }

            public void setCompanyId(int companyId) {
                this.companyId = companyId;
            }

            public String getFirstName() {
                return firstName;
            }

            public void setFirstName(String firstName) {
                this.firstName = firstName;
            }

            public String getLastName() {
                return lastName;
            }

            public void setLastName(String lastName) {
                this.lastName = lastName;
            }

            public String getCompanyName() {
                return companyName;
            }

            public void setCompanyName(String companyName) {
                this.companyName = companyName;
            }

            public Object getCompanyImage() {
                return companyImage;
            }

            public void setCompanyImage(Object companyImage) {
                this.companyImage = companyImage;
            }

            public String getContactPerson() {
                return contactPerson;
            }

            public void setContactPerson(String contactPerson) {
                this.contactPerson = contactPerson;
            }

            public Object getProfilePicture() {
                return profilePicture;
            }

            public void setProfilePicture(Object profilePicture) {
                this.profilePicture = profilePicture;
            }

            public String getEmail() {
                return email;
            }

            public void setEmail(String email) {
                this.email = email;
            }

            public Object getEmailVerifiedAt() {
                return emailVerifiedAt;
            }

            public void setEmailVerifiedAt(Object emailVerifiedAt) {
                this.emailVerifiedAt = emailVerifiedAt;
            }

            public String getCountryCode() {
                return countryCode;
            }

            public void setCountryCode(String countryCode) {
                this.countryCode = countryCode;
            }

            public String getPhoneNumber() {
                return phoneNumber;
            }

            public void setPhoneNumber(String phoneNumber) {
                this.phoneNumber = phoneNumber;
            }

            public String getAddress() {
                return address;
            }

            public void setAddress(String address) {
                this.address = address;
            }

            public String getCountry() {
                return country;
            }

            public void setCountry(String country) {
                this.country = country;
            }

            public String getState() {
                return state;
            }

            public void setState(String state) {
                this.state = state;
            }

            public String getCity() {
                return city;
            }

            public void setCity(String city) {
                this.city = city;
            }

            public String getCityTimezone() {
                return cityTimezone;
            }

            public void setCityTimezone(String cityTimezone) {
                this.cityTimezone = cityTimezone;
            }

            public String getDeviceToken() {
                return deviceToken;
            }

            public void setDeviceToken(String deviceToken) {
                this.deviceToken = deviceToken;
            }

            public String getDeviceType() {
                return deviceType;
            }

            public void setDeviceType(String deviceType) {
                this.deviceType = deviceType;
            }

            public String getLatitude() {
                return latitude;
            }

            public void setLatitude(String latitude) {
                this.latitude = latitude;
            }

            public String getLongitude() {
                return longitude;
            }

            public void setLongitude(String longitude) {
                this.longitude = longitude;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }

            public String getUserStatus() {
                return userStatus;
            }

            public void setUserStatus(String userStatus) {
                this.userStatus = userStatus;
            }

            public String getAuthyId() {
                return authyId;
            }

            public void setAuthyId(String authyId) {
                this.authyId = authyId;
            }

            public Object getStripeCustomerId() {
                return stripeCustomerId;
            }

            public void setStripeCustomerId(Object stripeCustomerId) {
                this.stripeCustomerId = stripeCustomerId;
            }

            public String getUserUniqueCode() {
                return userUniqueCode;
            }

            public void setUserUniqueCode(String userUniqueCode) {
                this.userUniqueCode = userUniqueCode;
            }

            public String getTaxId() {
                return taxId;
            }

            public void setTaxId(String taxId) {
                this.taxId = taxId;
            }

            public String getCommissionType() {
                return commissionType;
            }

            public void setCommissionType(String commissionType) {
                this.commissionType = commissionType;
            }

            public String getCommission() {
                return commission;
            }

            public void setCommission(String commission) {
                this.commission = commission;
            }

            public String getInvoiceFrequency() {
                return invoiceFrequency;
            }

            public void setInvoiceFrequency(String invoiceFrequency) {
                this.invoiceFrequency = invoiceFrequency;
            }

            public String getRole() {
                return role;
            }

            public void setRole(String role) {
                this.role = role;
            }

            public String getPassengerRateCard() {
                return passengerRateCard;
            }

            public void setPassengerRateCard(String passengerRateCard) {
                this.passengerRateCard = passengerRateCard;
            }

            public String getDocumentStatus() {
                return documentStatus;
            }

            public void setDocumentStatus(String documentStatus) {
                this.documentStatus = documentStatus;
            }

            public Object getWalletBalance() {
                return walletBalance;
            }

            public void setWalletBalance(Object walletBalance) {
                this.walletBalance = walletBalance;
            }

            public Object getDeletedAt() {
                return deletedAt;
            }

            public void setDeletedAt(Object deletedAt) {
                this.deletedAt = deletedAt;
            }

            public String getCreatedAt() {
                return createdAt;
            }

            public void setCreatedAt(String createdAt) {
                this.createdAt = createdAt;
            }

            public String getUpdatedAt() {
                return updatedAt;
            }

            public void setUpdatedAt(String updatedAt) {
                this.updatedAt = updatedAt;
            }
        }
    }
}
