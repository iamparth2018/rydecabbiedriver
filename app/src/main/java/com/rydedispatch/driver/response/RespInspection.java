package com.rydedispatch.driver.response;

import com.google.gson.annotations.SerializedName;

public class RespInspection {

    /**
     * error : false
     * data : {"driver_id":2,"company_id":0,"current_mileage":"20","other_information":null,"question_answer":"[{\"question\":\"Engine Oil and Coolant Levels\",\"options\":\"\",\"answer\":\"1\"},{\"question\":\"Windshield & Mirrors\",\"options\":[\"Windscreen washers work correctly\",\"Mirrors work correctly and not damaged\"],\"answer\":0},{\"question\":\"Doors and Windows\",\"options\":\"\",\"answer\":\"1\"},{\"question\":\"Emergency Brake\",\"options\":\"\",\"answer\":\"1\"},{\"question\":\"Tires \\u2013 wear and pressure (spare)\",\"options\":\"\",\"answer\":0},{\"question\":\"Inspection and License Plate Stickers\",\"options\":\"\",\"answer\":0},{\"question\":\"Check Ground under Vehicle for Fluid Leaks\",\"options\":\"\",\"answer\":\"1\"},{\"question\":\"Emergency Equipment\",\"options\":[\"First aid kit\",\"Fire extinguisher\",\"Warning reflectors and flares\",\"Flashlight\",\"Jack, lug-wrench and spare tire\",\"Communication device\"],\"answer\":\"1\"},{\"question\":\"Documentation\",\"options\":[\"Insurance card\",\"Registration\"],\"answer\":0},{\"question\":\"Check for Cleanliness & Damages [interior and exterior]\",\"options\":\"\",\"answer\":0},{\"question\":\"Fuel Level, Gauges, and Dash Warning Lights\",\"options\":\"\",\"answer\":\"1\"},{\"question\":\"Windshield Wipers\",\"options\":\"\",\"answer\":0},{\"question\":\"Horn\",\"options\":\"\",\"answer\":\"1\"},{\"question\":\"Head Lights, Tail Lights, Turn Signals, Flashers, Warning Lights\",\"options\":\"\",\"answer\":\"1\"},{\"question\":\"Defrosters, Heaters and Air Conditioner [when applicable]\",\"options\":\"\",\"answer\":0},{\"question\":\"Seat Belts\",\"options\":\"\",\"answer\":\"1\"}]","updated_at":"2019-10-23 10:17:14","created_at":"2019-10-23 10:17:14","id":3,"fleet_code":"RydeInsp1003"}
     * message : Inspection has been submitted successfully.
     */

    @SerializedName("error")
    private boolean error;
    @SerializedName("data")
    private DataEntity data;
    @SerializedName("message")
    private String message;

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public DataEntity getData() {
        return data;
    }

    public void setData(DataEntity data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public static class DataEntity {
        /**
         * driver_id : 2
         * company_id : 0
         * current_mileage : 20
         * other_information : null
         * question_answer : [{"question":"Engine Oil and Coolant Levels","options":"","answer":"1"},{"question":"Windshield & Mirrors","options":["Windscreen washers work correctly","Mirrors work correctly and not damaged"],"answer":0},{"question":"Doors and Windows","options":"","answer":"1"},{"question":"Emergency Brake","options":"","answer":"1"},{"question":"Tires \u2013 wear and pressure (spare)","options":"","answer":0},{"question":"Inspection and License Plate Stickers","options":"","answer":0},{"question":"Check Ground under Vehicle for Fluid Leaks","options":"","answer":"1"},{"question":"Emergency Equipment","options":["First aid kit","Fire extinguisher","Warning reflectors and flares","Flashlight","Jack, lug-wrench and spare tire","Communication device"],"answer":"1"},{"question":"Documentation","options":["Insurance card","Registration"],"answer":0},{"question":"Check for Cleanliness & Damages [interior and exterior]","options":"","answer":0},{"question":"Fuel Level, Gauges, and Dash Warning Lights","options":"","answer":"1"},{"question":"Windshield Wipers","options":"","answer":0},{"question":"Horn","options":"","answer":"1"},{"question":"Head Lights, Tail Lights, Turn Signals, Flashers, Warning Lights","options":"","answer":"1"},{"question":"Defrosters, Heaters and Air Conditioner [when applicable]","options":"","answer":0},{"question":"Seat Belts","options":"","answer":"1"}]
         * updated_at : 2019-10-23 10:17:14
         * created_at : 2019-10-23 10:17:14
         * id : 3
         * fleet_code : RydeInsp1003
         */

        @SerializedName("driver_id")
        private String driverId;
        @SerializedName("company_id")
        private String companyId;
        @SerializedName("current_mileage")
        private String currentMileage;
        @SerializedName("other_information")
        private String otherInformation;
        @SerializedName("question_answer")
        private String questionAnswer;
        @SerializedName("updated_at")
        private String updatedAt;
        @SerializedName("created_at")
        private String createdAt;
        @SerializedName("id")
        private String id;
        @SerializedName("fleet_code")
        private String fleetCode;

        public String getDriverId() {
            return driverId;
        }

        public void setDriverId(String driverId) {
            this.driverId = driverId;
        }

        public String getCompanyId() {
            return companyId;
        }

        public void setCompanyId(String companyId) {
            this.companyId = companyId;
        }

        public String getCurrentMileage() {
            return currentMileage;
        }

        public void setCurrentMileage(String currentMileage) {
            this.currentMileage = currentMileage;
        }

        public String getOtherInformation() {
            return otherInformation;
        }

        public void setOtherInformation(String otherInformation) {
            this.otherInformation = otherInformation;
        }

        public String getQuestionAnswer() {
            return questionAnswer;
        }

        public void setQuestionAnswer(String questionAnswer) {
            this.questionAnswer = questionAnswer;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getFleetCode() {
            return fleetCode;
        }

        public void setFleetCode(String fleetCode) {
            this.fleetCode = fleetCode;
        }
    }
}
