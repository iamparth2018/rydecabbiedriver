package com.rydedispatch.driver.response;

import com.google.gson.annotations.SerializedName;

public class RespCompleteRide {

    /**
     * error : false
     * data : {"extra_distance":0,"extra_distance_cost":0,"base_fare":"3","estimated_cost_without_base_fare":"10","ratecard_waiting_fare_per_minutes":"2","extra_ride_time_cost":0,"actual_cost":"13","total_cost":"13","actual_discount":0,"total_time":"1","distance_type":"kilometers","payment_type":"cash","display_passenger_name":"Urvish Kadia"}
     * message : Your ride has been completed.
     */

    @SerializedName("error")
    private boolean error;
    @SerializedName("data")
    private DataEntity data;
    @SerializedName("message")
    private String message;

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public DataEntity getData() {
        return data;
    }

    public void setData(DataEntity data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public static class DataEntity {
        /**
         * extra_distance : 0
         * extra_distance_cost : 0
         * base_fare : 3
         * estimated_cost_without_base_fare : 10
         * ratecard_waiting_fare_per_minutes : 2
         * extra_ride_time_cost : 0
         * actual_cost : 13
         * total_cost : 13
         * actual_discount : 0
         * total_time : 1
         * distance_type : kilometers
         * payment_type : cash
         * display_passenger_name : Urvish Kadia
         */

        @SerializedName("extra_distance")
        private String extraDistance;
        @SerializedName("extra_distance_cost")
        private String extraDistanceCost;
        @SerializedName("base_fare")
        private String baseFare;
        @SerializedName("estimated_cost_without_base_fare")
        private String estimatedCostWithoutBaseFare;
        @SerializedName("ratecard_waiting_fare_per_minutes")
        private String ratecardWaitingFarePerMinutes;
        @SerializedName("extra_ride_time_cost")
        private String extraRideTimeCost;
        @SerializedName("actual_cost")
        private String actualCost;
        @SerializedName("total_cost")
        private String totalCost;
        @SerializedName("actual_discount")
        private String actualDiscount;
        @SerializedName("total_time")
        private String totalTime;
        @SerializedName("distance_type")
        private String distanceType;
        @SerializedName("payment_type")
        private String paymentType;
        @SerializedName("display_passenger_name")
        private String displayPassengerName;
        @SerializedName("ride_instruction")
        private String ride_instruction;

        public String getRide_instruction() {
            return ride_instruction;
        }

        public void setRide_instruction(String ride_instruction) {
            this.ride_instruction = ride_instruction;
        }

        public String getExtraDistance() {
            return extraDistance;
        }

        public void setExtraDistance(String extraDistance) {
            this.extraDistance = extraDistance;
        }

        public String getExtraDistanceCost() {
            return extraDistanceCost;
        }

        public void setExtraDistanceCost(String extraDistanceCost) {
            this.extraDistanceCost = extraDistanceCost;
        }

        public String getBaseFare() {
            return baseFare;
        }

        public void setBaseFare(String baseFare) {
            this.baseFare = baseFare;
        }

        public String getEstimatedCostWithoutBaseFare() {
            return estimatedCostWithoutBaseFare;
        }

        public void setEstimatedCostWithoutBaseFare(String estimatedCostWithoutBaseFare) {
            this.estimatedCostWithoutBaseFare = estimatedCostWithoutBaseFare;
        }

        public String getRatecardWaitingFarePerMinutes() {
            return ratecardWaitingFarePerMinutes;
        }

        public void setRatecardWaitingFarePerMinutes(String ratecardWaitingFarePerMinutes) {
            this.ratecardWaitingFarePerMinutes = ratecardWaitingFarePerMinutes;
        }

        public String getExtraRideTimeCost() {
            return extraRideTimeCost;
        }

        public void setExtraRideTimeCost(String extraRideTimeCost) {
            this.extraRideTimeCost = extraRideTimeCost;
        }

        public String getActualCost() {
            return actualCost;
        }

        public void setActualCost(String actualCost) {
            this.actualCost = actualCost;
        }

        public String getTotalCost() {
            return totalCost;
        }

        public void setTotalCost(String totalCost) {
            this.totalCost = totalCost;
        }

        public String getActualDiscount() {
            return actualDiscount;
        }

        public void setActualDiscount(String actualDiscount) {
            this.actualDiscount = actualDiscount;
        }

        public String getTotalTime() {
            return totalTime;
        }

        public void setTotalTime(String totalTime) {
            this.totalTime = totalTime;
        }

        public String getDistanceType() {
            return distanceType;
        }

        public void setDistanceType(String distanceType) {
            this.distanceType = distanceType;
        }

        public String getPaymentType() {
            return paymentType;
        }

        public void setPaymentType(String paymentType) {
            this.paymentType = paymentType;
        }

        public String getDisplayPassengerName() {
            return displayPassengerName;
        }

        public void setDisplayPassengerName(String displayPassengerName) {
            this.displayPassengerName = displayPassengerName;
        }
    }
}
