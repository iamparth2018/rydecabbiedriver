package com.rydedispatch.driver.response;

import com.google.gson.annotations.SerializedName;

public class RespHistoryDetails {


    /**
     * error : false
     * data : {"id":16,"source_location":"4D Rd, Chandkheda, Ahmedabad, Gujarat 380005, India","source_latitude":"23.102505711305593","source_longitude":"72.59477388113737","destination_location":"10113/1012, Janta Nagar Ring Rd, Parshwanath Nagar, ONGC Colony, Chandkheda, Ahmedabad, Gujarat 382424, India","destination_latitude":"23.108710034934386","destination_longitude":"72.58095614612103","payment_type":"cash","base_fare":"3","distance_type":"kilometers","total_time":"0","total_distance":"0","actual_cost_without_commission":"0","commission":"0","actual_cost":"0","actual_discount":"0","wait_time_cost":"0","extra_distance_cost":"0","extra_ride_time_cost":"0","actual_estimated_cost":"13","estimated_cost_without_base_fare":"10","total_cost":"0","booking_time":"2019-08-21 17:09:04","start_riding_time":null,"completed_time":"2019-08-21 17:09:28","status":"cancelled","make":"BMW","model":"Z4","car_number":"1234","passenger_first_name":"Sandeep","passenger_last_name":"User","email":"sandeep.manala@gmail.com","profile_picture":"/assets/images/5d5a2db3d0739.jpg","driver_id":11,"first_name":"Devendra","last_name":"Sharma","driver_picture":null,"driver_rating":3.1944444444444446,"user_rating":3.1944444444444446}
     * message : History has beed get successfully.
     */

    @SerializedName("error")
    private boolean error;
    @SerializedName("data")
    private DataEntity data;
    @SerializedName("message")
    private String message;

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public DataEntity getData() {
        return data;
    }

    public void setData(DataEntity data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public static class DataEntity {
        /**
         * id : 16
         * source_location : 4D Rd, Chandkheda, Ahmedabad, Gujarat 380005, India
         * source_latitude : 23.102505711305593
         * source_longitude : 72.59477388113737
         * destination_location : 10113/1012, Janta Nagar Ring Rd, Parshwanath Nagar, ONGC Colony, Chandkheda, Ahmedabad, Gujarat 382424, India
         * destination_latitude : 23.108710034934386
         * destination_longitude : 72.58095614612103
         * payment_type : cash
         * base_fare : 3
         * distance_type : kilometers
         * total_time : 0
         * total_distance : 0
         * actual_cost_without_commission : 0
         * commission : 0
         * actual_cost : 0
         * actual_discount : 0
         * wait_time_cost : 0
         * extra_distance_cost : 0
         * extra_ride_time_cost : 0
         * actual_estimated_cost : 13
         * estimated_cost_without_base_fare : 10
         * total_cost : 0
         * booking_time : 2019-08-21 17:09:04
         * start_riding_time : null
         * completed_time : 2019-08-21 17:09:28
         * status : cancelled
         * make : BMW
         * model : Z4
         * car_number : 1234
         * passenger_first_name : Sandeep
         * passenger_last_name : User
         * email : sandeep.manala@gmail.com
         * profile_picture : /assets/images/5d5a2db3d0739.jpg
         * driver_id : 11
         * first_name : Devendra
         * last_name : Sharma
         * driver_picture : null
         * driver_rating : 3.1944444444444446
         * user_rating : 3.1944444444444446
         */

        @SerializedName("id")
        private int id;
        @SerializedName("sub_user_id")
        private String sub_user_id;
        @SerializedName("display_passenger_name")
        private String display_passenger_name;
        @SerializedName("source_location")
        private String sourceLocation;
        @SerializedName("source_latitude")
        private String sourceLatitude;
        @SerializedName("source_longitude")
        private String sourceLongitude;
        @SerializedName("destination_location")
        private String destinationLocation;
        @SerializedName("destination_latitude")
        private String destinationLatitude;
        @SerializedName("destination_longitude")
        private String destinationLongitude;
        @SerializedName("payment_type")
        private String paymentType;
        @SerializedName("base_fare")
        private String baseFare;
        @SerializedName("distance_type")
        private String distanceType;
        @SerializedName("total_time")
        private String totalTime;
        @SerializedName("estimated_time_to_arrival")
        private String estimated_time_to_arrival;
        @SerializedName("total_distance")
        private String totalDistance;
        @SerializedName("actual_cost_without_commission")
        private String actualCostWithoutCommission;
        @SerializedName("commission")
        private String commission;
        @SerializedName("bonus")
        private String bonus;


        @SerializedName("actual_cost")
        private String actualCost;
        @SerializedName("actual_discount")
        private String actualDiscount;
        @SerializedName("wait_time_cost")
        private String waitTimeCost;
        @SerializedName("extra_distance_cost")
        private String extraDistanceCost;
        @SerializedName("extra_ride_time_cost")
        private String extraRideTimeCost;
        @SerializedName("actual_estimated_cost")
        private String actualEstimatedCost;
        @SerializedName("estimated_cost_without_base_fare")
        private String estimatedCostWithoutBaseFare;
        @SerializedName("total_cost")
        private String totalCost;
        @SerializedName("booking_time")
        private String bookingTime;
        @SerializedName("start_riding_time")
        private String startRidingTime;
        @SerializedName("completed_time")
        private String completedTime;
        @SerializedName("status")
        private String status;
        @SerializedName("start_at")
        private String start_at;
        @SerializedName("make")
        private String make;
        @SerializedName("model")
        private String model;
        @SerializedName("car_number")
        private String carNumber;
        @SerializedName("passenger_first_name")
        private String passengerFirstName;
        @SerializedName("passenger_last_name")
        private String passengerLastName;
        @SerializedName("email")
        private String email;
        @SerializedName("profile_picture")
        private String profilePicture;
        @SerializedName("passenger_unique_code")
        private String passenger_unique_code;


        @SerializedName("driver_id")
        private int driverId;
        @SerializedName("first_name")
        private String firstName;
        @SerializedName("last_name")
        private String lastName;
        @SerializedName("driver_picture")
        private String driverPicture;
        @SerializedName("driver_rating")
        private String driverRating;
        @SerializedName("user_rating")
        private String userRating;

        public String getEstimated_time_to_arrival() {
            return estimated_time_to_arrival;
        }

        public void setEstimated_time_to_arrival(String estimated_time_to_arrival) {
            this.estimated_time_to_arrival = estimated_time_to_arrival;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }


        public String getSub_user_id() {
            return sub_user_id;
        }

        public void setSub_user_id(String sub_user_id) {
            this.sub_user_id = sub_user_id;
        }

        public String getDisplay_passenger_name() {
            return display_passenger_name;
        }

        public void setDisplay_passenger_name(String display_passenger_name) {
            this.display_passenger_name = display_passenger_name;
        }


        public String getSourceLocation() {
            return sourceLocation;
        }

        public void setSourceLocation(String sourceLocation) {
            this.sourceLocation = sourceLocation;
        }

        public String getSourceLatitude() {
            return sourceLatitude;
        }

        public void setSourceLatitude(String sourceLatitude) {
            this.sourceLatitude = sourceLatitude;
        }

        public String getSourceLongitude() {
            return sourceLongitude;
        }

        public void setSourceLongitude(String sourceLongitude) {
            this.sourceLongitude = sourceLongitude;
        }

        public String getDestinationLocation() {
            return destinationLocation;
        }

        public void setDestinationLocation(String destinationLocation) {
            this.destinationLocation = destinationLocation;
        }

        public String getDestinationLatitude() {
            return destinationLatitude;
        }

        public void setDestinationLatitude(String destinationLatitude) {
            this.destinationLatitude = destinationLatitude;
        }

        public String getDestinationLongitude() {
            return destinationLongitude;
        }

        public void setDestinationLongitude(String destinationLongitude) {
            this.destinationLongitude = destinationLongitude;
        }

        public String getPaymentType() {
            return paymentType;
        }

        public void setPaymentType(String paymentType) {
            this.paymentType = paymentType;
        }

        public String getBaseFare() {
            return baseFare;
        }

        public void setBaseFare(String baseFare) {
            this.baseFare = baseFare;
        }

        public String getDistanceType() {
            return distanceType;
        }

        public void setDistanceType(String distanceType) {
            this.distanceType = distanceType;
        }

        public String getTotalTime() {
            return totalTime;
        }

        public void setTotalTime(String totalTime) {
            this.totalTime = totalTime;
        }

        public String getTotalDistance() {
            return totalDistance;
        }

        public void setTotalDistance(String totalDistance) {
            this.totalDistance = totalDistance;
        }

        public String getActualCostWithoutCommission() {
            return actualCostWithoutCommission;
        }

        public void setActualCostWithoutCommission(String actualCostWithoutCommission) {
            this.actualCostWithoutCommission = actualCostWithoutCommission;
        }

        public String getCommission() {
            return commission;
        }

        public void setCommission(String commission) {
            this.commission = commission;
        }


        public String getBonus() {
            return bonus;
        }

        public void setBonus(String bonus) {
            this.bonus = bonus;
        }

        public String getActualCost() {
            return actualCost;
        }

        public void setActualCost(String actualCost) {
            this.actualCost = actualCost;
        }

        public String getActualDiscount() {
            return actualDiscount;
        }

        public void setActualDiscount(String actualDiscount) {
            this.actualDiscount = actualDiscount;
        }

        public String getWaitTimeCost() {
            return waitTimeCost;
        }

        public void setWaitTimeCost(String waitTimeCost) {
            this.waitTimeCost = waitTimeCost;
        }

        public String getExtraDistanceCost() {
            return extraDistanceCost;
        }

        public void setExtraDistanceCost(String extraDistanceCost) {
            this.extraDistanceCost = extraDistanceCost;
        }

        public String getExtraRideTimeCost() {
            return extraRideTimeCost;
        }

        public void setExtraRideTimeCost(String extraRideTimeCost) {
            this.extraRideTimeCost = extraRideTimeCost;
        }

        public String getActualEstimatedCost() {
            return actualEstimatedCost;
        }

        public void setActualEstimatedCost(String actualEstimatedCost) {
            this.actualEstimatedCost = actualEstimatedCost;
        }

        public String getEstimatedCostWithoutBaseFare() {
            return estimatedCostWithoutBaseFare;
        }

        public void setEstimatedCostWithoutBaseFare(String estimatedCostWithoutBaseFare) {
            this.estimatedCostWithoutBaseFare = estimatedCostWithoutBaseFare;
        }

        public String getTotalCost() {
            return totalCost;
        }

        public void setTotalCost(String totalCost) {
            this.totalCost = totalCost;
        }

        public String getBookingTime() {
            return bookingTime;
        }

        public void setBookingTime(String bookingTime) {
            this.bookingTime = bookingTime;
        }

        public String getStartRidingTime() {
            return startRidingTime;
        }

        public void setStartRidingTime(String startRidingTime) {
            this.startRidingTime = startRidingTime;
        }

        public String getCompletedTime() {
            return completedTime;
        }

        public void setCompletedTime(String completedTime) {
            this.completedTime = completedTime;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getStart_at() {
            return start_at;
        }

        public void setStart_at(String start_at) {
            this.start_at = start_at;
        }


        public String getMake() {
            return make;
        }

        public void setMake(String make) {
            this.make = make;
        }

        public String getModel() {
            return model;
        }

        public void setModel(String model) {
            this.model = model;
        }

        public String getCarNumber() {
            return carNumber;
        }

        public void setCarNumber(String carNumber) {
            this.carNumber = carNumber;
        }

        public String getPassengerFirstName() {
            return passengerFirstName;
        }

        public void setPassengerFirstName(String passengerFirstName) {
            this.passengerFirstName = passengerFirstName;
        }

        public String getPassengerLastName() {
            return passengerLastName;
        }

        public void setPassengerLastName(String passengerLastName) {
            this.passengerLastName = passengerLastName;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getProfilePicture() {
            return profilePicture;
        }

        public void setProfilePicture(String profilePicture) {
            this.profilePicture = profilePicture;
        }

        public String getPassenger_unique_code() {
            return passenger_unique_code;
        }

        public void setPassenger_unique_code(String passenger_unique_code) {
            this.passenger_unique_code = passenger_unique_code;
        }


        public int getDriverId() {
            return driverId;
        }

        public void setDriverId(int driverId) {
            this.driverId = driverId;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public String getDriverPicture() {
            return driverPicture;
        }

        public void setDriverPicture(String driverPicture) {
            this.driverPicture = driverPicture;
        }

        public String getDriverRating() {
            return driverRating;
        }

        public void setDriverRating(String driverRating) {
            this.driverRating = driverRating;
        }

        public String getUserRating() {
            return userRating;
        }

        public void setUserRating(String userRating) {
            this.userRating = userRating;
        }
    }
}
