package com.rydedispatch.driver.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RespGetDocumentList {


    /**
     * error : false
     * data : [{"id":3,"user_id":5,"unique_id":"ddddd","document_name":"Inspection Certificate","front_document_url":"/assets/images/1565958198_5d56a0360c133.jpg","back_document_url":null,"document_type":"","issue_date":"0000-00-00","expiry_date":"1986-01-16","status":"approved","deleted_at":null,"created_at":"2019-08-16 12:21:14","updated_at":"2019-08-19 05:06:01"},{"id":4,"user_id":5,"unique_id":"ffdd","document_name":"Driving License","front_document_url":"/assets/images/1565958263_5d56a07747eac.jpg","back_document_url":"/assets/images/1565958263_5d56a07747fd0.jpg","document_type":"tree","issue_date":"2000-01-10","expiry_date":"2020-04-10","status":"approved","deleted_at":null,"created_at":"2019-08-16 12:21:14","updated_at":"2019-08-19 05:06:01"}]
     * message : Docuements get successfully
     */

    @SerializedName("error")
    private boolean error;
    @SerializedName("message")
    private String message;
    @SerializedName("data")
    private List<DataEntity> data;

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataEntity> getData() {
        return data;
    }

    public void setData(List<DataEntity> data) {
        this.data = data;
    }

    public static class DataEntity {
        /**
         * id : 3
         * user_id : 5
         * unique_id : ddddd
         * document_name : Inspection Certificate
         * front_document_url : /assets/images/1565958198_5d56a0360c133.jpg
         * back_document_url : null
         * document_type :
         * issue_date : 0000-00-00
         * expiry_date : 1986-01-16
         * status : approved
         * deleted_at : null
         * created_at : 2019-08-16 12:21:14
         * updated_at : 2019-08-19 05:06:01
         */

        @SerializedName("id")
        private int id;
        @SerializedName("user_id")
        private int userId;
        @SerializedName("unique_id")
        private String uniqueId;
        @SerializedName("document_name")
        private String documentName;
        @SerializedName("front_document_url")
        private String frontDocumentUrl;
        @SerializedName("back_document_url")
        private Object backDocumentUrl;
        @SerializedName("document_type")
        private String documentType;
        @SerializedName("issue_date")
        private String issueDate;
        @SerializedName("expiry_date")
        private String expiryDate;
        @SerializedName("status")
        private String status;
        @SerializedName("deleted_at")
        private Object deletedAt;
        @SerializedName("created_at")
        private String createdAt;
        @SerializedName("updated_at")
        private String updatedAt;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getUserId() {
            return userId;
        }

        public void setUserId(int userId) {
            this.userId = userId;
        }

        public String getUniqueId() {
            return uniqueId;
        }

        public void setUniqueId(String uniqueId) {
            this.uniqueId = uniqueId;
        }

        public String getDocumentName() {
            return documentName;
        }

        public void setDocumentName(String documentName) {
            this.documentName = documentName;
        }

        public String getFrontDocumentUrl() {
            return frontDocumentUrl;
        }

        public void setFrontDocumentUrl(String frontDocumentUrl) {
            this.frontDocumentUrl = frontDocumentUrl;
        }

        public Object getBackDocumentUrl() {
            return backDocumentUrl;
        }

        public void setBackDocumentUrl(Object backDocumentUrl) {
            this.backDocumentUrl = backDocumentUrl;
        }

        public String getDocumentType() {
            return documentType;
        }

        public void setDocumentType(String documentType) {
            this.documentType = documentType;
        }

        public String getIssueDate() {
            return issueDate;
        }

        public void setIssueDate(String issueDate) {
            this.issueDate = issueDate;
        }

        public String getExpiryDate() {
            return expiryDate;
        }

        public void setExpiryDate(String expiryDate) {
            this.expiryDate = expiryDate;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public Object getDeletedAt() {
            return deletedAt;
        }

        public void setDeletedAt(Object deletedAt) {
            this.deletedAt = deletedAt;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }
    }
}
