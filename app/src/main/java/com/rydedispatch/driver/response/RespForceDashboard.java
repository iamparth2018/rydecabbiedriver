package com.rydedispatch.driver.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RespForceDashboard {

    /**
     * error : false
     * data : {"userDetails":{"id":2,"parent_id":0,"company_id":0,"first_name":"Parthh","last_name":"Samcom","company_name":"","company_image":null,"contact_person":"","profile_picture":"https://rydedispatch.nyc3.digitaloceanspaces.com/cVsujSoDdh0rUhb91TpKSUyI8L5sc1LniWgFKiqX.png","email":"parth.samcom2018@gmail.com","email_verified_at":null,"country_code":"+91","phone_number":"+919712146184","address":"Ranip, Gujarat, India","country":"India","state":"Gujarat","city":"Ahmedabad","city_timezone":"Asia/Calcutta","device_token":"dAp-Qugv8Qk:APA91bHk01di1vj0fePSjGRwofJLIegk7dN3y1wv7-MpN2HLGtb_SZTWAlakeBOB_f1Sfp6j6QSzpH7dfpPTi8-OtNq86y0ge5xmTQckDZtkbINAgCUNE9w2TBBZs_veyaLXIEZRPiKW","device_type":"android","latitude":"23.103347778320312","longitude":"72.59434225119453","status":"active","user_status":"online","authy_id":"127634683","stripe_customer_id":null,"user_unique_code":"Parth1002","tax_id":null,"commission_type":"ammount","commission":null,"invoice_frequency":"weekly","role":"driver","passenger_rate_card":"","document_status":"completed","wallet_balance":null,"deleted_at":null,"created_at":"2019-09-19 06:11:38","updated_at":"2019-10-15 11:23:58","users_vehicle":{"id":1,"user_id":2,"vin_code":"","car_number":"new","make":"Acura","model":"ILX","year":"2019","transmission":"Automatic","color":"red","driver_train":"4x4","car_type":"Premium","car_type_id":1,"deleted_at":null,"created_at":"2019-09-19 06:16:20","updated_at":"2019-09-19 06:16:52"},"vehicle":{"id":1,"user_id":2,"vin_code":"","car_number":"new","make":"Acura","model":"ILX","year":"2019","transmission":"Automatic","color":"red","driver_train":"4x4","car_type":"Premium","car_type_id":1,"deleted_at":null,"created_at":"2019-09-19 06:16:20","updated_at":"2019-09-19 06:16:52"},"vehicle_status":true,"support_number":"+16514139117"},"documents":[{"id":1,"user_id":2,"unique_id":null,"document_name":"Driving License","front_document_url":null,"back_document_url":null,"document_type":null,"issue_date":null,"expiry_date":null,"status":"approved","deleted_at":null,"created_at":"2019-09-19 06:15:57","updated_at":"2019-09-19 06:16:51"},{"id":2,"user_id":2,"unique_id":null,"document_name":"Inspection Certificate","front_document_url":null,"back_document_url":null,"document_type":null,"issue_date":null,"expiry_date":null,"status":"approved","deleted_at":null,"created_at":"2019-09-19 06:15:57","updated_at":"2019-09-19 06:16:51"},{"id":3,"user_id":2,"unique_id":null,"document_name":"Registration Certificate","front_document_url":null,"back_document_url":null,"document_type":null,"issue_date":null,"expiry_date":null,"status":"approved","deleted_at":null,"created_at":"2019-09-19 06:15:57","updated_at":"2019-09-19 06:16:51"}],"runningRide":""}
     * message : Dashboard has been load successfully
     */

    @SerializedName("error")
    private boolean error;
    @SerializedName("data")
    private DataEntity data;
    @SerializedName("message")
    private String message;

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public DataEntity getData() {
        return data;
    }

    public void setData(DataEntity data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public static class DataEntity {
        /**
         * userDetails : {"id":2,"parent_id":0,"company_id":0,"first_name":"Parthh","last_name":"Samcom","company_name":"","company_image":null,"contact_person":"","profile_picture":"https://rydedispatch.nyc3.digitaloceanspaces.com/cVsujSoDdh0rUhb91TpKSUyI8L5sc1LniWgFKiqX.png","email":"parth.samcom2018@gmail.com","email_verified_at":null,"country_code":"+91","phone_number":"+919712146184","address":"Ranip, Gujarat, India","country":"India","state":"Gujarat","city":"Ahmedabad","city_timezone":"Asia/Calcutta","device_token":"dAp-Qugv8Qk:APA91bHk01di1vj0fePSjGRwofJLIegk7dN3y1wv7-MpN2HLGtb_SZTWAlakeBOB_f1Sfp6j6QSzpH7dfpPTi8-OtNq86y0ge5xmTQckDZtkbINAgCUNE9w2TBBZs_veyaLXIEZRPiKW","device_type":"android","latitude":"23.103347778320312","longitude":"72.59434225119453","status":"active","user_status":"online","authy_id":"127634683","stripe_customer_id":null,"user_unique_code":"Parth1002","tax_id":null,"commission_type":"ammount","commission":null,"invoice_frequency":"weekly","role":"driver","passenger_rate_card":"","document_status":"completed","wallet_balance":null,"deleted_at":null,"created_at":"2019-09-19 06:11:38","updated_at":"2019-10-15 11:23:58","users_vehicle":{"id":1,"user_id":2,"vin_code":"","car_number":"new","make":"Acura","model":"ILX","year":"2019","transmission":"Automatic","color":"red","driver_train":"4x4","car_type":"Premium","car_type_id":1,"deleted_at":null,"created_at":"2019-09-19 06:16:20","updated_at":"2019-09-19 06:16:52"},"vehicle":{"id":1,"user_id":2,"vin_code":"","car_number":"new","make":"Acura","model":"ILX","year":"2019","transmission":"Automatic","color":"red","driver_train":"4x4","car_type":"Premium","car_type_id":1,"deleted_at":null,"created_at":"2019-09-19 06:16:20","updated_at":"2019-09-19 06:16:52"},"vehicle_status":true,"support_number":"+16514139117"}
         * documents : [{"id":1,"user_id":2,"unique_id":null,"document_name":"Driving License","front_document_url":null,"back_document_url":null,"document_type":null,"issue_date":null,"expiry_date":null,"status":"approved","deleted_at":null,"created_at":"2019-09-19 06:15:57","updated_at":"2019-09-19 06:16:51"},{"id":2,"user_id":2,"unique_id":null,"document_name":"Inspection Certificate","front_document_url":null,"back_document_url":null,"document_type":null,"issue_date":null,"expiry_date":null,"status":"approved","deleted_at":null,"created_at":"2019-09-19 06:15:57","updated_at":"2019-09-19 06:16:51"},{"id":3,"user_id":2,"unique_id":null,"document_name":"Registration Certificate","front_document_url":null,"back_document_url":null,"document_type":null,"issue_date":null,"expiry_date":null,"status":"approved","deleted_at":null,"created_at":"2019-09-19 06:15:57","updated_at":"2019-09-19 06:16:51"}]
         * runningRide :
         */

        @SerializedName("userDetails")
        private UserDetailsEntity userDetails;
        @SerializedName("runningRide")
        private String runningRide;
        @SerializedName("vehicleInspection")
        private String vehicleInspection;

        public String getVehicleInspection() {
            return vehicleInspection;
        }

        public void setVehicleInspection(String vehicleInspection) {
            this.vehicleInspection = vehicleInspection;
        }

        @SerializedName("documents")
        private List<DocumentsEntity> documents;

        public UserDetailsEntity getUserDetails() {
            return userDetails;
        }

        public void setUserDetails(UserDetailsEntity userDetails) {
            this.userDetails = userDetails;
        }

        public String getRunningRide() {
            return runningRide;
        }

        public void setRunningRide(String runningRide) {
            this.runningRide = runningRide;
        }

        public List<DocumentsEntity> getDocuments() {
            return documents;
        }

        public void setDocuments(List<DocumentsEntity> documents) {
            this.documents = documents;
        }

        public static class UserDetailsEntity {
            /**
             * id : 2
             * parent_id : 0
             * company_id : 0
             * first_name : Parthh
             * last_name : Samcom
             * company_name :
             * company_image : null
             * contact_person :
             * profile_picture : https://rydedispatch.nyc3.digitaloceanspaces.com/cVsujSoDdh0rUhb91TpKSUyI8L5sc1LniWgFKiqX.png
             * email : parth.samcom2018@gmail.com
             * email_verified_at : null
             * country_code : +91
             * phone_number : +919712146184
             * address : Ranip, Gujarat, India
             * country : India
             * state : Gujarat
             * city : Ahmedabad
             * city_timezone : Asia/Calcutta
             * device_token : dAp-Qugv8Qk:APA91bHk01di1vj0fePSjGRwofJLIegk7dN3y1wv7-MpN2HLGtb_SZTWAlakeBOB_f1Sfp6j6QSzpH7dfpPTi8-OtNq86y0ge5xmTQckDZtkbINAgCUNE9w2TBBZs_veyaLXIEZRPiKW
             * device_type : android
             * latitude : 23.103347778320312
             * longitude : 72.59434225119453
             * status : active
             * user_status : online
             * authy_id : 127634683
             * stripe_customer_id : null
             * user_unique_code : Parth1002
             * tax_id : null
             * commission_type : ammount
             * commission : null
             * invoice_frequency : weekly
             * role : driver
             * passenger_rate_card :
             * document_status : completed
             * wallet_balance : null
             * deleted_at : null
             * created_at : 2019-09-19 06:11:38
             * updated_at : 2019-10-15 11:23:58
             * users_vehicle : {"id":1,"user_id":2,"vin_code":"","car_number":"new","make":"Acura","model":"ILX","year":"2019","transmission":"Automatic","color":"red","driver_train":"4x4","car_type":"Premium","car_type_id":1,"deleted_at":null,"created_at":"2019-09-19 06:16:20","updated_at":"2019-09-19 06:16:52"}
             * vehicle : {"id":1,"user_id":2,"vin_code":"","car_number":"new","make":"Acura","model":"ILX","year":"2019","transmission":"Automatic","color":"red","driver_train":"4x4","car_type":"Premium","car_type_id":1,"deleted_at":null,"created_at":"2019-09-19 06:16:20","updated_at":"2019-09-19 06:16:52"}
             * vehicle_status : true
             * support_number : +16514139117
             */

            @SerializedName("id")
            private int id;
            @SerializedName("parent_id")
            private int parentId;
            @SerializedName("company_id")
            private int companyId;
            @SerializedName("first_name")
            private String firstName;
            @SerializedName("last_name")
            private String lastName;
            @SerializedName("company_name")
            private String companyName;
            @SerializedName("company_image")
            private Object companyImage;
            @SerializedName("contact_person")
            private String contactPerson;
            @SerializedName("profile_picture")
            private String profilePicture;
            @SerializedName("email")
            private String email;
            @SerializedName("email_verified_at")
            private Object emailVerifiedAt;
            @SerializedName("country_code")
            private String countryCode;
            @SerializedName("phone_number")
            private String phoneNumber;
            @SerializedName("address")
            private String address;
            @SerializedName("country")
            private String country;
            @SerializedName("state")
            private String state;
            @SerializedName("city")
            private String city;
            @SerializedName("city_timezone")
            private String cityTimezone;
            @SerializedName("device_token")
            private String deviceToken;
            @SerializedName("device_type")
            private String deviceType;
            @SerializedName("latitude")
            private String latitude;
            @SerializedName("longitude")
            private String longitude;
            @SerializedName("status")
            private String status;
            @SerializedName("user_status")
            private String userStatus;
            @SerializedName("authy_id")
            private String authyId;
            @SerializedName("stripe_customer_id")
            private Object stripeCustomerId;
            @SerializedName("user_unique_code")
            private String userUniqueCode;
            @SerializedName("tax_id")
            private Object taxId;
            @SerializedName("commission_type")
            private String commissionType;
            @SerializedName("commission")
            private Object commission;
            @SerializedName("invoice_frequency")
            private String invoiceFrequency;
            @SerializedName("role")
            private String role;
            @SerializedName("passenger_rate_card")
            private String passengerRateCard;
            @SerializedName("document_status")
            private String documentStatus;
            @SerializedName("wallet_balance")
            private Object walletBalance;
            @SerializedName("deleted_at")
            private Object deletedAt;
            @SerializedName("created_at")
            private String createdAt;
            @SerializedName("updated_at")
            private String updatedAt;
            @SerializedName("users_vehicle")
            private UsersVehicleEntity usersVehicle;
            @SerializedName("vehicle")
            private VehicleEntity vehicle;
            @SerializedName("vehicle_status")
            private boolean vehicleStatus;
            @SerializedName("support_number")
            private String supportNumber;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public int getParentId() {
                return parentId;
            }

            public void setParentId(int parentId) {
                this.parentId = parentId;
            }

            public int getCompanyId() {
                return companyId;
            }

            public void setCompanyId(int companyId) {
                this.companyId = companyId;
            }

            public String getFirstName() {
                return firstName;
            }

            public void setFirstName(String firstName) {
                this.firstName = firstName;
            }

            public String getLastName() {
                return lastName;
            }

            public void setLastName(String lastName) {
                this.lastName = lastName;
            }

            public String getCompanyName() {
                return companyName;
            }

            public void setCompanyName(String companyName) {
                this.companyName = companyName;
            }

            public Object getCompanyImage() {
                return companyImage;
            }

            public void setCompanyImage(Object companyImage) {
                this.companyImage = companyImage;
            }

            public String getContactPerson() {
                return contactPerson;
            }

            public void setContactPerson(String contactPerson) {
                this.contactPerson = contactPerson;
            }

            public String getProfilePicture() {
                return profilePicture;
            }

            public void setProfilePicture(String profilePicture) {
                this.profilePicture = profilePicture;
            }

            public String getEmail() {
                return email;
            }

            public void setEmail(String email) {
                this.email = email;
            }

            public Object getEmailVerifiedAt() {
                return emailVerifiedAt;
            }

            public void setEmailVerifiedAt(Object emailVerifiedAt) {
                this.emailVerifiedAt = emailVerifiedAt;
            }

            public String getCountryCode() {
                return countryCode;
            }

            public void setCountryCode(String countryCode) {
                this.countryCode = countryCode;
            }

            public String getPhoneNumber() {
                return phoneNumber;
            }

            public void setPhoneNumber(String phoneNumber) {
                this.phoneNumber = phoneNumber;
            }

            public String getAddress() {
                return address;
            }

            public void setAddress(String address) {
                this.address = address;
            }

            public String getCountry() {
                return country;
            }

            public void setCountry(String country) {
                this.country = country;
            }

            public String getState() {
                return state;
            }

            public void setState(String state) {
                this.state = state;
            }

            public String getCity() {
                return city;
            }

            public void setCity(String city) {
                this.city = city;
            }

            public String getCityTimezone() {
                return cityTimezone;
            }

            public void setCityTimezone(String cityTimezone) {
                this.cityTimezone = cityTimezone;
            }

            public String getDeviceToken() {
                return deviceToken;
            }

            public void setDeviceToken(String deviceToken) {
                this.deviceToken = deviceToken;
            }

            public String getDeviceType() {
                return deviceType;
            }

            public void setDeviceType(String deviceType) {
                this.deviceType = deviceType;
            }

            public String getLatitude() {
                return latitude;
            }

            public void setLatitude(String latitude) {
                this.latitude = latitude;
            }

            public String getLongitude() {
                return longitude;
            }

            public void setLongitude(String longitude) {
                this.longitude = longitude;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }

            public String getUserStatus() {
                return userStatus;
            }

            public void setUserStatus(String userStatus) {
                this.userStatus = userStatus;
            }

            public String getAuthyId() {
                return authyId;
            }

            public void setAuthyId(String authyId) {
                this.authyId = authyId;
            }

            public Object getStripeCustomerId() {
                return stripeCustomerId;
            }

            public void setStripeCustomerId(Object stripeCustomerId) {
                this.stripeCustomerId = stripeCustomerId;
            }

            public String getUserUniqueCode() {
                return userUniqueCode;
            }

            public void setUserUniqueCode(String userUniqueCode) {
                this.userUniqueCode = userUniqueCode;
            }

            public Object getTaxId() {
                return taxId;
            }

            public void setTaxId(Object taxId) {
                this.taxId = taxId;
            }

            public String getCommissionType() {
                return commissionType;
            }

            public void setCommissionType(String commissionType) {
                this.commissionType = commissionType;
            }

            public Object getCommission() {
                return commission;
            }

            public void setCommission(Object commission) {
                this.commission = commission;
            }

            public String getInvoiceFrequency() {
                return invoiceFrequency;
            }

            public void setInvoiceFrequency(String invoiceFrequency) {
                this.invoiceFrequency = invoiceFrequency;
            }

            public String getRole() {
                return role;
            }

            public void setRole(String role) {
                this.role = role;
            }

            public String getPassengerRateCard() {
                return passengerRateCard;
            }

            public void setPassengerRateCard(String passengerRateCard) {
                this.passengerRateCard = passengerRateCard;
            }

            public String getDocumentStatus() {
                return documentStatus;
            }

            public void setDocumentStatus(String documentStatus) {
                this.documentStatus = documentStatus;
            }

            public Object getWalletBalance() {
                return walletBalance;
            }

            public void setWalletBalance(Object walletBalance) {
                this.walletBalance = walletBalance;
            }

            public Object getDeletedAt() {
                return deletedAt;
            }

            public void setDeletedAt(Object deletedAt) {
                this.deletedAt = deletedAt;
            }

            public String getCreatedAt() {
                return createdAt;
            }

            public void setCreatedAt(String createdAt) {
                this.createdAt = createdAt;
            }

            public String getUpdatedAt() {
                return updatedAt;
            }

            public void setUpdatedAt(String updatedAt) {
                this.updatedAt = updatedAt;
            }

            public UsersVehicleEntity getUsersVehicle() {
                return usersVehicle;
            }

            public void setUsersVehicle(UsersVehicleEntity usersVehicle) {
                this.usersVehicle = usersVehicle;
            }

            public VehicleEntity getVehicle() {
                return vehicle;
            }

            public void setVehicle(VehicleEntity vehicle) {
                this.vehicle = vehicle;
            }

            public boolean isVehicleStatus() {
                return vehicleStatus;
            }

            public void setVehicleStatus(boolean vehicleStatus) {
                this.vehicleStatus = vehicleStatus;
            }

            public String getSupportNumber() {
                return supportNumber;
            }

            public void setSupportNumber(String supportNumber) {
                this.supportNumber = supportNumber;
            }

            public static class UsersVehicleEntity {
                /**
                 * id : 1
                 * user_id : 2
                 * vin_code :
                 * car_number : new
                 * make : Acura
                 * model : ILX
                 * year : 2019
                 * transmission : Automatic
                 * color : red
                 * driver_train : 4x4
                 * car_type : Premium
                 * car_type_id : 1
                 * deleted_at : null
                 * created_at : 2019-09-19 06:16:20
                 * updated_at : 2019-09-19 06:16:52
                 */

                @SerializedName("id")
                private int id;
                @SerializedName("user_id")
                private int userId;
                @SerializedName("vin_code")
                private String vinCode;
                @SerializedName("car_number")
                private String carNumber;
                @SerializedName("make")
                private String make;
                @SerializedName("model")
                private String model;
                @SerializedName("year")
                private String year;
                @SerializedName("transmission")
                private String transmission;
                @SerializedName("color")
                private String color;
                @SerializedName("driver_train")
                private String driverTrain;
                @SerializedName("car_type")
                private String carType;
                @SerializedName("car_type_id")
                private int carTypeId;
                @SerializedName("deleted_at")
                private Object deletedAt;
                @SerializedName("created_at")
                private String createdAt;
                @SerializedName("updated_at")
                private String updatedAt;

                public int getId() {
                    return id;
                }

                public void setId(int id) {
                    this.id = id;
                }

                public int getUserId() {
                    return userId;
                }

                public void setUserId(int userId) {
                    this.userId = userId;
                }

                public String getVinCode() {
                    return vinCode;
                }

                public void setVinCode(String vinCode) {
                    this.vinCode = vinCode;
                }

                public String getCarNumber() {
                    return carNumber;
                }

                public void setCarNumber(String carNumber) {
                    this.carNumber = carNumber;
                }

                public String getMake() {
                    return make;
                }

                public void setMake(String make) {
                    this.make = make;
                }

                public String getModel() {
                    return model;
                }

                public void setModel(String model) {
                    this.model = model;
                }

                public String getYear() {
                    return year;
                }

                public void setYear(String year) {
                    this.year = year;
                }

                public String getTransmission() {
                    return transmission;
                }

                public void setTransmission(String transmission) {
                    this.transmission = transmission;
                }

                public String getColor() {
                    return color;
                }

                public void setColor(String color) {
                    this.color = color;
                }

                public String getDriverTrain() {
                    return driverTrain;
                }

                public void setDriverTrain(String driverTrain) {
                    this.driverTrain = driverTrain;
                }

                public String getCarType() {
                    return carType;
                }

                public void setCarType(String carType) {
                    this.carType = carType;
                }

                public int getCarTypeId() {
                    return carTypeId;
                }

                public void setCarTypeId(int carTypeId) {
                    this.carTypeId = carTypeId;
                }

                public Object getDeletedAt() {
                    return deletedAt;
                }

                public void setDeletedAt(Object deletedAt) {
                    this.deletedAt = deletedAt;
                }

                public String getCreatedAt() {
                    return createdAt;
                }

                public void setCreatedAt(String createdAt) {
                    this.createdAt = createdAt;
                }

                public String getUpdatedAt() {
                    return updatedAt;
                }

                public void setUpdatedAt(String updatedAt) {
                    this.updatedAt = updatedAt;
                }
            }

            public static class VehicleEntity {
                /**
                 * id : 1
                 * user_id : 2
                 * vin_code :
                 * car_number : new
                 * make : Acura
                 * model : ILX
                 * year : 2019
                 * transmission : Automatic
                 * color : red
                 * driver_train : 4x4
                 * car_type : Premium
                 * car_type_id : 1
                 * deleted_at : null
                 * created_at : 2019-09-19 06:16:20
                 * updated_at : 2019-09-19 06:16:52
                 */

                @SerializedName("id")
                private int id;
                @SerializedName("user_id")
                private int userId;
                @SerializedName("vin_code")
                private String vinCode;
                @SerializedName("car_number")
                private String carNumber;
                @SerializedName("make")
                private String make;
                @SerializedName("model")
                private String model;
                @SerializedName("year")
                private String year;
                @SerializedName("transmission")
                private String transmission;
                @SerializedName("color")
                private String color;
                @SerializedName("driver_train")
                private String driverTrain;
                @SerializedName("car_type")
                private String carType;
                @SerializedName("car_type_id")
                private int carTypeId;
                @SerializedName("deleted_at")
                private Object deletedAt;
                @SerializedName("created_at")
                private String createdAt;
                @SerializedName("updated_at")
                private String updatedAt;

                public int getId() {
                    return id;
                }

                public void setId(int id) {
                    this.id = id;
                }

                public int getUserId() {
                    return userId;
                }

                public void setUserId(int userId) {
                    this.userId = userId;
                }

                public String getVinCode() {
                    return vinCode;
                }

                public void setVinCode(String vinCode) {
                    this.vinCode = vinCode;
                }

                public String getCarNumber() {
                    return carNumber;
                }

                public void setCarNumber(String carNumber) {
                    this.carNumber = carNumber;
                }

                public String getMake() {
                    return make;
                }

                public void setMake(String make) {
                    this.make = make;
                }

                public String getModel() {
                    return model;
                }

                public void setModel(String model) {
                    this.model = model;
                }

                public String getYear() {
                    return year;
                }

                public void setYear(String year) {
                    this.year = year;
                }

                public String getTransmission() {
                    return transmission;
                }

                public void setTransmission(String transmission) {
                    this.transmission = transmission;
                }

                public String getColor() {
                    return color;
                }

                public void setColor(String color) {
                    this.color = color;
                }

                public String getDriverTrain() {
                    return driverTrain;
                }

                public void setDriverTrain(String driverTrain) {
                    this.driverTrain = driverTrain;
                }

                public String getCarType() {
                    return carType;
                }

                public void setCarType(String carType) {
                    this.carType = carType;
                }

                public int getCarTypeId() {
                    return carTypeId;
                }

                public void setCarTypeId(int carTypeId) {
                    this.carTypeId = carTypeId;
                }

                public Object getDeletedAt() {
                    return deletedAt;
                }

                public void setDeletedAt(Object deletedAt) {
                    this.deletedAt = deletedAt;
                }

                public String getCreatedAt() {
                    return createdAt;
                }

                public void setCreatedAt(String createdAt) {
                    this.createdAt = createdAt;
                }

                public String getUpdatedAt() {
                    return updatedAt;
                }

                public void setUpdatedAt(String updatedAt) {
                    this.updatedAt = updatedAt;
                }
            }
        }

        public static class DocumentsEntity {
            /**
             * id : 1
             * user_id : 2
             * unique_id : null
             * document_name : Driving License
             * front_document_url : null
             * back_document_url : null
             * document_type : null
             * issue_date : null
             * expiry_date : null
             * status : approved
             * deleted_at : null
             * created_at : 2019-09-19 06:15:57
             * updated_at : 2019-09-19 06:16:51
             */

            @SerializedName("id")
            private int id;
            @SerializedName("user_id")
            private int userId;
            @SerializedName("unique_id")
            private Object uniqueId;
            @SerializedName("document_name")
            private String documentName;
            @SerializedName("front_document_url")
            private Object frontDocumentUrl;
            @SerializedName("back_document_url")
            private Object backDocumentUrl;
            @SerializedName("document_type")
            private Object documentType;
            @SerializedName("issue_date")
            private Object issueDate;
            @SerializedName("expiry_date")
            private Object expiryDate;
            @SerializedName("status")
            private String status;
            @SerializedName("deleted_at")
            private Object deletedAt;
            @SerializedName("created_at")
            private String createdAt;
            @SerializedName("updated_at")
            private String updatedAt;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public int getUserId() {
                return userId;
            }

            public void setUserId(int userId) {
                this.userId = userId;
            }

            public Object getUniqueId() {
                return uniqueId;
            }

            public void setUniqueId(Object uniqueId) {
                this.uniqueId = uniqueId;
            }

            public String getDocumentName() {
                return documentName;
            }

            public void setDocumentName(String documentName) {
                this.documentName = documentName;
            }

            public Object getFrontDocumentUrl() {
                return frontDocumentUrl;
            }

            public void setFrontDocumentUrl(Object frontDocumentUrl) {
                this.frontDocumentUrl = frontDocumentUrl;
            }

            public Object getBackDocumentUrl() {
                return backDocumentUrl;
            }

            public void setBackDocumentUrl(Object backDocumentUrl) {
                this.backDocumentUrl = backDocumentUrl;
            }

            public Object getDocumentType() {
                return documentType;
            }

            public void setDocumentType(Object documentType) {
                this.documentType = documentType;
            }

            public Object getIssueDate() {
                return issueDate;
            }

            public void setIssueDate(Object issueDate) {
                this.issueDate = issueDate;
            }

            public Object getExpiryDate() {
                return expiryDate;
            }

            public void setExpiryDate(Object expiryDate) {
                this.expiryDate = expiryDate;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }

            public Object getDeletedAt() {
                return deletedAt;
            }

            public void setDeletedAt(Object deletedAt) {
                this.deletedAt = deletedAt;
            }

            public String getCreatedAt() {
                return createdAt;
            }

            public void setCreatedAt(String createdAt) {
                this.createdAt = createdAt;
            }

            public String getUpdatedAt() {
                return updatedAt;
            }

            public void setUpdatedAt(String updatedAt) {
                this.updatedAt = updatedAt;
            }
        }
    }
}
