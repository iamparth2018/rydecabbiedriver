package com.rydedispatch.driver.response;

import com.google.gson.annotations.SerializedName;

public class RespGetVehicleDetails {

    /**
     * error : false
     * data : {"Model":"X3","ModelYear":"2019","Make":"BMW","TransmissionStyle":"","DriveType":"AWD/All Wheel Drive","Trims":"xDrive30i"}
     * message : VIN number details fetch successully
     */

    @SerializedName("error")
    private boolean error;
    @SerializedName("data")
    private DataEntity data;
    @SerializedName("message")
    private String message;

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public DataEntity getData() {
        return data;
    }

    public void setData(DataEntity data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public static class DataEntity {
        /**
         * Model : X3
         * ModelYear : 2019
         * Make : BMW
         * TransmissionStyle :
         * DriveType : AWD/All Wheel Drive
         * Trims : xDrive30i
         */

        @SerializedName("Model")
        private String Model;
        @SerializedName("ModelYear")
        private String ModelYear;
        @SerializedName("Make")
        private String Make;
        @SerializedName("TransmissionStyle")
        private String TransmissionStyle;
        @SerializedName("DriveType")
        private String DriveType;
        @SerializedName("Trims")
        private String Trims;

        public String getModel() {
            return Model;
        }

        public void setModel(String Model) {
            this.Model = Model;
        }

        public String getModelYear() {
            return ModelYear;
        }

        public void setModelYear(String ModelYear) {
            this.ModelYear = ModelYear;
        }

        public String getMake() {
            return Make;
        }

        public void setMake(String Make) {
            this.Make = Make;
        }

        public String getTransmissionStyle() {
            return TransmissionStyle;
        }

        public void setTransmissionStyle(String TransmissionStyle) {
            this.TransmissionStyle = TransmissionStyle;
        }

        public String getDriveType() {
            return DriveType;
        }

        public void setDriveType(String DriveType) {
            this.DriveType = DriveType;
        }

        public String getTrims() {
            return Trims;
        }

        public void setTrims(String Trims) {
            this.Trims = Trims;
        }
    }
}
