package com.rydedispatch.driver.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RespGetStatements {
    /**
     * error : false
     * data : {"statements":{"current_page":1,"data":[{"id":1,"user_id":2,"start_date":"2019-09-15 00:00:00","end_date":"2019-09-21 00:00:00","due_date":"2019-09-23 00:00:00","created_at":"2019-09-27 13:32:42","actual_cost_without_commission":"9.1","status":"unpaid"}],"first_page_url":"http://159.203.105.17/api/getStatements?page=1","from":1,"last_page":1,"last_page_url":"http://159.203.105.17/api/getStatements?page=1","next_page_url":null,"path":"http://159.203.105.17/api/getStatements","per_page":20,"prev_page_url":null,"to":1,"total":1},"totalCompeleted":"1","totalEarnings":"9.1"}
     * message : Driver statements list!
     */

    @SerializedName("error")
    private boolean error;
    @SerializedName("data")
    private DataEntityX data;
    @SerializedName("message")
    private String message;

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public DataEntityX getData() {
        return data;
    }

    public void setData(DataEntityX data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public static class DataEntityX {
        /**
         * statements : {"current_page":1,"data":[{"id":1,"user_id":2,"start_date":"2019-09-15 00:00:00","end_date":"2019-09-21 00:00:00","due_date":"2019-09-23 00:00:00","created_at":"2019-09-27 13:32:42","actual_cost_without_commission":"9.1","status":"unpaid"}],"first_page_url":"http://159.203.105.17/api/getStatements?page=1","from":1,"last_page":1,"last_page_url":"http://159.203.105.17/api/getStatements?page=1","next_page_url":null,"path":"http://159.203.105.17/api/getStatements","per_page":20,"prev_page_url":null,"to":1,"total":1}
         * totalCompeleted : 1
         * totalEarnings : 9.1
         */

        @SerializedName("statements")
        private StatementsEntity statements;
        @SerializedName("totalCompeleted")
        private String totalCompeleted;
        @SerializedName("totalEarnings")
        private String totalEarnings;

        public StatementsEntity getStatements() {
            return statements;
        }

        public void setStatements(StatementsEntity statements) {
            this.statements = statements;
        }

        public String getTotalCompeleted() {
            return totalCompeleted;
        }

        public void setTotalCompeleted(String totalCompeleted) {
            this.totalCompeleted = totalCompeleted;
        }

        public String getTotalEarnings() {
            return totalEarnings;
        }

        public void setTotalEarnings(String totalEarnings) {
            this.totalEarnings = totalEarnings;
        }

        public static class StatementsEntity {
            /**
             * current_page : 1
             * data : [{"id":1,"user_id":2,"start_date":"2019-09-15 00:00:00","end_date":"2019-09-21 00:00:00","due_date":"2019-09-23 00:00:00","created_at":"2019-09-27 13:32:42","actual_cost_without_commission":"9.1","status":"unpaid"}]
             * first_page_url : http://159.203.105.17/api/getStatements?page=1
             * from : 1
             * last_page : 1
             * last_page_url : http://159.203.105.17/api/getStatements?page=1
             * next_page_url : null
             * path : http://159.203.105.17/api/getStatements
             * per_page : 20
             * prev_page_url : null
             * to : 1
             * total : 1
             */

            @SerializedName("current_page")
            private int currentPage;
            @SerializedName("first_page_url")
            private String firstPageUrl;
            @SerializedName("from")
            private int from;
            @SerializedName("last_page")
            private int lastPage;
            @SerializedName("last_page_url")
            private String lastPageUrl;
            @SerializedName("next_page_url")
            private Object nextPageUrl;
            @SerializedName("path")
            private String path;
            @SerializedName("per_page")
            private int perPage;
            @SerializedName("prev_page_url")
            private Object prevPageUrl;
            @SerializedName("to")
            private int to;
            @SerializedName("total")
            private int total;
            @SerializedName("data")
            private List<DataEntity> data;

            public int getCurrentPage() {
                return currentPage;
            }

            public void setCurrentPage(int currentPage) {
                this.currentPage = currentPage;
            }

            public String getFirstPageUrl() {
                return firstPageUrl;
            }

            public void setFirstPageUrl(String firstPageUrl) {
                this.firstPageUrl = firstPageUrl;
            }

            public int getFrom() {
                return from;
            }

            public void setFrom(int from) {
                this.from = from;
            }

            public int getLastPage() {
                return lastPage;
            }

            public void setLastPage(int lastPage) {
                this.lastPage = lastPage;
            }

            public String getLastPageUrl() {
                return lastPageUrl;
            }

            public void setLastPageUrl(String lastPageUrl) {
                this.lastPageUrl = lastPageUrl;
            }

            public Object getNextPageUrl() {
                return nextPageUrl;
            }

            public void setNextPageUrl(Object nextPageUrl) {
                this.nextPageUrl = nextPageUrl;
            }

            public String getPath() {
                return path;
            }

            public void setPath(String path) {
                this.path = path;
            }

            public int getPerPage() {
                return perPage;
            }

            public void setPerPage(int perPage) {
                this.perPage = perPage;
            }

            public Object getPrevPageUrl() {
                return prevPageUrl;
            }

            public void setPrevPageUrl(Object prevPageUrl) {
                this.prevPageUrl = prevPageUrl;
            }

            public int getTo() {
                return to;
            }

            public void setTo(int to) {
                this.to = to;
            }

            public int getTotal() {
                return total;
            }

            public void setTotal(int total) {
                this.total = total;
            }

            public List<DataEntity> getData() {
                return data;
            }

            public void setData(List<DataEntity> data) {
                this.data = data;
            }

            public static class DataEntity {
                /**
                 * id : 1
                 * user_id : 2
                 * start_date : 2019-09-15 00:00:00
                 * end_date : 2019-09-21 00:00:00
                 * due_date : 2019-09-23 00:00:00
                 * created_at : 2019-09-27 13:32:42
                 * actual_cost_without_commission : 9.1
                 * status : unpaid
                 */

                @SerializedName("id")
                private int id;
                @SerializedName("user_id")
                private int userId;
                @SerializedName("start_date")
                private String startDate;
                @SerializedName("end_date")
                private String endDate;
                @SerializedName("due_date")
                private String dueDate;
                @SerializedName("created_at")
                private String createdAt;
                @SerializedName("actual_cost_without_commission")
                private String actualCostWithoutCommission;
                @SerializedName("status")
                private String status;

                public int getId() {
                    return id;
                }

                public void setId(int id) {
                    this.id = id;
                }

                public int getUserId() {
                    return userId;
                }

                public void setUserId(int userId) {
                    this.userId = userId;
                }

                public String getStartDate() {
                    return startDate;
                }

                public void setStartDate(String startDate) {
                    this.startDate = startDate;
                }

                public String getEndDate() {
                    return endDate;
                }

                public void setEndDate(String endDate) {
                    this.endDate = endDate;
                }

                public String getDueDate() {
                    return dueDate;
                }

                public void setDueDate(String dueDate) {
                    this.dueDate = dueDate;
                }

                public String getCreatedAt() {
                    return createdAt;
                }

                public void setCreatedAt(String createdAt) {
                    this.createdAt = createdAt;
                }

                public String getActualCostWithoutCommission() {
                    return actualCostWithoutCommission;
                }

                public void setActualCostWithoutCommission(String actualCostWithoutCommission) {
                    this.actualCostWithoutCommission = actualCostWithoutCommission;
                }

                public String getStatus() {
                    return status;
                }

                public void setStatus(String status) {
                    this.status = status;
                }
            }
        }
    }

    /**
     * error : false
     * data : {"statements":{"current_page":1,"data":[{"id":4,"user_id":11,"start_date":"2019-08-24 07:09:28","end_date":"2019-08-24 07:09:28","due_date":"2019-08-24 07:09:28","created_at":"2019-08-21 07:09:28","actual_cost_without_commission":"62.55","status":"unpaid"},{"id":2,"user_id":11,"start_date":"2019-08-24 06:33:52","end_date":"2019-08-24 06:33:52","due_date":"2019-08-24 06:33:52","created_at":"2019-08-21 06:33:52","actual_cost_without_commission":"39.15","status":"unpaid"}],"first_page_url":"http://159.203.105.17/api/getStatements?page=1","from":1,"last_page":1,"last_page_url":"http://159.203.105.17/api/getStatements?page=1","next_page_url":null,"path":"http://159.203.105.17/api/getStatements","per_page":20,"prev_page_url":null,"to":2,"total":2},"totalCompeleted":0,"totalEarnings":"101.7"}
     * message : Driver statements list!
     */

    /*@SerializedName("error")
    private boolean error;
    @SerializedName("data")
    private DataEntityX data;
    @SerializedName("message")
    private String message;

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public DataEntityX getData() {
        return data;
    }

    public void setData(DataEntityX data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public static class DataEntityX {
        *//**
         * statements : {"current_page":1,"data":[{"id":4,"user_id":11,"start_date":"2019-08-24 07:09:28","end_date":"2019-08-24 07:09:28","due_date":"2019-08-24 07:09:28","created_at":"2019-08-21 07:09:28","actual_cost_without_commission":"62.55","status":"unpaid"},{"id":2,"user_id":11,"start_date":"2019-08-24 06:33:52","end_date":"2019-08-24 06:33:52","due_date":"2019-08-24 06:33:52","created_at":"2019-08-21 06:33:52","actual_cost_without_commission":"39.15","status":"unpaid"}],"first_page_url":"http://159.203.105.17/api/getStatements?page=1","from":1,"last_page":1,"last_page_url":"http://159.203.105.17/api/getStatements?page=1","next_page_url":null,"path":"http://159.203.105.17/api/getStatements","per_page":20,"prev_page_url":null,"to":2,"total":2}
         * totalCompeleted : 0
         * totalEarnings : 101.7
         *//*

        @SerializedName("statements")
        private StatementsEntity statements;
        @SerializedName("totalCompeleted")
        private int totalCompeleted;
        @SerializedName("totalEarnings")
        private String totalEarnings;

        public StatementsEntity getStatements() {
            return statements;
        }

        public void setStatements(StatementsEntity statements) {
            this.statements = statements;
        }

        public int getTotalCompeleted() {
            return totalCompeleted;
        }

        public void setTotalCompeleted(int totalCompeleted) {
            this.totalCompeleted = totalCompeleted;
        }

        public String getTotalEarnings() {
            return totalEarnings;
        }

        public void setTotalEarnings(String totalEarnings) {
            this.totalEarnings = totalEarnings;
        }

        public static class StatementsEntity {
            *//**
             * current_page : 1
             * data : [{"id":4,"user_id":11,"start_date":"2019-08-24 07:09:28","end_date":"2019-08-24 07:09:28","due_date":"2019-08-24 07:09:28","created_at":"2019-08-21 07:09:28","actual_cost_without_commission":"62.55","status":"unpaid"},{"id":2,"user_id":11,"start_date":"2019-08-24 06:33:52","end_date":"2019-08-24 06:33:52","due_date":"2019-08-24 06:33:52","created_at":"2019-08-21 06:33:52","actual_cost_without_commission":"39.15","status":"unpaid"}]
             * first_page_url : http://159.203.105.17/api/getStatements?page=1
             * from : 1
             * last_page : 1
             * last_page_url : http://159.203.105.17/api/getStatements?page=1
             * next_page_url : null
             * path : http://159.203.105.17/api/getStatements
             * per_page : 20
             * prev_page_url : null
             * to : 2
             * total : 2
             *//*

            @SerializedName("current_page")
            private int currentPage;
            @SerializedName("first_page_url")
            private String firstPageUrl;
            @SerializedName("from")
            private int from;
            @SerializedName("last_page")
            private int lastPage;
            @SerializedName("last_page_url")
            private String lastPageUrl;
            @SerializedName("next_page_url")
            private Object nextPageUrl;
            @SerializedName("path")
            private String path;
            @SerializedName("per_page")
            private int perPage;
            @SerializedName("prev_page_url")
            private Object prevPageUrl;
            @SerializedName("to")
            private int to;
            @SerializedName("total")
            private int total;
            @SerializedName("data")
            private List<DataEntity> data;

            public int getCurrentPage() {
                return currentPage;
            }

            public void setCurrentPage(int currentPage) {
                this.currentPage = currentPage;
            }

            public String getFirstPageUrl() {
                return firstPageUrl;
            }

            public void setFirstPageUrl(String firstPageUrl) {
                this.firstPageUrl = firstPageUrl;
            }

            public int getFrom() {
                return from;
            }

            public void setFrom(int from) {
                this.from = from;
            }

            public int getLastPage() {
                return lastPage;
            }

            public void setLastPage(int lastPage) {
                this.lastPage = lastPage;
            }

            public String getLastPageUrl() {
                return lastPageUrl;
            }

            public void setLastPageUrl(String lastPageUrl) {
                this.lastPageUrl = lastPageUrl;
            }

            public Object getNextPageUrl() {
                return nextPageUrl;
            }

            public void setNextPageUrl(Object nextPageUrl) {
                this.nextPageUrl = nextPageUrl;
            }

            public String getPath() {
                return path;
            }

            public void setPath(String path) {
                this.path = path;
            }

            public int getPerPage() {
                return perPage;
            }

            public void setPerPage(int perPage) {
                this.perPage = perPage;
            }

            public Object getPrevPageUrl() {
                return prevPageUrl;
            }

            public void setPrevPageUrl(Object prevPageUrl) {
                this.prevPageUrl = prevPageUrl;
            }

            public int getTo() {
                return to;
            }

            public void setTo(int to) {
                this.to = to;
            }

            public int getTotal() {
                return total;
            }

            public void setTotal(int total) {
                this.total = total;
            }

            public List<DataEntity> getData() {
                return data;
            }

            public void setData(List<DataEntity> data) {
                this.data = data;
            }

            public static class DataEntity {
                *//**
                 * id : 4
                 * user_id : 11
                 * start_date : 2019-08-24 07:09:28
                 * end_date : 2019-08-24 07:09:28
                 * due_date : 2019-08-24 07:09:28
                 * created_at : 2019-08-21 07:09:28
                 * actual_cost_without_commission : 62.55
                 * status : unpaid
                 *//*

                @SerializedName("id")
                private int id;
                @SerializedName("user_id")
                private int userId;
                @SerializedName("start_date")
                private String startDate;
                @SerializedName("end_date")
                private String endDate;
                @SerializedName("due_date")
                private String dueDate;
                @SerializedName("created_at")
                private String createdAt;
                @SerializedName("actual_cost_without_commission")
                private String actualCostWithoutCommission;
                @SerializedName("status")
                private String status;

                public int getId() {
                    return id;
                }

                public void setId(int id) {
                    this.id = id;
                }

                public int getUserId() {
                    return userId;
                }

                public void setUserId(int userId) {
                    this.userId = userId;
                }

                public String getStartDate() {
                    return startDate;
                }

                public void setStartDate(String startDate) {
                    this.startDate = startDate;
                }

                public String getEndDate() {
                    return endDate;
                }

                public void setEndDate(String endDate) {
                    this.endDate = endDate;
                }

                public String getDueDate() {
                    return dueDate;
                }

                public void setDueDate(String dueDate) {
                    this.dueDate = dueDate;
                }

                public String getCreatedAt() {
                    return createdAt;
                }

                public void setCreatedAt(String createdAt) {
                    this.createdAt = createdAt;
                }

                public String getActualCostWithoutCommission() {
                    return actualCostWithoutCommission;
                }

                public void setActualCostWithoutCommission(String actualCostWithoutCommission) {
                    this.actualCostWithoutCommission = actualCostWithoutCommission;
                }

                public String getStatus() {
                    return status;
                }

                public void setStatus(String status) {
                    this.status = status;
                }
            }
        }
    }*/


}
