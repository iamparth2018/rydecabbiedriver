package com.rydedispatch.driver.response;

import com.google.gson.annotations.SerializedName;

public class RespUploadSignature {

    /**
     * error : true
     * message : Your signature has been uploaded successfully.
     * data : {}
     */

    @SerializedName("error")
    private boolean error;
    @SerializedName("message")
    private String message;
    @SerializedName("data")
    private DataEntity data;

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataEntity getData() {
        return data;
    }

    public void setData(DataEntity data) {
        this.data = data;
    }

    public static class DataEntity {
    }
}
