package com.rydedispatch.driver.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RespGetYearCar {

    /**
     * error : false
     * data : [{"model_year":"2019"},{"model_year":"2018"},{"model_year":"2017"},{"model_year":"2016"},{"model_year":"2015"},{"model_year":"2014"},{"model_year":"2013"},{"model_year":"2012"},{"model_year":"2011"},{"model_year":"2010"},{"model_year":"2009"},{"model_year":"2008"},{"model_year":"2007"},{"model_year":"2006"},{"model_year":"2005"},{"model_year":"2004"},{"model_year":"2003"},{"model_year":"2002"},{"model_year":"2001"},{"model_year":"2000"},{"model_year":"1999"},{"model_year":"1998"},{"model_year":"1997"},{"model_year":"1996"},{"model_year":"1995"},{"model_year":"1994"},{"model_year":"1993"},{"model_year":"1992"},{"model_year":"1991"}]
     * message : Successfully retrieve car.
     */

    @SerializedName("error")
    private boolean error;
    @SerializedName("message")
    private String message;
    @SerializedName("data")
    private List<DataEntity> data;

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataEntity> getData() {
        return data;
    }

    public void setData(List<DataEntity> data) {
        this.data = data;
    }

    public static class DataEntity {
        /**
         * model_year : 2019
         */

        @SerializedName("model_year")
        private String modelYear;

        public String getModelYear() {
            return modelYear;
        }

        public void setModelYear(String modelYear) {
            this.modelYear = modelYear;
        }
    }
}
