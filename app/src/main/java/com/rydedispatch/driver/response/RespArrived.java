package com.rydedispatch.driver.response;

import com.google.gson.annotations.SerializedName;

public class RespArrived {
    /**
     * error : false
     * data : {"distance_type":"miles","payment_type":"cash","display_passenger_name":"samcomredmi6 technologies","ride_instruction":"This is final"}
     * message : Driver has been arrived Successfully.
     */

    @SerializedName("error")
    private boolean error;
    @SerializedName("data")
    private DataEntity data;
    @SerializedName("message")
    private String message;

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public DataEntity getData() {
        return data;
    }

    public void setData(DataEntity data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public static class DataEntity {
        /**
         * distance_type : miles
         * payment_type : cash
         * display_passenger_name : samcomredmi6 technologies
         * ride_instruction : This is final
         */

        @SerializedName("distance_type")
        private String distanceType;
        @SerializedName("payment_type")
        private String paymentType;
        @SerializedName("display_passenger_name")
        private String displayPassengerName;
        @SerializedName("ride_instruction")
        private String rideInstruction;

        public String getDistanceType() {
            return distanceType;
        }

        public void setDistanceType(String distanceType) {
            this.distanceType = distanceType;
        }

        public String getPaymentType() {
            return paymentType;
        }

        public void setPaymentType(String paymentType) {
            this.paymentType = paymentType;
        }

        public String getDisplayPassengerName() {
            return displayPassengerName;
        }

        public void setDisplayPassengerName(String displayPassengerName) {
            this.displayPassengerName = displayPassengerName;
        }

        public String getRideInstruction() {
            return rideInstruction;
        }

        public void setRideInstruction(String rideInstruction) {
            this.rideInstruction = rideInstruction;
        }
    }

    /**
     * error : false
     * data : {}
     * message : Driver has been arrived Successfully.
     *//*

    @SerializedName("error")
    private boolean error;
    @SerializedName("data")
    private DataEntity data;
    @SerializedName("message")
    private String message;

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public DataEntity getData() {
        return data;
    }

    public void setData(DataEntity data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public static class DataEntity {
    }*/


}
